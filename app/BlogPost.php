<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class, 'blog_post_products')->withTimestamps();
    }

    public function blogCategoryId()
    {
        return $this->belongsTo(BlogCategory::class);
    }

    public function getImage()
    {
        if ($this->image) {
            return url('/storage/' . $this->image);
        }

        return '';
    }

    public function blogCategory()
    {
        return $this->belongsTo('App\BlogCategory', 'blog_category_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}