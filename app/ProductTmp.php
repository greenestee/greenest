<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTmp extends Model
{
    public $table = 'product_tmp';

    const PRODUCT_NOT_IN_STOCK = 0;

    const PRODUCT_IN_STOCK = 1;

    protected $fillable = [
        "name",
        "slug",
        "meta_title",
        "meta_description",
        "meta_keywords",
        "price",
        "purchase_price",
        "amount",
        "weight",
        "unit",
        "brand",
        "is_lightweight",
        "is_vegan",
        "is_gluten_free",
        "image",
        "sku",
        "product_url",
        "origin_country",
        "origin_country_of_ingredients",
        "shelf_life",
        "nutritional_value",
        "description",
        "short_description",
        "ingredients",
        "recommended_storage",
        "allergenic_information",
        "method_of_preparation",
        "how_to_use",
        "is_active",
        "categories",
        "created_at",
        "updated_at",
        "promoted_from",
        "promoted_to",
        "is_promoted",
        "name_et",
        "slug_et",
        "meta_title_et",
        "meta_keywords_et",
        "meta_description_et",
        "description_et",
        "short_description_et",
        "nutritional_value_et",
        "ingredients_et",
        "recommended_storage_et",
        "allergenic_information_et",
        "method_of_preparation_et",
        "how_to_use_et",
        "brand_et",
        "origin_country_et",
        "origin_country_of_ingredients_et",
        "shelf_life_et",
        "status",
        "quantity",
        "delivery_date",
        "is_black_list"
    ];

    public $is_lightweight = 0;
    public $is_vegan = 0;
    public $is_gluten_free = 0;
    public $meta_title_et = "";
    public $status = 1;
    public $quantity = 1;

    /**
     * Relationship method
     *
     * @return mixed
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_product',  'product_id', 'category_id');
    }

    /**
     * [scopeActive description]
     * @param  [type]  $query  [description]
     * @param  boolean $status [description]
     * @return [type]          [description]
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('is_active', $status)->whereNotNull('image');
    }

    /**
     * [getImage description]
     * @return [type] [description]
     */
    public function getImage()
    {
        if($this->image){
            return url('/storage/' . $this->image);
        }

        return url('/assets/img/product.jpg');
    }

    public function getAmount()
    {
        if($this->amount == 1){
            return '1 <span>piece</span>';
        }

        return $this->amount . ' <span>pieces</span>';
    }


    public function getPrice()
    {
        return number_format($this->price, 2, ',', ' ');
    }

    public function getPricePerPiece()
    {
        if($this->weight && $this->unit == 'kg'){
            return trans('static_text.Price_per_kg:') . ' ' . number_format($this->price / $this->weight, 2, ',', ' ');
        } elseif($this->weight && $this->unit == 'l'){
            return trans('static_text.Price_per_liter:') . ' ' . number_format($this->price / $this->weight, 2, ',', ' ');
        }

        return trans('static_text.Price_per_piece:') . ' ' . number_format($this->price / $this->amount, 2, ',', ' ');
    }

    /**
     * [getImage description]
     * @return [type] [description]
     */
    public function getThumb()
    {
        if($this->image){

            $path = pathinfo($this->image);

            return url('/storage/' . $path['dirname'] . '/' . $path['filename'] . '-cropped.' . $path['extension']);
        }

        return url('/assets/img/product.jpg');
    }

    /**
     * [scopeOrdered description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC')->orderBy('amount', 'ASC');
    }

    public function getPromotedToAttribute($value)
    {
        return (isset($value)) ? date('Y-m-d', strtotime($value)) : '';
    }

    public function getPromotedFromAttribute($value)
    {
        return (isset($value)) ? date('Y-m-d', strtotime($value)) : '';
    }

    public function setPromotedFromAttribute($value)
    {
        $this->attributes['promoted_from'] = ($value == "") ? null : $value;
    }

    public function setPromotedToAttribute($value)
    {
        $this->attributes['promoted_to'] = ($value == "") ? null : \Carbon\Carbon::parse($value)->addHour(23)->addMinutes(59)->addSeconds(59);
    }

    public static function getPromotedCount()
    {
        $now = \Carbon\Carbon::now();
        return Product::where('is_promoted', '1')
            ->where('promoted_from', '<', $now)
            ->where('promoted_to', '>', $now)
            ->count();
    }

}
