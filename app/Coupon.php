<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $guarded = [];
    public static function existsByCode($code)
    {
        return Coupon::where('code', $code)->where('remaining_number_of_uses', '>', 0)->exists();
    }

    public static function findByCode($code)
    {
        return Coupon::where('code', $code)->where('remaining_number_of_uses', '>', 0)->first();
    }

    public function decreaseUsage()
    {
        if (!$this->unlimited) {
            $this->remaining_number_of_uses -= 1;
        }
        $this->count_uses += 1;
        $this->save();
    }

    public static function generate($saleCoefficient, $remainingNumberOfUses, $unlimited)
    {
        $code = null;
        do {
            $code = str_random(3) . '-' . str_random(3) . '-' . str_random(3);
        } while (Coupon::where('code', $code)->exists());
        Coupon::insert([
            'code' => $code,
            'sale_coefficient' => $saleCoefficient,
            'remaining_number_of_uses' => $remainingNumberOfUses,
            'unlimited' => $unlimited
        ]);
    }
}
