<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_method' => 'required|in:refrigerated_delivery,eu_shipping,heavy_shipping,customer_pickup,courier_estonia,parcel_terminal',
            'shipping_parcel_terminal_phone' => 'required_if:shipping_method,parcel_terminal|max:10|nullable',
            'shipping_parcel_terminal_phone_code' => 'required_if:shipping_method,parcel_terminal|max:4|nullable',
            'shipping_parcel_terminal_id' => 'required_if:shipping_method,parcel_terminal|exists:parcel_terminals,id|nullable',
            
            'billing_address' => 'nullable|max:255',
            'billing_country' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'billing_state' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'billing_city' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'billing_zip_code' => 'required_unless:shipping_method,parcel_terminal,customer_pickup',

            'company_name' => 'nullable|max:255',
            'company_reg_number' => 'nullable|max:255',

            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required_unless:shipping_method,parcel_terminal|max:255',
            'vat_number' => 'nullable|max:255',

            'shipping_address' => 'nullable|max:255',
            'shipping_country' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'shipping_state' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'shipping_city' => 'required_unless:shipping_method,parcel_terminal,customer_pickup|max:255',
            'shipping_zip_code' => 'required_unless:shipping_method,parcel_terminal,customer_pickup',

            'payment_method' => 'required',
            'comment' => 'nullable|max:500',
            'hear_about_us' => 'nullable|max:12',

            'terms' => 'required',
            'sign_up' => '',
            
            'password' => 'required_if:sign_up,1|min:6|confirmed',
        ];
    }
}
