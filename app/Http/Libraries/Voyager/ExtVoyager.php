<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 07.11.17
 * Time: 17:15
 */

namespace App\Http\Libraries\Voyager;


use TCG\Voyager\Voyager;
//use TCG\Voyager\Facades\Voyager;

class ExtVoyager extends Voyager
{
    public function routes()
    {
        require base_path('routes/voyager.php');
    }
}