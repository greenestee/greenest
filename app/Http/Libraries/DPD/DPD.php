<?php

namespace App\Http\Libraries\DPD;


use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DPD
{
    private $username;
    private $password;
    private $trackingToken;
    private $isReadyApi;
    private $isReadyTracking;

    private $url = 'https://integration.dpd.ee:8443';
    private $trackingUrl = 'https://status.dpd.ee/external/tracking';

    function __construct()
    {
        $this->username = env('DPD_USERNAME', null);
        $this->password = env('DPD_PASSWORD', null);

        $this->trackingToken = env('DPD_TRACKING_TOKEN', null);

        $this->isReadyTracking = !empty($this->trackingToken);
        $this->isReadyApi = !(empty($this->username) || empty($this->password));
    }

    public function getParcels($country = 'EE')
    {
        $params = [
            'country' => $country,
            'fetchGsPUDOpoint' => 1
        ];
        
        $response = $this->requestDPDApi('ws-mapper-rest/parcelShopSearch_', $params);

        if (!empty($response) && isset($response->parcelshops)) {
            return $response->parcelshops;
        }
        
        return [];
    }

    public function createShipment($orderId, $name, $street, $city, $country, $postalCode, $phone, $parcelType, $dpdId = null)
    {
        $params = [
            'name1' => $name,
            'street' => $street,
            'city' => $city,
            'country' => $country,
            'pcode' => $postalCode,
            'phone' => $phone,
            'num_of_parcel' => 1,
            'parcel_type' => $parcelType,
            'order_number' => $orderId,
            'fetchGsPUDOpoint' => 1,
        ];
        if (!empty($dpdId)) {
            $params['parcelshop_id'] = $dpdId;
        }

        $response = $this->requestDPDApi('ws-mapper-rest/createShipment_', $params);

        if (!empty($response) && !empty($response->pl_number) && is_array($response->pl_number)) {
            return array_first($response->pl_number);
        }

        return null;
    }

    public function printLabels($rawShipmentIds)
    {
        $params = [
            'parcels' => $rawShipmentIds,
            'printType' => 'pdf',
            'printFormat' => 'A4',
            'printPosition' => 'LeftTop'
        ];

        $response = $this->requestDPDApi('ws-mapper-rest/parcelPrint_', $params);

        if (is_string($response)) {
            $name = str_random(5) . Carbon::now()->format('YmdHis') . '.pdf';
            file_put_contents(public_path("storage/tmp/{$name}"), $response);
            return public_path("storage/tmp/{$name}");
        }

        return null;
    }

    public function rejectShipment($shipmentId)
    {
        $params = [
            'parcels' => $shipmentId
        ];

        $response = $this->requestDPDApi('/ws-mapper-rest/parcelDelete_', $params);

        if (isset($response->status) && $response->status == 'ok') {
            return true;
        }
        return false;
    }

    public function getStatuses($shipmentIds)
    {
        if (!$this->isReadyTracking) {
            return null;
        }
        $params = [
            'pknr' => $shipmentIds
        ];

        $url = $this->trackingUrl . '?' . http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'content-type' => 'application/x-www-form-urlencoded',
            'Authorization' => "Bearer {$this->trackingToken}"
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return json_decode($output);
    }

    private function requestDPDApi($route, $params = [])
    {
        if (!$this->isReadyApi) {
            return null;
        }
        $url = "{$this->url}/{$route}";

        $params['username'] = $this->username;
        $params['password'] = $this->password;

        $url .= '?' . http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if (!empty($err)) {
            Log::error($err);
            return $err;
        }

        $outputDecoded = json_decode($output);

        if (isset($outputDecoded->status) && $outputDecoded->status == 'err' && isset($outputDecoded->errLog)) {
            Log::error($outputDecoded->errLog);
            return $outputDecoded->errLog;
        }

        return isset($outputDecoded) ? $outputDecoded : $output;
    }
}