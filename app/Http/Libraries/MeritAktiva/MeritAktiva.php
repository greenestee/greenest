<?php

namespace App\Http\Libraries\MeritAktiva;


use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MeritAktiva
{
    const ApiID = 'bffa83c0-24b7-4e0d-9da7-f51b51339292';
    const ApiKey = '0IIlrd8I6reyXB63pP0Tx5rVibPYbuaEVN0LYSo7+2s=';

    const RefMask = [7,3,1];


    public static function getItems()
    {
        $now = Carbon::now()->format('YmdHis');

        $data = [
            'Code' => '',
            'Description' => ''
        ];

        $string = self::ApiID . $now . json_encode($data);
        $hash = hash_hmac('sha256', $string, self::ApiKey, true);
        $signature = base64_encode($hash);

        $url = 'https://aktiva.merit.ee/api/v1/getitems?ApiId=' . self::ApiID . '&timestamp=' . $now . '&signature=' . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/json',));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        if (isset(json_decode($output)->Message)) {
            return json_decode($output);
        }

        return json_decode(json_decode($output));
    }

    public static function setPayment($order)
    {
        $now = Carbon::now()->format('YmdHis');

        $data = [
            'IBAN' => 'EE482200221061511058',
            'CustomerName' => $order->name,
            'InvoiceNo' => $order->id,
            'RefNo' => self::getRefNo($order->id),
            'Amount' => (string)$order->sum_total
        ];

        $string = self::ApiID . $now . json_encode($data);
        $hash = hash_hmac('sha256', $string, self::ApiKey, true);
        $signature = base64_encode($hash);

        $url = 'https://aktiva.merit.ee/api/v1/sendpayment?ApiId=' . self::ApiID . '&timestamp=' . $now . '&signature=' . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/json',));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $output = curl_exec($ch);
        Log::info($output, $data);

        if (isset(json_decode($output)->Message)) {
            return json_decode($output);
        }

        return json_decode(json_decode($output));
    }

    public static function setOrder($order)
    {
        $now = Carbon::now()->format('YmdHis');
        $invoice = [];
        $subtotal = 0;
        $meritTotal = 0;

        foreach ($order->items()->get() as $item) {
            $product = Product::find($item->product_id);
            if ($product) {
                $invoice[] = [
                    'Item' => [
                        'Code' => trim($product->sku),
                        'Description' => empty($product->name_et) ? $product->name : $product->name_et,
                        'UOMName' => 'tk',
                        'Type' => 3,
                        'DefLocationCode' => 1,
                    ],
                    'Quantity' => $item->quantity,
                    'Price' => (string)round($item->price / $item->quantity, 2),
                    'DiscountPct' => 0,
                    'TaxId' => 'b9b25735-6a15-4d4e-8720-25b254ae3d21',
                    'DiscountAmount' => 0,
                    'LocationCode' => 1,
                ];
            } else {
                $shipping = '';
                if (stristr($item->name, 'EU Shipping') || stristr($item->name, 'EU transport')) {
                    $shipping = 'eushipping';
                } else if(stristr($item->name, 'Parcel terminal') || stristr($item->name, 'Pakiautomaat')) {
                    $shipping = 'parcelterminal';
                } else if(stristr($item->name, 'Courier in Estonia') || stristr($item->name, 'Kuller Eestis')) {
                    $shipping = 'courierestonia';
                }

                $invoice[] = [
                    'Item' => [
                        'Code' => $shipping,
                        'Description' => $item->name,
                        'UOMName' => 'tk',
                    ],
                    'Quantity' => $item->quantity,
                    'Price' => (string)round($item->price / $item->quantity, 2),
                    'DiscountPct' => 0,
                    'TaxId' => 'b9b25735-6a15-4d4e-8720-25b254ae3d21',
                    'DiscountAmount' => 0,
                    'LocationCode' => 1,
                ];
            }
            $subtotal += round($item->price / $item->quantity, 2) * $item->quantity;
            round($item->price / $item->quantity, 2);
        }

        $delta = number_format($subtotal - $order->subtotal, 2);
        foreach ($invoice as $key => $value) {
            if ($invoice[$key]['Quantity'] == 1) {
                $invoice[$key]['Price'] = number_format((double)$invoice[$key]['Price'] - (double)$delta, 2);
                break;
            }
        }

        foreach ($invoice as $key => $value) {
            $meritTotal += round((double)$invoice[$key]['Price'] * 1.2 * $invoice[$key]['Quantity'], 2);
        }

        $rounding = number_format($order->sum_total - $meritTotal, 2);

        $subtotal = number_format($order->subtotal,2);

        $data = [
            'Customer' => [
                'Name' => $order->name,
                'Address' => $order->billing_address,
                'CurrencyCode' => 'EUR',
                'County' => $order->billing_country,
                'CountryCode' => 'EE',
                'NotTDCustomer' => true,
                'City' => $order->billing_city,
                'PostalCode' => $order->billing_zip_code,
                'PhoneNo' => $order->phone,
                'Email' => $order->email,
            ],
            'DocDate' => $now,
            'DueDate' => $now,
            'InvoiceNo' => $order->id,
            'RefNo' => self::getRefNo($order->id),
            'InvoiceRow' => $invoice,
            'RoundingAmount' => $rounding,
            'TotalAmount' => $subtotal,
            'TaxAmount' => [[
                'TaxId' => 'b9b25735-6a15-4d4e-8720-25b254ae3d21',
                'Amount' => (string)($order->subtotal * 0.2),
            ],],
        ];


        $string = self::ApiID . $now . json_encode($data);
        $hash = hash_hmac('sha256', $string, self::ApiKey, true);
        $signature = base64_encode($hash);

        $url = 'https://aktiva.merit.ee/api/v1/sendinvoice?ApiId=' . self::ApiID . '&timestamp=' . $now . '&signature=' . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/json',));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $output = curl_exec($ch);
        Log::info($output, $data);

        if (isset(json_decode($output)->Message)) {
            return json_decode($output);
        }

        return json_decode(json_decode($output));
    }

    public static function getRefNo($orderId)
    {
        $str = (string) $orderId;
        $numbers = [];
        $strMask = [];

        for ($i = 0; $i < strlen($str); $i++) {
            $numbers[] = $str[$i];
            $strMask[] = self::RefMask[$i % 3];
        }
        $strMask = array_reverse($strMask);

        foreach ($numbers as $key => $value) {
            $strMask[$key] *= $numbers[$key];
        }

        $sum = array_sum($strMask);

        if ($sum % 10 === 0) {
            return "{$orderId}0";
        } else {
            $lastDigit = 10 - ($sum - (floor($sum / 10) * 10));
            return "{$orderId}{$lastDigit}";
        }
    }
}