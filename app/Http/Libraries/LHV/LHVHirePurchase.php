<?php

namespace App\Http\Libraries\LHV;


use App\Http\Libraries\MeritAktiva\MeritAktiva;

class LHVHirePurchase
{
    private $cartContent;
    private $order;
    private $bank;

    private $activeBanks = [
        'SEB'       => 'https://banklink.swedbank.com/EE/EEUHEE2X',
        'LHV'       => 'https://banklink.swedbank.com/EE/LHVBEE22',
        'SWEDBANK'  => 'https://www.swedbank.ee/banklink',
    ];

    private $request = [
        'VK_SERVICE' => '1012',
        'VK_VERSION' => '008',
    ];

    public function __construct($cartContent, $order, $bank)
    {
        $this->cartContent = $cartContent;
        $this->order = $order;
        $this->bank = $bank;
    }

    public function hirePurchase()
    {
        $this->formRequest();
        return $this->request;
    }

    /**
     * Forming request to LHV according to the rules
     */
    private function formRequest()
    {
        $this->request['VK_SND_ID'] = env('LHV_SND_ID');
        $this->request['VK_STAMP'] = $this->getStamp();

        $this->request['VK_AMOUNT'] = number_format($this->order->sum_total, 2, '.', '');
        $this->request['VK_CURR'] = 'EUR';
        $this->request['VK_REF'] = MeritAktiva::getRefNo($this->order->id);
        $this->request['VK_MSG'] = "Order #{$this->order->id}";

        $this->request['VK_RETURN'] = url(\App::getLocale() . "/checkout/success/{$this->order->id}/" . $this->order->makeUrlSignature());
        $this->request['VK_CANCEL'] = url(\App::getLocale() . "/checkout/success/{$this->order->id}/cancel"); //url('/' . \App::getLocale());
        $this->request['VK_DATETIME'] = date('Y-m-d\TH:i:s\+0000');

        $vkMac = static::getVkMac($this->request);
        $encrypted = $this->encryptVkMac($vkMac);
        $this->request['VK_MAC'] = $encrypted;
    }

    /**
     * Stamp - unique request id - is formed using orderId and date 
     * 
     * @return string $stamp
     */
    private function getStamp()
    {
        $stamp = $this->order->id;
        $date = strtotime(date('c'));
        $stamp .= '|'.date('YmdHm', strtotime("+1 month", $date));
        return $stamp;
    }

    /**
     * Forms XML part of the request using Products data
     * 
     * @return string $XMLData
     */
    private function getXML()
    {
        $XMLData = '<CofContractProductList>';

        foreach ($this->cartContent as $item) {
            $quantity = $item->qty;
            if ($item->price > 0) {
                while ($quantity > 0) {
                    $costWithVat = $item->price + ($item->price / 100) * 20;
                    $costWithVat = number_format($costWithVat, 2, '.', '');

                    $item->id = is_string($item->id) ? $this->order->id : $item->id;

                    $XMLData .= <<<EOT
<CofContractProduct><Name>$item->name</Name><Code>$item->id</Code><Currency>EUR</Currency><CostInclVatAmount>$costWithVat</CostInclVatAmount><CostVatPercent>$item->taxRate</CostVatPercent></CofContractProduct>
EOT;
                    $quantity -= 1;
                }
            }
        }

        $date = strtotime(date('c'));
        $validToDate = date('c', strtotime("+1 month", $date));
        $XMLData .= "<ValidToDtime>$validToDate</ValidToDtime></CofContractProductList>";

        return $XMLData;
    }

    /**
     * VK_MAC parameter is formed using the length of each request value + value itself
     *
     * @param array $request
     * @return string
     */
    public static function getVkMac($request)
    {
        $vkMac = '';
        foreach ($request as $key => $value) {
            if (mb_strlen($value) < 10) {
                $vkMac .= '00'.mb_strlen($value).$value;
            } elseif (mb_strlen($value) < 100) {
                $vkMac .= '0'.mb_strlen($value).$value;
            } else {
                $vkMac .= mb_strlen($value).$value;
            }
        }

        return $vkMac;
    }

    /**
     * VK_MAC parameter is encrypted using private key
     * 
     * @param  string $vkMac
     * @return string
     */
    private function encryptVkMac($vkMac)
    {
        $privKey = file_get_contents(storage_path('LHVkeys/privkey.pem'));
        $privKeyResource = openssl_pkey_get_private($privKey);

        openssl_sign($vkMac, $signature, $privKeyResource);
        openssl_free_key($privKeyResource);

        return base64_encode($signature);
    }

    /**
     * VK_MAC sent by the bank is verified using bank's public key
     *
     * @param string $data
     * @param string $vkMac
     * @return bool
     */
    public static function verifyVkMac($data, $vkMac)
    {
        $pubKey = file_get_contents(storage_path('LHVkeys/LHVPublicKey.pem'));
        $pubKeyResource = openssl_pkey_get_public($pubKey);

        $result = openssl_verify($data, base64_decode($vkMac) , $pubKeyResource);
        openssl_free_key($pubKeyResource);

        return $result === 1;
    }

    public function getRequestUrl()
    {
        $url = '';
        if (key_exists($this->bank, $this->activeBanks)) {
            $url = $this->activeBanks[$this->bank];
        }

        return $url;
    }
}