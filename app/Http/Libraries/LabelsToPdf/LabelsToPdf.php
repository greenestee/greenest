<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.11.18
 * Time: 11:48
 */

namespace App\Http\Libraries\LabelsToPdf;


use App\Category;
use App\CategoryProduct;
use App\Console\Commands\sendListOfMissingProducts;
use App\Order;
use App\Product;

class LabelsToPdf
{
    public static function generateRefrigeratedPackageCards(array $ids)
    {
        $columns = 4;
        $rows = 2;
        $width = function () use ($columns) {
            return floor(288 / $columns);
        };
        $height = function () use ($rows) {
            return floor(210 / $rows);
        };

        $headers = [
            'refrigerated' => 'Jahetooted',
            'frozen' => 'Külmatooted',
            'regular' => 'Tavatooted'
        ];

        $w = $width();
        $h = 4.5;
        $font = 12;
        $countLabelsPerOrder = 2;

        $x = function ($col) use ($width) {
            return ($col * $width() + 5);
        };
        $y = function ($row) use ($height) {
            return ($row * $height() + 20);
        };

        $pdf = new MyFPDF();
        $pdf->SetFont('arial','',$font);
        $pdf->SetMargins(0, 0, 0);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage('L');
        $row = 0;
        $col = 0;
        $linesCount = 0;

        $addText = function ($text, $bold = false, $textFont = null) use (&$row, &$col, &$linesCount, &$pdf, &$w, &$h, &$font, $x, $y) {
            $pdf->SetXY($x($col), $y($row) + $linesCount * $h);
            $pdf->SetFont('arial', $bold ? 'B' : '', $textFont ?? $font);
            $linesCount += $pdf->MultiCell($w, $h, self::textToCell($text), '', 'C');
        };

        foreach ($ids as $id) {

            /** @var Order $order */
            $order = Order::find($id);

            $orderItemTypes = [
                'refrigerated' => $order->isHaveRefrigerated(),
                'frozen' => $order->isHaveFrozen(),
                'regular' => $order->isHaveRegularItems()
            ];

            if (!$orderItemTypes['refrigerated'] && !$orderItemTypes['frozen']) {
                continue;
            }

            $countLabelsPerOrder = 0;
            $needHeaders = [];
            foreach ($orderItemTypes as $type => $isset) {
                if ($isset) {
                    $needHeaders[] = $headers[$type];
                    $countLabelsPerOrder++;
                }
            }

            $shippingAddress = $order->shipping_country . ', '
                . $order->shipping_state . ', '
                . $order->shipping_city . ', '
                . $order->shipping_address . ', '
                . $order->shipping_zip_code;

            for($i = 0; $i < $countLabelsPerOrder; $i ++) {
                if ($col > $columns - 1) {
                    if ($row == $rows - 1) {
                        $pdf->AddPage('L');
                        $row = 0;
                        $col = 0;
                    } else {
                        $row++;
                        $col = 0;
                    }
                }

                $linesCount = 0;
                // ORDER ID
                $addText("\n\nOrder ID: {$order->id}\n\n");

                // HEADER
                $headerToCard = strtoupper(array_shift($needHeaders));
                $addText("\n{$headerToCard}\n\n\n", true, $font + 2);

                // CLIENT INFO
                $addText("Client name:\n");
                $addText("{$order->name}\n", true);
                $addText("Client phone:\n");
                $addText("{$order->phone}\n\n", true);

                // SHIPPING ADDRESS
                $addText("Shipping address:\n", true);
                $addText("{$shippingAddress}\n\n");

                // FOOTER
                $addText("Saatja:\n");
                $addText("greenest.ee\n", true);
                $addText('+372 51 900 330');

                $col++;
            }
        }

        return $pdf->Output('D');
    }

    private static function textToCell($text)
    {
        return iconv('utf-8', 'cp1252//IGNORE', self::replace($text));
    }

    public static function generateLabels($ids = null)
    {
        $pdf = new MyFPDF();
        $pdf->SetFont('arial','',9);
        $pdf->SetMargins(0, 0, 0);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage('L');
        $row = 0;
        $col = 0;
        $clItems = [];
        $rfItems = [];

        foreach ($ids as $id) {
            /** @var Order $order */
            $order = Order::find($id);
            if(isset($order)) {
                foreach ($order->itemsWithMissed()->get() as $item) {

                    $product = Product::find($item->product_id);
                    if ($item->product_id == null || !$product) {
                        continue;
                    }
                    $categories = CategoryProduct::where('product_id', '=', $item->product_id)
                        ->select('category_id')->get();
                    $cleanItem = false;
                    $refrigeratedFood = false;
                    foreach ($categories as $category) {
                        $parentCategory = Category::find($category->category_id)->parent_id;
                        if ($category->category_id == 48 || $category->category_id == 81 || $parentCategory == 5) {
                            $cleanItem = true;
                        } elseif ($category->category_id == 116) {
                            $refrigeratedFood = true;
                        }
                    }
                    for ($q = 0; $q < $item->quantity; $q++) {
                        if ($cleanItem) {
                            $arrayProduct = $product->toArray();
                            if ($order->id != $item->order_id) {
                                $arrayProduct['origin_order_id'] = $item->order_id;
                            }
                            $clItems[$order->id][] = $arrayProduct;
                        } elseif ($refrigeratedFood) {
                            $rfItems[] = [
                                'order' => $order,
                                'product' => $product,
                                'item' => $item
                            ];
                        } else {
                            if ($col > 5) {
                                if ($row == 1) {
                                    $pdf->AddPage('L');
                                    $row = 0;
                                    $col = 0;
                                } else {
                                    $row++;
                                    $col = 0;
                                }
                            }

                            $text = self::textAnalitics("order ID: $order->id "
                                . ($order->id != $item->order_id ? "({$item->order_id})" : '') . "\n",
                                ($product->name_et ? $product->name_et : $product->name),
                                self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients),
                                self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value) . "Allergeenid: " .
                                self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information) . "\n",
                                "Päritolumaa: " . $product->origin_country_et);


                            $pdf->SetFont('arial', '', $text['font']);

//                        $pdf->SetXY(self::X($col), self::Y($row));
//                        $pdf->Cell(48, 105, "", 'LRTB', 0, 'C');
                            $pdf->SetXY(self::X($col), self::Y($row));
                            $linesCount = $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', "\n\n\norder ID: $order->id "
                                . ($order->id != $item->order_id ? "({$item->order_id})" : '') . "\n")
                                , '', 'C');
                            $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                            $pdf->SetFont('arial', 'B', $text['font']);
                            $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', ($product->name_et ? $product->name_et : $product->name)), '', 'C');
                            $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                            $pdf->SetFont('arial', '', $text['font']);

                            $formattedString = (self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients)
                                ? "Koostisosad:\n" . self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients) . "\n"
                                : ''
                            );

                            $re = '/(\S)\s+\*\s+(\d|\()/mu';
                            $subst = '$1* $2';
                            $formattedString = preg_replace($re, $subst, $formattedString);

                            $re = '/\s\*\s+(\w)/mu';
                            $subst = ' *$1';
                            $formattedString = preg_replace($re, $subst, $formattedString);

                            $re = '/\(\*\s+(\w)/mu';
                            $subst = '(*$1';
                            $formattedString = preg_replace($re, $subst, $formattedString);

                            $re = '/(\S)\s+\*,/mu';
                            $subst = '$1*,';
                            $formattedString = preg_replace($re, $subst, $formattedString);

                            $re = '/(\S)\s+\*\s/mu';
                            $subst = '$1* ';
                            $formattedString = preg_replace($re, $subst, $formattedString);

                            $formattedString = str_replace('**', '* *', $formattedString);
                            $formattedString = str_replace(')*', ') *', $formattedString);
                            $formattedString = str_replace(' *)', '*)', $formattedString);

                            if (preg_match('/^Koostisosad:\n/mu', $formattedString)) {
                                $linesCount++;
                                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                                $pdf->SetFont('arial', 'B', $text['font']);
                                $pdf->Cell(48, $text['height'], "Koostisosad:", '', '', 'C');
                                $linesCount++;
                                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                                $pdf->SetFont('arial', '', $text['font']);
                                $formattedString = substr($formattedString, 13);
                            }

                            $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', $formattedString), '', 'C');

                            $secondString = (self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value)
                                    ? "Toitumisalane teave 100 g kohta:\n" .
                                    self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value) . "\n"
                                    : '') .
                                (self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information)
                                    ? "Allergeenid: " .
                                    self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information) . "\n"
                                    : '') .
                                'Päritolumaa: ' . $product->origin_country_et .
                                "\nTurustaja: Nature Design OÜ\nTornimäe 5, Tallinn";

                            if (preg_match('/^Toitumisalane teave 100 g kohta:\n/mu', $secondString)) {

                                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                                $pdf->SetFont('arial', 'B', $text['font']);
                                $pdf->Cell(48, $text['height'], "Toitumisalane teave 100 g kohta:", '', '', 'C');
                                $linesCount++;
                                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                                $pdf->SetFont('arial', '', $text['font']);
                                $secondString = substr($secondString, 33);
                            }

                            $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                            $pdf->SetFont('arial', '', $text['font']);
                            $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', $secondString)
                                , '', 'C');

                            $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                            $pdf->SetFont('arial', 'B', $text['font']);
                            $pdf->Cell(48, $text['height'], "greenest.ee", '', '', 'C');
                            $linesCount++;
                            $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                            $pdf->SetFont('arial', '', $text['font']);
                            $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', "Parimate hindadega mahepood!\n+372 51 900 330"), '', 'C');
//                        $pdf->SetXY(self::X($col),  self::Y($row) + $linesCount * $text['height']);
//                        $pdf->SetFont('arial', 'B', $text['font']);
//                        $pdf->Cell(48, $text['height'], " lol", '', '', 'C');
                            $col++;
                        }
                    }
                }
            }
        }
        if(count($clItems) > 0) {
            $pdf->AddPage();
            $row = 0;
            $col = 0;

            $pdf->SetFontSize(8);

            foreach ($clItems as $keyOr => $clItem) {

                foreach ($clItem as $item) {
                    if ($col > 1) {
                        if ($row > 0) {
                            $pdf->AddPage();
                            $row = 0;
                            $col = 0;
                        } else {
                            $row++;
                            $col = 0;
                        }
                    }

                    $pdf->SetXY(self::Xcl($col), self::Ycl($row));
                    $pdf->Cell(95, 133, "", 'LRTB', 0, 'C');
                    $pdf->SetXY(self::Xcl($col), self::Ycl($row));
                    $pdf->MultiCell(95, 3, iconv('utf-8', 'cp1252//IGNORE', "order ID: $keyOr "
                            . (isset($item['origin_order_id']) ? "({$item['origin_order_id']})" : '') . "\n" .
                            ($item['name_et'] ? $item['name_et'] : $item['name']) .
                            "\nKoostisosad:\n" .
                            self::replace($item['ingredients_et'] ? $item['ingredients_et'] : $item['ingredients']) .
                            "\n\n" .
                            self::replace($item['recommended_storage_et'] ? $item['recommended_storage_et'] : $item['recommended_storage']) .
                            "\n\n" .
                            self::replace($item['allergenic_information_et'] ? $item['allergenic_information_et'] : $item['allergenic_information']) .
                            "\n\n" .
                            self::replace($item['description_et'] ? $item['description_et'] : $item['description']) .
                            "\n\n" .
                            self::replace($item['how_to_use_et'] ? $item['how_to_use_et'] : $item['how_to_use']) .
                            "\n\n" .
                            'Päritolumaa: ' . $item['origin_country_et'] .
                            "\n\nTurustaja: Nature Design OÜ\nTornimäe 5, Tallinn\ngreenest.ee\nParimate hindadega mahepood!\n+372 51 900 330")
                        , '', 'C');

                    $col++;
                }
            }
        }

        if (count($rfItems) > 0) {
            $pdf->AddPage('L');
            $row = 0;
            $col = 0;

            foreach ($rfItems as $rfItem) {

                $order = $rfItem['order'];
                $product = $rfItem['product'];
                $item = $rfItem['item'];

                if ($col > 5) {
                    if ($row == 1) {
                        $pdf->AddPage('L');
                        $row = 0;
                        $col = 0;
                    } else {
                        $row++;
                        $col = 0;
                    }
                }

                $text = self::textAnalitics("Jahetoode\norder ID: $order->id "
                    . ($order->id != $item->order_id ? "({$item->order_id})" : '') . "\n",
                    ($product->name_et ? $product->name_et : $product->name),
                    self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients),
                    self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value) . "Allergeenid: " .
                    self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information) . "\n",
                    "Päritolumaa: " . $product->origin_country_et);


                $pdf->SetFont('arial', '', $text['font']);

                $pdf->SetXY(self::X($col), self::Y($row));
                $linesCount = $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', "\n\n\nJahetoode\n\norder ID: $order->id "
                        . ($order->id != $item->order_id ? "({$item->order_id})" : '') . "\n")
                    , '', 'C');
                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                $pdf->SetFont('arial', 'B', $text['font']);
                $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', ($product->name_et ? $product->name_et : $product->name)), '', 'C');
                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                $pdf->SetFont('arial', '', $text['font']);

                $formattedString = (self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients)
                    ? "Koostisosad:\n" . self::replace($product->ingredients_et ? $product->ingredients_et : $product->ingredients) . "\n"
                    : ''
                );

                $re = '/(\S)\s+\*\s+(\d|\()/mu';
                $subst = '$1* $2';
                $formattedString = preg_replace($re, $subst, $formattedString);

                $re = '/\s\*\s+(\w)/mu';
                $subst = ' *$1';
                $formattedString = preg_replace($re, $subst, $formattedString);

                $re = '/\(\*\s+(\w)/mu';
                $subst = '(*$1';
                $formattedString = preg_replace($re, $subst, $formattedString);

                $re = '/(\S)\s+\*,/mu';
                $subst = '$1*,';
                $formattedString = preg_replace($re, $subst, $formattedString);

                $re = '/(\S)\s+\*\s/mu';
                $subst = '$1* ';
                $formattedString = preg_replace($re, $subst, $formattedString);

                $formattedString = str_replace('**', '* *', $formattedString);
                $formattedString = str_replace(')*', ') *', $formattedString);
                $formattedString = str_replace(' *)', '*)', $formattedString);

                if (preg_match('/^Koostisosad:\n/mu', $formattedString)) {
                    $linesCount++;
                    $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                    $pdf->SetFont('arial', 'B', $text['font']);
                    $pdf->Cell(48, $text['height'], "Koostisosad:", '', '', 'C');
                    $linesCount++;
                    $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                    $pdf->SetFont('arial', '', $text['font']);
                    $formattedString = substr($formattedString, 13);
                }

                $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', $formattedString), '', 'C');

                $secondString = (self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value)
                        ? "Toitumisalane teave 100 g kohta:\n" .
                        self::replace($product->nutritional_value_et ? $product->nutritional_value_et : $product->nutritional_value) . "\n"
                        : '') .
                    (self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information)
                        ? "Allergeenid: " .
                        self::replace($product->allergenic_information_et ? $product->allergenic_information_et : $product->allergenic_information) . "\n"
                        : '') .
                    'Päritolumaa: ' . $product->origin_country_et .
                    "\nTurustaja: Nature Design OÜ\nTornimäe 5, Tallinn";

                if (preg_match('/^Toitumisalane teave 100 g kohta:\n/mu', $secondString)) {

                    $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                    $pdf->SetFont('arial', 'B', $text['font']);
                    $pdf->Cell(48, $text['height'], "Toitumisalane teave 100 g kohta:", '', '', 'C');
                    $linesCount++;
                    $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                    $pdf->SetFont('arial', '', $text['font']);
                    $secondString = substr($secondString, 33);
                }

                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                $pdf->SetFont('arial', '', $text['font']);
                $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', $secondString)
                    , '', 'C');

                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                $pdf->SetFont('arial', 'B', $text['font']);
                $pdf->Cell(48, $text['height'], "greenest.ee", '', '', 'C');
                $linesCount++;
                $pdf->SetXY(self::X($col), self::Y($row) + $linesCount * $text['height']);
                $pdf->SetFont('arial', '', $text['font']);
                $linesCount += $pdf->MultiCell(48, $text['height'], iconv('utf-8', 'cp1252//IGNORE', "Parimate hindadega mahepood!\n+372 51 900 330"), '', 'C');

                $col++;
            }
        }

        return $pdf->Output('D');
    }

    public static function X($col)
    {
        return ($col * 48 + 5);
    }

    public static function Y($row)
    {
        return ($row * 105);
    }

    public static function Xcl($col)
    {
        return ($col * 95 + 10);
    }

    public static function Ycl($row)
    {
        return ($row * 133 + 10);
    }

    public static function replace($string)
    {
        $string = strip_tags($string);
        $string = html_entity_decode($string);

        $string = preg_replace('/(\s*\n\s*)+/', "\n", $string);
        $string = preg_replace('/^(\s*\n\s*)+/', '', $string);
        $string = preg_replace('/(\s*\n\s*)+$/', '', $string);

        return $string;
    }

    public static function textAnalitics($id, $name, $ing, $nut, $country)
    {
        // $text['text'] = iconv('utf-8', 'cp1252',$id . $name . "\n\n" . ($ing ? $ing . "\n" : '') . ($nut ? "Toitumisalane teave 100 g kohta:\n" . $nut . "\n" : '') . $country .
        //     "\nMaaletooja: Nature Design OÜ\nTornimäe 5, Tallinn\ngreenest.ee\nParimate hindadega mahepood!\n+372 51 900 330");

        $text['rows'] = 8;

        if (strlen($name) < 20) {
            $text['rows']++;
            $text['rows']++;
        } else {
            $text['rows']++;
            $text['rows']++;
            $string = '';
            foreach(explode(' ', $name) as $word) {
                if (strlen($string . ' ' . $word) < 20) {
                    $string .= ' ' . $word;
                } else {
                    $string = '';
                    $text['rows']++;
                }
            }
        }

        if (strlen($ing) < 21 && strlen($ing) > 0) {
            $text['rows']++;
            $text['rows']++;
        } else if (strlen($ing) > 0) {
            $text['rows']++;
            $text['rows']++;
            $string = '';
            foreach(explode(' ', $ing) as $word) {
                if (strlen($string . ' ' . $word) < 21) {
                    $string .= ' ' . $word;
                } else {
                    $string = '';
                    $text['rows']++;
                }
            }
        }

        if (strlen($nut) < 22 && strlen($nut) > 0) {
            $text['rows']++;
            $text['rows'] += substr_count($nut, "\n");
        } else if (strlen($nut) > 0) {
            $text['rows']++;
            $text['rows'] += substr_count($nut, "\n");
            $string = '';
            foreach(explode(' ', $nut) as $word) {
                if (strlen($string . ' ' . $word) < 22) {
                    $string .= ' ' . $word;
                } else {
                    $string = '';
                    $text['rows']++;
                }
            }
        }

        if (strlen($country) < 21 && strlen($country) > 0) {
            $text['rows']++;
        } else if (strlen($country) > 0) {
            $text['rows']++;
            $string = '';
            foreach(explode(' ', $country) as $word) {
                if (strlen($string . ' ' . $word) < 21) {
                    $string .= ' ' . $word;
                } else {
                    $string = '';
                    $text['rows']++;
                }
            }
        }

        if ($text['rows'] > 26 && $text['rows'] < 33) {
            $text['font'] = 7;
            $text['height'] = 3;
        } else if ($text['rows'] >= 33) {
            $text['font'] = 7;
            $text['height'] = 2.5;
        } else {
            $text['font'] = 8;
            $text['height'] = 3;
        }

        return $text;
    }
}

class MyFPDF extends \FPDF
{
    function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false)
    {
        $lineCounter = 1;
        // Output text with automatic or explicit line breaks
        if(!isset($this->CurrentFont))
            $this->Error('No font has been set');
        $cw = &$this->CurrentFont['cw'];
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $wmax = ($w-2*$this->cMargin)*1000/$this->FontSize;
        $s = str_replace("\r",'',$txt);
        $nb = strlen($s);
        if($nb>0 && $s[$nb-1]=="\n")
            $nb--;
        $b = 0;
        if($border)
        {
            if($border==1)
            {
                $border = 'LTRB';
                $b = 'LRT';
                $b2 = 'LR';
            }
            else
            {
                $b2 = '';
                if(strpos($border,'L')!==false)
                    $b2 .= 'L';
                if(strpos($border,'R')!==false)
                    $b2 .= 'R';
                $b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
            }
        }
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $ns = 0;
        $nl = 1;
        while($i<$nb)
        {
            // Get next character
            $c = $s[$i];
            if($c=="\n")
            {
                $lineCounter++;
                // Explicit line break
                if($this->ws>0)
                {
                    $this->ws = 0;
                    $this->_out('0 Tw');
                }
                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $ns = 0;
                $nl++;
                if($border && $nl==2)
                    $b = $b2;
                continue;
            }
            if($c==' ')
            {
                $sep = $i;
                $ls = $l;
                $ns++;
            }
            $l += $cw[$c];
            if($l>$wmax)
            {
                $lineCounter++;
                // Automatic line break
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                    if($this->ws>0)
                    {
                        $this->ws = 0;
                        $this->_out('0 Tw');
                    }
                    $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                }
                else
                {
                    if($align=='J')
                    {
                        $this->ws = ($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
                        $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
                    }
                    $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                    $i = $sep+1;
                }
                $sep = -1;
                $j = $i;
                $l = 0;
                $ns = 0;
                $nl++;
                if($border && $nl==2)
                    $b = $b2;
            }
            else
                $i++;
        }
        // Last chunk
        if($this->ws>0)
        {
            $this->ws = 0;
            $this->_out('0 Tw');
        }
        if($border && strpos($border,'B')!==false)
            $b .= 'B';
        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
        $this->x = $this->lMargin;
        return $lineCounter;
    }
}