<?php

namespace App\Http\Libraries\Montonio;


use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class MontonioPayment
{
    private $apiKey;
    private $apiSecret;
    private $apiUrl;
    private $redirectUrl;
    private $isTest;

    private $activeBanks = [
        'montonio_swedbank_ee'  => 'HABAEE2X',
        'montonio_seb_ee'       => 'EEUHEE2X',
        'montonio_lhv_ee'       => 'LHVBEE22',
        'montonio_luminor_ee'   => 'RIKOEE22',
        'montonio_coop_pank_ee' => 'EKRDEE22',
        'montonio_citadele_ee'  => 'PARXEE22',
        'montonio_op_fi'        => 'OKOYFIHH',
        'montonio_nordea_fi'    => 'NDEAFIHH',
    ];

    private $algorithm = 'HS256';

    function __construct()
    {
        $this->isTest = config('montonio.mode.is_test', false);

        $type = $this->isTest ? 'test' : 'live';

        $this->apiKey       = config("montonio.api.{$type}.key");
        $this->apiSecret    = config("montonio.api.{$type}.secret");
        $this->apiUrl       = config("montonio.api.{$type}.url");
        $this->redirectUrl       = config("montonio.redirect.{$type}.url");
    }

    public function getAvailableBanks()
    {
        $req = new Client();

        $token = $this->encodeData([
            'access_key' => $this->apiKey,
            'iat'        => time(),
            'exp'        => time() + (60 * 60)
        ]);

        $headers = ['Authorization' => "Bearer {$token}"];

        $result = $req->get($this->apiUrl . '/v2/merchants/aspsps', compact('headers'));

        return $result->getBody()->getContents();
    }

    public function generatePaymentUrl($amount, $orderId, $bankBic)
    {
        $locale = \App::getLocale();

        $paymentData = array(
            'amount'                           => round($amount, 2),
            'currency'                         => 'EUR',
            'access_key'                       => $this->apiKey,
            'merchant_reference'               => $orderId,
            'merchant_return_url'              => url("/{$locale}/checkout/redirect/{$orderId}"),

            'merchant_notification_url'        => url("/{$locale}/checkout/webhook/{$orderId}"),
            'payment_information_unstructured' => 'Payment for order ' . $orderId,
            'preselected_aspsp'                => $bankBic,
            'preselected_locale'               => $locale == 'en' ? 'en_US' : 'et',
            'exp'                              => time() + (60 * 10),
        );

        $token = $this->encodeData($paymentData);

        return "{$this->redirectUrl}?payment_token={$token}";
    }

    public function validatePaymentData($token, $orderId)
    {
        try {
            $data = $this->decodeData($token);
        } catch (\Exception $e) {
            Log::error('Failed validate returned payment data. Token: "' . $token . '". Order ID: ' . $orderId . '.');

            return false;
        }

        Log::info("Validate payment Montonio.OrderId: {$orderId}. Token: {$token}");

        return $data->access_key === $this->apiKey &&
            $data->merchant_reference == $orderId &&
            $data->status === 'finalized';
    }

    protected function encodeData($data)
    {
        $token = JWT::encode($data, $this->apiSecret);

        return $token;
    }

    protected function decodeData($token)
    {
        $data = JWT::decode($token, $this->apiSecret, [$this->algorithm]);

        return $data;
    }

    public function getBankBic($ourName)
    {
        if (key_exists($ourName, $this->activeBanks)) {
            return $this->activeBanks[$ourName];
        }

        return false;
    }

    public function getActiveMethods()
    {
        return array_keys($this->activeBanks);
    }
}