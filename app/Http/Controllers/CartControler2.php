<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryProduct;
use App\Http\Requests\CheckoutRequest;
use App\Product;
use App\Order;
use App\OrderItem;
use App\ParcelTerminal;
use App\UserCart;
use Cart;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use URL;
use Mail;
use App\User;
use App\Http\Libraries\WebToPay;
use Session;
use Mollie\Laravel\Facades\Mollie;

class CartController extends Controller
{
    /**
     * [getCheckout description]
     * @return [type] [description]
     */
    public function getCheckout(Request $request)
    {
        if (!Cart::count()) {
            return redirect('/food-and-drinks');
        }

        $back_url = URL('/food-and-drinks');

        if(URL::previous() != URL::current()){
            $request->session()->put('back_url', URL::previous());
        }

        if($request->session()->has('back_url')) {
            $back_url = $request->session()->get('back_url');
        }


        $shippingMethods = $this->fetchShippingMethods();
        $paymentMethods = $this->fetchPaymentMethods();

        //$dataLayer = $this->fetchDataLayerForCheckout(); //:TODO remove
        //Session::flash('dataLayer', $dataLayer);

        return view('cart.checkout', [
            'shipping_methods'  => $shippingMethods,
            'payment_methods'  => $paymentMethods,
            'back_url' => $back_url,
        ]);
    }

    /**
     *
     */
    public function postCheckout(CheckoutRequest $request)
    {
        $this->addShippingMethodToCart($request->shipping_method);

        $order = new Order;
        $order->fill($request->all());
        $order->status = 'new';
        $order->subtotal = Cart::subtotal();
        $order->tax = Cart::tax();
        $order->sum_total = Cart::total();
        $order->shipping_parcel_terminal_phone = $request->shipping_parcel_terminal_phone_code . $request->shipping_parcel_terminal_phone;
        $order->save();

        Cart::content()->each(function ($item) use ($order) {
            $orderItem = new OrderItem([
                'name' => $item->name,
                'quantity' => $item->qty,
                'price' => $item->subtotal,
                'sum_total' => $item->total,
            ]);

            if ($item->associatedModel) {
                $orderItem->product_id = $item->model->id;
            }

            $order->items()->save($orderItem);
        });

        if ($request->sign_up && !User::where('email', $order->email)->first()) {
            $user = new User;
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->shipping_parcel_terminal_phone = $request->shipping_parcel_terminal_phone_code . $request->shipping_parcel_terminal_phone;
            $user->save();

            $order->user_id = $user->id;
            $order->save();
        }

        $dataLayer = $this->fetchDataLayerForInvoice($order);
        \Session::flash('dataLayer', $dataLayer);
        if ($order->payment_method == 'bank_link'
            ||
            $order->payment_method == 'card_payment'
        ) {
            return $this->getMolliePayments($order);
        }
        elseif($order->payment_method == 'bank_transfer'
        ){
            return redirect()->action('CartController@getInvoice', $order);
        }
        elseif ($order->payment_method == 'e_banking'){
            return redirect()->action('CartController@getPayseraIp', $order);
        }else {
            //throw new Exception('unknown payment method');
            return redirect()->back();
        }
    }


    public function getMolliePayments(Order $order)
    {
        $locale = \App::getLocale();
        switch ($order->payment_method) {
            case 'bank_link':  // SEPA
                $payment = Mollie::api()->payments()->create([
                    'amount' => $order->sum_total + 0.25,
                    'description' => 'payment for the order: ' . $order->id,
                    'redirectUrl' => url('/') . "/{$locale}/status/{$order->id}",
                    'webhookUrl' => url('/') . '/en/webhook-verification',  //:TODO check on prod
                    'metadata' => array(
                        'order_id' => $order->id,
                    ),
                    'method' => 'banktransfer',
                ]);
                break;
            case 'card_payment':
                $payment = Mollie::api()->payments()->create([
                    'amount' => $order->sum_total + 0.50,
                    'description' => 'payment for the order: ' . $order->id,
                    'redirectUrl' => url('/') . "/{$locale}/status/{$order->id}",
                    'webhookUrl' => url('/') . '/en/webhook-verification',  //:TODO check on prod
                    'metadata' => array(
                        'order_id' => $order->id,
                    ),
                    'method' => 'creditcard',
                ]);
                break;
        }

        if (isset($payment)) {
            $order->mollie_payment_id = $payment->id;
            $order->mollie_was_showed = 0;
        }

        $order->save();

        $this->exitFromCheckout();
        return redirect()->to($payment->links->paymentUrl);
    }

    /**
     * [getPayseraPayments description]
     * @return [type] [description]
     */
    public function getPayseraPayments(Order $order, Request $request)
    {
        $locale = \App::getLocale();
        $country = $request->country;
        $eMoneyPayments = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountry($country)->getGroup('e-money')->setDefaultLanguage('ee');
        $eBankingPayments = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountry($country)->getGroup('e-banking')->setDefaultLanguage('ee');
        $order->e_money_payments = !empty($eMoneyPayments) ? $eMoneyPayments : [];
        $order->e_banking_payments = !empty($eBankingPayments) ? $eBankingPayments : [];
        $order->locale = $locale;

        return $this->getInvoice($order);
    }

    /**
     * [getPayseraIp description]
     * @return [type] [description]
     */
    public function getPayseraIp(Order $order, Request $request)
    {
        $geoip = geoip($request->ip());

        $paymentCountryList = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountries();
        $currentCountry = mb_strtolower($geoip->iso_code);
        if (array_key_exists($currentCountry, $paymentCountryList)) {
            $slectCountry = $currentCountry;
        } else {
            $slectCountry = 'ee';
        }

        return view('cart.identifyTheCountry', [
            'slectCountry' => $slectCountry,
            'order' => $order,
            'paymentCountryList' => $paymentCountryList,
        ]);
    }

    /**
     * [getInvoice description]
     * @return [type] [description]
     */
    public function getInvoice(Order $order)
    {
        $locale = \App::getLocale();

        if(!Cart::content()->where('id', 'shipping')->first()){
            if ($locale == 'en') {
                return redirect("/{$locale}/food-and-drinks");
            }
            else {
                return redirect("/{$locale}/toit-ja-jook");
            }
        }

        $this->exitFromCheckout();

        $this->sendMails($order);

        return view('cart.invoice', [
            'order' => $order
        ]);
    }

    /**
     * for Mollie/ after payment redirectUrl
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getStatusPayment(Order $order)
    {
        $locale = \App::getLocale();

        if($order->mollie_was_showed != 0){
            if ($locale == 'en') {
                return redirect("/{$locale}/food-and-drinks");
            }
            else {
                return redirect("/{$locale}/toit-ja-jook");
            }
        }

        $payment = Mollie::api()->payments()->get($order->mollie_payment_id);

        $status = $this->getMollieStatusText($payment);

        $order->mollie_payment_status = $status;
        $order->mollie_was_showed = 1;
        $order->save();

        $this->exitFromCheckout();

        Cart::destroy();

        \Auth::logout();

        $this->sendMails($order);

        return view('cart.status', [
            'order' => $order,
            'status' => $status,
        ]);
    }

    public function refundPayment(Order $order)
    {
        try {
            $payment = Mollie::api()->payments()->get($order->mollie_payment_id);

            if ($payment->canBeRefunded()) {
                /*
                 * https://www.mollie.com/en/docs/reference/refunds/create
                 */
                $refund = Mollie::api()->payments()->refund($payment, $payment->amount);

                echo "€ $payment->amount of payment { $order->mollie_payment_id } refunded.", PHP_EOL;

                //:TODO for test (will use hook)
                $payment = Mollie::api()->payments()->get($order->mollie_payment_id);
                $status = $this->getMollieStatusText($payment);
                $order->mollie_payment_status = $status;
                $order->save();

                OrderController::sendStatusUpdatingNotification($order, 4);
                Log::info('CartController | Notification about refund was sent ' . $order->id . '. Event: 4');
            } else {
                echo "Payment { $order->mollie_payment_id } can not be refunded.", PHP_EOL;
            }
        } catch (Mollie_API_Exception $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());  // :TODO
        }
        return redirect()->back();

    }

    public function webhookVerification(Request $request)
    {
        \Log::debug(['Request'=>$request]);

        if ($request->has("id") && strpos($_SERVER['HTTP_USER_AGENT'], 'Mollie.nl') !== FALSE) {

//            \Log::debug(['Server'=>$_SERVER]);
            \Log::debug(['Id_payments'=>$request->id]);

            $payment  = Mollie::api()->payments()->get($request->id);

            $status = $this->getMollieStatusText($payment);

            $order = Order::find(intval($payment->metadata->order_id));

            $order->mollie_payment_status = $status;
            $order->save();
            echo "is updated payments";
        }
        echo "fail";
    }


    /**
     * [getAdd description]
     * @param  Product $product [description]
     * @return [type]           [description]
     */
    /* remove to the Controllers\Ajax\Cart@addProduct */

    public function getAdd(Product $product)
    {
        $CartData = [
            'id' => $product->id,
            'name' => $product->name,
            'qty' => 1,
            'price' => $product->price / 1.2,
        ];

        $cartItem = Cart::add($CartData);
        if (Auth::check()) {
            $userCart = new UserCart();
            $userCart['user_id'] = Auth::id();
            $userCart['product_id'] = $cartItem->id;
            $userCart->save();
        }
        $cartItem->associate('App\Product');

        // dataLayer event for google
        $dataLayer = $this->fetchDatalayerGetAdd($CartData);
        Session::flash('dataLayer', $dataLayer);

        return back();
    }

    /**
     * [getTake description]
     * @param  Product $product [description]
     * @return [type]           [description]
     */
    public function getTake(Product $product)
    {
        $cartItem = Cart::content()->where('id', $product->id)->first();

        if($cartItem){
            Cart::update($cartItem->rowId, $cartItem->qty - 1);
            if(Auth::check()) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->first()->delete();
            }
        }

        // dataLayer event for google
        $dataLayer = $this->fetchDatalayer($product->id);

        Session::flash('dataLayer', $dataLayer);

        return back();
    }

    /**
     * [getRemove description]
     * @param  Product $product [description]
     * @return [type]           [description]
     */
    public function getRemove(Product $product)
    {
        $cartItem = Cart::content()->where('id', $product->id)->first();

        if($cartItem){
            Cart::remove($cartItem->rowId);
            if(Auth::check()) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->delete();
            }
        }
        // dataLayer event for google
        $dataLayer = $this->fetchDatalayer($product->id, $cartItem->qty);
        Session::flash('dataLayer', $dataLayer);

        return back();
    }

    /**
     * [getDestroy description]
     * @return [type] [description]
     */
    public function getDestroy()
    {
        Cart::destroy();
        if(Auth::check()) {
            UserCart::where('user_id', Auth::id())->delete();
        }
        return back();
    }

    /**
     * [fetchShippingMethods description]
     * @return [type] [description]
     */
    public function fetchShippingMethods($method = null)
    {
        $methods = [
            'eu_shipping' 	  => [
                'name'    => trans('checkout.EU Shipping'). ' 14,99 EUR',
                'price'   => 12.49, // 14.99 / 1.2 (tax)
            ],
            'courier_estonia' => [
                'name'    => trans('checkout.Courier in Estonia 6 EUR'),
                'price'   => 5.00, // 6 / 1.2 (tax)
            ],
            'parcel_terminal' => [
                'name'    => trans('checkout.Parcel terminal'),
                'price'   => 3.33, // 3.99 / 1.2 (tax)
                'items'   => ['' => ''] + 	ParcelTerminal::all()->keyBy('id')->map(function ($item){
                        return $item->provider . ' ' . $item->name;
                    })->all()
            ],
        ];

        if(Cart::count() == 1 && Cart::content()->first()->model->is_lightweight){
            $methods['eu_shipping']['name'] = 'EU Shipping 6 EUR';
            $methods['eu_shipping']['price'] = 5.00; // 6 / 1.2 (tax)

            $methods['courier_estonia']['name'] = 'Courier in Estonia 2 EUR';
            $methods['courier_estonia']['price'] = 1.66; // 2 / 1.2 (tax)
        }

        if(Cart::total() >= 50){
            //$methods['courier_estonia']['name'] = 'Courier in Estonia FREE';
            //$methods['courier_estonia']['price'] = 0.00;
            $methods['parcel_terminal']['price'] = 0.00;
        }

        return $methods;
    }

    /*
If the person chooses "Pangaülekanne" he/she still has to see this page
If she chooses "Card payment" = "Kaardimakse" or "SEPA payment" = "Pangalink" then she will be leaded to Mollie's payment page
    */
    public function fetchPaymentMethods()
    {

        $methods = [
            'bank_transfer' => [
                'name'    => trans('checkout.Bank transfer'),  //:TODO translate  old_invoice  Pangaülekanne
                'checked' => TRUE,
            ],
            'card_payment' => [
                'name'    => trans('checkout.Card payment (Fee 0.50 EUR)'), // :TODO translate  Kaardimakse to Mollie
                'checked' => FALSE,
            ],
            'e_banking' => [
                'name'    => trans('checkout.Bank link (Fee 0,25 EUR)'),
                'checked' => FALSE,
            ],
//                'bank_link' => [  //:TODO те кнопочки снизу
//                    'name'    => trans('checkout.SEPA payment (Fee 0.25 EUR)'),  //:TODO Pangalink  to Mollie
//                    'checked' => FALSE,
//                ],
        ];
        return $methods;
    }

    /**
     * [addShippingMethod description]
     */
    public function addShippingMethodToCart($method)
    {
        $methods = $this->fetchShippingMethods();

        Cart::add([
            'id'      => 'shipping',
            'name' 	  => $methods[$method]['name'],
            'qty'     => 1,
            'price'   => $methods[$method]['price'],
        ]);
    }

    public function sendMails($order)
    {
        $emailText = \App::getLocale() == 'et' ? 'email_text_et' : 'email_text';
        $locale = \App::getLocale();
        $order = Order::find($order->id);
        if($order->email){
            Mail::send('emails.invoice', ['order' => $order->load('items', 'parcelTerminal')->toArray(), 'locale' => $locale, 'text' => \Voyager::setting($emailText)], function($message) use ($order)
            {
                $message->to($order->email)->subject("Order number $order->id from greenest.ee");
            });
        }

        Mail::send('emails.invoice', ['for_admin' => true, 'order' => $order->load('items.product', 'parcelTerminal')->toArray()], function($message) use ($order)
        {
            $message->to('orders@greenest.ee')->subject("New order $order->id");
        });
    }

    //:TODO not used
    public function ebanking()
    {
        $data = request()->only('payment','order_id');
        $order = Order::find($data['order_id']);
        if ( ! empty($order))
        {
            switch ($order->payment_method)
            {
                case 'e_banking':
                    $price = $order->sum_total*100+25;
                    break;
                default :
                    $price = $order->sum_total*100;
            }
            $locale = \App::getLocale();
            if ($locale == 'et')
                $localeInPaysera = 'EST';
            elseIf ($locale == 'en')
                $localeInPaysera = 'ENG';
            $requestData = WebToPay::buildRequest(
                [
                    'projectid'     => WebToPay::PROJECT_ID,
                    'sign_password' => WebToPay::PASSWORD,
                    'orderid'       => $order->id,
                    'amount'        => $price,
                    'currency'      => 'EUR',
                    'accepturl'     => url("/") . "/{$locale}/ebanking/accept",
                    'cancelurl'     => url("/") . "/.{$locale}/ebanking/cancel",
                    'callbackurl'   => url("/") . "/{$locale}/ebanking/callback",
                    'payment'       => $data['payment'],
                    'lang'          => $localeInPaysera,
                    //'p_email'       => $order->email,
                    'test'          => 0,
                ]);
            $payUrl = WebToPay::PAY_URL;
            return view('cart.paysera', [
                'requestData' => $requestData,
                'payUrl'     => $payUrl,
            ]);
        }
    }
//
    public function ebankingAccept()
    {
        return redirect('');
    }

    public function ebankingCancel()
    {
        return redirect('');
    }

    public function ebankingCallback()
    {
        try
        {
            $response = WebToPay::checkResponse($_GET, array(
                'projectid'     => WebToPay::PROJECT_ID,
                'sign_password' => WebToPay::PASSWORD,
            ));

            if ($response['test'] !== '0') {
                throw new \Exception('Testing, real payment was not made');
            }

            if ($response['type'] !== 'macro') {
                throw new \Exception('Only macro payment callbacks are accepted');
            }

            $order = Order::find($response['orderid']);

//            if ($order && $order->sum_total == $response['amount'] / 100) {
            $order->status = 'paid';
            $order->save();
            echo "OK";
//            } else {
//                throw new \Exception('Invalid order or ammount');
//            }
        } catch (Exception $e) {
            echo get_class($e) . ': ' . $e->getMessage();
        }
    }


    protected function fetchDatalayerGetAdd($product)
    {
        $p = Product::find($product['id']);

        $categories = [];

        foreach (CategoryProduct::where('product_id', $product['id'])->get() as $categoryProduct) {
            $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
        }

        $arr = [
            'name' => $product['name'],
            'sku' => $p['sku'],
            'price' => $p['price'],
            'category' => implode($categories),
            'quantity' => 1,
        ];

        $str = 'dataLayer.push({
            "event":"addToCart",
            "ecommerce":{
                "currencyCode":"EUR",
                "add":{
                    "actionField":{"list":"checkout"},
                    "products":['
            . json_encode($arr, JSON_UNESCAPED_UNICODE)
            . ']
                    }
                }
            });';
        return $str;
    }


    /**
     * for remove product
     */
    protected function fetchDatalayer($productId, $quantity = 1)
    {
        $products = Product::where('id', $productId)->get();

        foreach ($products as $product) {
            $categories = [];
            foreach (CategoryProduct::where('product_id', $product->id)->get() as $categoryProduct) {
                $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
            }

            $productsArray[] = [
                'name' => $product->name,
                'sku' => $product->sku,
                'price' => $product->price,
                'category' => implode($categories),
                'quantity' => $quantity,
            ];
        }

        $dataLayer = [
            'event' => 'removeFromCart',
            'ecommerce' => [
                'remove' => [
                    'products' => [
                        $productsArray
                    ]
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ');';

        return $str;

    }


    protected function fetchDataLayerForInvoice($order)
    {
        $cart = Cart::content()->toArray();

        $productsArray = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => round($item['price'], 2),
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $productsArray[] = $arrObject;
            }
        }

        $dataLayer = [
            'ecommerce' => [
                'purchase' => [
                    'actionField' => [
                        'id' => '\'' . $order->id . '\'',
                        'affiliation' => 'greenest',
                        'revenue' => round($order->subtotal / 1.2, 2),
                        'tax' => round($order->tax, 2),
                        'shipping' => null,
                        'coupon' => null,
                    ],
                    'products' => $productsArray
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ')';

        return $str;
    }


    protected function fetchDataLayerForCheckout()
    {
        $cart = Cart::content()->toArray();
        $arr = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => $item['price'],
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $arr[] = $arrObject;
            }

        }

        $dataLayer = [
            'event' => 'checkout',
            'ecommerce' => [
                'checkout' => [
                    'actionField' => [
                        'products' => $arr
                    ]
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ')';

        return $str;
    }



    public function fetchDataLayerForCheckoutAjax($step = 1)
    {
        $cart = Cart::content()->toArray();
        $arr = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => $item['price'],
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $arr[] = $arrObject;
            }

        }

        $dataLayer = [
            'event' => 'checkout',
            'ecommerce' => [
                'checkout' => [
                    'actionField' => ['step' => $step, 'option' => null],
                    'products' => $arr
                ]
            ]
        ];

        $str =  json_encode($dataLayer, JSON_UNESCAPED_UNICODE);

        return $str;
    }

    /**
     * Mollie (api)
     * @param $payment
     * @return string
     */
    protected function getMollieStatusText($payment)
    {
        $status = '';

        if ($payment->isPaid()) $status = 'Paid';
        if ($payment->isCancelled()) $status = 'Cancelled';
        if ($payment->isExpired()) $status = 'Expired';
        if ($payment->isOpen()) $status = 'Open';
        if ($payment->isFailed()) $status = 'Failed';
        if ($payment->isRefunded()) $status = 'Refunded';

        return $status;
    }

    protected function exitFromCheckout()
    {
        if (Auth::check()) {
            foreach (Cart::content() as $cartItem) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->delete();
            }
        }

        Cart::destroy();

        \Auth::logout();
    }


}