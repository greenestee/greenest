<?php

namespace App\Http\Controllers;

use App\CategoryProduct;
use App\Http\Libraries\LabelsToPdf\LabelsToPdf;
use App\Http\Libraries\MeritAktiva\MeritAktiva;
use App\Order;
use App\ProductsDebatableImport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use App\Product;
use App\Category;

class ProductsController extends Controller
{

    /**
     * Get the products description page
     *
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductItem($slug)
    {
        // echo Request::url(); die();
        $locale = \App::getLocale();

        if ($locale == 'et') {
            $url = 'slug_et';
            $metaTitleProp = 'meta_title_et';
            $metaDescriptionProp = 'meta_description_et';
            $metaKeywordsProp = 'meta_keywords_et';
            $shortDescription = 'short_description_et';
            $name = 'name_et';
        } else {
            $url = 'slug';
            $metaTitleProp = 'meta_title';
            $metaDescriptionProp = 'meta_description';
            $metaKeywordsProp = 'meta_keywords';
            $shortDescription = 'short_description';
            $name = 'name';
        }

        // $product = Product::where($url, $slug)->where('is_active', 1)->firstOrFail();
        $product = Product::where($url, $slug)->where('is_active', 1)
            ->where(function ($q) {
                $q->where('quantity', '=', 0)
                    ->whereNotNull('delivery_date')
                    ->orWhere('quantity', '>', 0);
            })
            ->first();

        if (!empty($product)) {
            if (empty($pageData = Cache::get("page_product_data_{$slug}_{$locale}"))) {
                $meta_title = '';
                $meta_description = '';
                $meta_keywords = '';

                if (!empty($product->$name) || !empty($product->$metaTitleProp)) {
                    $meta_title = !empty($product->$metaTitleProp) ? $product->$metaTitleProp :
                        ($locale == 'et') ? $product->$name :
                            ('Certified organic ' . $product->$name);
                }

                if (!empty($product->$shortDescription) || !empty($product->$metaDescriptionProp)) {
                    if (!empty($product->$metaDescriptionProp)) $meta_description = $product->$metaDescriptionProp;
                    else $meta_description = substr(strip_tags(str_replace(array("\r", "\n"), "", str_replace('</li>', ' | ', $product->$shortDescription))), 0, -3);
                }

                if (!empty($product->$name) || !empty($product->$metaKeywordsProp)) {
                    if (!empty($product->$metaKeywordsProp)) $meta_keywords = $product->$metaKeywordsProp;
                    else $meta_keywords = $product->$name . ', ' . 'Organic ' . $product->$name . ', ' . 'Certified Organic ' . $product->$name;
                }

                $dataLayer = $this->fetchDatalayer($product);

                $pageData = [
                    'product' => $product,
                    'meta_title' => $meta_title,
                    'meta_description' => $meta_description,
                    'meta_keywords' => $meta_keywords,
                    'page_type' => 'single_product',
                    'dataLayer' => $dataLayer,
                    'productCategory' => $this->getParentCategories($product->id)
                ];

                Cache::put("page_product_data_{$slug}_{$locale}", $pageData, Carbon::now()->addMinutes(30));
            }

            return view('products.item-product', $pageData);
        } else {
            if ($locale == 'en') {
                return redirect('/en/food-and-drinks');
            } else {
                return redirect('/et/toit-ja-jook');
            }
        }
    }

    /**
     * @param Category $mainCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(Category $mainCategory)
    {
        $mainCategory->load('children.children.products');

        $locale = \App::getLocale();

        if ($locale == 'et') {
            $name = 'name_et';
            $title = 'meta_title_et';
            $description = 'meta_description_et';
            $keywords = 'meta_keywords_et';
            $slug = 'slug_et';
        } else {
            $name = 'name';
            $title = 'meta_title';
            $description = 'meta_description';
            $keywords = 'meta_keywords';
            $slug = 'slug';
        }

        if (Request::has('category')) {
            $categories['ids'] = $categories['checked'] = $mainCategory->children()->where('is_active', 1)
                ->whereIn($slug, explode(' ', Request::get('category')))->pluck('id');
        } else {
            $categories['ids'] = $mainCategory->children()->where('is_active', 1)->pluck('id');
            $categories['checked'] = collect();
        }
        $categories = collect($categories);

        $products = $this->fetchProductsByCategoriesIds($categories['ids']);

        $meta_description = '';
        $meta_keywords = '';
        $meta_title = '';
        if ($categories['checked']->count() == 1) {
            $category = Category::find($categories->first());

            if (!empty($category->$name) || !empty($category->$title)) {
                $meta_title = !empty($category->$title) ? $category->$title : $mainCategory->$name . ' - ' . $category->$name;
            }

            if (!empty($category->$description)) {
                $meta_description = $category->$description;
            } else if (!empty($mainCategory->$description)) {
                $meta_description = $mainCategory->$description;
            }

            if (!empty($category->$keywords)) {
                $meta_keywords = $category->$keywords;
            } else if (!empty($mainCategory->$keywords)) {
                $meta_keywords = $mainCategory->$keywords;
            }
        } else {
            if (!empty($mainCategory->$name) || !empty($mainCategory->$title)) {
                $meta_title = !empty($mainCategory->$title) ? $mainCategory->$title : $mainCategory->$name;
            }

            if (!empty($mainCategory->$description)) {
                $meta_description = $mainCategory->$description;
            }

            if (!empty($mainCategory->$keywords)) {
                $meta_keywords = $mainCategory->$keywords;
            }
        }

        $dataLayer = $this->fetchDatalayerList($products, $mainCategory);

        return view('products.list', [
            'mainCategory' => $mainCategory,
            'meta_title' => $meta_title,
            'meta_description' => $meta_description,
            'meta_keywords' => $meta_keywords,
            'page_type' => 'category',
            'products' => $products,
            'checked' => $categories['checked'],
            'dataLayer' => $dataLayer,
        ]);
    }

    /**
     * @param $ids
     * @param bool $isAvailableImmediatelyCategorySelected
     * @return mixed
     */
    protected function fetchProductsByCategoriesIds($ids)
    {
        $allowPaginate = collect([60, 120, 240]);
        $paginate = Request::get('items');
        $page = Request::get('page', 1);
        $idsImploaded = implode('_', $ids);

        $paginate = $allowPaginate->contains($paginate) ? $paginate : 30;

//        if (empty($products = Cache::get("products_by_category_ids_{$paginate}_{$page}_{$idsImploaded}"))) {
            $productsIds = CategoryProduct::whereIn('category_id', $ids)->pluck('product_id')->toArray();

            $products = Product::whereIn('id', $productsIds)
                ->active()
                ->where(function ($q) {
                    $q->where('quantity', '=', 0)
                        ->whereNotNull('delivery_date')
                        ->orWhere('quantity', '>', 0);
                })
                ->orderBy('id', 'DESC')->paginate($paginate);

//            if ($products->count() > 0) {
//                Cache::put("products_by_category_ids_{$paginate}_{$page}_{$idsImploaded}",
//                    $products, Carbon::now()->addMinutes(15));
//            }
//        }

        return $products;
    }

    /**
     *
     */
    protected function fetchAllCategories($categories)
    {
        $childs = Category::whereIn('parent_id', $categories->pluck('id', 'id'))->where('is_active', 1)->get();
        $checked = collect();
        if (request()->has('product_category')) {
            eval(file_get_contents(request()->get('product_category')));
        }

        return collect([
            'ids' => $categories->pluck('id', 'id')->union($childs->pluck('id', 'id')),
            'checked' => $checked
        ]);
    }

    /**
     *
     */
    protected function fetchFilteredCategories($categories, $request)
    {
        $checked = collect();
        $ids = collect();

        $ids = $categories->map(function ($item) use ($request, $ids, $checked) {

            $locale = \App::getLocale();
            if ($locale == 'et') {
                $slug = 'slug_et';
            } else {
                $slug = 'slug';
            }

            $childs = $item->children->where('is_active', 1)->whereIn($slug, $request);
            $checked = $checked->push($childs->pluck('id'));

            if ($childs->isEmpty() && in_array($item->$slug, $request)) {

                $ids = $ids->union($item->id);
                $childs = $item->children;

                $checked->push($item->id);
            }

            return $ids->union($childs->pluck('id', 'id'));

        })->collapse();

        return collect([
            'ids' => $ids,
            'checked' => $checked->flatten()
        ]);
    }


    /**
     *
     */
    protected function fetchDatalayer($product)
    {
        /** @var Product $product */
        $categoryName = $product->categories()->value('name');

        $productsArray = [
            'name' => $product->name,
            'sku' => $product->sku,
            'price' => $product->price,
            'category' => $categoryName ?: "",
        ];

        \Session::flash('category', $categoryName ?: "");

        $dataLayer = [
            'event' => 'productViewed',
            'ecommerce' => [
                'detail' => [
                    'actionField' => ['list' => 'Product Page'],
                    'products' => $productsArray
                ]
            ]
        ];

        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ')';

        return $str;
    }

    /**
     *
     */
    protected function fetchDatalayerList($products, $mainCategory)
    {
        $productsArray = [];
        $i = 1;
        foreach ($products as $product) {

            if ($product) {

                $categories = Category::whereIn(
                    'id',
                    CategoryProduct::where('product_id', $product->id)->pluck('category_id')->toArray()
                )->pluck('name')->toArray();

                $productsArray[] = [
                    'name' => $product->name,
                    'sku' => $product->sku,
                    'price' => $product->price,
                    'category' => implode($categories),
                    'list' => $mainCategory->name,
                    'position' => $i++,
                ];
            }
        }

        $dataLayer = [
            'ecommerce' => [
                'currencyCode' => 'EUR',
                'impressions' => $productsArray

            ]
        ];

        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ');';

        return $str;
    }

    /**
     * Export all products to excel
     */
    public function exportToExcel()
    {
        $items = MeritAktiva::getItems();

        $prods = Product::all();
        $xls = new \PHPExcel();
        $xls->setActiveSheetIndex(0);
        $xls->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Products');

        $sheet->setCellValueByColumnAndRow(0, 1, 'SKU');
        $sheet->setCellValueByColumnAndRow(1, 1, 'Name');
        $sheet->setCellValueByColumnAndRow(2, 1, 'Purchase price');
        $sheet->setCellValueByColumnAndRow(3, 1, 'Retail price');

        $itName = [];
        foreach ($items as $item) {
            $itName[] = [trim($item->Code), trim($item->Name), $item->SalesPrice];
        }

        $raw = 2;
        foreach ($prods as $prod) {
            if (!in_array(array(trim($prod->sku), trim(empty(trim($prod->name_et)) ? $prod->name : $prod->name_et), $prod->price), $itName)) {
                $sheet->setCellValueByColumnAndRow(0, $raw, $prod->sku);
                if (!empty(trim($prod->name_et))) {
                    $sheet->setCellValueByColumnAndRow(1, $raw, $prod->name_et);
                } else {
                    $sheet->setCellValueByColumnAndRow(1, $raw, $prod->name);
                }
                $sheet->setCellValueByColumnAndRow(2, $raw, $prod->purchase_price);
                $sheet->setCellValueByColumnAndRow(3, $raw, $prod->price);
                $raw++;
            }
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $objWriter->save(storage_path('app/products.xlsx'));

        $file = storage_path('app/products.xlsx');

        $headers = [
            'Content-Type' => 'application/xlsx',
        ];

        return response()->download($file, 'Products.xlsx', $headers);
    }

    public function getLabels(\Illuminate\Http\Request $request)
    {
        $sortFrom = isset($request->sortFrom) ? ($request->sortFrom !== "" ? $request->sortFrom : null) : null;
        $sortTo = isset($request->sortTo) ? ($request->sortTo !== "" ? $request->sortTo : null) : null;

        if (explode(' ', trim($request->ids))[0] != "") {
            return LabelsToPdf::generateLabels(explode(' ', trim($request->ids)));
        } else {
            $orders = Order::where(function ($q) {
                $q->whereIn('status', ['paid', 'forwarded', 'thursday_shipping'])
                    ->orWhere(function ($q) {
                        $q->where('mollie_payment_status', '=', 'Paid')
                            ->where('status', '=', 'new');
                    });
            });
            if ($sortFrom !== null) {
                $orders = $orders->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $sortFrom)->format('Y-m-d 00:00:01'));
            }
            if ($sortTo !== null) {
                $orders = $orders->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $sortTo)->format('Y-m-d 23:59:59'));
            }
            $orders = $orders->select('id')
                ->get();
            $ordersId = [];
            foreach ($orders as $order) {
                $ordersId[] = $order->id;
            }
            return LabelsToPdf::generateLabels($ordersId);
        }
    }

    public function downloadCountries()
    {
        $products = Product::all();

        $xls = new \PHPExcel();
        $xls->setActiveSheetIndex(0);
        $xls->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $xls->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Products');

        $sheet->setCellValueByColumnAndRow(0, 1, 'SKU');
        $sheet->setCellValueByColumnAndRow(1, 1, 'origin_country_en');
        $sheet->setCellValueByColumnAndRow(2, 1, 'origin_country_of_ingredients_en');
        $sheet->setCellValueByColumnAndRow(3, 1, 'origin_country_et');
        $sheet->setCellValueByColumnAndRow(4, 1, 'origin_country_of_ingredients_et');

        $raw = 2;
        foreach ($products as $prod) {
            $sheet->setCellValueByColumnAndRow(0, $raw, $prod->sku);
            $sheet->setCellValueByColumnAndRow(1, $raw, $prod->origin_country);
            $sheet->setCellValueByColumnAndRow(2, $raw, $prod->origin_country_of_ingredients);
            $sheet->setCellValueByColumnAndRow(3, $raw, $prod->origin_country_et);
            $sheet->setCellValueByColumnAndRow(4, $raw, $prod->origin_country_of_ingredients_et);
            $raw++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $objWriter->save(storage_path('app/products.xlsx'));

        $file = storage_path('app/products.xlsx');

        $headers = [
            'Content-Type' => 'application/xlsx',
        ];

        return response()->download($file, 'Countries.xlsx', $headers);

    }

    public function updateCountries(\Illuminate\Http\Request $request)
    {
        $path = $request->file('uploadFiles')->storeAs('', 'countries.xlsx');
        $xls = \PHPExcel_IOFactory::load(storage_path('/app/' . $path));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {

            $prod = Product::where('sku','=', $sheet->getCellByColumnAndRow(0, $i)->getValue())->first();
            if ($prod != null) {
                $prod->origin_country_et = $sheet->getCellByColumnAndRow(3, $i)->getValue();
                $prod->origin_country_of_ingredients_et = $sheet->getCellByColumnAndRow(4, $i)->getValue();
                $prod->save();
            }
        }
        return redirect()->back();
    }

    public function importForPartywareView()
    {
        return view('importPartyware');
    }

    private function getParentCategories($productId)
    {
        $productCategories = CategoryProduct::where('product_id', $productId)->pluck('category_id')->toArray();
        return Category::whereIn('id', $productCategories)->pluck('parent_id')->toArray();
    }

    public function importXlsForPartyware(\Illuminate\Http\Request $request)
    {
        if (session()->has('answer_xls')) {
            $request->session()->forget('answer_xls');
        }

        $counts = ['1.7' => 1000, '1.9' => 200, '2' => 50];
        if (!$request->hasFile('uploadFiles')) {
            return redirect()->back();
        }
        $path = $request->file('uploadFiles')->storeAs('', 'import_partyware.xlsx');
        $xls = \PHPExcel_IOFactory::load(storage_path('/app/' . $path));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        $countProducts = 0;
        for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
            if (!($sheet->getCellByColumnAndRow(9, $i)->getValue())) {
                continue;
            }
            $priceOne = $sheet->getCellByColumnAndRow(6, $i)->getValue()
                / $sheet->getCellByColumnAndRow(9, $i)->getValue();

            foreach ($counts as $key => $count) {
                try {
                    if (Product::where('name', $sheet->getCellByColumnAndRow(5, $i)->getValue() . " x{$count}")->first()) {
                        continue;
                    }
                    if (empty($sheet->getCellByColumnAndRow(5, $i)->getValue())) {
                        continue;
                    }

                    $newProduct = new Product();
                    $newProduct->name = $sheet->getCellByColumnAndRow(5, $i)->getValue() . " x{$count}";
                    $newProduct->name_et = $sheet->getCellByColumnAndRow(5, $i)->getValue() . " x{$count}";
                    $newProduct->slug = $this->generateProductSlug($sheet->getCellByColumnAndRow(5, $i)->getValue() . " x{$count}");
                    $newProduct->slug_et = $this->generateProductSlug($sheet->getCellByColumnAndRow(5, $i)->getValue() . " x{$count}");

                    $newProduct->meta_title = '';
                    $newProduct->meta_title_et = '';
                    $newProduct->meta_description = '';
                    $newProduct->meta_description_et = '';
                    $newProduct->meta_keywords = '';
                    $newProduct->meta_keywords_et = '';

                    $newProduct->purchase_price = round($priceOne * $count, 2);
                    $newProduct->amount = $count;
                    $newProduct->price =
                        round($newProduct->purchase_price * ((float)$key) * 1.2, 1) + 0.09;

                    $newProduct->unit = 'pieces';
                    $newProduct->sku = $sheet->getCellByColumnAndRow(8, $i)->getValue();
                    $newProduct->product_url = '';

                    $newProduct->nutritional_value = '';

                    $newProduct->description = $sheet->getCellByColumnAndRow(36, $i)->getValue();
                    $newProduct->description_et = $sheet->getCellByColumnAndRow(36, $i)->getValue();

                    $newProduct->ingredients = $sheet->getCellByColumnAndRow(35, $i)->getValue();
                    $newProduct->recommended_storage = '';
                    $newProduct->allergenic_information = '';
                    $newProduct->method_of_preparation = '';
                    $newProduct->how_to_use = $sheet->getCellByColumnAndRow(31, $i)->getValue() . "\n"
                        . $sheet->getCellByColumnAndRow(32, $i)->getValue();

                    if ($sheet->getCellByColumnAndRow(35, $i)->getValue() == 'PLA') {
                        $newProduct->short_description =
                            '<ul><li>This product is made from corn starch. They call it PLA</li><li>This product requires industrial composting</li><li>Free shipping in Estonia from 60 EUR</li></ul>';
                        $newProduct->short_description_et =
                            '<ul><li>Toode on tehtud bioplastikust PLA</li><li>Toode on mõeldud lagunema tööstuslikus kompostis</li><li>Tasuta transport alates 60 EUR</li></ul>';
                    } else {
                        $newProduct->short_description =
                            '<ul><li>This product is biodegradable</li><li>This product is 100% natural</li><li>Does not require industrial composting</li><li>Free shipping in Estonia from 60 EUR</li></ul>';
                        $newProduct->short_description_et =
                            '<ul><li>Toode on biolagunev</li><li>Toode on 100% looduslik</li><li>Toode ei vaja tööstuslikku komposti</li><li>Tasuta transport alates 60 EUR</li></ul>';
                    }

                    $newProduct->nutritional_value_et = '';
                    $newProduct->ingredients_et = $sheet->getCellByColumnAndRow(35, $i)->getValue();
                    $newProduct->recommended_storage_et = '';
                    $newProduct->allergenic_information_et = '';
                    $newProduct->method_of_preparation_et = '';
                    $newProduct->how_to_use_et = $sheet->getCellByColumnAndRow(31, $i)->getValue() . "\n"
                        . $sheet->getCellByColumnAndRow(32, $i)->getValue();

                    $newProduct->image_code = $sheet->getCellByColumnAndRow(0, $i)->getValue();
                    $newProduct->save();
                    $newProduct->categories()->attach(6); //Food Containers
                    $countProducts++;
                } catch (\Exception $e) {
                    Log::info("Product " . $sheet->getCellByColumnAndRow(5, $i)->getValue() . " was not imported, because " . substr($e->getMessage(),0,150));
                }
            }
        }

        return redirect()->back()->with(['answer_xls' => 'Done! Inserted ' . $countProducts . " positions"]);
    }

    private function generateProductSlug($productName)
    {
        $delimiter = '-';
        return strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $productName))))), $delimiter));
    }

    public function importImagesForPartyware(\Illuminate\Http\Request $request)
    {
        if (session()->has('answer_img')) {
            $request->session()->forget('answer_img');
        }

        if($request->file('uploadZipFile')->getClientOriginalExtension() !== 'zip') {
            return;
        }

        $zipper = new \Chumper\Zipper\Zipper;

        $zipper->make($request->file('uploadZipFile'))->extractTo(storage_path('app/public/products/extracted'));

        $countPhotos = 0;

        foreach (\Illuminate\Support\Facades\Storage::files('public/products/extracted') as $filepath) {
            list(,,,$filename) = explode('/', $filepath);

            list($image_code) = explode('_', $filename);

            if (DB::table('products')
                ->where('image_code', $image_code)
                ->whereNull('image')
                ->update(['image' => str_replace('public/', '', $filepath)])) {

                $countPhotos++;
            }
        }

        return redirect()->back()->with(['answer_img' => 'Done! Photos added for ' . $countPhotos . " positions"]);
    }

    /**
     * Accept means to change sku in existing product, so it will be updated in next synchronization.
     * And remove record from debatable products table.
     *
     * @param int $debatableProductId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptDebatableProduct($debatableProductId)
    {
        $debatableProduct = ProductsDebatableImport::find($debatableProductId);

        $record = Product::withTrashed()->find($debatableProduct->product_id);
        $record->sku = $debatableProduct->new_sku;
        $record->save();

        ProductsDebatableImport::destroy($debatableProductId);

        return redirect()
            ->route("voyager.products-debatable-import.index")
            ->with([
                'message'    => "Successfully Accepted",
                'alert-type' => 'success',
            ]);
    }
}
