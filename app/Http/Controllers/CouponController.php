<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function viewList()
    {
        $coupons = Coupon::orderBy('remaining_number_of_uses', 'desc')->orderBy('created_at', 'desc')->get();

        return view('coupons.index', compact('coupons'));
    }

    public function store(Request $request)
    {
        for ($i = 0; $i < $request->count; $i++) {
            Coupon::generate(
                $request->sale_coefficient,
                $request->has('unlimited') ? 1 : $request->get('remaining_number_of_uses', 1),
                (int) $request->has('unlimited')
            );
        }
        return redirect()->back()->with([
            'message'    => "Successfully created " . ($request->count == 1 ? 'coupon' : "{$request->count} coupons"),
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request)
    {
        $coupon = Coupon::find($request->id);
        if (!empty($coupon)) {
            $coupon->update([
                'sale_coefficient' => $request->sale_coefficient,
                'remaining_number_of_uses' => $request->has('unlimited') ? 1 : $request->get('remaining_number_of_uses', 1),
                'unlimited' => (int) $request->has('unlimited')
            ]);
        }

        return redirect()->back()->with([
            'message'    => "Successfully updated",
            'alert-type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $coupon = Coupon::find($request->id);
        if (!empty($coupon)) {
            $coupon->delete();
        }

        return redirect()->back()->with([
            'message'    => "Successfully deleted",
            'alert-type' => 'success',
        ]);
    }

    public function checkCoupon(Request $request)
    {
        if (Coupon::existsByCode($request->get('code'))) {
            $coupon = Coupon::findByCode($request->get('code'));
            return response()->json(['status' => 'success', 'sale_coefficient' => $coupon->sale_coefficient]);
        }

        return response()->json(['status' => 'error']);
    }
}
