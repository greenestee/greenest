<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Product;
use App\UserCart;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Cart extends Controller
{
    public function addProduct(Request $request)
    {
        if(!empty($request->product_id))
		{
            $locale = $request->get('lang') ? $request->get('lang') : 'en';

            $product = Product::find($request->product_id);

            $cartItem = \Cart::add([
                'id'      => $product->id,
                'name' 	  =>  $locale == 'en' ? $product->name : $product->name_et,
                'qty'     => 1,
                'price'   => $product->price  / 1.2,
            ]);

            if(Auth::check()) {
                $userCart = new UserCart();
                $userCart['user_id'] = Auth::id();
                $userCart['product_id'] = $cartItem->id;
                $userCart->save();

                \Cart::destroy();
                foreach (UserCart::where('user_id', Auth::id())->get() as $item) {
                    $product = Product::find($item['product_id']);
                    if (empty($product)) {
                        continue;
                    }
                    $cartItem = \Cart::add([
                        'id' => $product['id'],
                        'name' => $product['name'],
                        'qty' => 1,
                        'price' => $product['price'] / 1.2,
                    ]);
                }
                $cartItem->associate('App\Product');
            }

			$cartItem->associate('App\Product');

			return getCartInfo($locale);
		}
    }

    public function displayProductsCount()
    {
    	return getProductsCount();
    } 
}
