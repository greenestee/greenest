<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryProduct;
use App\Coupon;
use App\Http\Libraries\LHV\LHVHirePurchase;
use App\Http\Libraries\Montonio\MontonioPayment;
use App\Http\Requests\CheckoutRequest;
use App\Product;
use App\Order;
use App\OrderItem;
use App\ParcelTerminal;
use App\UserCart;
use Carbon\Carbon;
use Cart;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use mysql_xdevapi\Collection;
use URL;
use Mail;
use App\User;
use App\Http\Libraries\WebToPay;
use Session;
use Mollie\Laravel\Facades\Mollie;

class CartController extends Controller
{
    /**
     * [getCheckout description]
     * @return [type] [description]
     */
    public function getCheckout(Request $request)
    {
        if (!Cart::count()) {
            return redirect('/food-and-drinks');
        }

        $back_url = URL('/food-and-drinks');

        if(URL::previous() != URL::current()){
            $request->session()->put('back_url', URL::previous());
        }

        if($request->session()->has('back_url')) {
            $back_url = $request->session()->get('back_url');
        }

		$shippingMethods = $this->fetchShippingMethods();
        $paymentMethods = $this->fetchPaymentMethods();
        $cartContentCase = $this->getCartContentCase();
        $productsAvailability = $this->getAvailableImmediatelyProducts();
        $productsQuantity = $this->getProductsQuantity();
        $products = $this->getProductsInCart();

        return view('cart.checkout', [
				'shipping_methods'  => $shippingMethods,
				'payment_methods'  => $paymentMethods,
				'availableProducts'  => $productsAvailability,
				'productsQuantity'  => $productsQuantity,
                'back_url' => $back_url,
                'products' => $products,
                'ip' => $request->ip(),
                'cart_content' => $cartContentCase,
                'allowParcelTerminal' => $this->doesAllowParcelTerminal($shippingMethods)
			]);
	}

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getProductsInCart()
    {
        $ids = Cart::content()->pluck('id')->toArray();

        $products = Product::findMany($ids);

        return $products;
	}

    /**
     * @return mixed
     */
    public function getProductsQuantity()
    {
        $cartItemsIds = Cart::content()->pluck('id')->toArray();

        $productsAvailability = Product::whereIn('id', $cartItemsIds)
            ->get()
            ->pluck('quantity', 'id')
            ->toArray();

        return $productsAvailability;
	}

    /**
     * @return mixed
     */
    private function getAvailableImmediatelyProducts()
    {
        $cartItemsIds = Cart::content()->pluck('id')->toArray();

        $needCategories = Category::where('slug', 'like', '%available-immediately%')
            ->where('is_active', 1)->pluck('id')->toArray();
        $needProducts = CategoryProduct::whereIn('category_id', $needCategories)->pluck('product_id')->toArray();

        $productsAvailability = Product::whereIn('id', $needProducts)
            ->whereIn('id', $cartItemsIds)
            ->get()
            ->pluck('id', 'id')
            ->toArray();

        return $productsAvailability;
	}

    /**
     * Cases:
     * 1 - Only "on order" products in cart.
     * 2 - Only "in stock" products in cart.
     * 3 - Mixed products in cart.
     *
     * @return string $cartCase
     */
    private function getCartContentCase()
    {
        $cartItemsIds = Cart::content()->pluck('id')->toArray();

        $needCategories = Category::where('slug', 'like', '%available-immediately%')
            ->where('is_active', 1)->pluck('id')->toArray();
        $needProducts = CategoryProduct::whereIn('category_id', $needCategories)->pluck('product_id')->toArray();

        $productsFromCartInStockAmount = Product::whereIn('id', $needProducts)
            ->whereIn('id', $cartItemsIds)
            ->get()
            ->count();

        $overallInCartAmount = Cart::content()->count();

        $result = $overallInCartAmount - $productsFromCartInStockAmount;

        if ($result === 0) {
            $cartCase = 'all_in_stock';
        } elseif ($result === $overallInCartAmount) {
            $cartCase = 'all_on_order';
        } else {
            $cartCase = 'mixed';
        }

        return $cartCase;
    }

    /**
     * @return mixed
     */
    private function getProductsAvailability()
    {
        $cartItemsIds = Cart::content()->pluck('id')->toArray();

        $productsAvailability = Product::whereIn('id', $cartItemsIds)->select('id', 'quantity')->get()->keyBy('id');

        $productsAvailability->transform( function($item) {
            $item->in_stock = ($item->quantity > 0) ? true : false;
            return $item;
        });

        return $productsAvailability->toArray();
    }
    /**
     * Returns true, if shipping method is single.
     * This means that product is heavyweight and has special shipping method.
     *
     * @param $shippingMethods
     * @return bool
     */
    private function doesAllowParcelTerminal($shippingMethods)
    {
        return array_key_exists('parcel_terminal', $shippingMethods);
	}

    /**
     * @param string $additionalShipping
     */
    public function addAdditionalShippingMethodToCart($additionalShipping)
    {
        if (!isset($additionalShipping)) {
            return;
        }

        $additionalMethod = [];

        if ($additionalShipping === 'ship_available') {
            $additionalMethod['name'] = 'Ship available products ASAP! (Extra fee 3 EUR)';
            $additionalMethod['price'] = 2.50;
        } elseif ($additionalShipping === 'ship_all_products') {
            $additionalMethod['name'] = 'Ship all products together';
            $additionalMethod['price'] = 0;
        }

        Cart::add([
            'id'      => 'additionalShipping',
            'name' 	  => $additionalMethod['name'],
            'qty'     => 1,
            'price'   => $additionalMethod['price'],
        ]);
	}

	/**
	 *
	 */
	public function postCheckout(CheckoutRequest $request)
    {
        $this->addShippingMethodToCart($request->shipping_method, $request->shipping_parcel_terminal_phone_code);
        $this->addAdditionalShippingMethodToCart($request->get('shipping_stock'));

        $coupon = Coupon::findByCode($request->get('coupon'));
        $discountCoefficient = $coupon->sale_coefficient ?? 0;

        $order = new Order;
        $order->fill($request->all());
        $order->status = 'new';
        $order->discount_coefficient = $discountCoefficient;
        $order->coupon = $coupon->code ?? null;
        $order->subtotal = Cart::subtotal();
        $order->tax = Cart::tax();
        $order->sum_total = floor(((float) str_replace(',', '.', Cart::total())) * ((float) (1 - $discountCoefficient)) * 100) / 100;
        $order->shipping_parcel_terminal_phone = $request->shipping_parcel_terminal_phone_code . $request->shipping_parcel_terminal_phone;
        $order->save();

        Cart::content()->each(function ($item) use ($order) {
            $orderItem = new OrderItem([
                'name' => $item->name,
                'quantity' => $item->qty,
                'price' => $item->subtotal,
                'sum_total' => $item->total,
            ]);

            if ($item->associatedModel) {
                $orderItem->product_id = $item->model->id;
            }

            $order->items()->save($orderItem);
        });

        if ($request->sign_up && !User::where('email', $order->email)->first()) {
            $user = new User;
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->shipping_parcel_terminal_phone = $request->shipping_parcel_terminal_phone_code . $request->shipping_parcel_terminal_phone;
            $user->save();

            $order->user_id = $user->id;
            $order->save();
        }

        $dataLayer = $this->fetchDataLayerForInvoice($order);

        //put dataLayer to cash to avoid too early variable expiring
        $expiresAt = Carbon::now()->addMinutes(20);
        Cache::store('file')->put("{$order->id}dataLayer", $dataLayer, $expiresAt);

        $montonio = new MontonioPayment();

        if ($order->payment_method == 'bank_link' || $order->payment_method == 'card_payment') {

            return $this->getMolliePayments($order);

        } elseif($order->payment_method == 'bank_transfer'){

            return redirect()->action('CartController@getInvoice', $order);

        } elseif ($order->payment_method == 'e_banking'){

            return redirect()->action('CartController@getPayseraIp', $order);

        } elseif (in_array($order->payment_method, ['hire_purchase_lhv','hire_purchase_seb','hire_purchase_swedbank'])) {
            $cartContent = Cart::content();
            $bank = strtoupper(str_replace('hire_purchase_', '', $order->payment_method));
            $this->exitFromCheckout();
            $this->sendMails($order);
            $LHV = new LHVHirePurchase($cartContent, $order, $bank);
            $data = $LHV->hirePurchase();
            Log::info(json_encode($data) . ' url: ' . $LHV->getRequestUrl());
            return view('cart.lhvForm', ['data' => $data, 'url' => $LHV->getRequestUrl()]);

        } elseif (preg_match('#^montonio_([\w_]+)$#', $order->payment_method)
                && ($montonio->getBankBic($order->payment_method) !== false)) {
            $url = $montonio->generatePaymentUrl($order->sum_total, $order->id, $montonio->getBankBic($order->payment_method));

            Log::info("Montonio payment ({$order->payment_method}). Order ID: {$order->id}. Url: {$url}");

            $this->exitFromCheckout();
            $this->sendMails($order);

            return redirect($url);
        }

        //throw new Exception('unknown payment method');
        return redirect()->back();
    }


    public function getMolliePayments(Order $order)
    {
        $locale = \App::getLocale();
        switch ($order->payment_method) {
            case 'bank_link':  // SEPA
                $payment = Mollie::api()->payments()->create([
                    'amount' => $order->sum_total + 0.25,
                    'description' => 'payment for the order: ' . $order->id,
                    'redirectUrl' => url('/') . "/{$locale}/status/{$order->id}",
                    'webhookUrl' => url('/') . '/en/webhook-verification',  //:TODO check on prod
                    'metadata' => array(
                        'order_id' => $order->id,
                    ),
                    'method' => 'banktransfer',
                ]);
                break;
            case 'card_payment':
                $payment = Mollie::api()->payments()->create([
                    'amount' => $order->sum_total + 0.50,
                    'description' => 'payment for the order: ' . $order->id,
                    'redirectUrl' => url('/') . "/{$locale}/status/{$order->id}",
                    'webhookUrl' => url('/') . '/en/webhook-verification',  //:TODO check on prod
                    'metadata' => array(
                        'order_id' => $order->id,
                    ),
                    'method' => 'creditcard',
                ]);
                break;
        }

        if (isset($payment)) {
            $order->mollie_payment_id = $payment->id;
            $order->mollie_was_showed = 0;
        }

        $order->save();

        $this->exitFromCheckout();
        return redirect()->to($payment->links->paymentUrl);
    }

    /**
     * [getPayseraPayments description]
     * @return [type] [description]
     */
    public function getPayseraPayments(Order $order, Request $request)
    {
        $locale = \App::getLocale();
        $country = $request->country;
        $eMoneyPayments = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountry($country)->getGroup('e-money')->setDefaultLanguage('ee');
        $eBankingPayments = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountry($country)->getGroup('e-banking')->setDefaultLanguage('ee');
        $order->e_money_payments = !empty($eMoneyPayments) ? $eMoneyPayments : [];
        $order->e_banking_payments = !empty($eBankingPayments) ? $eBankingPayments : [];
        $order->locale = $locale;

        return $this->getInvoice($order);
    }

    /**
     * [getPayseraIp description]
     * @return [type] [description]
     */
    public function getPayseraIp(Order $order, Request $request)
    {
        $geoip = geoip($request->ip());

        $paymentCountryList = WebToPay::getPaymentMethodList(WebToPay::PROJECT_ID)->getCountries();
        $currentCountry = mb_strtolower($geoip->iso_code);
        if (array_key_exists($currentCountry, $paymentCountryList)) {
            $slectCountry = $currentCountry;
        } else {
            $slectCountry = 'ee';
        }

        return view('cart.identifyTheCountry', [
            'slectCountry' => $slectCountry,
            'order' => $order,
            'paymentCountryList' => $paymentCountryList,
        ]);
    }

	/**
	 * [getInvoice description]
	 * @return [type] [description]
	 */
	public function getInvoice(Order $order)
	{
        $locale = \App::getLocale();

        if(!Cart::content()->where('id', 'shipping')->first()){
            if ($locale == 'en') {
                return redirect("/{$locale}/food-and-drinks");
            }
            else {
                return redirect("/{$locale}/toit-ja-jook");
            }
        }

        $this->exitFromCheckout();

        $this->sendMails($order);

        return view('cart.invoice', [
            'order' => $order
        ]);
	}

    /**
     * for Mollie/ after payment redirectUrl
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getStatusPayment(Order $order)
    {
        $locale = \App::getLocale();

        if($order->mollie_was_showed != 0){
            if ($locale == 'en') {
                return redirect("/{$locale}/food-and-drinks");
            }
            else {
                return redirect("/{$locale}/toit-ja-jook");
            }
        }

        $payment = Mollie::api()->payments()->get($order->mollie_payment_id);

        $status = $this->getMollieStatusText($payment);

        $order->mollie_payment_status = $status;
        $order->mollie_was_showed = 1;
        $order->save();

        $this->exitFromCheckout();

        Cart::destroy();

        \Auth::logout();

        $this->sendMails($order);

        return view('cart.status', [
            'order' => $order,
            'status' => $status,
            ]);
    }

    public function refundPayment(Order $order)
    {
        try {
            $payment = Mollie::api()->payments()->get($order->mollie_payment_id);

            if ($payment->canBeRefunded()) {
                /*
                 * https://www.mollie.com/en/docs/reference/refunds/create
                 */
                $refund = Mollie::api()->payments()->refund($payment, $payment->amount);

                echo "€ $payment->amount of payment { $order->mollie_payment_id } refunded.", PHP_EOL;

                //:TODO for test (will use hook)
                $payment = Mollie::api()->payments()->get($order->mollie_payment_id);
                $status = $this->getMollieStatusText($payment);
                $order->mollie_payment_status = $status;
                $order->save();

                OrderController::sendStatusUpdatingNotification($order, 4);
                Log::info('CartController | Notification about refund was sent ' . $order->id . '. Event: 4');
            } else {
                echo "Payment { $order->mollie_payment_id } can not be refunded.", PHP_EOL;
            }
        } catch (Mollie_API_Exception $e) {
            echo "API call failed: " . htmlspecialchars($e->getMessage());  // :TODO
        }
        return redirect()->back();

    }

    public function webhookVerification(Request $request)
    {
        \Log::debug(['Request'=>$request]);

        if ($request->has("id") && strpos($_SERVER['HTTP_USER_AGENT'], 'Mollie') !== FALSE) {

//            \Log::debug(['Server'=>$_SERVER]);
            \Log::debug(['Id_payments'=>$request->id]);

            $payment  = Mollie::api()->payments()->get($request->id);

            $status = $this->getMollieStatusText($payment);

            $order = Order::find(intval($payment->metadata->order_id));

            $order->mollie_payment_status = $status;
            $order->save();
            echo "is updated payments";
        }
        echo "fail";
    }


    /**
     * [getAdd description]
     * @param  Product $product [description]
     * @return [type]           [description]
     */
    /* remove to the Controllers\Ajax\Cart@addProduct */

    public function getAdd(Product $product)
    {
        $CartData = [
            'id' => $product->id,
            'name' => $product->name,
            'qty' => 1,
            'price' => $product->price / 1.2,
        ];

        $cartItem = Cart::add($CartData);
        if (Auth::check()) {
            $userCart = new UserCart();
            $userCart['user_id'] = Auth::id();
            $userCart['product_id'] = $cartItem->id;
            $userCart->save();
        }
        $cartItem->associate('App\Product');

        // dataLayer event for google
        $dataLayer = $this->fetchDatalayerGetAdd($CartData);

        return response()->json([
            'dataLayer' => $dataLayer,
            'subtotal' => Cart::subtotal(),
            'tax' => Cart::tax()

        ]);
    }

    /**
     * [getAdd description]
     * @param  Product $product [description]
     * @return [type]           [description]
     */
    public function getUpdate(Product $product, Request $request)
    {
        $quantity = $request->get('quantity');

        $CartData = [
            'id' => $product->id,
            'name' => $product->name,
            'qty' => $quantity,
            'price' => $product->price / 1.2,
        ];

        $rowId = 0;
        if ($cartRow = Cart::content()->where('id', $product->id)->first()) {
            $rowId = $cartRow->rowId;
            Cart::update($rowId, 0);
        }

        $cartItem = Cart::add($CartData);
        if (Auth::check()) {
            UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->delete();
            for ($i = 0; $i < $quantity; $i++) {
                $userCart = new UserCart();
                $userCart['user_id'] = Auth::id();
                $userCart['product_id'] = $cartItem->id;
                $userCart->save();
            }
        }
        $cartItem->associate('App\Product');
        // dataLayer event for google
        $dataLayer = $this->fetchDatalayerGetAddObject($CartData);

        //check if there are enough products in stock
        $enoughInStock = ($product->quantity >= $quantity) ? true : false;

        $inStock = ($product->quantity >= 0) ? $product->quantity : 0;

        $isAvailableImmediately = $product->categories()
            ->where('slug', 'like', '%available-immediately%')
            ->where('is_active', 1)
            ->exists();

        return response()->json([
            'dataLayer' => json_decode($dataLayer, 1),
            'subtotal' => Cart::subtotal(),
            'totalSum' => Cart::total(),
            'tax' => Cart::tax(),
            'cart_weight' => $this->getWeight(),
            'enough_in_stock' => $enoughInStock,
            'amount_in_cart' => Cart::content()->where('id', $product->id)->first()->qty,
            'in_stock' => $inStock,
            'is_available_immediately' => $isAvailableImmediately,
            'cart_content' => $this->getCartContentCase(),
            'item' => [
                'price' => formatNumber($cartItem->total),
                'netPrice' => formatNumber($cartItem->subtotal),
            ]
        ]);
    }

    private function getWeight()
    {
        $weight = 0;
        foreach (Cart::content() as $item) {
            if (!empty($item->model->weight)) {
                $weight += ($item->model->weight > 25
                    ? $item->model->weight / 1000 * $item->qty
                    : $item->model->weight * $item->qty);
            }
        }
        return $weight;
    }

    private function isHaveRefrigeratedOrFrozenProducts()
    {
        foreach (Cart::content() as $item) {
            if (!empty($item->model->id) && ($item->model->isRefrigerated() || $item->model->isFrozen())) {
                return true;
            }
        }
        return false;
    }

	/**
	 * [getTake description]
	 * @param  Product $product [description]
	 * @return [type]           [description]
	 */
	public function getTake(Product $product)
	{
		$cartItem = Cart::content()->where('id', $product->id)->first();

        if($cartItem){
			Cart::update($cartItem->rowId, $cartItem->qty - 1);
            if(Auth::check()) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->first()->delete();
            }
		}

        // dataLayer event for google
        $dataLayer = $this->fetchDatalayer($product->id);

        return response()->json([
            'dataLayer' => $dataLayer,
            'subtotal' => Cart::subtotal(),
            'tax' => Cart::tax()

        ]);
	}

	/**
	 * [getRemove description]
	 * @param  Product $product [description]
	 * @return [type]           [description]
	 */
	public function getRemove(Product $product)
	{
		$cartItem = Cart::content()->where('id', $product->id)->first();

		if($cartItem){
			Cart::remove($cartItem->rowId);
            if(Auth::check()) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->delete();
            }
        }
        // dataLayer event for google
        $dataLayer = $this->fetchDatalayer($product->id, $cartItem->qty);
        Session::flash('dataLayer', $dataLayer);

        return back();
    }

	/**
	 * [getDestroy description]
	 * @return [type] [description]
	 */
	public function getDestroy()
	{
		Cart::destroy();
        if(Auth::check()) {
            UserCart::where('user_id', Auth::id())->delete();
        }
		return back();
	}

	/**
	 * [fetchShippingMethods description]
	 * @return [type] [description]
	 */
	public function fetchShippingMethods($method = null)
	{
        if ($this->isHaveRefrigeratedOrFrozenProducts()) {
            $methods =  [
                'refrigerated_delivery' 	  => [
                    'name'    => trans('checkout.refrigerated_delivery'),
                    'price'   => 6.67, // 14.99 / 1.2 (tax)
                    'active'  => true,
                    'view'  => true,
                ],
                'heavy_shipping' => [
                    'name'    => trans('checkout.heavy_shipping'),
                    'price'   => 10,
                    'active'  => false,
                    'view'  => false,
                ],
            ];

            if ($this->getWeight() >= 30) {
                $methods['heavy_shipping']['active'] = true;
                $methods['heavy_shipping']['view'] = true;

                $methods['refrigerated_delivery']['active'] = false;
                $methods['refrigerated_delivery']['view'] = false;
            }

            return $methods;
        }

        $methods = [
			'eu_shipping' 	  => [
						'name'    => trans('checkout.EU Shipping'). ' 14,99 EUR',
						'price'   => 12.49, // 14.99 / 1.2 (tax)
                        'active'  => false,
                        'view'  => true,
					],
            'heavy_shipping' => [
                        'name'    => trans('checkout.heavy_shipping'),
                        'price'   => 10,
                        'active'  => false,
                        'view'  => false,
                    ],
			'courier_estonia' => [
						'name'    => trans('checkout.Courier in Estonia 6 EUR'),
						'price'   => 5.00, // 6 / 1.2 (tax)
                        'active'  => false,
                        'view'  => true,
					],
//            'customer_pickup' => [
//                        'name'    => trans('checkout.customer_pickup'),
//                        'price'   => 0,
//                        'active'  => false,
//                        'view'  => true,
//                    ],
			'parcel_terminal' => [
						'name'    => trans('checkout.Parcel terminal'). ' 4 EUR',
						'price'   => 3.33, // 3.99 / 1.2 (tax)
                        'active'  => true,
                        'view'    => true,
						'items'   => ['' => ''] + 	ParcelTerminal::smartpost(false)->get()->keyBy('id')->map(function ($item){
														return $item->provider . ' ' . $item->name;
													})->all()
					],
			];

		if (Cart::count() == 1 && Cart::content()->first()->model->is_lightweight) {
			$methods['eu_shipping']['name'] = 'EU Shipping 6 EUR';
			$methods['eu_shipping']['price'] = 5.00; // 6 / 1.2 (tax)

			$methods['courier_estonia']['name'] = 'Courier in Estonia 2 EUR';
			$methods['courier_estonia']['price'] = 1.66; // 2 / 1.2 (tax)
		}

		if ($this->getWeight() >= 30) {
		    $methods['heavy_shipping']['active'] = true;
		    $methods['heavy_shipping']['view'] = true;

		    $methods['courier_estonia']['active'] = false;
		    $methods['courier_estonia']['view'] = false;

		    $methods['parcel_terminal']['active'] = false;
		    $methods['parcel_terminal']['view'] = false;

		} elseif ($this->getWeight() < 30) {
            $methods['heavy_shipping']['active'] = false;
            $methods['heavy_shipping']['view'] = false;

            $methods['courier_estonia']['active'] = false;
            $methods['courier_estonia']['view'] = true;

            $methods['parcel_terminal']['active'] = true;
            $methods['parcel_terminal']['view'] = true;
        }

        if ($this->hasHeavyweightProduct()) {
            unset($methods);
            switch (Lang::locale()) {
                case 'en':
                    $methods['eu_shipping']['name'] = "We can't show you shipping price at the moment but don't be sad, we'll get back to you with personal shipping offer after you have placed the order. Thank you!";
                    $methods['eu_shipping']['price'] = 0.00; // 2 / 1.2 (tax)
                    break;
                case 'et':
                    $heavyweightShippingPrice = $this->getHeavyweightProductShippingPrice();
                    $methods['courier_estonia']['name'] = 'Courier in Estonia ' . $heavyweightShippingPrice . ' EUR';
                    $methods['courier_estonia']['price'] = number_format((float)$heavyweightShippingPrice, 2, '.', ''); // 2 / 1.2 (tax)
                    break;
            }
        } elseif (Cart::total() >= 60) {
            //$methods['courier_estonia']['name'] = 'Courier in Estonia FREE';
            //$methods['courier_estonia']['price'] = 0.00;
//            $methods['parcel_terminal']['price'] = 0.00;
        }

		return $methods;
	}

    /**
     * Checks if any of products in cart is heavyweight.
     *
     * @return int
     */
    private function hasHeavyweightProduct()
    {
        $isHeavyweight = 0;
        try {
            foreach (Cart::content() as $item) {
                if ($item->model->is_heavyweight) {
                    $isHeavyweight = 1;
                }
            }
        } catch (\Exception $e) {
            \Log::error('Checking if heavyweight product failed, caused by ' . $e->getMessage() . ' - ' . print_r(Cart::content(), 1));
        }

        return $isHeavyweight;
    }

    /**
     * Gets shipping price of the heaviest product in cart.
     *
     * @return int
     */
    private function getHeavyweightProductShippingPrice()
    {
        $heavyweightShippingPrice = 0;
        $productWeight = 0;

        foreach (Cart::content() as $item) {
            if ($item->model->is_heavyweight && $item->model->weight > $productWeight) {
                $productWeight = $item->model->weight;
                $heavyweightShippingPrice = $item->model->heavyweight_shipping;
            }
        }

        $heavyweightShippingPrice = ($heavyweightShippingPrice == null) ? 25 : $heavyweightShippingPrice;

        return $heavyweightShippingPrice;
    }

	/*
If the person chooses "Pangaülekanne" he/she still has to see this page
If she chooses "Card payment" = "Kaardimakse" or "SEPA payment" = "Pangalink" then she will be leaded to Mollie's payment page
	*/
    public function fetchPaymentMethods()
    {

        $methods = [
            'bank_transfer' => [
                'name'    => trans('checkout.Bank transfer'),  //:TODO translate  old_invoice  Pangaülekanne
                'checked' => TRUE,
            ],
//            'hire_purchase_swedbank' => [
//                'name' => trans('checkout.swedbank_hire_purchase'),
//                'checked' => FALSE,
//            ],
//            'hire_purchase_seb' => [
//                'name' => trans('checkout.seb_hire_purchase'),
//                'checked' => FALSE,
//            ],
//            'hire_purchase_lhv' => [
//                'name' => trans('checkout.lhv_hire_purchase'),
//                'checked' => FALSE,
//            ],

            // MONTONIO EESTI
            'montonio_swedbank_ee' => [ // Swedbank
                'name' => trans('checkout.montonio_swedbank_ee'),
                'checked' => FALSE,
            ],
            'montonio_seb_ee' => [ // SEB
                'name' => trans('checkout.montonio_seb_ee'),
                'checked' => FALSE,
            ],
            'montonio_lhv_ee' => [ // LHV
                'name' => trans('checkout.montonio_lhv_ee'),
                'checked' => FALSE,
            ],
            'montonio_luminor_ee' => [ // Luminor
                'name' => trans('checkout.montonio_luminor_ee'),
                'checked' => FALSE,
            ],
            'montonio_coop_pank_ee' => [ // Coop Pank
                'name' => trans('checkout.montonio_coop_pank_ee'),
                'checked' => FALSE,
            ],
            'montonio_citadele_ee' => [ // Citadele
                'name' => trans('checkout.montonio_citadele_ee'),
                'checked' => FALSE,
            ],

             // MONTONIO FINLAND
            'montonio_op_fi' => [ // OP
                'name' => trans('checkout.montonio_op_fi'),
                'checked' => FALSE,
            ],
            'montonio_nordea_fi' => [ // Nordea
                'name' => trans('checkout.montonio_nordea_fi'),
                'checked' => FALSE,
            ],

            'card_payment' => [
                'name'    => trans('checkout.Card payment (Fee 0.50 EUR)'), // :TODO translate  Kaardimakse to Mollie
                'checked' => FALSE,
            ],
//            'e_banking' => [
//                'name'    => trans('checkout.Bank link (Fee 0,25 EUR)'),
//                'checked' => FALSE,
//            ],
//                'bank_link' => [  //:TODO те кнопочки снизу
//                    'name'    => trans('checkout.SEPA payment (Fee 0.25 EUR)'),  //:TODO Pangalink  to Mollie
//                    'checked' => FALSE,
//                ],
        ];
        return $methods;
    }

	/**
	 * [addShippingMethod description]
	 */
	public function addShippingMethodToCart($method, $phoneCode = null)
	{
		$methods = $this->fetchShippingMethods();

		if ($phoneCode === '+358') {
            $methods['parcel_terminal']['price'] = 10;
        }

		if ((Cart::subtotal() >= 50 && $phoneCode !== '+358') || (Cart::subtotal() >= 125 && $phoneCode === '+358')) {
		    $methods['parcel_terminal']['price'] = 0;
        }

		Cart::add([
			'id'      => 'shipping',
			'name' 	  => $methods[$method]['name'],
			'qty'     => 1,
			'price'   => $methods[$method]['price'],
		]);
	}

	public function sendMails($order)
	{
	    $emailText = \App::getLocale() == 'et' ? 'email_text_et' : 'email_text';
        $locale = \App::getLocale();
		$order = Order::find($order->id);
		if($order->email){
			Mail::send('emails.invoice', ['order' => $order->load('items', 'parcelTerminal')->toArray(), 'locale' => $locale,'text' => \Voyager::setting($emailText)], function($message) use ($order)
			{
			    $message->to($order->email)->subject("Order number $order->id from greenest.ee");
			});
		}

		Mail::send('emails.invoice', ['for_admin' => true, 'order' => $order->load('items.product', 'parcelTerminal')->toArray()], function($message) use ($order)
		{
		    $message->to('orders@greenest.ee')->subject("New order $order->id");
		});
	}

	//:TODO not used
    public function ebanking()
    {
        $data = request()->only('payment','order_id');
        $order = Order::find($data['order_id']);
        if ( ! empty($order))
        {
            switch ($order->payment_method)
            {
                case 'e_banking':
                    $price = $order->sum_total*100+25;
                    break;
                default :
                    $price = $order->sum_total*100;
            }
            $locale = \App::getLocale();
            if ($locale == 'et')
                $localeInPaysera = 'EST';
            elseIf ($locale == 'en')
                $localeInPaysera = 'ENG';
            $requestData = WebToPay::buildRequest(
            [
                'projectid'     => WebToPay::PROJECT_ID,
                'sign_password' => WebToPay::PASSWORD,
                'orderid'       => $order->id,
                'amount'        => $price,
                'currency'      => 'EUR',
                'accepturl'     => url("/") . "/{$locale}/ebanking/accept",
                'cancelurl'     => url("/") . "/.{$locale}/ebanking/cancel",
                'callbackurl'   => url("/") . "/{$locale}/ebanking/callback",
                'payment'       => $data['payment'],
                'lang'          => $localeInPaysera,
                //'p_email'       => $order->email,
                'test'          => 0,
            ]);
            $payUrl = WebToPay::PAY_URL;
            return view('cart.paysera', [
                'requestData' => $requestData,
                'payUrl'     => $payUrl,
            ]);
        }
    }
//
    public function ebankingAccept()
    {
        return redirect('');
    }

    public function ebankingCancel()
    {
        return redirect('');
    }

    public function ebankingCallback()
    {
        try
        {
            $response = WebToPay::checkResponse($_GET, array(
                'projectid'     => WebToPay::PROJECT_ID,
                'sign_password' => WebToPay::PASSWORD,
            ));

            if ($response['test'] !== '0') {
                throw new \Exception('Testing, real payment was not made');
            }

            if ($response['type'] !== 'macro') {
                throw new \Exception('Only macro payment callbacks are accepted');
            }

            $order = Order::find($response['orderid']);

//            if ($order && $order->sum_total == $response['amount'] / 100) {
                $order->status = 'paid';
                $order->save();
                echo "OK";
//            } else {
//                throw new \Exception('Invalid order or ammount');
//            }
        } catch (Exception $e) {
            echo get_class($e) . ': ' . $e->getMessage();
        }
    }


    protected function fetchDatalayerGetAdd($product)
    {
        $p = Product::find($product['id']);

        $categories = [];

        foreach (CategoryProduct::where('product_id', $product['id'])->get() as $categoryProduct) {
            $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
        }

        $arr = [
            'name' => $product['name'],
            'sku' => $p['sku'],
            'price' => $p['price'],
            'category' => implode($categories),
            'quantity' => 1,
        ];

        $str = 'dataLayer.push({
            "event":"addToCart",
            "ecommerce":{
                "currencyCode":"EUR",
                "add":{
                    "actionField":{"list":"checkout"},
                    "products":['
                        . json_encode($arr, JSON_UNESCAPED_UNICODE)
                    . ']
                    }
                }
            });';
        return $str;
    }


    private function fetchDatalayerGetAddObject($product)
    {
        $p = Product::find($product['id']);

        $categories = [];

        foreach (CategoryProduct::where('product_id', $product['id'])->get() as $categoryProduct) {
            $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
        }

        $arr = [
            'name' => $product['name'],
            'sku' => $p['sku'],
            'price' => $p['price'],
            'category' => implode($categories),
            'quantity' => 1,
        ];

        $str = '{"event":"addToCart","ecommerce":{"currencyCode":"EUR","add":{"actionField":{"list":"checkout"},"products":['.
            json_encode($arr, JSON_UNESCAPED_UNICODE). ']}}}';
        return $str;
    }


    /**
     * for remove product
     */
    protected function fetchDatalayer($productId, $quantity = 1)
    {
        $products = Product::where('id', $productId)->get();

        foreach ($products as $product) {
            $categories = [];
            foreach (CategoryProduct::where('product_id', $product->id)->get() as $categoryProduct) {
                $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
            }

            $productsArray[] = [
                'name' => $product->name,
                'sku' => $product->sku,
                'price' => $product->price,
                'category' => implode($categories),
                'quantity' => $quantity,
            ];
        }

        $dataLayer = [
           'event' => 'removeFromCart',
            'ecommerce' => [
                'remove' => [
                    'products' => [
                        $productsArray
                    ]
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ');';

        return $str;

    }


    protected function fetchDataLayerForInvoice($order)
    {
        $cart = Cart::content()->toArray();

        $productsArray = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => round($item['price'], 2),
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $productsArray[] = $arrObject;
            }
        }

        $dataLayer = [
            'ecommerce' => [
                'purchase' => [
                    'actionField' => [
                        'id' => '\'' . $order->id . '\'',
                        'affiliation' => 'greenest',
                        'revenue' => round($order->subtotal / 1.2, 2),
                        'tax' => round($order->tax, 2),
                        'shipping' => null,
                        'coupon' => null,
                    ],
                    'products' => $productsArray
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ')';

        return $str;
    }


    protected function fetchDataLayerForCheckout()
    {
        $cart = Cart::content()->toArray();
        $arr = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => $item['price'],
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $arr[] = $arrObject;
            }

        }

        $dataLayer = [
            'event' => 'checkout',
            'ecommerce' => [
                'checkout' => [
                    'actionField' => [
                        'products' => $arr
                    ]
                ]
            ]
        ];


        $str = 'dataLayer.push(' . json_encode($dataLayer, JSON_UNESCAPED_UNICODE) . ')';

        return $str;
    }



    public function fetchDataLayerForCheckoutAjax($step = 1)
    {
        $cart = Cart::content()->toArray();
        $arr = [];

        foreach ($cart as $item) {
            $p = Product::find($item['id']);

            if ($p) {

                $categories = [];

                foreach (CategoryProduct::where('product_id', $p->id)->get() as $categoryProduct) {
                    $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
                }
                $arrObject = [
                    'name' => $p->name,
                    'id' => $p->id,
                    'price' => $item['price'],
                    'category' => implode($categories),
                    'quantity' => $item['qty'],
                ];

                $arr[] = $arrObject;
            }

        }

        $dataLayer = [
            'event' => 'checkout',
            'ecommerce' => [
                'checkout' => [
                    'actionField' => ['step' => $step, 'option' => null],
                    'products' => $arr
                ]
            ]
        ];

        $str =  json_encode($dataLayer, JSON_UNESCAPED_UNICODE);

        return $str;
    }

    /**
     * Mollie (api)
     * @param $payment
     * @return string
     */
    protected function getMollieStatusText($payment)
    {
        $status = '';

        if ($payment->isPaid()) $status = 'Paid';
        if ($payment->isCancelled()) $status = 'Cancelled';
        if ($payment->isExpired()) $status = 'Expired';
        if ($payment->isOpen()) $status = 'Open';
        if ($payment->isFailed()) $status = 'Failed';
        if ($payment->isRefunded()) $status = 'Refunded';

        return $status;
    }

    protected function exitFromCheckout()
    {
        if (Auth::check()) {
            foreach (Cart::content() as $cartItem) {
                UserCart::where('user_id', Auth::id())->where('product_id', $cartItem->id)->delete();
            }
        }

        Cart::destroy();

        \Auth::logout();
    }

    public function successSwedbankPayment($orderId, $sign)
    {
        $order = Order::find($orderId);

        Log::info("{$_SERVER['REQUEST_METHOD']}:{$_SERVER['REQUEST_URI']}  |  Request data for order #{$orderId}: " . json_encode($_REQUEST));

        if (isset($_REQUEST['VK_MAC'])) {
            $data = $_REQUEST;
            $bankSignature = $data['VK_MAC'];

            unset($data['VK_MAC']);
            Log::info("Test verify bank signature:  " . intval(LHVHirePurchase::verifyVkMac(LHVHirePurchase::getVkMac($data), $bankSignature)));

            unset($data['VK_ENCODING']);
            Log::info("Test verify bank signature:  " . intval(LHVHirePurchase::verifyVkMac(LHVHirePurchase::getVkMac($data), $bankSignature)));

            unset($data['VK_LANG']);
            Log::info("Test verify bank signature:  " . intval(LHVHirePurchase::verifyVkMac(LHVHirePurchase::getVkMac($data), $bankSignature)));
        }

        if (empty($order)) {
            return redirect('/' . \App::getLocale());
        }

        $ourSign = $order->makeUrlSignature();

        $setPaidStatus = function ($remark = '') use ($order) {
            $order->status = 'paid';
            $order->save();

            Log::info('Order #'.$order->id.' was paid. ' . $remark);

            return view('cart.lhv-success', ['dataLayerKey' => "{$order->id}dataLayer"]);
        };

        if (!empty($_REQUEST['VK_SERVICE'])) {
            if ($_REQUEST['VK_SERVICE'] == '1111' && $ourSign === $sign) {
                $setPaidStatus();
            } elseif ($_REQUEST['VK_SERVICE'] == '1911' && $ourSign !== $sign && $order->makeUrlSignature('new') === $sign && $order->status === 'paid') {
                $order->status = 'new';
                $order->save();

                Log::info('Order #'.$order->id.' was return to status "new".');

                return redirect('/' . \App::getLocale());
            }
        } elseif ($ourSign === $sign) {
            $setPaidStatus('(Without VK_SERVICE)');
        }

        if ($order->status == 'paid' && $_SERVER['REQUEST_METHOD'] == 'GET') {
            return view('cart.lhv-success', ['dataLayerKey' => "{$order->id}dataLayer"]);
        }

        return redirect('/' . \App::getLocale());
    }

}
