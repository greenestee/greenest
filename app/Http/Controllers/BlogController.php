<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BlogPost;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $locale = \App::getLocale();

        if ($locale == 'et') {
            $url = 'slug_et';
            $categoryFields = ['id', 'name_et', 'slug_et'];
            $articleFields = ['id', 'title_et', 'text_et', 'slug_et', 'image', 'blog_category_id'];
        } else {
            $url = 'slug';
            $categoryFields = ['id', 'name', 'slug'];
            $articleFields = ['id', 'title', 'text', 'slug', 'image', 'blog_category_id'];
        }

        $chosenCategory = $request->get('category');

        $allowPaginate = collect([60, 120, 240]);
        $paginate = $request->get('items');
        $paginate = $allowPaginate->contains($paginate) ? $paginate : 30;

        if (isset($chosenCategory) && !empty($chosenCategory)) {
            $category = BlogCategory::select($categoryFields)->whereIn($url, explode(' ', $chosenCategory))->get();
            $categoriesIds = $category->pluck('id')->toArray();
            $articles = BlogPost::select($articleFields)->whereIn('blog_category_id', $categoriesIds)->orderBy('created_at', 'desc')->paginate($paginate);
        } else {
            $articles = BlogPost::select($articleFields)->orderBy('created_at', 'desc')->paginate($paginate);
            $category = collect([]);
        }

        $articles = $this->cutArticlesText($articles);

        $categories = BlogCategory::select($categoryFields)->get();

        return view('blog.index', [
            'articles' => $articles,
            'categories' => $categories,
            'checked' => $category
        ]);
    }

    private function cutArticlesText($articles)
    {
        foreach ($articles as &$article) {
            if (mb_strlen($article->text, 'UTF-8')) {
                $article->text = mb_substr($article->text, 0, 350, 'UTF-8') . '...';
            }
            if (mb_strlen($article->text_et, 'UTF-8')) {
                $article->text_et = mb_substr($article->text_et, 0, 350, 'UTF-8') . '...';
            }
        }

        return $articles;
    }

    /**
     * Get the products description page
     *
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getArticle($category, $slug)
    {
        $locale = \App::getLocale();

        if ($locale == 'et') {
            $url = 'slug_et';
        } else {
            $url = 'slug';
        }

        $article = BlogPost::where($url, $slug)->first();

        if (!empty($article)) {
            if ($locale == 'et') {
                $meta_title = $article->meta_title_et;
                $meta_description = $article->meta_description_et;
                $meta_keywords = $article->meta_keywords_et;
                $productFields = ['id', 'name_et as name', 'slug_et as slug', 'price', 'status', 'is_active', 'image'];
            } else {
                $meta_title = $article->meta_title;
                $meta_description = $article->meta_description;
                $meta_keywords = $article->meta_keywords;
                $productFields = ['id', 'name', 'slug', 'price', 'status', 'is_active', 'image'];
            }

            if (isset($article->video_link) && !empty($article->video_link)) {
                $rx = '~
                      ^(?:https?://)?
                       (?:www[.])?
                       (?:youtube[.]com/watch[?]v=|youtu[.]be/)
                        ~x';

                if (preg_match($rx, $article->video_link, $matches)) {
                    $article->video_link = str_replace('watch?v=', 'embed/', $article->video_link);
                } else {
                    $article->video_link = null;
                }
            }

            $article->text = str_replace("\n", '<br>', $article->text);
            $article->text_et = str_replace("\n", '<br>', $article->text_et);
            $article->created_at = $article->created_at->setTimezone('Europe/Tallinn');

            $products = $article->products()->get();

            return view('blog.item', [
                'article' => $article,
                'products' => $products,
                'meta_title' => $meta_title,
                'meta_description' => $meta_description,
                'meta_keywords' => $meta_keywords
            ]);
        } else {
            if ($locale == 'en') {
                return redirect('/en/food-and-drinks');
            } else {
                return redirect('/et/toit-ja-jook');
            }
        }
    }
}
