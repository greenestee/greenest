<?php

namespace App\Http\Controllers;

use App\Http\Libraries\Montonio\MontonioPayment;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MontonioController extends Controller
{
    public function redirect($orderId, Request $request)
    {
        Log::info("Montonio redirect. Order ID: {$orderId}. Token: {$request->get('payment_token', '""')}");
        if ($request->has('payment_token')) {
            $payment = new MontonioPayment();

            if ($payment->validatePaymentData($request->get('payment_token'), $orderId)) {
                $order = Order::find($orderId);
                if (!empty($order)) {
                    $order->status = 'paid';
                    $order->save();

                    return view('cart.lhv-success', ['dataLayerKey' => "{$order->id}dataLayer"]);
                }
            }
        }

        return redirect()->to('/' . \App::getLocale());
    }

    public function webhook($orderId, Request $request)
    {
        Log::info("Montonio webhook. Order ID: {$orderId}. Token: {$request->get('payment_token', '""')}");
        if ($request->has('payment_token')) {
            $payment = new MontonioPayment();

            $order = Order::find($orderId);
            if (!empty($order)) {
                if ($payment->validatePaymentData($request->get('payment_token'), $orderId)) {
                    if ($order->status !== 'paid') {
                        $order->status = 'paid';
                        $order->save();
                    }
                } elseif ($order->status === 'paid') {
                    $order->status = 'new';
                    $order->save();
                }
            }
        }

        return response()->json();
    }
}
