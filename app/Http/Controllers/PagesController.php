<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Product;
use App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\MenuItem;
//use Torann\GeoIP\Facades\GeoIP;

class PagesController extends Controller
{
	/**
	 * Show page method.
	 */
    public function getWelcome()
    {
        $locale = \App::getLocale();
        $slug = 'slug';
        $lang = 'en';

        if ($locale == 'et') {
            $slug = 'slug_et';
            $lang = 'et';
        }

        $category = Category::where('is_main', 1)->where('parent_id', null)->first();

        $pageUrl = isset($category) ? $category->$slug : 'pages/about';
        $uri = "{$lang}/" . $pageUrl;

        $url = url($uri);

        return redirect()->to($url);
    }

	/**
	 * Show page method.
	 */
    public function getPage(Page $page)
    {
    	return view('pages.page', compact('page'));
    }

    public function changeLanguage(Request $request, $locale)
    {
        $previous_url = url()->previous();
        $previousRequest = app('request')->create($previous_url);
        $query = $previousRequest->query();
        $routeName = app('router')->getRoutes()->match($previousRequest)->getName();
        $segments = $previousRequest->segments();
        $previousLang = $segments[0];

        if (array_key_exists($locale, config('languages.locale'))) {
            if ($routeName && Lang::has('routes.' . $routeName, $locale)) {
                if (count($query)) {
                    return redirect()->to($locale . '/' .  trans('routes.' . $routeName, [], $locale) . '?' . http_build_query($query));
                }
                return redirect()->to($locale . '/' .  trans('routes.' . $routeName, [], $locale));
            }

            $segments[0] = $locale;

            if (count($query) && $routeName !== 'category') {
                if ($routeName !== 'articles') {
                    return redirect()->to(implode('/', $segments) . '?' . http_build_query($query));
                }
            }

            if ($routeName == 'page') {
                $locale = array_shift($segments);
                $page = MenuItem::where(function ($query) use ($segments) {
                    $query->where('url', '=', implode('/', $segments))
                        ->orWhere('url_et', '=', implode('/', $segments));
                })->firstOrFail();
                if ($locale == 'et') {
                    $url = $page->url_et;
                } else {
                    $url = $page->url;
                }
                return redirect()->to($locale . '/' . $url);
            }

            if ($routeName == 'products' || $routeName == 'products_et' ) {
                $productName = $segments[2];

                if ($segments[0] == 'et') {
                    $product = 'slug';
                    $newUrl = 'slug_et';
                    $segments[1] = 'toode';
                } else {
                    $product = 'slug_et';
                    $newUrl = 'slug';
                    $segments[1] = 'product';
                }

                $productName = Product::where($product, $productName)->firstOrFail();
                $segments[2] = $productName->$newUrl;
                return redirect()->to(implode("/", $segments));
            }

            if ($routeName == 'article' || $routeName == 'article_et' ) {
                $categorySlug = $segments[2];
                $articleSlug = $segments[3];

                if ($segments[0] == 'et') {
                    $article = 'slug';
                    $newUrl = 'slug_et';
                    $segments[1] = 'blogi';
                } else {
                    $article = 'slug_et';
                    $newUrl = 'slug';
                    $segments[1] = 'blog';
                }

                $category = App\BlogCategory::where($article, $categorySlug)->firstOrFail();
                $segments[2] = $category->$newUrl;
                $article = App\BlogPost::where($article, $articleSlug)->firstOrFail();
                $segments[3] = $article->$newUrl;
                return redirect()->to(implode("/", $segments));
            }

            if ($routeName == 'category') {
                $locale = array_shift($segments);
                $category = array_shift($segments);
                $subCategory = $previousRequest->input('category');

                if ($previousLang == 'et') {
                    $slug = 'slug_et';
                } else {
                    $slug = 'slug';
                }

                $subCategory = explode(' ', $subCategory);
                $categories = Category::whereIn($slug, $subCategory)->get();

                $url = '';

                if ($locale == 'et') {
                    $slug = Category::where($slug, $category)->first()->slug_et;
                    foreach ($categories as $category) {
                        $url .= $category->slug_et.'+';
                    }
                } else {
                    $slug = Category::where($slug, $category)->first()->slug;
                    foreach ($categories as $category) {
                        $url .= $category->slug.'+';
                    }
                }

                if (empty($url)) {
                    return redirect()->to($locale . '/' . $slug);
                }

                return redirect()->to($locale . '/' . $slug . '?category=' . substr($url,0,-1));
            }

            if ($routeName == 'articles') {
                $locale = array_shift($segments);
                $category = array_shift($segments);
                $subCategory = $previousRequest->input('category');

                if ($previousLang == 'et') {
                    $slug = 'slug_et';
                    $blogName = 'blog';
                } else {
                    $slug = 'slug';
                    $blogName = 'blogi';
                }

                if (count($subCategory) > 0) {
                    $subCategory = explode(' ', $subCategory);
                    $categories = App\BlogCategory::whereIn($slug, $subCategory)->get();

                    $url = '';

                    if ($locale == 'et') {
                        foreach ($categories as $category) {
                            $url .= $category->slug_et.'+';
                        }
                    } else {
                        foreach ($categories as $category) {
                            $url .= $category->slug.'+';
                        }
                    }
                }

                if (empty($url)) {
                    return redirect()->to($locale . '/' . $blogName);
                }

                return redirect()->to($locale . '/' . $blogName . '/' . '?category=' . substr($url,0,-1));
            }

            return redirect()->to(implode('/', $segments));
        }

        return redirect()->back();
    }
}
