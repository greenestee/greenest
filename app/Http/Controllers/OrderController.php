<?php

namespace App\Http\Controllers;

use App\Http\Libraries\DPD\DPD;
use App\Http\Libraries\LabelsToPdf\LabelsToPdf;
use App\Jobs\SendDelayedOrderNotification;
use App\Order;
use App\OrderItem;
use App\OrderShipment;
use App\OrderStatusNotification;
use App\ParcelTerminal;
use App\Product;
use Carbon\Carbon;
use DOMDocument;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use SimpleXMLElement;
//use Symfony\Component\HttpFoundation\StreamedResponse;
use Storage;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    private $errors = array();

    public function downloadRefrigeratedShippingCards(Request $request)
    {
        $sortFrom = isset($request->sortFrom) ? ($request->sortFrom !== "" ? $request->sortFrom : null) : null;
        $sortTo = isset($request->sortTo) ? ($request->sortTo !== "" ? $request->sortTo : null) : null;

        if (explode(' ', trim($request->ids))[0] != "") {
            return LabelsToPdf::generateRefrigeratedPackageCards(
                explode(' ', trim($request->ids))
            );
        } else {
            $orders = Order::where('id', '>', 0);
            if ($sortFrom !== null) {
                $orders = $orders->where(
                    'created_at',
                    '>=',
                    Carbon::createFromFormat('d/m/Y', $sortFrom)->format('Y-m-d 00:00:01')
                );
            }
            if ($sortTo !== null) {
                $orders = $orders->where(
                    'created_at',
                    '<=',
                    Carbon::createFromFormat('d/m/Y', $sortTo)->format('Y-m-d 23:59:59')
                );
            }
            $orders = $orders->pluck('id')->toArray();
            return LabelsToPdf::generateRefrigeratedPackageCards($orders);
        }
    }

    // documentation https://uus.smartpost.ee/ariklient/ostukorvi-rippmenuu-lisamise-opetus/automaatse-andmevahetuse-opetus

    public function createSmartpostPackageCards(Request $request)
    {
        if($request->ids == "") {
            return response()->json(['errors' => ['Nothing to print']]);
        }

        $printPreviouslyPrinted = false;
        if (isset($request->print_previously_printed) && $request->print_previously_printed == 'true') {
            $printPreviouslyPrinted = true;
        }
        $sortFrom = isset($request->sortFrom) ? ($request->sortFrom !== "" ? $request->sortFrom : null) : null;
        $sortTo = isset($request->sortTo) ? ($request->sortTo !== "" ? $request->sortTo : null) : null;

        $orders = Order::where(function ($q) {
            $q->whereNotIn('status', ['new', 'rejected'])
                ->orWhere(function ($q) {
                    $q->where('mollie_payment_status', '=', 'Paid')
                        ->where('status', '=', 'new');
                });
        });

        if (explode(' ', trim($request->ids))[0] != "") {
            $ordersIds = explode(' ', trim($request->ids));
            $orders = $orders->whereIn('id', $ordersIds);
        } else {
            if ($sortFrom !== null) {
                $orders = $orders->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $sortFrom)->format('Y-m-d 00:00:01'));
            }
            if ($sortTo !== null) {
                $orders = $orders->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $sortTo)->format('Y-m-d 23:59:59'));
            }

        }
        $orders = $orders->get();
        if ($orders->count() == 0) {
            $this->errors[] = "No orders available.";
            return response()->json(['errors' => $this->errors]);
        }

        $pdfUrl = $this->returnSmartpostLabels($orders, $printPreviouslyPrinted);

        if ($pdfUrl == null) {
            $this->errors[] = 'Smartpost response with error';
            return response()->json(['errors' => $this->errors]);
        }
        if ($pdfUrl === false) {
            return response()->json(['errors' => $this->errors]);
        }
        return response()->json(['pdf' => $pdfUrl, 'errors' => $this->errors]);
    }

    protected function returnSmartpostLabels(Collection $orders, $printPreviouslyPrinted)
    {
        $ordersForShipment = array();
        $shipmentIds = array();
        foreach ($orders as $order) {
            if (empty($order->shipment)) {
                $ordersForShipment[] = $order;
            } elseif ($printPreviouslyPrinted) {
                $ordersForShipment[] = $order;

//                $shipmentIds[] = $order->shipment->shipment_id;
            }
        }

        $orders = $this->sendOrdersToSmartpost($ordersForShipment);
        if (is_numeric($orders)) {
            return null;
        }

        foreach ($orders as $order) {
            $shipmentIds[] = $order->shipment->shipment_id;
        }

        return $this->getSmartpostLabels($shipmentIds);
    }

    protected function sendOrdersToSmartpost(array $orders)
    {
        foreach ($orders as $key => $order) {
            if ($order->shipping_method == 'parcel_terminal') {
                if ($order->parcelTerminal->provider != 'Smartpost') {
                    $this->errors[] = "Order {$order->id} has not Smartpost parcel terminal.";
                    unset($orders[$key]);
                }
            } elseif ($order->shipping_method == 'courier_estonia') {
                if (empty($order->shipping_address) || empty($order->shipping_country) || empty($order->shipping_zip_code)) {
                    $this->errors[] = "Order {$order->id} is missing country, postal code and/or address.";
                    unset($orders[$key]);
                }
            } else {
                $this->errors[] = "Order {$order->id} has unsupported shipping method.";
                unset($orders[$key]);
            }
        }
        if (count($orders) < 1) {
            return [];
        }
        $xmlBody = $this->makeXmlForSmartpostShipmentRequest($orders);

        $client = new GuzzleClient();
        try {
            $result = $client->request('POST', 'http://iseteenindus.smartpost.ee/api/', [
                'query' => ['request' => 'shipment'],
                'body' => $xmlBody,
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }

        if ($result->getStatusCode() != 200) {
            return 'Smartpost return error!';
        }

        $body = $result->getBody();

        $responsedXml = new SimpleXMLElement($body);
        foreach ($responsedXml as $item) {
            $orderShipment = new OrderShipment();
            $orderShipment->shipment_id = $item->barcode;
            $orderShipment->provider = 'Smartpost';

            foreach ($orders as $order) {
                if ($order->id == $item->reference) {
                    $order->shipment()->save($orderShipment);
                    $order->shipment = $orderShipment;
                    break;
                }
            }
        }

        return $orders;
    }

    protected function makeXmlForSmartpostShipmentRequest(array $ordersArray)
    {
        if (!$this->hasCredentials()) {
            return 'Smartpost login and password not defined!';
        }
        $xmlBody = new DOMDocument();
        $orders = $xmlBody->createElement('orders');
        $ordersNode = $xmlBody->appendChild($orders);
        $authentication = $xmlBody->createElement('authentication');
        $authenticationNode = $ordersNode->appendChild($authentication);
        $user = $xmlBody->createElement('user');
        $userNode = $authenticationNode->appendChild($user);
        $userNode->appendChild($xmlBody->createTextNode(config('smartpost.login')));
        $password = $xmlBody->createElement('password');
        $passwordNode = $authenticationNode->appendChild($password);
        $passwordNode->appendChild($xmlBody->createTextNode(config('smartpost.password')));
        foreach ($ordersArray as $order) {
            $item = $xmlBody->createElement('item');
            $itemNode = $ordersNode->appendChild($item);

            $reference = $xmlBody->createElement('reference');
            $referenceNode = $itemNode->appendChild($reference);
            $referenceNode->appendChild($xmlBody->createTextNode($order->id));

            $recipient = $xmlBody->createElement('recipient');
            $recipientNode = $itemNode->appendChild($recipient);

            $name = $xmlBody->createElement('name');
            $nameNode = $recipientNode->appendChild($name);
            $nameNode->appendChild($xmlBody->createTextNode($order->name));

            $phone = $xmlBody->createElement('phone');
            $phoneNode = $recipientNode->appendChild($phone);

            $destination = $xmlBody->createElement('destination');
            $destinationNode = $itemNode->appendChild($destination);

            if ($order->shipping_method == 'parcel_terminal') {
                $phoneNode->appendChild($xmlBody->createTextNode($order->shipping_parcel_terminal_phone));

                if ($order->parcelTerminal->country == 'EE') {
                    $place_id = $xmlBody->createElement('place_id');
                    $place_idNode = $destinationNode->appendChild($place_id);
                    $place_idNode->appendChild($xmlBody->createTextNode($order->parcelTerminal->smartpost_id));
                } else {
                    $postalcode = $xmlBody->createElement('postalcode');
                    $postalcodeNode = $destinationNode->appendChild($postalcode);
                    $postalcodeNode->appendChild($xmlBody->createTextNode($order->parcelTerminal->postalcode));

                    $routingcode = $xmlBody->createElement('routingcode');
                    $routingcodeNode = $destinationNode->appendChild($routingcode);
                    $routingcodeNode->appendChild($xmlBody->createTextNode($order->parcelTerminal->smartpost_routingcode));
                }
            } else {
                $phoneNode->appendChild($xmlBody->createTextNode($order->phone));

                $street = $xmlBody->createElement('street');
                $streetNode = $destinationNode->appendChild($street);
                $streetNode->appendChild($xmlBody->createTextNode($order->shipping_address));

                $city = $xmlBody->createElement('city');
                $cityNode = $destinationNode->appendChild($city);
                $cityNode->appendChild($xmlBody->createTextNode($order->shipping_city));

                $postalcode = $xmlBody->createElement('postalcode');
                $postalcodeNode = $destinationNode->appendChild($postalcode);
                $postalcodeNode->appendChild($xmlBody->createTextNode($order->shipping_zip_code));

                $country = $xmlBody->createElement('country');
                $countryNode = $destinationNode->appendChild($country);
                $countryNode->appendChild($xmlBody->createTextNode('EE'));
            }
        }
        return $xmlBody->saveXML();
    }

    protected function getSmartpostLabels(array $barcodes)
    {
        if (count($barcodes) == 0) {
            $this->errors[] = "No orders available.";
            return false;
        }

        $xmlBody = $this->makeXmlForSmartpostLabesRequest($barcodes);

        $putStream = tmpfile();
        $client = new GuzzleClient();
        $result = $client->request('POST', 'http://iseteenindus.smartpost.ee/api/', [
            'query' => ['request' => 'labels'],
            'body' => $xmlBody,
            'sink' => $putStream
        ]);
        if ($result->getStatusCode() != 200) {
            return 'Smartpost server does not response!';
        }
        @ unlink(public_path("/storage/tmp/package_cards.pdf"));
        if (Storage::disk('local')->put("public/tmp/package_cards.pdf", $putStream)) {
            fclose($putStream);
            return url("/storage/tmp/package_cards.pdf");
        } else {
            fclose($putStream);
            return 'Undefined error while saving pdf.';

        }

//        $body = $result->getBody();
//        $response = new StreamedResponse(function () use ($body) {
//            while (!$body->eof()) {
//                echo $body->read(1024);
//            }
//        });
//
//        $response->headers->set('Content-Type', 'application/pdf');

//        return $response;
    }

    protected function makeXmlForSmartpostLabesRequest(array $barcodes)
    {
        if (!$this->hasCredentials()) {
            return 'Smartpost login and password not defined!';
        }

        $xmlBody = new DOMDocument();
        $labels = $xmlBody->createElement('labels');
        $labelsNode = $xmlBody->appendChild($labels);
        $authentication = $xmlBody->createElement('authentication');
        $authenticationNode = $labelsNode->appendChild($authentication);
        $user = $xmlBody->createElement('user');
        $userNode = $authenticationNode->appendChild($user);
        $userNode->appendChild($xmlBody->createTextNode(config('smartpost.login')));
        $password = $xmlBody->createElement('password');
        $passwordNode = $authenticationNode->appendChild($password);
        $passwordNode->appendChild($xmlBody->createTextNode(config('smartpost.password')));
        $format = $xmlBody->createElement('format');
        $formatNode = $labelsNode->appendChild($format);
        $formatNode->appendChild($xmlBody->createTextNode(config('smartpost.labelsFormat')));
        foreach ($barcodes as $barcode) {
            $b = $xmlBody->createElement('barcode');
            $bNode = $labelsNode->appendChild($b);
            $bNode->appendChild($xmlBody->createTextNode($barcode));
        }
        return $xmlBody->saveXML();
    }

    protected function hasCredentials()
    {
        if (empty(config('smartpost.login')) || empty(config('smartpost.password'))) {
            return false;
        }
        return true;
    }

    public function checkPrinted(Request $request)
    {
        $sortFrom = isset($request->sortFrom) ? ($request->sortFrom !== "" ? $request->sortFrom : null) : null;
        $sortTo = isset($request->sortTo) ? ($request->sortTo !== "" ? $request->sortTo : null) : null;
        $ids = isset($request->item_id) ? ($request->item_id !== "" ? $request->item_id : null) : null;

//        $orders = Order::where(function ($q) {
//            $q->where('status', 'paid')
//                ->orWhere(function ($q) {
//                    $q->where('mollie_payment_status', '=', 'Paid')
//                        ->where('status', '=', 'new');
//                });
//        });

        $orders = Order::orderBy('id');
        if (explode(' ', trim($ids))[0] != "") {
            $ordersIds = explode(' ', trim($ids));
            $orders = $orders->whereIn('id', $ordersIds);
        } else {
            if ($sortFrom !== null) {
                $orders = $orders->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $sortFrom)->format('Y-m-d 00:00:01'));
            }
            if ($sortTo !== null) {
                $orders = $orders->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $sortTo)->format('Y-m-d 23:59:59'));
            }
        }
        $orders = $orders->get();

        $ordersForShipment = array();
        $shipmentIds = array();
        foreach ($orders as $order) {
            if (empty($order->shipment)) {
                $ordersForShipment[] = $order->id;
            } else {
                $shipmentIds[] = $order->shipment->order_id;
            }
        }

        return response()->json([
            'notShippedOrders' => $ordersForShipment,
            'shippedOrders' => $shipmentIds
            ]);
    }

    /**
     * Events ids:
     * 1 = New > Paid;
     * 2 = Paid > Forwarded;
     * 3 = Forwarded > Completed;
     * 4 = Refund was made by admin, and notification will be sent to client
     * 5 = Notification to client that product is back in stock
     *
     * @param Order $order
     * @param $eventId
     * @param $hasCard - to send 3rd event_id after package card created
     * @param 0 $productId
     */
    public static function sendStatusUpdatingNotification(Order $order, $eventId, $hasCard = false, $productId = 0)
    {
        try {
            $emailText = ($order->shipping_method == 'eu_shipping') ? 'text_en' : 'text_et';

            $text = OrderStatusNotification::where('event', $eventId)->firstOrFail()->$emailText;

            $subject = self::getEmailSubject($emailText, $order->id);

            if ($eventId == 3 && $hasCard == true) {
                $data = OrderShipment::where('order_id', $order->id)->first();
                if ($order->email) {
                    Mail::send('emails.statusChange', ['data' => $data, 'text' => $text],
                        function ($message) use ($order, $subject) {
                            $message->to($order->email)->subject($subject);
                        });
                }
            } elseif ($eventId == 5) {
                $product = Product::where('id', $productId)->select(['name', 'price', 'name_et'])->first();
                if ($order->email) {
                    Mail::send('emails.statusChange', ['text' => $text, 'product' => $product], function ($message) use ($order, $subject) {
                        $message->to($order->email)->subject($subject);
                    });
                }
            } else {
                if ($order->email) {
                    Mail::send('emails.statusChange', ['text' => $text], function ($message) use ($order, $subject) {
                        $message->to($order->email)->subject($subject);
                    });
                }
            }
        } catch (\Exception $e) {
            Log::error('OrderController | Status change email notification sending failed for order ' . $order->id . ', caused by ' . $e->getMessage());
        }
    }

    /**
     * Returns email subject in specified language.
     *
     * @param $lang
     * @param $orderId
     * @return string
     */
    public static function getEmailSubject($lang, $orderId)
    {
        if ($lang == 'en' || $lang == 'text_en') {
            $subject = "Order number $orderId status from greenest.ee";
        } else {
            $subject = "Teavitus greenest.ee tellimuse $orderId kohta";
        }

        return $subject;
    }

    /**
     * Changes order item status.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeOrderItemStatus(Request $request)
    {
        $field = $request->get('statusName');

        $orderItem = OrderItem::find($request->get('itemId'));

        $orderItem->$field = $request->get('value') == 'true' ? 1 : 0;

        $orderItem->save();

        return response()->json(['id' => $request->get('itemId')]);
    }

    /**
     * Receives client answer on missing product.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function receiveClientAnswerOnMissingProduct(Request $request)
    {
        try {
            $choice = $request->get('choice');

            if ($choice) {
                $productIds = explode(',' , $request->get('productIds'));

                $order = Order::find($request->get('orderId'));
                $products = Product::whereIn('id', $productIds)->select(['id', 'price', 'name'])->get();

                if ($order->email) {
                    if (!\DB::table('missing_product_choice')->where('order_id', $order->id)->whereIn('product_id', $productIds)->exists()) {
                        if ($choice == 'refund') {
                            Mail::send('emails.notification-about-client-choice-to-refund',
                                ['products' => $products, 'order' => $order],
                                function ($message) use ($order) {
                                    $message->to('valdo@greenest.ee')->subject("Order number $order->id from greenest.ee");
                                });
                            Log::info('OrderController | Client choice notification sent to admin for order ' . $order->id);
                        } elseif ($choice == 'includeInNextOrder') {
                            Order::where('id', $order->id)->update(['status' => 'partially_completed_and_forwarded']);
                            Log::info('OrderController | Client choose product to be included in next order: order #' . $order->id);
                        }

                        $timestamp = Carbon::now()->format('Y-m-d H:i:s');

                        $arrayToInsert = [];
                        foreach ($products as $product) {
                            $arrayToInsert[] = [
                                'answer' => $choice,
                                'created_at' => $timestamp,
                                'updated_at' => $timestamp,
                                'order_id' => $order->id,
                                'product_id' => $product->id,
                                'item_id' => ''
                            ];
                        }

                        \DB::table('missing_product_choice')->insert($arrayToInsert);
                    }
                }
            }

        } catch (\Exception $e) {
            Log::error('OrderController | Client choice received with error for order ' . $order->id . ', caused by ' . $e->getMessage());
        }

        return redirect()->back();
    }

    /**
     * Send notification to client about missing product.
     *
     * @param Request $request
     */
    public static function sendMissingProductNotification(Request $request)
    {
        try {
            $orderId = $request->get('orderId');

            $orderItems = OrderItem::where('order_id', $orderId)
                ->where('missing', 1)
                ->leftJoin('products', 'order_items.product_id', '=', 'products.id')
                ->select(['products.name', 'products.price', 'products.name_et', 'order_items.id', 'order_items.product_id', 'order_items.order_id'])
                ->get();

            $productsIds = implode(',',$orderItems->pluck('product_id')->toArray());

            $order = Order::find($orderItems->first()->order_id);
            $lang = ($order->shipping_method == 'eu_shipping') ? 'en' : 'et';

            $subject = self::getEmailSubject($lang, $order->id);

            if ($order->email) {
                if ($lang == 'en') {
                    Mail::send('emails.missing-product', ['order' => $order, 'orderItems' => $orderItems, 'productsIds' => $productsIds],
                        function ($message) use ($order, $subject) {
                            $message->to($order->email)->subject($subject);
                        });
                } else {
                    Mail::send('emails.missing-product_et', ['order' => $order, 'orderItems' => $orderItems, 'productsIds' => $productsIds],
                        function ($message) use ($order, $subject) {
                            $message->to($order->email)->subject($subject);
                        });
                }

                Log::info('OrderController | Client choice email notification sent for order ' . $order->id);
            }

        } catch (\Exception $e) {
            Log::error('OrderController | Client choice email notification sending failed for order ' . $order->id . ', caused by ' . $e->getMessage());
        }
    }

    public function sendDelayedOrdersNotification(Request $request)
    {
        $orderIds = $request->get('orderIds', []);

        foreach ($orderIds as $orderId) {
            dispatch(new SendDelayedOrderNotification($orderId));
        }

        return response()->json(['status' => 'success']);
    }

    public function makeDPDLabels(Request $request)
    {
        $service = new DPD();

        $pdpParcelIds = ParcelTerminal::dpd()->pluck('id')->toArray();
        $sortFrom = isset($request->sortFrom) ? ($request->sortFrom !== "" ? $request->sortFrom : null) : null;
        $sortTo = isset($request->sortTo) ? ($request->sortTo !== "" ? $request->sortTo : null) : null;

        if (explode(' ', $request->package_ids)[0] != '') {
            $shipments = explode(' ', $request->package_ids);
        } else {
            $orders = Order::where(function ($q) {
                $q->whereNotIn('status', ['new', 'rejected'])
                    ->orWhere(function ($q) {
                        $q->where('mollie_payment_status', '=', 'Paid')
                            ->where('status', '=', 'new');
                    });
            })->where(function ($q) use (&$pdpParcelIds) {
                $q->where(function ($qu) use (&$pdpParcelIds) {
                    $qu->where('shipping_method', 'parcel_terminal')
                        ->whereIn('shipping_parcel_terminal_id', $pdpParcelIds);
                })->orWhere('shipping_method', 'courier_estonia');
            });

            if (explode(' ', trim($request->ids))[0] != "") {
                $ordersIds = explode(' ', trim($request->ids));
                $orders = $orders->whereIn('id', $ordersIds);
            } else {
                if ($sortFrom !== null) {
                    $orders = $orders->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $sortFrom)->format('Y-m-d 00:00:01'));
                }
                if ($sortTo !== null) {
                    $orders = $orders->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $sortTo)->format('Y-m-d 23:59:59'));
                }

            }
            $orders = $orders->get();
            if ($orders->count() == 0) {
                $this->errors[] = "No orders available.";
                return redirect()->back();
            }

            $shipments = [];

            /** @var Order $order */
            foreach ($orders as $order) {
                if (empty($order->shipment)) {
                    $shipment = $order->sendToDPD($service);
                    if (!empty($shipment)) {
                        $shipments[] = $shipment;
                    }
                    continue;
                }

                if ($order->shipment()->orderBy('created_at', 'desc')->first()->provider == 'DPD') {
                    $shipments[] = $order->shipment()->orderBy('created_at', 'desc')->first()->shipment_id;
                }
            }
        }

        $shipments = array_chunk($shipments, 199);

        $pdfs = [];

        foreach ($shipments as $chunkShipments) {
            $rawIds = implode('|', $chunkShipments);

            $pdfs[] = $service->printLabels($rawIds);

            OrderShipment::where('provider', 'DPD')->whereIn('shipment_id', $chunkShipments)->update(['printed' => 1]);
        }

        $zipper = new \ZipArchive();
        $zipPath = public_path('storage/tmp/dpd_cards_archive.zip');
        if (file_exists($zipPath)) {
            @unlink($zipPath);
        }

        $zipper->open($zipPath, \ZipArchive::CREATE);
        foreach ($pdfs as $pdf) {
            if (file_exists($pdf)) {
                $zipper->addFromString(basename($pdf), file_get_contents($pdf));
                @unlink($pdf);
            }
        }
        $zipper->close();

        if (file_exists($zipPath)) {
            chmod($zipPath, 0755);
            return response()->download($zipPath);
        }

        return redirect()->back();
    }

    public function viewListDPDShipping(Request $request)
    {
        $shipments = OrderShipment::where('order_shipments.provider', 'DPD')
            ->join('orders', 'orders.id', '=', 'order_shipments.order_id')
            ->leftJoin('parcel_terminals', 'parcel_terminals.id', '=', 'orders.shipping_parcel_terminal_id')
            ->select(
                'order_shipments.*',
                'orders.name',
                'orders.shipping_address',
                'orders.shipping_method',
                'orders.shipping_city',
                'orders.created_at as ordered_at',
                'orders.shipping_country',
                'orders.shipping_zip_code',
                'orders.shipping_parcel_terminal_phone',
                'parcel_terminals.country as parcel_country',
                'parcel_terminals.city as parcel_city',
                'parcel_terminals.address as parcel_address',
                'parcel_terminals.postalcode as parcel_postalcode'
            )->orderBy('created_at', 'desc')->get();

        $deliveredStatesShipment = [
            'Picked up by Consignee from Pickup point'
        ];

        return view('vendor.voyager.dpd-shipment.browse',
            [
                'shipments' => $shipments,
                'deliveredStatesShipment' => $deliveredStatesShipment
            ]
        );
    }

    public function rejectDPDShipment(Request $request)
    {
        $service = new DPD();

        if ($service->rejectShipment($request->shipment_id)) {
            OrderShipment::where('provider', 'DPD')->where('shipment_id', $request->shipment_id)->delete();
        }

        return redirect()->back();
    }

    public function renewDPDShipment(Request $request)
    {
        /** @var Order $order */
        $order = Order::find($request->get('order_id'));
        if (!empty($order)) {
            $order->sendToDPD();
        }

        return redirect()->back();
    }

    public function changeOrderItemComment(Request $request)
    {
        $item = OrderItem::find($request->get('itemId', null));
        if (empty($item)) {
            return response()->json(['status' => 'error', 'message' => 'Empty item']);
        }

        $item->comment = $request->get('comment', '');
        $item->comment_link_text = $request->get('linkText', '');
        $item->comment_link = $request->get('link', '');
        $item->save();

        return response()->json(['status' => 'success']);
    }

    public function completePartiallyOrder(Request $request)
    {
        $order = Order::find($request->get('orderId'));

        if (empty($order)) {
            return response()->json(['status' => 'error', 'message' => 'empty order']);
        }

        if (!in_array($order->status, ['partially_completed', 'partially_completed_and_forwarded'])) {
            return response()->json(['status' => 'error', 'message' => 'bad status changing: ' . $order->status]);
        }

        $order->status = 'completed';
        $order->save();
        return response()->json(['status' => 'success']);
    }

    public function changeOrderItemQuantity(Request $request)
    {
        if (!$request->has('itemId') || !$request->has('quantity')) {
            return response()->json(['status' => 'error', 'message' => 'Excepted required parameter']);
        }

        $item = OrderItem::find($request->get('itemId'));
        if (empty($item)) {
            return response()->json(['status' => 'error', 'message' => 'Not found item']);
        }

        $order = Order::find($item->order_id);
        if (empty($order)) {
            return response()->json(['status' => 'error', 'message' => 'Not found order']);
        }

        $oneItemPrice = $item->sum_total / $item->quantity;
        $quantityDiff = $request->get('quantity') - $item->quantity;

        $order->sum_total += $quantityDiff * $oneItemPrice;
        $order->subtotal += round(($quantityDiff * $oneItemPrice) / 1.2, 2);
        $order->tax += ($quantityDiff * $oneItemPrice - round(($quantityDiff * $oneItemPrice) / 1.2, 2));

        $item->quantity = $request->get('quantity');
        $item->price += round(($quantityDiff * $oneItemPrice) / 1.2, 2);
        $item->sum_total += $quantityDiff * $oneItemPrice;

        $item->save();
        $order->save();

        $response = [
            'status' => 'success',
            'item' => [
                'quantity' => $item->quantity,
                'subtotal' => floor($item->price * 100) / 100,
                'total' => $item->sum_total,
            ],
            'order' => [
                'subtotal' => $order->subtotal,
                'tax' => $order->tax,
                'total' => $order->sum_total,
            ],
        ];

        return response()->json($response);
    }
}
