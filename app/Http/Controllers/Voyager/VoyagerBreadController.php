<?php

namespace App\Http\Controllers\Voyager;

use App\BuyerListStatistic;
use App\CategoryProduct;
use App\Classes\FrozenFoodConstant;
use App\Classes\RefrigeratedFoodConstant;
use App\OrderItem;
use App\Repositories\BuyerStatisticRepository;
use App\Traits\ShortDescriptionEditorTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Category;
use App\Product;
use TCG\Voyager\Models\DataType;

class VoyagerBreadController extends BaseVoyagerBreadController
{
    use ShortDescriptionEditorTrait;

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('browse_'.$dataType->name);

        $getter = $dataType->server_side ? 'paginate' : 'get';

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            $relationships = $this->getRelationships($dataType);

            if ($model->timestamps) {
                if (get_class($model) == 'App\Product') {
                    $moderationProducts =
                        CategoryProduct::where('category_id', Category::where('name', '=', 'moderation')->value('id'))
                            ->pluck('product_id')->toArray();
                    if ($request->has('q')) {
                        $dataTypeContent = $model->with($relationships)
                            ->whereNotIn('id', $moderationProducts)
                            ->where('name', 'like', '%' . $request->q . '%')
                            ->orWhere('brand', 'like', '%' . $request->q . '%')
                            ->orWhere('sku', $request->q)
                            ->orderBy('is_blacklist', 'asc')
                            ->orderBy('id', 'desc')

                            ->paginate(50);
                    } else {
                        $dataTypeContent = $model->with($relationships)
                            ->whereNotIn('id', $moderationProducts)
                            ->orderBy('is_blacklist', 'asc')
                            ->orderBy('id', 'desc')
                            ->paginate(50);
                    }
                } elseif (get_class($model) == 'App\ImportedProduct') {
                    $moderationProducts =
                        CategoryProduct::where('category_id', Category::where('name', '=', 'moderation')->value('id'))
                            ->pluck('product_id')->toArray();
                    if ($request->has('q')) {
                        $dataTypeContent = $model->with($relationships)
                            ->whereIn('id', $moderationProducts)
                            ->where('name', 'like', '%' . $request->q . '%')
                            ->orWhere('brand', 'like', '%' . $request->q . '%')
                            ->orWhere('sku', $request->q)
                            ->orderBy('is_blacklist', 'asc')
                            ->paginate(50);
                    } else {
                        $dataTypeContent = $model->with($relationships)
                            ->whereIn('id', $moderationProducts)
                            ->orderBy('is_blacklist', 'asc')
                            ->orderBy('id', 'desc')
                            ->paginate(50);
                    }
                } elseif (get_class($model) == 'App\Order') {
                    if ($request->has('q') && $request->has('sort')) {
                        list($column, $order) = explode('.', $request->sort);
                        $productIds = Product::where('name', 'like', '%' . $request->q . '%')
                            ->orWhere('brand', 'like', '%' . $request->q . '%')
                            ->orWhere('sku', $request->q)->pluck('id')->toArray();
                        $orderIds = array_unique(OrderItem::whereIn('product_id', $productIds)->pluck('order_id')->toArray());
                        $dataTypeContent = $model->with($relationships)
                            ->where('name', 'LIKE', '%' . $request->q . '%')
                            ->orWhere('phone', 'LIKE', '%' . $request->q . '%')
                            ->orWhereIn('id', $orderIds)
                            ->orderBy($column, $order)
                            ->paginate(50);
                    } elseif ($request->has('sort')) {
                        list($column, $order) = explode('.', $request->sort);
                        $dataTypeContent = $model->with($relationships)
                            ->orderBy($column, $order)
                            ->paginate(50);
                    } elseif ($request->has('q')) {
                        $productIds = Product::where('name', 'like', '%' . $request->q . '%')
                            ->orWhere('brand', 'like', '%' . $request->q . '%')
                            ->orWhere('sku', $request->q)->pluck('id')->toArray();
                        $orderIds = array_unique(OrderItem::whereIn('product_id', $productIds)->pluck('order_id')->toArray());
                        $dataTypeContent = $model->with($relationships)
                            ->where('name', 'LIKE', '%' . $request->q . '%')
                            ->orWhere('phone', 'LIKE', '%' . $request->q . '%')
                            ->orWhereIn('id', $orderIds)
                            ->latest()
                            ->paginate(50);
                    } else {
                        $dataTypeContent = call_user_func([$model->with($relationships)->latest(), $getter]);
                    }
                    $column = isset($column) ? $column : 'created_at';
                } else {
                	$dataTypeContent = call_user_func([$model->with($relationships)->latest(), $getter]);
            	}
            } else {
                $dataTypeContent = call_user_func([$model->with($relationships)->orderBy('id', 'DESC'), $getter]);
            }

            //Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        if (isset($column)) {
            $array = compact('dataType', 'dataTypeContent');
            $array['orderBy'] = $column;
            if (isset($order)) {
                $array['order'] = $order;
            }
            if ($request->has('q')) {
                $array['q'] = $request->q;
            }
            return view($view, $array);
        } else {
            if ($view === 'voyager::buyer-list-statistics.browse') {
                $buyerRep = new BuyerStatisticRepository();
                $percent = $buyerRep->getPercentPermanentBuyer(null);
                $dataTypeContent = $dataTypeContent->sortByDesc('total_turnover')->values();
                $years = $buyerRep->getYears();
                return view($view, compact('dataType', 'dataTypeContent', 'percent', 'years'));
            }
            return view($view, compact('dataType', 'dataTypeContent'));
        }
    }

    public function getTopBuyerPeriod(Request $request)
    {
        $buyerRep = new BuyerStatisticRepository();
        $year = request('year');

        if (request('year') === 'all_time') {
            $data = BuyerListStatistic::all();
            $dataTypeContent = $data->sortByDesc('total_turnover')->values();
        } else {
            $data = $buyerRep->getOrderStat($year);

            foreach ($data as $key => $val) {
                $data[$key] = (object) $val;
            }

            $dataTypeContent = collect($data)->sortByDesc('total_turnover')->values();
        }

        $dataType = DataType::where('name', 'buyer_list_statistics')->first();
        $view = 'voyager::buyer-list-statistics.browse';
        $percent = $buyerRep->getPercentPermanentBuyer($year);
        $years = $buyerRep->getYears();

        return view($view, compact('percent', 'dataTypeContent', 'dataType', 'years'));
    }

        // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        if ($dataType->name == 'products') {
            foreach (config('languages.locale') as $locale => $name) {
                $dataSlugFieldName = $locale == 'et' ? 'slug_et' : 'slug';
                Cache::forget("page_product_data_{$data->{$dataSlugFieldName}}_{$locale}");
            }

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = Str::random(20);
                $path = $slug.'/'.date('F').date('Y').'/';
                $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();
                $file->storeAs("/public/{$path}", $filename.'.'.$file->getClientOriginalExtension());
                $data->image = $fullPath;
                $request->offsetUnset('image');
            }

            if (!$data->categories()->where('categories.id', 62)->exists()
                && in_array(62, $request->get('categories', []))) {
                $data->short_description = FrozenFoodConstant::SHORT_DESCRIPTION;
                $data->short_description_et = FrozenFoodConstant::SHORT_DESCRIPTION_ET;

                $request->offsetUnset('short_description');
                $request->offsetUnset('short_description_et');
            }

            if (!$data->categories()->where('categories.id', 116)->exists()
                && in_array(116, $request->get('categories', []))) {
                $data->short_description = RefrigeratedFoodConstant::SHORT_DESCRIPTION;
                $data->short_description_et = RefrigeratedFoodConstant::SHORT_DESCRIPTION_ET;

                $category = Category::find(116);
                if (!empty($category) && !empty($category->available_from)) {
                    $date = Carbon::parse($category->available_from)->format('d.m.Y');

                    $data->delivery_date = $category->available_from;
                    $data->status = Product::PRODUCT_NOT_IN_STOCK;

                    $data->short_description = $this->getShortDescription(
                        $data->short_description, 'Out of stock! Expected arrival date ' . $date, false);
                    $data->short_description_et = $this->getShortDescription(
                        $data->short_description_et, 'Läbi müüdud! Eeldatav saabumiskuupäev  ' . $date, false);

                    $request->offsetUnset('delivery_date');
                    $request->offsetUnset('status');
                }

                $request->offsetUnset('short_description');
                $request->offsetUnset('short_description_et');
            }

            if ($request->has('active_additional_description')) {
                $data->active_additional_description = 1;
                $request->offsetUnset('active_additional_description');
            } else {
                $data->active_additional_description = 0;
            }

            if ($request->has('calculate_by_weight_price')) {
                $data->calculate_by_weight_price = 1;
                $request->offsetUnset('calculate_by_weight_price');
            } else {
                $data->calculate_by_weight_price = 0;
            }

            $data->save();
        }

        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        if($request->has('is_ajax') && $request->get('is_ajax')){
            return response()->json(['status' => 'success']);
        }

        if($request->has('action-close')){
            return redirect($request->get('action-close'))
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

        if($slug == 'order-status-notifications'){
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $id])
            ->with([
                'message'    => "Successfully Updated {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function updateImported(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        if($request->has('action-close')){
            return redirect($request->get('action-close'))
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

        return redirect()
            ->route("voyager.imported-products.edit", ['id' => $id])
            ->with([
                'message'    => "Successfully Updated {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    // POST BRE(A)D
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('add_'.$dataType->name);

        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        if($request->has('action')){
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => "Successfully Updated {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.edit", ['id' => $data->id])
            ->with([
                'message'    => "Successfully Added New {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }


    // move to blacklist
    public function setBlackList($productId)
    {
        $product = Product::where('id', '=', $productId)->withTrashed()->first();
        if ($product->is_active == 1){
            $product->is_active = 0;
            $product->is_blacklist = 1;
            $msg = 'on';
        } else {
            $product->is_active = 1;
            $product->is_blacklist = 0;
            $msg = 'off';
        }
        $product->save();

        return $msg;
    }


    /**
     *
     */
    public function getCategoryDisable(Category $category)
    {
        $category->is_active = 0;
        $category->save();

        return redirect()->back();
    }

    /**
     *
     */
    public function getCategoryActivate(Category $category)
    {
        $category->is_active = 1;
        $category->save();

        return redirect()->back();
    }

    /**
     *
     */
    public function getProductDisable(Product $product)
    {
        $product->is_active = 0;
        $product->save();

        return redirect()->back();
    }

    /**
     *
     */
    public function getProductActivate(Product $product)
    {
        $product->is_active = 1;
        $product->save();

        return redirect()->back();
    }

}
