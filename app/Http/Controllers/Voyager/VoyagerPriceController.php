<?php

namespace App\Http\Controllers\Voyager;

use App\CategoryProduct;
use App\Product;
use App\ProductTmp;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;
use TCG\Voyager\Facades\Voyager;

class VoyagerPriceController extends BaseVoyagerBreadController
{
    public function acceptNewPrice($productId)
    {
        $productTmp = ProductTmp::where('id', '=', $productId)->first();
        $product = Product::where('sku', '=', $productTmp->sku)->first();

        $product->price = $productTmp->price;
        $product->purchase_price = $productTmp->purchase_price;
        $product->save();
        $productTmp->delete();
//        return redirect()->to(route('voyager.confirmPrice.index'));
        return response()->json(['status' => 'success']);
    }

    public function editNewPrice($productId, Request $request)
    {
        $productTmp = ProductTmp::where('id', '=', $productId)->first();
        $product = Product::where('sku', '=', $productTmp->sku)->first();

        $product->price = $request->new_retail_price;
        $product->purchase_price = $productTmp->purchase_price;
        $product->save();
        $productTmp->delete();
//        return redirect()->to(route('voyager.confirmPrice.index'));
        return response()->json(['status' => 'success']);
    }

    public function rejectNewPrice($productId)
    {
        $productTmp = ProductTmp::where('id', '=', $productId)->first();
        $product = Product::where('sku', '=', $productTmp->sku)->first();

        $product->purchase_price = $productTmp->purchase_price;

        $product->save();
        $productTmp->delete();
//        return redirect()->to(route('voyager.confirmPrice.index'));
        return response()->json(['status' => 'success']);
    }

    public function deleteProductTmp(Request $request)
    {
        $productTmp = ProductTmp::find($request->id);
        $productTmp->delete();
        return redirect()->back();
    }

    public function getConfirmPrice(Request $request)
    {
        $dataType = Voyager::model('DataType')->where('slug', '=', 'products')->first();

        $moderationProducts = CategoryProduct::where('category_id', 98)->pluck('product_id')->toArray();
        $dataTypeContent = Product::join('product_tmp', function($join) {
                $join->on('product_tmp.sku', '=', 'products.sku');
                $join->on('product_tmp.name', 'LIKE', 'products.name');
            })
            ->whereNotIn('products.id', $moderationProducts)
            ->select([
                'product_tmp.id as id',
                'product_tmp.is_active as is_active',
                'product_tmp.brand as brand',
                'product_tmp.sku as sku',
                'product_tmp.name as name',
                'product_tmp.price as price_new',
                'product_tmp.purchase_price as purchase_price_new',
                'product_tmp.amount as amount',
                'product_tmp.is_promoted as is_promoted',
                'product_tmp.delivery_date as delivery_date',
                'products.price as price_old',
                'products.purchase_price as purchase_price_old',
                'products.id as old_id',
                'products.is_blacklist as is_blacklist'
            ])
            ->orderBy('product_tmp.updated_at', 'desc')
            ->paginate(50);

        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);

        return view('vendor.voyager.confirm-price.confirm-price')
            ->with([
                'namePage' => 'Confirm Price',
                'dataTypeContent' => $dataTypeContent,
                'dataType' => $dataType,
                'countAllCompareProducts' => $dataTypeContent->total()
            ]);
    }

    /**
     * Gets list of products that prices were changed.
     *
     * @return VoyagerPriceController|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|\TCG\Voyager\Http\Controllers\Traits\Can
     */
    public function getConfirmPriceProductsList()
    {
        $model = app('App\Product');
        $dataType = Voyager::model('DataType')->where('slug', '=', 'products')->first();

        $relationships = $this->getRelationships($dataType);
        if ($model->timestamps) {
            if (get_class($model) == 'App\Product') {
                $moderationProducts = CategoryProduct::where('category_id', 98)->pluck('product_id')->toArray();
                $dataTypeContent = $model->with($relationships)
                    ->join('product_tmp', function($join) {
                        $join->on('product_tmp.sku', '=', 'products.sku');
                        $join->on('product_tmp.name', 'LIKE', 'products.name');
                    })
                    ->whereNotIn('products.id', $moderationProducts)
                    ->select([
                        'product_tmp.id as id',
                        'product_tmp.is_active as is_active',
                        'product_tmp.brand as brand',
                        'product_tmp.sku as sku',
                        'product_tmp.name as name',
                        'product_tmp.price as price_new',
                        'product_tmp.purchase_price as purchase_price_new',
                        'product_tmp.amount as amount',
                        'product_tmp.is_promoted as is_promoted',
                        'product_tmp.delivery_date as delivery_date',
                        'products.price as price_old',
                        'products.purchase_price as purchase_price_old',
                        'products.id as old_id',
                        'products.is_blacklist as is_blacklist'
                    ])
                    ->get();
            }
        }

        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);

        return $dataTypeContent;
    }
}
