<?php

namespace App\Http\Controllers\Voyager;

use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {
        if (\Auth::user()->role_id === 3) {
            return redirect()->to(url('en/admin/blog-posts'));
        }

        return view('voyager::index');
    }
}
