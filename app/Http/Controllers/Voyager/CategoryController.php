<?php

namespace App\Http\Controllers\Voyager;

use App\Category;
use App\CategoryAvailableImmediatelyStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $mainCategories = Category::with('activationStatus')
            ->select('name', 'id')
            ->where('is_main', 1)
            ->where('is_active', 1)
            ->get();

        return view('vendor.voyager.categories.available-immediately')->with(['categories' => $mainCategories]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $category = Category::where('slug', 'available-immediately')->first();

        return view('vendor.voyager.categories.available-immediately-edit')->with(['category' => $category]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $arrToInsert = [];

        foreach (request('activation-statuses') as $categoryId => $categoryStatus) {
            $arrToInsert[$categoryId][]['category_id'] = $categoryId;
            $arrToInsert[$categoryId][]['is_active'] = $categoryStatus;
        }

        foreach ($arrToInsert as $item) {
            list($condition, $data) = $item;
            CategoryAvailableImmediatelyStatus::updateOrCreate($condition, $data);
        }

        if (request()->has('action-close')) {
            return redirect(request('action-close'))
                ->with([
                    'message'    => "Available Immediately Category Successfully Updated",
                    'alert-type' => 'success',
                ]);
        }

        return redirect()->route('available-immediately.index')
            ->with([
                'message'    => "Available Immediately Category Successfully Updated",
                'alert-type' => 'success',
            ]);
    }
}
