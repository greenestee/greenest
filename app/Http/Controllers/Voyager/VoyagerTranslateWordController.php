<?php

namespace App\Http\Controllers\Voyager;

use App\TranslateWords;
use Illuminate\Http\Request;

class VoyagerTranslateWordController extends VoyagerController
{

    public function editTranslateWord($idWord, Request $request)
    {
        $translateItem = TranslateWords::where('id', '=', $idWord)->first();

        $translateItem->word_en = $request->word_en;
        $translateItem->word_et = $request->word_et;
        $translateItem->save();
        return redirect()->back();
    }

    public function removeTranslateWord($idWord)
    {
        $translateItem = TranslateWords::where('id', '=', $idWord)->first();
        $translateItem->delete();
        return redirect()->back();
    }

    public function addTranslateWord(Request $request)
    {
        $translateItem = TranslateWords::where('word_en', '=', $request->word_en)->first();

        if ($translateItem == NULL) {
            $translateItem = new TranslateWords;
        }
        $translateItem->word_en = $request->word_en;
        $translateItem->word_et = $request->word_et;
        $translateItem->save();
        return redirect()->back();
    }

    public function getViewTranslate()
    {
        $words = TranslateWords::get();

        return view('vendor.voyager.translate.translate-word')->with([
            'words' => $words
        ]);
    }

    public function getWordTranslate($wordEn)
    {
        $translateWordItem = TranslateWords::where('word_en', '=', $wordEn)->first();

        return response()->json([
            'success' => true,
            'word_et' => $translateWordItem->word_et
        ]);
    }

    public function getTranslateText(Request $request)
    {
        $viewText = "";
        $texts = \GuzzleHttp\json_decode($request->text);
        foreach ($texts as $text) {
            $text = trim($text);
            if ($text)
                $viewText .= $this->splitTextAndCreateViewText($text);
        }
        return response()->json([
            'text' => $viewText
        ]);
    }

    private function splitTextAndCreateViewText($line)
    {
        $viewLine = "";
        $words = explode(' ', $line);
        $untranslateWord = "";
        foreach ($words as $index => $word) {
            if ($untranslateWord == '') {
                $mass = $this->translateWord($word);
            }
            else {
                $mass = $this->translateWord($untranslateWord . $word);
                $untranslateWord = '';
            }

            if ($mass['status'] == 0) {

                $untranslateWord .= $mass['word'] . ' ';

            }

            if ($mass['status'] == 1) {
                $viewLine .=  $mass['word'];
                $untranslateWord = '';
            }

        }
        $viewLine .= ' ' . $untranslateWord;
        return '<p>' .$viewLine . '</p>';
    }

    private function translateWord($word)
    {
        $word = trim($word);
        $mass['status'] = 0;

        $translateItem = TranslateWords::where('word_en', '=', $word)->first();
        if ($translateItem)
            $translateItem = TranslateWords::where('word_en', '=', $word)->first();

        if ($translateItem) {
            $mass['status'] = 1;

            $word = $translateItem->word_et;
        }
        $mass['word'] = $word;
        return $mass;
    }
}
