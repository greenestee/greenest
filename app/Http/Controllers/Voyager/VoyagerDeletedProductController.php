<?php

namespace App\Http\Controllers\Voyager;

use App\Category;
use App\CategoryProduct;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;
use TCG\Voyager\Facades\Voyager;

class VoyagerDeletedProductController extends BaseVoyagerBreadController
{
    public function getDeletedProduct(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', 'products')->first();

        $getter = $dataType->server_side ? 'paginate' : 'get';

        if (strlen($dataType->model_name) != 0) {
            $model = app('App\Product');

            $relationships = $this->getRelationships($dataType);
            if ($model->timestamps) {
                if (get_class($model) == 'App\Product') {
                    $moderationCategories = Category::where('name', 'moderation')->pluck('id')->toArray();
                    $moderationProducts = CategoryProduct::where('category_id', $moderationCategories)->pluck('product_id')->toArray();
                    $dataTypeContent = $model->with($relationships)
                        ->whereNotIn('id', $moderationProducts)
                        ->select([
                            'product.id as id',
                            'product.is_active as is_active',
                            'product.brand as brand',
                            'product.sku as sku',
                            'product.name as name',
                            'product.price as price',
                            'product.purchase_price as purchase_price',
                            'product.amount as amount',
                            'product.is_promoted as is_promoted',
                            'product.delivery_date as delivery_date',
                            'products.is_blacklist as is_blacklist'
                        ])
                        ->paginate(50);
                }
            }
            //Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        }
        $countAllCompareProducts = ProductTmp::count();
        return view('vendor.voyager.deleted.deletedProduct')
            ->with([
                'namePage' => 'Confirm Price',
                'dataTypeContent' => $dataTypeContent,
                'dataType' => $dataType,
                'countAllCompareProducts' => $countAllCompareProducts
            ]);
    }

    public function index(Request $request)
    {
        $dataType = Voyager::model('DataType')->where('slug', '=', 'products')->first();

        if ($request->has('q')) {
            $dataTypeContent = Product::where('name', 'like', '%' . $request->q . '%')
                ->orWhere('brand', 'like', '%' . $request->q . '%')
                ->orWhere('sku', $request->q)
                ->orderBy('is_blacklist', 'asc')
                ->onlyTrashed()
                ->paginate(50);
        } else {
            $dataTypeContent = Product::onlyTrashed()
                ->paginate(50);
        }
        $view = "vendor.voyager.deleted.deletedProduct";

        return view('vendor.voyager.deleted.deletedProduct')
            ->with([
                'namePage' => 'Confirm Price',
                'dataTypeContent' => $dataTypeContent,
                'dataType' => $dataType,
            ]);
    }

    public function editDeletedProduct($id)
    {
        $slug = 'products';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->onlyTrashed()->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->withTrashed()->first(); // If Model doest exist, get data from table name

        $view = "vendor.voyager.deleted.editDeletedProducts";
        return view($view, compact('dataType', 'dataTypeContent'));
    }

    public function update(Request $request, $id)
    {
        $slug = 'products';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        Voyager::canOrFail('edit_'.$dataType->name);

        $data = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->onlyTrashed()->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->withTrashed()->first();;

        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        return redirect()
            ->route("voyager.deleted.product.index")
            ->with([
                'message'    => "Successfully Updated {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}