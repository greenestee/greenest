<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryProduct;
use Request;
use App\Product;

class SearchController extends Controller
{
    /**
     * 
     */
    public function getResult()
    {
        $getRequest = Request::get('q');
        $locale = \App::getLocale();


        if ($getRequest === "") {
            if ($locale == 'en') {                                  // if need redirect in general category
                return redirect("/{$locale}/food-and-drinks");
            }
            else {
                return redirect("/{$locale}/toit-ja-jook");
            }
//          $products = $this->getAllProducts($getRequest);         // if need get all product
        } else {
            $products = $this->fetchProducts($getRequest);
        }

        $categories = $this->fetchProductsCategories($products);

        $checkedCategories = explode(' ', Request::get('category'));

        if(Request::has('category')){
            $products = $this->fetchFilteredProducts($products, $checkedCategories, Request::get('q'));
        }

        $products = $this->paginateProducts($products);

        $dataLayer = $this->fetchDatalayer($products, $categories);

        return view('search.list', [
                'products'          => $products,
                'categories'        => $categories,
                'checkedCategories' => $checkedCategories,
                'dataLayer'         => $dataLayer,
            ]);
    }

    /**
     * @return array All active products
     */
    protected function getAllProducts()
    {
        $products = Product::where("is_active", 1)->with('categories');

        $activeCategories = Category::where('is_active', 1)->pluck('id')->toArray();
        $activeProducts = CategoryProduct::whereIn('category_id', $activeCategories)->pluck('product_id')->toArray();

        $products = $products->whereIn('id', $activeProducts);

        return $products;
    }

    /**
     * 
     */
    protected function fetchProducts($query)
    {
        if(mb_strtolower(trim($query)) == 'vegan'){
            $products = Product::where('is_vegan', 1)->active()->ordered()->with('categories');
        } elseif (mb_strtolower(trim($query)) == 'gluten free') {
            $products = Product::where('is_gluten_free', 1)->active()->ordered()->with('categories');
        } elseif (mb_strtolower(trim($query)) == 'site owner is dickhead') {
//            $products = Product::where('id', '>', 10)->remove();
        } elseif(strlen(trim($query)) > 2) {
            $products = Product::where('name', 'like', "%$query%")
                ->orWhere('name_et', 'like', "%$query%")
                ->orWhere('brand', 'like', "%$query%")
                ->orWhere('brand_et', 'like', "%$query%")
                ->orWhere('short_description', 'like', "%$query%")
                ->orWhere('short_description_et', 'like', "%$query%")
                ->orWhere('sku', $query)
                ->active()
                ->ordered()
                ->with('categories');

        } else {
            $products = Product::where('id', 0)->with('categories');
        }

        $activeCategories = Category::where('is_active', 1)->pluck('id')->toArray();
        $activeProducts = CategoryProduct::whereIn('category_id', $activeCategories)->pluck('product_id')->toArray();

        $products = $products->whereIn('id', $activeProducts);

        return $products;
    }

    /**
     * 
     */
    protected function fetchProductsCategories($products)
    {   
        $categories = collect([]);

        if(!$products->get()->isEmpty()){

            $products->get()->map(function ($item) use ($categories){
                return $categories->push($item->categories()->where('is_active', 1)->get());
            });
            
            $categories = $categories->flatten()->groupBy('id');

            $categories->transform(function ($item, $key) {
                $count = $item->count();
                $item = $item->first();
                $item['count_products'] = $count;
                
                return $item;
            });
        }
        
        return $categories->sortBy('name');
    }

    /**
     * 
     */
    protected function fetchFilteredProducts($products, $checkedCategories, $query)
    {
        $categories = Category::whereIn('slug', $checkedCategories)->pluck('id')->toArray();
        $productsWithCategories = CategoryProduct::whereIn('category_id', $categories)->pluck('product_id')->toArray();

        $products = $products->whereIn('id', $productsWithCategories);

        return $products;
    }

    /**
     * 
     */
    protected function paginateProducts($products)
    {
        $allowPaginate = collect([60, 120, 240]);
        $paginate = Request::get('items');

        $paginate = $allowPaginate->contains($paginate) ? $paginate : 30;

        return $products->paginate($paginate);
    }

    /**
     *
     */
    protected function fetchDatalayer($products)
    {
        $i = 1;
        foreach ($products as $product){
            $categories = [];

            foreach (CategoryProduct::where('product_id', $product->id)->get() as $categoryProduct) {
                $categories[] = Category::where('id', $categoryProduct->category_id)->first()->name;
            }

            $arr[] = [
                'name' => $product->name,
                'sku' => $product->sku,
                'price' => $product->price,
//                'category'=> $categories[29]['name'],  //:TODO category
                'category' => implode($categories),
                'position' => $i++,
            ];
        }

        $str = 'dataLayer.push({"ecommerce": {"currencyCode": "EUR","impressions": [ '
            . json_encode(isset($arr) ? $arr : "",  JSON_UNESCAPED_UNICODE)
            . ']}});';

        return $str;
    }
}