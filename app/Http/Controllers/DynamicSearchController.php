<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryProduct;
use App\Product;
use Illuminate\Http\Request;

class DynamicSearchController extends Controller
{
    public function index(Request $request)
    {
        $lang = request('lang') == 'et' ? '_et' : '';
        $productInUri = request('lang') == 'et' ? '/et/toode' : '/en/product';

        $searchText = $request->get('searchText');

        $searchText = mb_convert_encoding($searchText, 'HTML-ENTITIES', 'UTF-8'); //encode special letter, like õ

        $activeCategories = Category::where('is_active', 1)->pluck('id')->toArray();
        $activeProducts = CategoryProduct::whereIn('category_id', $activeCategories)->pluck('product_id')->toArray();
        $moderationProducts = CategoryProduct::where('category_id', 98)->pluck('product_id')->toArray();

        $results = Product::whereNotIn('id', $moderationProducts)
        ->whereIn('id', $activeProducts)
        ->where('name', 'LIKE', "%{$searchText}%")
        ->orWhere('name_et', 'LIKE', "%{$searchText}%")
        ->orWhere('brand', 'like', "%$searchText%")
        ->orWhere('brand_et', 'like', "%$searchText%")
        ->orWhere('short_description', 'like', "%$searchText%")
        ->orWhere('short_description_et', 'like', "%$searchText%")
        ->orWhere('sku', $searchText)
        ->active()
        ->ordered()
        ->select('id', "name{$lang} as name", "slug{$lang} as slug", 'price', 'image')
        ->orderBy('created_at', 'desc')
        ->get();

        $results->transform( function($item) use ($productInUri) {

            $item->image = url('/storage') . '/' . $item->image;
            $item->slug = url($productInUri) . '/' . $item->slug;
            return $item;

        });

        return response()->json([
            'resultsQuantity' => $results->count(),
            'results' => $results->take(5)
        ]);
    }
}
