<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'en/webhook-verification',
        'et/webhook-verification',
        'en/checkout/success/*',
        'et/checkout/success/*',
        'en/checkout/webhook/*',
        'et/checkout/webhook/*',
    ];
}
