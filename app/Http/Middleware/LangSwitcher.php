<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Session;


class LangSwitcher
{
    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax()) {
            $locale = $request->segment(1);

            if (!array_key_exists($locale, config('languages.locale'))) {
                $segments = $request->segments();

                $geoip = geoip($request->ip());

                if ($geoip['country'] == 'Estonia' && env('LOCALIZATION_BY_IP', false) == true) {
                    $segments[0] = 'et';
                    if (isset($segments[1]) && $segments[1] == 'product') {
                      $segments[1] = 'toode';
                    }
                } else {
                    $segments[0] = $this->app->config->get('app.fallback_locale');
                }

                return $this->redirector->to(implode('/', $segments));
            }

            $this->app->setLocale($locale);
        }


        return $next($request);
    }
}
