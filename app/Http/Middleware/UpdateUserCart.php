<?php

namespace App\Http\Middleware;

use App\Product;
use App\UserCart;
use Closure;
use Auth;

class UpdateUserCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            \Cart::destroy();
            foreach (UserCart::where('user_id', Auth::id())->orderBy('id')->get() as $item) {
                $product = Product::find($item['product_id']);
                if (empty($product)) {
                    continue;
                }
                $cartItem = \Cart::add([
                    'id' => $product->id,
                    'name' => $product->name,
                    'qty' => 1,
                    'price' => $product->price / 1.2,
                ]);

                $cartItem->associate('App\Product');
            }
        }
        return $next($request);
    }
}
