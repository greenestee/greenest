<?php

if(!function_exists('urlByCategory')){
	
	function urlByCategory($category, $q = null){

		if(Request::has('category')){
			
			$categories = explode(' ', Request::get('category'));

			if(in_array($category, $categories)){

				$categories = array_diff($categories, [$category]);
			
			} else {
				
				array_push($categories, $category);
			}

		} else {
			$categories = [$category];
		}

		if($q){
			return url()->current() . '?q=' . $q . '&category=' . implode('+', $categories);
		}

		return url()->current() . '?category=' . implode('+', $categories);
	}

}

if(!function_exists('getCartInfo')){
	
	function getCartInfo($lang = null) {

	    //default parameters
        $parameters = [];
        $domain = 'messages';
        $locale = $lang;

        switch (Cart::count()) {
            case 0:
                return trans('static_text.0_products', $parameters, $domain, $locale);
            case 1:
                return '1 '.trans('static_text.1_product', $parameters, $domain, $locale).' = ' . Cart::total() . ' €';
            default:
                return Cart::count() . ' '.trans('static_text.multiple_products', $parameters, $domain, $locale).' = ' . Cart::total() . ' €';
        }
	}

}

if(!function_exists('getProductsCount')){

	function getProductsCount(){
		return Cart::count();
	}
}



if(!function_exists('formatNumber')){
	
	function formatNumber($number){

		return number_format($number, 2, ',', ' ');
	}

}