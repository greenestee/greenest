<?php

namespace App\Observers;


use App\Product;

class ProductObserver
{
    /**
     * When trashed product sku value is updated and new SKU equals to any existing one, this happens:
     * Existing not trashed products removed and updated trashed is restored.
     *
     * @param Product $product
     */
    public function updated(Product $product)
    {
        if ($product->trashed() && $product->isDirty('sku')) {
            $productToBeDeleted = Product::where('sku', $product->sku)->first();

            if (isset($product)) {
                Product::withTrashed()->find($product->id)->update(['sku' => $productToBeDeleted->sku]);

                Product::where('sku', $productToBeDeleted->sku)->delete();
                \Log::info("Product #{$productToBeDeleted->id} deleted and replaced with #{$product->id}");

                Product::withTrashed()->find($product->id)->restore();
                \Log::info("Product #{$product->id} restored");
            }
        }
    }
}