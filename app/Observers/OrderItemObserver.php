<?php

namespace App\Observers;

use App\Http\Controllers\OrderController;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Log;

class OrderItemObserver
{
    public function updated(OrderItem $orderItem)
    {
        try {
            if ($orderItem->isDirty('in_stock')) {
                $order = Order::find($orderItem->order_id);
                $productId = $orderItem->product_id;

                if ($orderItem->getOriginal('in_stock') == 0 && $orderItem->in_stock == 1) {
                    OrderController::sendStatusUpdatingNotification($order, 5, false, $productId);
                }
            }
        } catch (\Exception $e) {
            Log::error('OrderItemObserver | Sending email notification that product is back in stock failed for order ' . $orderItem->order_id . ', caused by ' . $e->getMessage());
        }
    }
}