<?php

namespace App\Observers;

use App\Http\Controllers\OrderController;
use App\Order;
use App\OrderShipment;
use Illuminate\Support\Facades\Log;

class OrderShipmentObserver
{
    public function created(OrderShipment $orderShipment)
    {
        try {
            $order = Order::find($orderShipment->order_id);

            if ($order->status == 'completed') {
                OrderController::sendStatusUpdatingNotification($order, 3, true);
                Log::info('OrderShipmentObserver | Status change email notification sent for order ' . $order->id . '. Event: 3 - Smartpost');
            }
        } catch (\Exception $e) {
            Log::error('OrderShipmentObserver | Status change email notification failed for order ' . $order->id . ', caused by ' . $e->getMessage());
        }

    }
}