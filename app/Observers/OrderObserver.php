<?php

namespace App\Observers;

use App\Http\Controllers\OrderController;
use App\Http\Libraries\MeritAktiva\MeritAktiva;
use App\Http\Libraries\Montonio\MontonioPayment;
use App\Order;
use App\ParcelTerminal;
use App\Repositories\OrderRepository;
use App\SendedToMeritOrder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class OrderObserver
{
    public function updated(Order $order)
    {
        if (
            ($order->status == 'paid' || $order->mollie_payment_status == 'Paid')
            && !SendedToMeritOrder::where('order_id', '=', $order->id)->exists()
        ) {
            $output = MeritAktiva::setOrder($order);

            if (isset($output->CustomerId)) {
                $send = new SendedToMeritOrder();
                $send->order_id = $order->id;
                $send->paid = 0;
                $send->save();
            }

            $montonio = new MontonioPayment();

            $autoPaymentMethods = array_merge(
                ['bank_transfer', 'hire_purchase_swedbank', 'hire_purchase_seb', 'hire_purchase_lhv'],
                $montonio->getActiveMethods()
            );

            if (in_array($order->payment_method, $autoPaymentMethods) && !empty($send)) {
                $output = MeritAktiva::setPayment($order);

                if (isset($output->CustomerId)) {
                    $send->paid = 1;
                    $send->save();
                }
            }
        }

        try {
            if ($order->isDirty('status')) {

                if ($order->status == 'paid') {
                    OrderRepository::updateQuantitiesProductsInPaidProducts($order);
                    if (!empty($order->shipping_method) && $order->shipping_method == 'parcel_terminal') {
                        $order->sendToDPD();
                    }
                }

                if ($order->status == 'rejected') {
                    $order->removeFromDPD();
                }

                if ($order->getOriginal('status') == 'new' && $order->status == 'paid') {
                    OrderController::sendStatusUpdatingNotification($order, 1);
                    Log::info('OrderObserver | Status change email notification sent for order ' . $order->id . '. Event: 1');
                }

                if ($order->getOriginal('status') == 'paid' && $order->status == 'forwarded') {
                    OrderController::sendStatusUpdatingNotification($order, 2);
                    Log::info('OrderObserver | Status change email notification sent for order ' . $order->id . '. Event: 2');
                }

                if ($order->getOriginal('status') == 'paid' && $order->status == 'thursday_shipping') {
                    OrderController::sendStatusUpdatingNotification($order, 7);
                    Log::info('OrderObserver | Status change email notification sent for order ' . $order->id . '. Event: 7');
                }

                if ($order->getOriginal('status') == 'forwarded' && $order->status == 'completed') {
                    if ($order->shipping_method == 'parcel_terminal') {
                        if (\DB::table('parcel_terminals')->where('id', $order->shipping_parcel_terminal_id)->first()->provider == 'Omniva') {
                            OrderController::sendStatusUpdatingNotification($order, 3);
                            Log::info('OrderObserver | Status change email notification sent for order ' . $order->id . '. Event: 3 - Omniva');
                        }
                    }
                }

            }
        } catch (\Exception $e) {
            Log::error('OrderObserver | Status change email notification failed for order ' . $order->id . ', caused by ' . $e->getMessage());
        }

    }
}