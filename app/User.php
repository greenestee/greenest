<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'name',
            'email', 
            'password',
            'phone',
            'company_name',
            'company_reg_number',
            'vat_number',
            'billing_address',
            'billing_country',
            'billing_state',
            'billing_city',
            'billing_zip_code',
            'shipping_address',
            'shipping_country',
            'shipping_state',
            'shipping_city',
            'shipping_zip_code',
            'shipping_method',
            'shipping_parcel_terminal_phone',
            'shipping_parcel_terminal_id',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}