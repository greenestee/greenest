<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParcelTerminal extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'provider',
        'name',
        'address',
        'open'
    ];

    public function scopeSmartpost($query, $withTrashed = true)
    {
        $query = $query->where('provider', 'LIKE', 'Smartpost');
        if ($withTrashed) {
            $query = $query->withTrashed();
        }
        return $query;
    }

    public function scopeOmniva($query, $withTrashed = true)
    {
        $query = $query->where('provider', 'LIKE', 'Omniva');
        if ($withTrashed) {
            $query = $query->withTrashed();
        }
        return $query;
    }

    public function scopeDpd($query, $withTrashed = true)
    {
        $query = $query->where('provider', 'LIKE', 'DPD');
        if ($withTrashed) {
            $query = $query->withTrashed();
        }
        return $query;
    }
}
