<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAvailableImmediatelyStatus extends Model
{
    protected $table = 'category_available_immediately_statuses';

    protected $fillable = [
        'category_id',
        'is_active'
    ];
}
