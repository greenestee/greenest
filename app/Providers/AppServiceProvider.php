<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Models\Menu;
use Illuminate\Support\Facades\View;
use App\Page;
use App\Product;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        // TODO : Base table or view not found: 1146
        if (!$request->ajax()) {
            View::share('footerNav', $this->footerNav($request));
            View::share('headerNav', $this->headerNav($request));
        }

        $this->eventProductCreating();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    public function footerNav()
    {
        if($menu = Menu::where('name', 'footer')->first()) {
            $locale = $this->app->getLocale();
            $url = 'url';
            $title = 'title';
            if ($locale == 'et') {
                $url = 'url_et';
                $title = 'title_et';
            }
            $menu = $menu->items()->orderBy('order')->pluck($title, $url)->toArray();
            $langMenu = [];
            $locale = $this->app->getLocale();
            foreach ($menu as $url => $name) {
                $langMenu[$locale . '/' . $url] = $name;
            }
            $menu = collect($langMenu);
        } else {
            $menu = [];
        }

        return $menu;
    }

    public function headerNav()
    {
        if ($menu = Menu::where('name', 'header')->first()) {
            $locale = $this->app->getLocale();
            $url = 'url';
            $title = 'title';

            if ($locale == 'et') {
                $url = 'url_et';
                $title = 'title_et';
            }

            $menu = $menu->items()->orderBy('order')->pluck($title, $url)->toArray();
            $langMenu = [];
            $locale = $this->app->getLocale();
            foreach ($menu as $url => $name) {
                $langMenu[$locale . '/' . $url] = $name;
            }
            $menu = collect($langMenu);
        } else {
            $menu = [];
        }

        return $menu;
    }

    public function eventProductCreating()
    {
        $amount25 = collect([10000, 5000, 2000, 1000, 500, 200, 100, 50]);
        $amount10 = collect([100, 50, 25]);
        $amount3 = collect([5, 10]);

        Product::creating(function ($product) use ($amount25, $amount3, $amount10) {

            if(request()->has('action')){

                if($product->amount == 25 && !Product::where('name', $product->name)->where('amount', 50)->first()){
                    
                    $amount25->each(function($item, $key) use ($product){
                        $duplicate = $product->replicate();
                        
                        $duplicate->amount = $item;
                        $duplicate->sku = $product->sku . '-' . $item;
                        $duplicate->slug = $product->slug . $item;
                        $duplicate->price = $item / 25 * $product->price;

                        $duplicate->save();
                        $duplicate->categories()->attach(request()->get('categories'));
                    });
                    
                } elseif ($product->amount == 10 && !Product::where('name', $product->name)->where('amount', 5)->first()){
                    
                    $amount10->each(function($item, $key) use ($product){
                        $duplicate = $product->replicate();
                        
                        $duplicate->amount = $item;
                        $duplicate->sku = $product->sku . '-' . $item;
                        $duplicate->slug = $product->slug . $item;
                        $duplicate->price = $item / 10 * $product->price;

                        $duplicate->save();
                        $duplicate->categories()->attach(request()->get('categories'));
                    });

                } elseif ($product->amount == 3){
                    
                    $amount3->each(function($item, $key) use ($product){
                        $duplicate = $product->replicate();
                        
                        $duplicate->amount = $item;
                        $duplicate->sku = $product->sku . '-' . $item;
                        $duplicate->slug = $product->slug . $item;
                        $duplicate->price = $item / 3 * $product->price;

                        $duplicate->save();
                        $duplicate->categories()->attach(request()->get('categories'));
                    });
                }
            }

        });
    }
    
}
