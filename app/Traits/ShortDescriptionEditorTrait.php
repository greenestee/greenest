<?php

namespace App\Traits;

use PHPHtmlParser\Dom;

trait ShortDescriptionEditorTrait
{

    /**
     * @param $shortDescription
     * @param $newText
     * @param bool $inStock
     * @return mixed
     */
    private function getShortDescription($shortDescription, $newText, $inStock = true)
    {
        $dom = new Dom;
        $dom->load($shortDescription);
        $li = $dom->find('li.delivery')[0];

        if ($li) {

            $li->delete();

            $ul = $dom->find('ul')[0];
            $newLi = new \PHPHtmlParser\Dom();

            if (!$inStock) {
                $outStock = 'out-stock';
            } else {
                $outStock = '';
            }

            $newLi->load('<li class="delivery ' . $outStock . '" >' . $newText . '</li>');

            $ul->addChild($newLi->root);

            return $dom->outerHtml;

        } else {
            return $shortDescription;
        }

    }
}