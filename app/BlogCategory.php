<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    public function articles()
    {
        return $this->hasMany('App\BlogPost', 'blog_category_id', 'id');
    }

    public function getCountArticles()
    {
        $count = $this->articles->count();
        return $count;
    }
}