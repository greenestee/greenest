<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsDebatableImport extends Model
{
    protected $table = 'products_debatable_import';

    protected $fillable = [
        'product_id',
        'old_sku',
        'new_sku',
        'old_url',
        'new_url',
        'created_at',
        'updated_at'
    ];

    protected $perPage = 25;
}
