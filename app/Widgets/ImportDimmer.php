<?php

namespace App\Widgets;

use App\Category;
use App\CategoryProduct;
use Arrilot\Widgets\AbstractWidget;
use App\Product;

class ImportDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $moderationCategoryId = Category::where('name', 'moderation')->value('id');
        $moderationProducts = CategoryProduct::where('category_id', $moderationCategoryId)
            ->pluck('product_id')->toArray();

        $count = Product::whereIn('id', $moderationProducts)->where('is_blacklist', '=', 0)
            ->count();

        $countBlacklist = Product::whereIn('id', $moderationProducts)->where('is_blacklist', '=', 1)
            ->count();

        if ($countBlacklist){
            $title = "{$count} new products and {$countBlacklist} products in blacklist";
        } else {
            $title = "{$count} new products";
        }


        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-cup',
            'title'  => $title,
            'text'   => "You have products for moderation. Click on button below to view products.",
            'button' => [
                'text' => 'View imported products',
                'link' => url('admin/imported-products'),
            ],
            'image' => url('/assets/img/widget-backgrounds/bio-planet.png'),
        ]));
    }
}