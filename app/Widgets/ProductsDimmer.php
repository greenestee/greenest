<?php

namespace App\Widgets;

use App\Category;
use App\CategoryProduct;
use Arrilot\Widgets\AbstractWidget;
use App\Product;

class ProductsDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $moderationAndBlacklistCategories = Category::whereIn('name', ['moderation', 'blacklist'])
            ->pluck('id')->toArray();
        $products = CategoryProduct::whereNotIn('category_id', $moderationAndBlacklistCategories)
            ->pluck('product_id')->toArray();
        $count = Product::whereIn('id', $products)->count();

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-cup',
            'title'  => "{$count} products",
            'text'   => "You have {$count} products. Click on button below to view all products.",
            'button' => [
                'text' => 'View all products',
                'link' => url('admin/products'),
            ],
            'image' => url('/assets/img/widget-backgrounds/products.jpg'),
        ]));
    }
}