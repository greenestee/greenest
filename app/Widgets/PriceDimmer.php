<?php

namespace App\Widgets;

use App\ProductTmp;
use Arrilot\Widgets\AbstractWidget;

class PriceDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = ProductTmp::count();

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-cup',
            'title'  => "Total need to change price: {$count}",
            'text'   => "You have {$count} unconfirmed products. Click on button below to view all unconfirmed products.",
            'button' => [
                'text' => 'View all product to confirm price',
                'link' => url('admin/confirm-price'),
            ],
            'image' => url('/assets/img/widget-backgrounds/orders.jpg'),
        ]));
    }
}
