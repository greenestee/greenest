<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Category;

class CategoriesDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Category::count();

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-categories',
            'title'  => "{$count} categories",
            'text'   => "You have {$count} categories. Click on button below to view all categories.",
            'button' => [
                'text' => 'View all categories',
                'link' => url('admin/categories'),
            ],
            'image' => url('/assets/img/widget-backgrounds/categories.jpg'),
        ]));
    }
}