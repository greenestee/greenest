<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Order;

class OrdersDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Order::count();
        $countNew = Order::where('status', 'new')->count();
        $string = $countNew  ? $countNew . ' new orders | ' : '';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-wallet',
            'title'  => "{$string} Total: {$count}",
            'text'   => "You have {$countNew} new orders. Click on button below to view all orders.",
            'button' => [
                'text' => 'View all orders',
                'link' => url('admin/orders'),
            ],
            'image' => url('/assets/img/widget-backgrounds/orders.jpg'),
        ]));
    }
}