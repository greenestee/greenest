<?php

namespace App\Repositories;


use App\CategoryProduct;
use App\Order;
use App\OrderItem;
use App\Product;
use Carbon\Carbon;
use PHPHtmlParser\Dom;

class OrderRepository
{
    /**
     * When order status is set to paid, subtract amount of
     * paid products from total quantity of this product in stock.
     *
     * @param Order $order
     */
    public static function updateQuantitiesProductsInPaidProducts(Order $order)
    {
        $orderItems = OrderItem::where('order_id', $order->id)->select('product_id', 'quantity')->get();
        $productRepo = new ProductRepository();

        foreach ($orderItems as $orderItem) {
            if (is_null($orderItem->product_id)) {
                continue;
            }

            if (!$productRepo->isAvailableImmediatelyActivated($orderItem->product)) {
                continue;
            }

            Product::where('id', $orderItem->product_id)->first()->decrementOverridden('quantity', $orderItem->quantity);

            $product = Product::where('id', $orderItem->product_id)->select('id', 'quantity', 'short_description', 'short_description_et')->first();

            if ($product->quantity <= 0) {
                $categories = Product::find($orderItem->product_id)
                    ->categories()
                    ->where('slug', 'like', "%available-immediately")
                    ->get()
                    ->pluck('id')
                    ->toArray();

                CategoryProduct::where('product_id', $orderItem->product_id)
                    ->whereIn('category_id', $categories)
                    ->delete();

                if ($product->is_imported == 0) {
                    $date = Carbon::now()->addDays(30)->format('Y-m-d');
                    $engBulletin = 'Expected arrival date ' . $date;
                    $etBulletin = 'Eeldatav saabumiskuupäev ' . $date;

                    $product->short_description = self::getShortDescription($product->short_description, $engBulletin);
                    $product->short_description_et = self::getShortDescription($product->short_description_et, $etBulletin);
                    $product->save();
                } elseif ($product->delivery_date > Carbon::now()->format('Y-m-d')) {
                    $product->short_description = self::getShortDescription($product->short_description, 'Out of stock! Expected arrival date ' . $product->delivery_date);
                    $product->short_description_et = self::getShortDescription($product->short_description_et, 'Läbi müüdud! Eeldatav saabumiskuupäev  ' . $product->delivery_date);
                    $product->status = Product::PRODUCT_NOT_IN_STOCK;
                    $product->save();
                } else {
                    $product->is_active = 0;
                    $product->status = Product::PRODUCT_NOT_IN_STOCK;
                    $product->save();
                }
            }

            \Log::info('Available category removed and in quantity is decremented by ' . $orderItem->quantity . 'for product #' . $orderItem->product_id);
        }
    }

    /**
     * Updated last list item with new text.
     *
     * @param $shortDescription
     * @param $newText
     * @return mixed
     */
    private static function getShortDescription($shortDescription, $newText)
    {
        $dom = new Dom();
        $dom->load($shortDescription);
        $list = $dom->find('li');

        $li = $list[$list->count()-1];

        if ($li) {

            $li->delete();

            $ul = $dom->find('ul')[0];
            $newLi = new Dom();

            $newLi->load('<li class="delivery" >' . $newText . '</li>');

            $ul->addChild($newLi->root);

            return $dom->outerHtml;

        } else {
            return $shortDescription;
        }
    }

}