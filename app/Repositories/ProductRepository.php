<?php

namespace App\Repositories;


class ProductRepository
{
    /**
     * Return true if menu, where product is located has activated "available immediately" category.
     *
     * @param $product
     * @return bool
     */
    public function isAvailableImmediatelyActivated($product)
    {
        try {
            $isActive = $product->categories()->where('slug', 'like', "%available-immediately")->where('is_active', 1)->exists();

            return $isActive;
        } catch (\Exception $e) {
            \Log::error("Error when checking if given product category is in menu with activated available immediately category :{$product->id}, {$e->getMessage()}");

            return false;
        }
    }
}