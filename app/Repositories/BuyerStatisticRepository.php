<?php


namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BuyerStatisticRepository
{
    public function getOrderStat($date = null)
    {
        $data = $this->getAllOrder($date);
        $ordersCollects = $data->sort();

        $cachedBayerStat = [];
        $needSave = false;

        foreach ($ordersCollects as $key => $ordersCollect) {
            $cachedBayerStat[$key]['phone'] = null;
            $cachedBayerStat[$key]['total_turnover'] = 0;
            foreach ($ordersCollect as $val) {
                $cachedBayerStat[$key]['total_turnover'] += $val->sum_total;
                $cachedBayerStat[$key]['email'] = $key;
                $cachedBayerStat[$key]['buyer_name'] = $val->name;
                if (!is_null($val->phone)) {
                    $cachedBayerStat[$key]['phone'] = $val->phone;
                }
                $cachedBayerStat[$key]['shipping_parcel_terminal_phone'] = $val->shipping_parcel_terminal_phone;
                $cachedBayerStat[$key]['last_purchase_date'] = $val->created_at;
                $cachedBayerStat[$key]['number_of_purchases'] = count($ordersCollect);
            }
            $needSave = true;
        }

        if (!is_null($date)) {
            return $cachedBayerStat;
        }

        if ($needSave) {
            $this->savedNewCachedStat($cachedBayerStat);
        }
    }

    private function savedNewCachedStat(array $cachedBayerStat)
    {
        DB::table('buyer_list_statistics')->truncate();
        DB::table('buyer_list_statistics')->insert(array_values($cachedBayerStat));
    }

    private function getAllOrder($date)
    {
        if (is_null($date)) {
            $orders = DB::table('orders')->get();
        } else{
            $orders = DB::table('orders')->where('created_at', 'LIKE', '%'.$date.'%')->get();
        }

        $toLow = $orders->map(function ($order) {
            $order->email = strtolower($order->email);
            return $order;
        });

        $ordersCollects = $toLow->where('status', 'completed')->groupBy('email');

        return $ordersCollects;
    }

    public function getPercentPermanentBuyer($year)
    {
        if ($year === 'all_time') {
            $data = $this->getAllOrder(null);
        } else {
            $data = $this->getAllOrder($year);
        }
        $buyers = [];

        foreach ($data as $key => $collect) {
            if ($collect->count() > 1) {
                $buyers[$key] = $collect->count();
            }
        }

        if (!empty($buyers) && $data->isNotEmpty()) {
            return round((count($buyers) / $data->count()) * 100, 2, PHP_ROUND_HALF_DOWN );
        } else {
            return 0;
        }

    }

    public function getYears()
    {
        $year = Carbon::parse()->year;
        $date[$year] = $year;

        for ($i = 1; $i <= 2; $i++) {
            $date[$year - $i] = $year - $i;
        }

        return $date;
    }

}