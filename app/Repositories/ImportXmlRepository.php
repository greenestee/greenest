<?php

namespace App\Repositories;


use App\Console\Commands\NewImportXml;
use App\ProductsDebatableImport;
use Carbon\Carbon;

class ImportXmlRepository
{
    /**
     * Update or create record for product, if sku changed, but url remains the same.
     *
     * @param $product
     * @param $productParse
     */
    public function updateOrCreateDebatableProductRecord($product, $productParse)
    {
        $timestamp = Carbon::now()->format('Y-m-d H:i:s');

        ProductsDebatableImport::updateOrCreate(
            [
                'old_sku' => trim($product->sku),
                'new_sku' => $productParse->{NewImportXml::SKU}
            ],
            [
                'product_id' => $product->id,
                'old_url' => $product->product_url,
                'new_url' => trim($productParse->{NewImportXml::PRODUCT_URL}),
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ]
        );
    }
}