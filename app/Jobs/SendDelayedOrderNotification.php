<?php

namespace App\Jobs;

use App\Http\Controllers\OrderController;
use App\Order;
use App\OrderStatusNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendDelayedOrderNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
        $this->queue = 'default';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $order = Order::find($this->orderId);

            $emailText = ($order->shipping_method == 'eu_shipping') ? 'text_en' : 'text_et';

            $text = OrderStatusNotification::where('event', 6)->firstOrFail()->$emailText;

            $subject = OrderController::getEmailSubject($emailText, $order->id);

            if ($order->email) {
                Mail::send('emails.statusChange', ['order' => $order, 'text' => $text],
                    function ($message) use ($order, $subject) {
                        $message->to($order->email)->subject($subject);
                    });

                Log::info(__METHOD__ . ' | Notification sent for order ' . $order->id);
            }
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' | Notification sending failed for order ' . $order->id . ', caused by ' . $e->getMessage());
        }
    }
}
