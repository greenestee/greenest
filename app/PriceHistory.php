<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PriceHistory extends Model
{
    public $table = 'price_history';

    public $timestamps = false;


    protected $fillable = [
            'product_id',
            'old_price',
    	];

    public function priceHistory()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
