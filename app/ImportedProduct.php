<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ImportedProduct extends Model
{
    use SoftDeletes;

    protected $table = "products";

    public function importedCategories()
    {
        return $this->belongsToMany('App\Category', 'category_product',  'product_id', 'category_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_product',  'product_id', 'category_id');
    }

}
