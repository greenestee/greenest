<?php

namespace App;

use TCG\Voyager\Models\Category as BaseCategory;

class Category extends BaseCategory
{
    protected $fillable = ['id', 'is_active', 'parent_id'];
    /**
     * Relationship method for nested categories
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    /**
     * Relationship method for nested categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    /**
     * Relationship method
     * 
     * @return mixed
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'category_product', 'category_id', 'product_id');
//          ->active()
//          ->orderBy('created_at', 'DESC');
    }

    /**
     * 
     */
    public function scopeIsMain($query)
    {
        return $query->where('is_main', true)->whereNull('parent_id');
    }

    /**
     * 
     */
    public function getCountProducts()
    {
        $count = $this->products->where('image', '!=', null)->where('is_active', 1)->count();

        $count += $this->children->where('is_active', 1)->reduce(function ($carry, $item){
            return $carry + $item->products->where('image', '!=', null)->where('is_active', 1)->count();
        });

        return $count;
    }

    /**
     * Get the phone record associated with the category.
     */
    public function activationStatus()
    {
        return $this->hasOne('App\CategoryAvailableImmediatelyStatus');
    }
}
