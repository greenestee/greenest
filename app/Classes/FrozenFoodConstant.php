<?php

namespace App\Classes;


class FrozenFoodConstant
{
    const SHORT_DESCRIPTION = '<ul>' .
    '<li>Certified organic product</li>' .
    '<li>Requires special temperature -18 °C</li>' .
    '<li>Requires refrigerated transport </li>' .
    '<li>Shipping cost in Estonia 8 EUR</li>' .
    '<li class="delivery">We ship within 2-7 working days</li>' .
    '</ul>';

    const SHORT_DESCRIPTION_ET = '<ul>' .
    '<li>Kontrollitud mahetoode</li>' .
    '<li>Säilitustemperatuur -18 °C</li>' .
    '<li>Toode vajab eritransporti</li>' .
    '<li>Transport Eesti piires 8 EUR</li>' .
    '</ul>';
}