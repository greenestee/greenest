<?php

namespace App;

use App\Category;
use App\Observers\ProductObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PHPHtmlParser\Dom;

class Product extends Model
{
    use SoftDeletes;

    const PRODUCT_NOT_IN_STOCK = 0;

    const PRODUCT_IN_STOCK = 1;

    protected $dates = ['deleted_at'];

    protected $fillable = ['sku'];

    public static function boot()
    {
        Product::observe(ProductObserver::class);
        parent::boot();
    }

    /**
     * Relationship method
     * 
     * @return mixed
     */
	public function categories()
	{
	    return $this->belongsToMany('App\Category', 'category_product',  'product_id', 'category_id');
	}
	
	/**
	 * [scopeActive description]
	 * @param  [type]  $query  [description]
	 * @param  boolean $status [description]
	 * @return [type]          [description]
	 */
	public function scopeActive($query, $status = true)
	{
		return $query->where('is_active', $status)->whereNotNull('image');
	}

	/**
	 * [getImage description]
	 * @return [type] [description]
	 */
	public function getImage()
	{
		if($this->image){
			return url('/storage/' . $this->image);
		}

		return url('/assets/img/product.jpg');
	}

    public function isRefrigerated()
    {
        return $this->categories()->where('categories.id', 116)->exists();
	}

    public function isFrozen()
    {
        return $this->categories()->where('categories.id', 62)->exists();
	}

	public function getAmount()
	{
		if($this->amount == 1){
			return '1 <span>'. trans('static_text.single_piece') .'</span>';
		}

		return $this->amount . ' <span>'. trans('static_text.multiple_pieces') .'</span>';
	}

	public function getPrice()
	{
		return number_format($this->price, 2, ',', ' ');
	}

	public function getPricePerPiece()
	{
		if($this->weight && $this->unit == 'kg'){
			return trans('static_text.Price_per_kg:') . ' ' . number_format($this->price / $this->weight, 2, ',', ' ');
		} elseif($this->weight && $this->unit == 'l'){
			return trans('static_text.Price_per_liter:') . ' ' . number_format($this->price / $this->weight, 2, ',', ' ');
		}

		return trans('static_text.Price_per_piece:') . ' ' . number_format($this->price / $this->amount, 2, ',', ' ');
	}

    public function getShortDescription()
    {
        $langPostfix = \App::getLocale() == 'en' ? '' : '_et';
        $description = $this->{"short_description{$langPostfix}"};

        if ($this->active_additional_description) {
            $dom = new Dom();
            $dom->load($description);

            $ul = $dom->find('ul')[0]->innerHtml;
            $description = '<ul>' . trans('static_text.additional_description_bulletin') . "{$ul}</ul>";
        }

        return $description;
    }

	/**
	 * [getImage description]
	 * @return [type] [description]
	 */
	public function getThumb()
	{
		if($this->image){
			
			$path = pathinfo($this->image);

			return url('/storage/' . $path['dirname'] . '/' . $path['filename'] . '-cropped.' . $path['extension']);
		}

		return url('/assets/img/product.jpg');
	}

	/**
	 * [scopeOrdered description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeOrdered($query)
	{
		return $query->orderBy('created_at', 'DESC')->orderBy('amount', 'ASC');
	}

    public function getPromotedToAttribute($value)
    {
        return (isset($value)) ? date('Y-m-d', strtotime($value)) : '';
    }

    public function getPromotedFromAttribute($value)
    {
        return (isset($value)) ? date('Y-m-d', strtotime($value)) : '';
    }

    public function setPromotedFromAttribute($value)
    {
        $this->attributes['promoted_from'] = ($value == "") ? null : $value;
    }

    public function setPromotedToAttribute($value)
    {
        $this->attributes['promoted_to'] = ($value == "") ? null : \Carbon\Carbon::parse($value)->addHour(23)->addMinutes(59)->addSeconds(59);
    }

    public static function getPromotedCount()
    {
        $now = \Carbon\Carbon::now();
        return Product::where('is_promoted', '1')
            ->where('promoted_from', '<', $now)
            ->where('promoted_to', '>', $now)
            ->count();
    }

    /**
     * Overridden to fire observer.
     *
     * @param string $column
     * @param int $amount
     * @param array $extra
     * @return int|void
     */
    public function increment($column, $amount = 1, array $extra = [])
    {
        $this->$column = $this->$column + $amount;

        $this->save();
    }

    /**
     * Overridden to fire observer.
     *
     * @param string $column
     * @param int $amount
     * @param array $extra
     * @return int|void
     */
    public function decrementOverridden($column, $amount = 1, array $extra = [])
    {
        $this->$column = $this->$column - $amount;

        $this->save();
    }
}
