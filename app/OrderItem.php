<?php

namespace App;

use App\Observers\OrderItemObserver;
use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{
	public $timestamps = false;

    protected $fillable = [
			'name',
			'quantity',
			'price',
			'sum_total'
		];
		
   	public function product()
   	{
   		return $this->belongsTo('App\Product', 'product_id');
   	}

    public static function boot()
    {
        OrderItem::observe(OrderItemObserver::class);
        parent::boot();
    }

}
