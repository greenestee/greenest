<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusNotification extends Model
{
    //List of events
    /*
     * 1 = New > Paid;
     * 2 = Paid > Forwarded;
     * 3 = Forwarded > Completed;
     * 4 = Refund was made by admin, and notification will be sent to client
     * 5 = Notification to client that product is back in stock
     * 6 = Notification to client that forwarded order is delayed
     * 7 = Paid > Thursday shipping
     * */
}
