<?php

namespace App\Console;

use App\Console\Commands\ReplaseProductShortDescription;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SitemapCreate::class,
//        Commands\ImportXML::class,
        Commands\DellOverdueProduct::class,
        Commands\SendMollieToMerit::class,
        Commands\SendPayseraToMerit::class,
        Commands\NewImportXml::class,
        Commands\ReplaseProductShortDescription::class,
//        Commands\UpdateTerminal::class
        Commands\SyncronizeDPDParcels::class,
        Commands\UpdateDPDShippingStatuses::class,
        Commands\CleanOldUserCartItems::class,
        Commands\ImportTerminals::class,
        Commands\AutomaticallyExceptRaisedPrices::class,
        Commands\sendListOfMissingProducts::class,
        Commands\CachedBayerListStatistics::class,
        Commands\RestoreDeletedProductsWithSameUrl::class,
        Commands\UpdateProductsBulletins::class,
        Commands\CheckAvailabilityCategories::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('sitemap:create')
             ->cron('17 1,4,6 * * *');

        $schedule->command('import-new-xml')
            ->cron('35 * * * *')->when(function () {
                $hour = (int) Carbon::now()->format('H');
                return (
                    in_array($hour, range(7, 21, 3))
                        || ($hour % 2 === 0)
                    )
                    && !Cache::get('importIsStarted', false);
            });//At minute 35 past every 3nd hour

         $schedule->command('user-cart:clear')->daily()->at('1:03');

         $schedule->command('dell:overdue-product')
            ->daily()->at('1:14');

         $schedule->command('check:availability-categories')
            ->daily()->at('0:05');

         $schedule->command('update:dpd-shipment-statuses')
            ->daily()->at('2:17');

         $schedule->command('merit:send_paysera')
            ->daily()->at('3:16')->when(function () {
                return Carbon::parse('last day of this month')->format('Y-m-d') == Carbon::now()->format('Y-m-d');
             });

         $schedule->command('merit:send_mollie')
             ->daily()->at('3:12');

//        $schedule->command('sync:dpd-parcels')
//            ->daily();

        $schedule->command('import-terminals')
            ->daily()->at('2:23');

        $schedule->command('automatically-except-raised-prices')
            ->monthlyOn(10, '3:48');

        $schedule->command('send-list-of-missing-products')
            ->weeklyOn(1, '4:30');

        $schedule->command('cached:bayer-list-statistic')
            ->dailyAt('3:08');

        $schedule->command('command:update-product-bulletins')
            ->cron('27 */2 * * *'); //At every minute past every 2nd hour.
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
