<?php

namespace App\Console\Commands;

use App\Category;
use App\CategoryProduct;
use App\Product;
use Illuminate\Console\Command;
use PHPHtmlParser\Dom;

class UpdateProductsBulletins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-product-bulletins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $needCategories = Category::where('slug', 'like', '%available-immediately%')
            ->where('is_active', 1)->pluck('id')->toArray();
        $needProducts = CategoryProduct::whereIn('category_id', $needCategories)->pluck('product_id')->toArray();

        $products = Product::whereIn('id', $needProducts)
        ->where(function ($query) {
            $query->where('short_description', 'not like', '%Product is in stock and available immediately%')
                ->orWhere('short_description_et', 'not like', '%Toode on laos ja koheselt saadaval%');
        })
        ->get();

        foreach ($products as $product) {
            $product->short_description = $this->getShortDescription($product->short_description, 'Product is in stock and available immediately ');
            $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Toode on laos ja koheselt saadaval ');
            $product->save();
        }

        echo "Available immediately products bulletins updated. \n";
    }

    /**
     * Updated last list item with new text.
     *
     * @param $shortDescription
     * @param $newText
     * @return mixed
     */
    private function getShortDescription($shortDescription, $newText)
    {
        $dom = new Dom();
        $dom->load($shortDescription);
        $list = $dom->find('li');

        $li = $list[$list->count()-1];

        if ($li) {

            $li->delete();

            $ul = $dom->find('ul')[0];
            $newLi = new Dom();

            $newLi->load('<li class="delivery" >' . $newText . '</li>');

            $ul->addChild($newLi->root);

            return $dom->outerHtml;

        } else {
            return $shortDescription;
        }
    }
}
