<?php

namespace App\Console\Commands;

use App\Http\Controllers\Voyager\VoyagerPriceController;
use App\Product;
use App\ProductTmp;
use Illuminate\Console\Command;

class AutomaticallyExceptRaisedPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automatically-except-raised-prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All products that are not changed till every month 10th date but whats price have raised must be changed automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $voyagerPriceControllerObj = new VoyagerPriceController();
            $products = $voyagerPriceControllerObj->getConfirmPriceProductsList();

            foreach ($products as $product) {
                if ((float)$product->price_new > (float)$product->price_old) {
                    $this->acceptNewPrice($product->id);
                }
            }

        } catch (\Exception $e) {
            \Log::error('Accepting products with higher price failed, caused by ' . $e->getMessage());
        }

        \Log::info('Accepting products with higher price succeed!');
    }

    /**
     * Accepts new price.
     *
     * @param $productId
     */
    private function acceptNewPrice($productId)
    {
        $productTmp = ProductTmp::where('id', '=', $productId)->first();
        $product = Product::where('sku', '=', $productTmp->sku)->first();

        if (!isset($product) || !isset($productTmp)) {
            return;
        }

        $product->price = $productTmp->price;
        $product->purchase_price = $productTmp->purchase_price;
        $product->save();
        $productTmp->delete();
    }
}
