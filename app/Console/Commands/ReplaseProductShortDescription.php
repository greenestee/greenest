<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;

class ReplaseProductShortDescription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:replace_short_description';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replace product short description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::all();

        foreach ($products as $product) {
            $product->short_description = str_replace('within 2-5 working', 'within 2-7 working', $product->short_description);
            $product->short_description_et = str_replace('Pakiautomaati 2-5', 'Pakiautomaati 2-7', $product->short_description_et);
            $product->save();
        }

        echo "Success \n";
    }
}
