<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;

class DellOverdueProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dell:overdue-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dell overdue product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = \Carbon\Carbon::yesterday();
        $products = Product::select('sku', 'delivery_date', 'quantity')
            ->where('delivery_date', '<=', $yesterday)
            ->where('quantity', '<=', 0)
            ->delete();
    }
}
