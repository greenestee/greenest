<?php

namespace App\Console\Commands;

use App\OrderItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class sendListOfMissingProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-list-of-missing-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Every Monday morning 07:30 the system sends to valdo@greenest the list of all the missing products that don’t have “Arrived/In stock” check with the links';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $domain = env('APP_URL');

            $orderItems = OrderItem::where('missing', 1)
                ->where('in_stock', 0)
                ->with(['product' => function ($query) {
                    $query->select('id', 'name', 'slug');
                }])
                ->get()
                ->toArray();

            foreach ($orderItems as $key => $orderItem) {
                $orderItems[$key]['orderLink'] = $domain . "/en/admin/orders/{$orderItem['order_id']}/edit";
                $orderItems[$key]['productLink'] = $domain . "/en/admin/products/{$orderItem['product_id']}/edit";
            }

            Mail::send('emails.report-about-missing-order-items', ['orderItems' => $orderItems],
                function ($message) {
                    $message->to('valdo@greenest.ee')->subject("List of products that don't have 'in stock' check in orders");
                });
            Log::info("List of products that don't have 'in stock' check in orders was sent to admin");
        } catch (\Exception $e) {
            Log::error("List of products that don't have 'in stock' check in orders was not sent to admin, caused by {$e->getMessage()}");
        }

    }
}
