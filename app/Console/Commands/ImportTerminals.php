<?php

namespace App\Console\Commands;

use App\ParcelTerminal;
use Illuminate\Console\Command;
use Log;

class ImportTerminals extends Command
{
    protected $smartpostCountries = ['EE', 'FI'];
    protected $smartpostUrl = 'http://iseteenindus.smartpost.ee/api/?request=destinations&type=APT&country=';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-terminals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Parcel Terminals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importSmartpostTerminals();
    }

    private function importSmartpostTerminals()
    {
        $localSmartpostTerminals = ParcelTerminal::smartpost()->get();

        foreach ($this->smartpostCountries as $country) {
            $fileXml = $this->getSmartpostXml($this->smartpostUrl, $country);
            if (!$fileXml) continue;

            $xml = simplexml_load_file($fileXml);

            if (!$xml) {
                foreach (libxml_get_errors() as $error) {
                    Log::error('Error loading Smartpost ' . $country . ' XML:' . $error->message);
                    continue;
                }
            }

            foreach ($xml->item as $item) {
                foreach ($localSmartpostTerminals as $key => $localTerminal) {
                    if ($item->place_id == $localTerminal->smartpost_id) {
                        $localSmartpostTerminals->forget($key);
                        // оновлюємо
                        $localTerminal->restore();
                        $localTerminal->name = $item->name;
                        $localTerminal->city = $item->city;
                        $localTerminal->country = $item->country;
                        $localTerminal->open = $item->availability;
                        if (isset($item->routingcode)) {
                            $localTerminal->smartpost_routingcode = $item->routingcode;
                        }
                        $localTerminal->lat = $item->lat;
                        $localTerminal->lng = $item->lng;
                        $localTerminal->address = "{$item->address}, {$item->city}";
                        $localTerminal->save();
                        continue 2;
                    }
                }
                // створюємо
                $terminal = new ParcelTerminal();
                $terminal->provider = 'Smartpost';
                $terminal->name = $item->name;
                $terminal->city = $item->city;
                $terminal->country = $item->country;
                $terminal->postalcode = $item->postalcode;
                $terminal->smartpost_id = $item->place_id;
                $terminal->smartpost_routingcode = $item->routingcode;
                $terminal->open = $item->availability;
                $terminal->lat = $item->lat;
                $terminal->lng = $item->lng;
                $terminal->address = "{$item->address}, {$item->city}";
                $terminal->save();
            }
            Log::notice('Success parcel terminals import for country ' . $country);
        }

        // видаляємо
        foreach ($localSmartpostTerminals as $item) {
            if (!$item->trashed()) {
                $item->delete();
            }
        }
    }

    /**
     * @return string
     */
    private function getSmartpostXml($url, $country)
    {
        $fileName = 'smartpost_' . $country . '.xml';
        $storagePath = storage_path('import_xml/' . $fileName);

        if($this->loadingFile($url . $country, $storagePath)) {
            return $storagePath;
        } else {
            return false;
        }
    }

    /**
     * @param $url
     * @param $storagePath
     * @return bool
     */
    private function loadingFile($url, $storagePath){
        $client = new \GuzzleHttp\Client();
        try {
            $client->request(
                'GET',
                $url,
                ['sink' => $storagePath]
            );
        } catch (Exception $e) {
            Log::error('Error download from ' . $url);
            return false;
        }

        return true;
    }
}
