<?php

namespace App\Console\Commands;

use App\Category;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckAvailabilityCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:availability-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set available_from to null for categories which available from today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = Category::where('available_from', Carbon::now()->toDateString())->get();
        if ($categories->count() > 0) {
            foreach ($categories as $category) {
                $category->available_from = null;
                $category->save();
            }
        }
    }
}
