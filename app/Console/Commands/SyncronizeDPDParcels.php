<?php

namespace App\Console\Commands;

use App\Http\Libraries\DPD\DPD;
use App\ParcelTerminal;
use Illuminate\Console\Command;

class SyncronizeDPDParcels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:dpd-parcels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import DPD parcel terminals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $service = new DPD();
        $countries = ['EE', 'LV', 'LT'];
        $ourSystemIds = [];

        foreach ($countries as $country) {
            $parcels = $service->getParcels($country);

            foreach ($parcels as $parcel) {
                if (ParcelTerminal::dpd()->where('dpd_id', $parcel->parcelshop_id)->exists()) {
                    $ourSystemIds[] = ParcelTerminal::dpd()->where('dpd_id', $parcel->parcelshop_id)->value('id');

                    $ourTerminal = ParcelTerminal::dpd()->where('dpd_id', $parcel->parcelshop_id)
                        ->where([
                            'name' => $parcel->company,
                            'address' => $parcel->street,
                            'country' => $parcel->country,
                            'city' => $parcel->city,
                            'postalcode' => $parcel->pcode,
                            'lat' => $parcel->latitude,
                            'lng' => $parcel->longitude
                        ])->exists();
                    if ($ourTerminal) {
                        ParcelTerminal::dpd()->where('dpd_id', $parcel->parcelshop_id)
                            ->update([
                                'name' => $parcel->company,
                                'address' => $parcel->street,
                                'country' => $parcel->country,
                                'city' => $parcel->city,
                                'postalcode' => $parcel->pcode,
                                'lat' => $parcel->latitude,
                                'lng' => $parcel->longitude
                            ]);
                    }
                    continue;
                }

                $newParcel = new ParcelTerminal();
                $newParcel->provider = 'DPD';
                $newParcel->open = '';
                $newParcel->name = $parcel->company;
                $newParcel->address = $parcel->street;
                $newParcel->country = $parcel->country;
                $newParcel->city = $parcel->city;
                $newParcel->postalcode = $parcel->pcode;
                $newParcel->lat = $parcel->latitude;
                $newParcel->lng = $parcel->longitude;
                $newParcel->dpd_id = $parcel->parcelshop_id;
                $newParcel->save();

                $ourSystemIds[] = $newParcel->id;
            }
        }

        ParcelTerminal::dpd()->whereNotIn('id', $ourSystemIds)->delete();
    }
}
