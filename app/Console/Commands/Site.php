<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;
use Illuminate\Support\Facades\Storage as Storage;
use App\Page;
use Mockery\Exception;
use Mail;

class Site extends Command
{
    protected $data;

    protected $signature = 'site:create';


    protected $description = 'Command description';

    /**
     * SitemapCreate constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


//Date: Tue, 08 May 2018 11:53:46 +0000
//Subject: Order number 502 from greenest.ee
//From: "Greenest.ee" <orders@greenest.ee>
//To: k4shortie@gmail.com
//MIME-Version: 1.0
//Content-Type: text/html; charset=utf-8
//Content-Transfer-Encoding: quoted-printable

    public function handle()
    {

        $this->data = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greenest - Invoice</title>
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
  </head>

    <body style="box-sizing: inherit;margin: 0;padding: 0;background: #fefefe;font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-weight: normal;line-height: 1.5;color: #0a0a0a;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;">

            <h3>Dear  Kadi Idak! </h3>
        <h3>TÃ¤name Teid ostu eest! Loodame, et kÃ¼lastate meid jÃ¤lle. Siin on Teie arve.</h3>
    
    <section class="header row" style="box-sizing: inherit;display: block;margin-right: auto;margin-left: auto;box-shadow: 0 0 10px rgba(0,0,0,0.5);">
        <div class="row bitmap" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: 0;margin-left: 0;background-image: url(https://greenest.ee/assets/img/bitmap.jpg);background-repeat: no-repeat;background-size: cover;box-shadow: 0 0 10px rgba(0, 0, 0, 0.35);border-radius: 0.3125rem 0.3125rem 0 0;">
            <div class="large-12 text-center headWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 100%;text-align: center;">
                <h2 style="box-sizing: inherit;margin: 0;padding: 0;font-family: androgyne;font-style: normal;font-weight: normal;color: #fff;text-rendering: optimizeLegibility;font-size: 2.5rem;line-height: 1.4;margin-top: 10px;margin-bottom: 0;orphans: 3;widows: 3;page-break-after: avoid;">Nature Design OÃœ</h2>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 1rem;font-size: inherit;line-height: 1.6;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-family: \'Droid Sans\', sans-serif;color: #fff;">Organic & Biodegradable products</p>
            </div>
        </div>
    </section>
    
    <section class="clientBio row" style="box-sizing: inherit;display: block;margin-right: auto;margin-left: auto;margin-top: 0;padding-top: 3.4375rem;box-shadow: 0 0 10px rgba(0,0,0,0.5);margin-bottom: 150px;">
        <div class="row" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: -0.625rem;margin-left: -0.625rem;">
            <div class="large-5 large-offset-1 column playerWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 41.66667%;float: left;padding-right: 0.625rem;padding-left: 0.625rem;margin-left: 8.33333%;padding-top: 1.5rem;">
                <h5 style="box-sizing: inherit;margin: 0;padding: 0;font-family: proximanovabold;font-style: normal;font-weight: 700;color: inherit;text-rendering: optimizeLegibility;font-size: 1.5rem;line-height: 1.5rem;margin-top: 0;margin-bottom: 0.5rem;">Payer:  Kadi Idak </h5>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">E-mail: k4shortie@gmail.com</p>
                                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">Smartpost, Keila Selver</p>
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">Telephone: +37256868603</p>
                            </div>
            <div class="large-5 end column text-center dataWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 41.66667%;float: left;padding-right: 0.625rem;padding-left: 0.625rem;text-align: center;background-color: rgba(243, 243, 243, 0.78);border-radius: 0.3125rem 0.25rem 0.25rem 0.3125rem;padding-top: 1.875rem;padding-bottom: 1.375rem;">
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovabold;"><span style="box-sizing: inherit;font-weight: 700;font-family: proximanovalight;">Date:</span> 08.05.2018</p>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovabold;"><span style="box-sizing: inherit;font-weight: 700;font-family: proximanovalight;">Invoice number:</span> 502</p>
                <p class="redText" style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;color: #c20a0a;font-weight: 300;font-family: proximanovabold;">Payment date: 
                                            11.05.2018
                                        </p>
            </div>
        </div>
        <div class="row productInfo" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: -0.625rem;margin-left: -0.625rem;margin-top: 3.625rem;">
            <div class="large-offset-1 large-10" style="box-sizing: inherit;margin: 0;padding: 0;">
                <table class="unstriped" style="box-sizing: inherit;width: 100%;margin-bottom: 150px;border-radius: 0;border-top: 0.4375rem solid #0a0a0a;border-spacing: 0;font-size: 1.125rem;">
                    <thead style="box-sizing: inherit;display: table-header-group;border: 0.125rem solid #f1f1f1;background-color: #fefefe;background: #f8f8f8;color: #0a0a0a;">
                    <tr style="box-sizing: inherit;page-break-inside: avoid;background: transparent;">
                        <th class="tableTitle" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: left;background-color: #ebebeb;">Product name</th>
                        <th class="text-center tableTitle" width="200" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: center;background-color: #ebebeb;">Quantity</th>
                        <th class="text-center tableTitle" width="150" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: center;background-color: #ebebeb;">Net Price</th>
    
                    </tr>
                    </thead>
                    <tbody style="box-sizing: inherit;border: 1px solid #f1f1f1;background-color: #fefefe;">
                                        <tr style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;">CARROT OIL BIO 100 ml </td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">1</td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">6,66</td>
                    </tr>
                                        <tr style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;">Pakiautomaat </td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">1</td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">3,33</td>
                    </tr>
                                         <tr class="total" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;">&nbsp;</td>
                        <td class="text-right" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;text-align: right;">Sum: <br style="box-sizing: inherit;">VAT 20%<br style="box-sizing: inherit;"></td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;text-align: center;">9,99<br style="box-sizing: inherit;">2<br style="box-sizing: inherit;"></td>
                    </tr>
                     <tr style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;margin-bottom: 15px;color: #c51919;">&nbsp;</td>
                        <td class="text-right" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: right;border: none;margin-bottom: 15px;color: #c51919;background: #ebebeb;"><span style="box-sizing: inherit;">Total Sum:</span></td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;border: none;margin-bottom: 15px;color: #c51919;background: #ebebeb;"><span style="box-sizing: inherit;">11,99 â‚¬</span></td>
                    </tr>
                    <tr class="address" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;"> 
                            <span style="box-sizing: inherit;">RÃ¤vala 7, Tallinn</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">Harjumaa 10143</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">AS Swedpank: <span style="box-sizing: inherit;font-family: proximanovabold;">EE482200221061511058</span></span>
                        </td>
                        <td colspan="2" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;border: none;">     <span style="box-sizing: inherit;">Nature Design OÃœ</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">Registrikood: 12802934</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">EE101777465</span>
                        </td>
                       
                    </tr>
                    <tr class="contact" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;"> 
                            <img src="https://greenest.ee/assets/img/shape-3.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin-right: 10px;margin-top: -5px;max-width: 100% !important;"><a href="mailto:sales@greenest.ee" style="box-sizing: inherit;background-color: transparent;-webkit-text-decoration-skip: objects;line-height: inherit;color: #1779ba;text-decoration: underline;cursor: pointer;">sales@greenest.ee</a>
                        </td>
                        <td colspan="2" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;border: none;">  <img src="https://greenest.ee/assets/img/layer-37.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin-right: 10px;margin-top: -5px;max-width: 100% !important;">   <span style="box-sizing: inherit;">+372 51 900 330</span>
                          
                        </td>
                       
                    </tr>
                     <tr class="payment" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" colspan="3" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;"> 
                            <span style="box-sizing: inherit;">Payment options for individuals:</span>
                            <a href="https://www.swedbank.ee/private"><img src="https://greenest.ee/assets/img/swedbank.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.seb.ee/ip/ipank"><img src="https://greenest.ee/assets/img/2.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et"><img src="https://greenest.ee/assets/img/3.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.lhv.ee"><img src="https://greenest.ee/assets/img/4.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                        </td>
                       
                    </tr>
                    <tr class="payment" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" colspan="3" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;"> 
                            <span style="box-sizing: inherit;">Payment options for businesses:</span>
                            <a href="https://business.swedbank.ee"><img src="https://greenest.ee/assets/img/swedbank.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.seb.ee/cgi-bin/unet3.sh/ufirma.w"><img src="https://greenest.ee/assets/img/2.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et"><img src="https://greenest.ee/assets/img/3.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.lhv.ee"><img src="https://greenest.ee/assets/img/4.png" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                        </td>
                       
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>

            <h3>Thank you!</h3>
        <h3>greenest.ee team</h3>
    
  </body>
</html> ';










try{

        Mail::send('emails.invoice', $this->data, function($message)
        {
            $message->to('k4shortie@gmail.com')->subject("Order number 502 from greenest.ee");
        });
    } catch (Exception $e){
    echo 'error';
}

//        Mail::send('emails.invoice', ['for_admin' => true, 'order' => $order->load('items.product', 'parcelTerminal')->toArray()], function($message) use ($order)
//		{
//		    $message->to('orders@greenest.ee')->subject("New order $order->id");
//		});

    }


}