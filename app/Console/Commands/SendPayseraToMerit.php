<?php

namespace App\Console\Commands;

use App\Http\Libraries\MeritAktiva\MeritAktiva;
use App\Order;
use App\SendedToMeritOrder;
use Illuminate\Console\Command;

class SendPayseraToMerit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merit:send_paysera';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send to Merit Aktiva payments(Paysera) for paid orders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = SendedToMeritOrder::where('paid', '=', 0)->pluck('order_id')->toArray();

        $orders = Order::whereIn('id', $ids)
            ->where('payment_method', '=', 'e_banking')
            ->where(function ($q) {
                $q->where('status', '=', 'paid')
                    ->orWhere('status', '=', 'completed');
            })
            ->get();

        foreach ($orders as $order) {
            $output = MeritAktiva::setPayment($order);

            if (isset($output->CustomerId)) {
                $send = SendedToMeritOrder::where('order_id', '=', $order->id)->first();
                $send->paid = 1;
                $send->save();
            }

        }
    }
}
