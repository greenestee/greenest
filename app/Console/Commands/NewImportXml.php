<?php

namespace App\Console\Commands;

use App\Product;
use App\Repositories\ImportXmlRepository;
use App\Traits\ShortDescriptionEditorTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use PHPHtmlParser\Dom;
use App\Category;
use App\ProductTmp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mockery\Exception;

class NewImportXml extends Command
{
    use ShortDescriptionEditorTrait;

    const URL_API        = 'https://bioplanet.pl/xmlapi/1/999/UTF8/7eea0802-7dfe-49d5-929e-ce9e19533b73';
    const NAME           = 'nazwa';                 // <nazwa><![CDATA[AMARANTUS EKSPANDOWANY BIO (SUROWIEC) (11 kg) 6]]></nazwa>
    const UNIT           = 'JednostkaPodstawowa';   // <JednostkaPodstawowa><![CDATA[kg ]]></JednostkaPodstawowa>
    const WEIGHT         = 'MasaNetto';             //
    const PRODUCT_URL    = 'LinkDoProduktu';        //<LinkDoProduktu><![CDATA[https://bioplanet.pl/produkt-sojowy-waniliowy-bezglutenowy-bio-400-g-sojade/p90/m]]></LinkDoProduktu>
    const URL            = 'ZdjecieGlowneLinkUrl';  // <ZdjecieGlowneLinkUrl><![CDATA[https://bioplanet.pl/zasoby/import/5/5905198000519.jpg]]></ZdjecieGlowneLinkUrl>
    const SKU            = 'kod_kreskowy';          //<kod_kreskowy><![CDATA[5905198000519]]></kod_kreskowy>
    const PURCHASE_PRICE = 'CenaPoRabacieNetto';    // <CenaPoRabacieNetto>19,2</CenaPoRabacieNetto>
    const IN_STOCK       = 'NaStanie';              // <NaStanie>False</NaStanie>
    const IMAGES_URL     = 'ZdjecieGlowneLinkUrl';  //<ZdjecieGlowneLinkUrl><![CDATA[https://bioplanet.pl/zasoby/import/5/5905198000519.jpg]]></ZdjecieGlowneLinkUrl>
    const QUANTITY       = 'ilosc';
    const ORIGIN_COUNTRY = 'Kraj Producenta';
    const ATTRIBUTES     = 'Atrybuty';              // <Atrybuty>
    const IS_GLUTEN_FREE = 'BEZ GLUTENU';
    const IS_VEGAN       = 'WEGETARIAŃSKI';
    const INGREDIENTS    = '<p>(*Certified organic ingredient)</p>';
    const INGREDIENTS_ET = '<p>(*Kontrollitud mahetoode)</p>';
    const NUTRITIONAL_VALUE = '<p>Energy value  kJ / kcal</p>
                                <p>Fat  g</p>
                                <p>including saturated fatty acids g</p>
                                <p>Carbohydrates  g</p>
                                <p>including sugars  g</p>
                                <p>Protein  g</p>
                                <p>Salt  g</p>
                               ';

    const NUTRITIONAL_VALUE_ET = '';
    const RECOMMENDED_STORAGE    = '<p>Store in a dry and cool place.<br />Store in a dry and cool place, after opening in the refrigerator.<br />Store in a dry, cool and dark place.<br />Store at room temperature.</p>';
    const RECOMMENDED_STORAGE_ET = '<p>Hoida kuivas ja jahedas.<br />Hoida kuivas ja jahedas, pärast avamist külmikus.<br />Hoida kuivas, jahedas ja pimedas.<br />Hoida toatemperatuuril.</p>';
    const BRAND = 'Producent';
    const PRODUCT = 'produkty';

    private $importXmlRepo;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-new-xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import XML';

    /**
     * ImportXML constructor.
     */
    public function __construct()
    {
        $this->importXmlRepo = new ImportXmlRepository();
        parent::__construct();
    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        Cache::put('importIsStarted', true, Carbon::now()->addHours(2));

        ini_set('memory_limit', '256M');
        \Log::info('Import started.');
        $fileXml = $this->getXml(self::URL_API);
        if (!$fileXml) exit;

        try {
            $xml = simplexml_load_file($fileXml, 'SimpleXMLElement', LIBXML_NOCDATA);
            \Log::info('XML loaded successfully.');
        } catch (\Exception $e) {
            \Log::info('Problem with XML.');
        }

        if (!$xml) {
            foreach (libxml_get_errors() as $error) {
                \Log::error('Error loading XML:' . $error->message);
                return false;
            }
        }

        if (count($xml->{self::PRODUCT}) <= 200) {
            return 0;
        }

        $products = Product::where('product_url', 'like', '%bioplanet.pl%')->withTrashed()->get();

        $categoriesWithAvailableFrom = Category::whereNotNull('available_from')->get();

        $counters = [
            'all_in_db' => $products->count(),
            'count_in_xml' => count($xml->{self::PRODUCT})
        ];
        $c = function ($name) use (&$counters) {
            if (!isset($counters[$name])) {
                $counters[$name] = 0;
            }
            $counters[$name]++;
        };

        foreach ($xml->{self::PRODUCT} as $productParse) {
            $trimmed = trim($productParse->{self::SKU});
            $trimmedUrl = trim($productParse->{self::PRODUCT_URL});

            foreach ($products as $key => $product) {

                if (trim($product->sku) == $trimmed) {
                    $c('found');

                    if ($product->trashed()) {
                        $product->restore();
                    }

                    $isAvailableImmediatelyActivated = $this->isAvailableImmediatelyActivated($product);


                    $products->forget($key);
                    if ($product->is_blacklist == 1) {
                        $c('found_black');
                        continue 2;
                    }

                    $dateCategoryAvailableFrom = null;
                    if ($categoriesWithAvailableFrom->count() > 0) {
                        $prodCategIds = $product->categories()->pluck('categories.id')->toArray();
                        $dateCategoryAvailableFrom = $categoriesWithAvailableFrom->whereIn('id', $prodCategIds)
                            ->pluck('available_from')->max();
                    }

                    if ($isAvailableImmediatelyActivated && $product->quantity > 0) {
                        if ($product->status != Product::PRODUCT_IN_STOCK) {
                            $product->status = Product::PRODUCT_IN_STOCK;
                        }
                        if (isset($product->deleted_at)) {
                            $product->restore();
                        }
                        $product->short_description = $this->getShortDescription($product->short_description, 'Product is in stock and available immediately ');
                        $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Toode on laos ja koheselt saadaval ');
                        $product->save();
                        $c('found_AI');
                        continue 2;
                    }

                    // for inStock = 0 and deliveryData = 0
                    if (($productParse->DostepnyOd == '' && $productParse->{self::QUANTITY} == 0) && is_null($dateCategoryAvailableFrom)) {
                        $product->is_active = 0;
                        $product->status = Product::PRODUCT_NOT_IN_STOCK;
                        $product->save();
                        $c('found_unavailable');
                        continue 2;
                    }

                    /* Generate exists description for product */
                    if ($productParse->{self::IN_STOCK} == 'True' && is_null($dateCategoryAvailableFrom)) {
                        if ($isAvailableImmediatelyActivated) {
                            $product->short_description = $this->getShortDescription($product->short_description, 'Product is in stock and available immediately ');
                            $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Toode on laos ja koheselt saadaval ');
                        } else {
                            $product->short_description = $this->getShortDescription($product->short_description, 'We ship within 2-7 working days ');
                            $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Pakiautomaati 2-7 tööpäeva jooksul ');
                        }

                        $product->status = Product::PRODUCT_IN_STOCK;
                        if (isset($product->deleted_at)) {
                            $product->restore();
                        }
                    } else {
                        if (!is_null($dateCategoryAvailableFrom)) {
                            $date = Carbon::parse($dateCategoryAvailableFrom)->format('d.m.Y');
                            $product->delivery_date = $dateCategoryAvailableFrom;
                        } else {
                            $date = date("d.m.Y", strtotime($productParse->DostepnyOd));
                            $product->delivery_date = Carbon::parse($productParse->DostepnyOd)->format('Y-m-d');
                        }
                        if ($product->delivery_date > Carbon::today() && !empty($product->deleted_at)) {
                            $product->restore();
                        }
                        $product->short_description = $this->getShortDescription($product->short_description, 'Out of stock! Expected arrival date ' . $date, false);
                        $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Läbi müüdud! Eeldatav saabumiskuupäev  ' . $date, false);
                        $product->status = Product::PRODUCT_NOT_IN_STOCK;
                    }
                    $product->is_active = 1;

                    $product->product_url = $productParse->{self::PRODUCT_URL};

                    if ($product->image == NULL) {
                        $imagesUrl = $productParse->{self::IMAGES_URL};
                        if (!empty($imagesUrl[0])) {
                            $product->image = $this->getImagesPath((string)$imagesUrl[0]);
                        } else {
                            $product->image = null;
                        }
                    }
                    $oldCount = $product->quantity;
                    $newCount = (int)$productParse->{self::QUANTITY};

                    if ($isAvailableImmediatelyActivated) {
                        if ($product->quantity <= 0) {
                            $product->quantity = $newCount;
                        }
                    } elseif ($oldCount != $newCount) {
                        $product->quantity = $newCount;
                    }

                    $product->is_imported = 1;

                    $product->save();
                    if ($product->purchase_price != 0) {
                        $this->comparePrice($product, $productParse);
                    }

                    continue 2;
                } elseif (trim($product->product_url) == $trimmedUrl) {
                    $products->forget($key);

                    $c('found_debatable');

                    $this->importXmlRepo->updateOrCreateDebatableProductRecord($product, $productParse);

                    continue 2;
                }

            }

            if ($productParse->DostepnyOd == '' && $productParse->{self::QUANTITY} == 0) {
                $c('new_unavailable');
                continue;
            }

            $product = new Product();

            // attributes
            $test = ($productParse->{self::ATTRIBUTES});
            $attributes = $this->getAttributes($test[0]);
            $product->is_gluten_free = $attributes[0][self::IS_GLUTEN_FREE] ? true : false;
            $product->is_vegan = $attributes[0][self::IS_VEGAN] == 'WEGETARIAŃSKI' ? true : false;
            $isCertificateBio = $attributes[0]['Certyfikat bio'] == 'jest' ? true : false;

            $product->sku = trim($productParse->{self::SKU});
            $product->name = $productParse->{self::NAME};
            $product->product_url = $productParse->{self::PRODUCT_URL};

            $product->meta_title = '';
            $product->meta_title_et = '';
            $product->meta_description = '';
            $product->meta_description_et = '';
            $product->meta_keywords = '';
            $product->meta_keywords_et = '';

            $product->purchase_price = $this->getPurchasePrice($productParse->{self::PURCHASE_PRICE});
            $product->price = $this->getPrice($product->purchase_price);

            // unit l, kg, pieces. For 'pieces' a translation of a word
            $unitTmp = str_replace('.', '', $productParse->{self::UNIT});
            $product->unit = ($unitTmp == 'szt') ? 'pieces' : $unitTmp;
            $product->weight = isset($productParse->{self::WEIGHT}) ? (float)$productParse->{self::WEIGHT} : null;

            $product->quantity = (int)$productParse->{self::QUANTITY};
            $product->short_description =
                '<ul><li>Certified organic product</li><li>Free shipping in Estonia from 60 EUR</li>';
            $product->short_description_et =
                '<ul><li>Kontrollitud mahetoode</li><li>Tasuta transport alates 60 EUR</li>';

            // for description
            $outStock = '';
            if ($productParse->{self::IN_STOCK} == 'True') {
                $inStock = 'We ship within 2-7 working days';
                $inStockEt = 'Pakiautomaati 2-7 tööpäeva jooksul';
                $product->status = Product::PRODUCT_IN_STOCK;
                $product->deleted_at = NULL;
            } else {
                $date = date("d.m.Y", strtotime($productParse->DostepnyOd));
                $inStock = 'Out of stock! Expected arrival date ' . $date;
                $inStockEt = 'Läbi müüdud! Eeldatav saabumiskuupäev ' . $date;
                $product->status = 0;

                if (!empty($productParse->DostepnyOd)) {
                    $product->delivery_date = Carbon::parse($productParse->DostepnyOd)->format('Y-m-d');
                }

                $outStock = 'out-stock';
            }
            $product->short_description .= '<li class="delivery ' . $outStock . '">' . $inStock . '</li></ul>';
            $product->short_description_et .= '<li class="delivery ' . $outStock . '">' . $inStockEt . '</li></ul>';

            $product->ingredients = self::INGREDIENTS;
            $product->ingredients_et = self::INGREDIENTS_ET;
            $product->nutritional_value = self::NUTRITIONAL_VALUE;
            $product->nutritional_value_et = self::NUTRITIONAL_VALUE_ET;
            $product->recommended_storage_et = self::RECOMMENDED_STORAGE_ET;
            $product->origin_country = $attributes[0][self::ORIGIN_COUNTRY] ? $attributes[0][self::ORIGIN_COUNTRY] : null;
            $product->origin_country_et = $attributes[0][self::ORIGIN_COUNTRY] ? $attributes[0][self::ORIGIN_COUNTRY] : null;

            $str = $productParse->{self::BRAND};
            $product->brand = substr($str, 0, strpos($str, ' ('));
            $product->brand_et = $product->brand;

            $imagesUrl = $productParse->{self::IMAGES_URL};
            if (!empty($imagesUrl[0])) {
                $product->image = $this->getImagesPath((string)$imagesUrl[0]);
            } else {
                $product->image = null;
            }

            $product->is_imported = 1;

            $product->save();
            $c('new_saved');

            $category = Category::where('slug', 'moderation')->first();
            $product->categories()
                ->attach($category->id);
        }

        foreach ($products as $product) {
            $c('deleted_all');
            if (!$product->trashed()) {
                $product->delete();
                $c('deleted_in_this_import');
            }
        }

        Cache::forget('importIsStarted');

        $memoryUsed = ((memory_get_usage(true) / 1024) / 1024);

        Log::info("Import from bioplanet.xml finished, memory consumption {$memoryUsed}MB");
        Log::info("Import counter: \n" . json_encode($counters, 128));
    }


    /**
     * Return true if menu, where product is located has activated "available immediately" category.
     *
     * @param $product
     * @return bool
     */
    private function isAvailableImmediatelyActivated($product)
    {
        try {
            $isActive = $product->categories()->where('slug', 'like', "%available-immediately")->where('is_active', 1)->exists();

            return $isActive;
        } catch (\Exception $e) {
            \Log::error("Error when checking if given product category is in menu with activated available immediately category :{$product->id}, {$e->getMessage()}");

            return false;
        }
    }

    /**
     * @param $product
     * @param $productParse
     */
    private function comparePrice($product, $productParse)
    {
        $coefficient = 1;
        if ($product->calculate_by_weight_price && $product->unit == 'kg' && !empty($product->weight)) {
            $coefficient = $product->weight;
        }
        $purchase_price  = $this->getPurchasePrice($productParse->{self::PURCHASE_PRICE}, $coefficient);
        $price           = $this->getPrice($purchase_price);

        if ($this->checkProfit($product, $price, $purchase_price) === true) {
            $product->purchase_price = $purchase_price;
            $product->save();
        } else {
            $productTmp = ProductTmp::where('sku', '=', $product->sku)->first();
            if ($productTmp == NULL) {
                $productTmp = ProductTmp::create($product->toArray());
                $productTmp->purchase_price = $purchase_price;
                $productTmp->price = $price;
            } else {
                $productTmp->purchase_price = $purchase_price;
                $productTmp->price = $price;
            }
            $productTmp->save();
        }
    }

    /**
     * @param $product - old product
     * @param $price - new price
     * @param $purchase_price - new price
     * @return bool
     */
    private function checkProfit($product, $price, $purchase_price)
    {
        $sub = $product->price - $price; //if -0.10, then new increased, if +0.20 - decreased

        if (($sub >= -0.1 && $sub <= 0.2) || trim($purchase_price) == trim($product->purchase_price)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * lets say price in PLN is 6,64
     * Which means that purcahse price = 1,93 (6,64/4,2+0,35)
     * Price = 1,93x1,35x1,2=3,13~round up 3,19
     * Price without VAT will be 3,19/1,2=2,66
     *
     * @param $purchasePriceFromXML
     * @return float
     */
    private function getPurchasePrice($purchasePriceFromXML, $coefficient = 1)
    {
        $purchasePriceFromXML = str_replace(',','.',$purchasePriceFromXML);
        $purchasePriceFromXML *= $coefficient;
        $additive = (float) $purchasePriceFromXML <= 7 ? 0.2 : 0.35;
        return round((float)$purchasePriceFromXML / 4.2 + $additive, 2);
    }

    /**
     * @param $purchasePrice
     * @return float
     */
    private function getPrice($purchasePrice)
    {
        $price = (float)$purchasePrice * 1.35 * 1.2;
        $price = (int) ($price * 10);
        $price = $price / 10;
        // 3,13 ~round up 3,19
        $roundUpPrice = $price + 0.09;

        return $roundUpPrice;
    }

    /**
     * get parameters of attributes
     * @param $data
     * @return mixed
     */
    private function getAttributes($data)
    {
        $attributes = $data;
        foreach ($data as $v) {
            $attributes[$v->nazwa] = $v->label;
        }
        return $attributes;
    }

    /**
     * @param $url
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getImagesPath($url)
    {
        // return products/March2017/YydiXJIsaQaWJ7XvR3mx.jpggit
        $fileName = Str::random(20) . '.jpg';

        $folderName = date('F') . date('Y');

        if (!file_exists(storage_path('app/public/products/' . $folderName))) {
            mkdir(storage_path('app/public/products/' . $folderName), 0777, true);
        }

        $storagePath = 'products/' . $folderName . '/' . $fileName;

        if ($this->loadingFile($url, storage_path('app/public/' . $storagePath))) {
            return $storagePath;
        } else {
            return false;
        }

    }

    /**
     * @param $url
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getXml($url)
    {
        $fileName = 'bioplanet_pl.xml';
        $storagePath = storage_path('import_xml/' . $fileName);

        if($this->loadingFile($url, $storagePath)) {
            return $storagePath;
        } else {
            return false;
        }
    }

    /**
     * @param $url
     * @param $storagePath
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function loadingFile($url, $storagePath)
    {
        $client = new \GuzzleHttp\Client();
        try {
            $client->request(
                'GET',
                $url,
                ['sink' => $storagePath]
            );
        } catch (Exception $e) {
            Log::error('Error download from ' . $url);
            return false;
        }

        return true;
    }
}
