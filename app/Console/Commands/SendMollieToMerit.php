<?php

namespace App\Console\Commands;

use App\Http\Libraries\MeritAktiva\MeritAktiva;
use App\Order;
use App\SendedToMeritOrder;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendMollieToMerit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merit:send_mollie';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send to Merit Aktiva payments(Mollie) for orders paid 8 days ago.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unpaidOrders = SendedToMeritOrder::where('paid', '=', 0)
            ->select('order_id')->get();
        $ids = [];

        foreach ($unpaidOrders as $order) {
            $ids[] = $order->order_id;
        }

        $orders = Order::whereIn('id', $ids)
            ->where('payment_method', '=', 'card_payment')
            ->where(function ($query) {
                $query->where('mollie_payment_status', '=', 'Paid')
                    ->orWhere('status', '=', 'paid');
            })
            ->get();

        $now = Carbon::now();

        foreach ($orders as $order) {
            $updated = Carbon::createFromFormat('Y-m-d H:i:s', $order->updated_at);

            if ($updated->diffInDays($now) >= 8) {
                $output = MeritAktiva::setPayment($order);

                if (isset($output->CustomerId)) {
                    $send = SendedToMeritOrder::where('order_id', '=', $order->id)->first();

                    $send->paid = 1;
                    $send->save();
                }
            }
        }
    }
}
