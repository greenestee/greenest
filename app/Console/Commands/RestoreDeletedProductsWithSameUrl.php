<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;

class RestoreDeletedProductsWithSameUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restore-deleted-products-with-same-url';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products1 = Product::where('short_description', 'LIKE', '%50 EUR%')->get();

        foreach ($products1 as $product) {

            $product->short_description = str_replace('50 EUR', '60 EUR', $product->short_description);


            $product->save();

            echo $product->id . "\n";
        }

        $products2 = Product::where('short_description_et', 'LIKE', '%50 EUR%')->get();
//
        foreach ($products2 as $product) {


            $product->short_description_et = str_replace('50 EUR', '60 EUR', $product->short_description_et);

            $product->save();

            echo $product->id . "\n";
        }
        dump($products1, $products2);

//        $trashedProducts = Product::onlyTrashed()->where('sku', '!=', '5907814664266-greenest')->get();
//        $count = 0;
//
//        foreach ($trashedProducts as $trashedProduct) {
//            if (isset($trashedProduct->product_url)
//                && !empty($trashedProduct->product_url)
//                && Product::where('product_url', $trashedProduct->product_url)
//                    ->where('sku', 'not like', $trashedProduct->sku)
//                    ->exists()) {
//
//                $product =  Product::where('product_url', $trashedProduct->product_url)->where('sku', 'not like', $trashedProduct->sku)->first();
//
//
//                $trashedProduct->update(['sku' => $product->sku]);
//
//                $trashedProduct->restore();
//                $product->forceDelete();
////echo $product->id . " deleted \n";
////echo $trashedProduct->id . " restored \n";
//                $count++;
//            }
//        }
//
//        dd($trashedProducts->count(), $count, 'finish!');
    }
}
