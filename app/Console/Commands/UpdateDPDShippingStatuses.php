<?php

namespace App\Console\Commands;

use App\Http\Libraries\DPD\DPD;
use App\OrderShipment;
use Illuminate\Console\Command;

class UpdateDPDShippingStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:dpd-shipment-statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get last DPD shipments statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $service = new DPD();
        $shipments = OrderShipment::where('provider', 'DPD')->get();

        if ($shipments->count() > 0) {
            $shipments = $shipments->chunk(29);
            foreach ($shipments as $chunked) {
                $ids = $chunked->pluck('shipment_id')->toArray();
                $ids = implode('|', $ids);
                $statuses = $service->getStatuses($ids);
                if (!empty($statuses)) {
                    foreach ($statuses as $status) {
                        if (empty($status->error) && !empty($status->details[0]->status)) {
                            OrderShipment::where('provider', 'DPD')
                                ->where('shipment_id', $status->parcelNumber)
                                ->update(['status' => $status->details[0]->status]);
                        }
                    }
                }
                sleep(1);
            }
        }
    }
}
