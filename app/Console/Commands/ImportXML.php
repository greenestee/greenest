<?php

namespace App\Console\Commands;
use PHPHtmlParser\Dom;

use App\Category;
use App\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mockery\Exception;

class ImportXML extends Command
{
    const URL_API        = 'https://bioplanet.pl/xmlapi/1/999/UTF8/7eea0802-7dfe-49d5-929e-ce9e19533b73';
    const NAME           = 'nazwa';                 // <nazwa><![CDATA[AMARANTUS EKSPANDOWANY BIO (SUROWIEC) (11 kg) 6]]></nazwa>
    const UNIT           = 'JednostkaPodstawowa';   // <JednostkaPodstawowa><![CDATA[kg ]]></JednostkaPodstawowa>
    const WEIGHT         = 'MasaNetto';             //
    const PRODUCT_URL    = 'LinkDoProduktu';        //<LinkDoProduktu><![CDATA[https://bioplanet.pl/produkt-sojowy-waniliowy-bezglutenowy-bio-400-g-sojade/p90/m]]></LinkDoProduktu>
    const URL            = 'ZdjecieGlowneLinkUrl';  // <ZdjecieGlowneLinkUrl><![CDATA[https://bioplanet.pl/zasoby/import/5/5905198000519.jpg]]></ZdjecieGlowneLinkUrl>
    const SKU            = 'kod_kreskowy';          //<kod_kreskowy><![CDATA[5905198000519]]></kod_kreskowy>
    const PURCHASE_PRICE = 'CenaPoRabacieNetto';    // <CenaPoRabacieNetto>19,2</CenaPoRabacieNetto>
    const IN_STOCK       = 'NaStanie';              // <NaStanie>False</NaStanie>
    const IMAGES_URL     = 'ZdjecieGlowneLinkUrl';  //<ZdjecieGlowneLinkUrl><![CDATA[https://bioplanet.pl/zasoby/import/5/5905198000519.jpg]]></ZdjecieGlowneLinkUrl>
    const QUANTITY       = 'ilosc';
    const ORIGIN_COUNTRY = 'Kraj Producenta';
    const ATTRIBUTES     = 'Atrybuty';              // <Atrybuty>
    const IS_GLUTEN_FREE = 'BEZ GLUTENU';
    const IS_VEGAN       = 'WEGETARIAŃSKI';
    const INGREDIENTS    = '<p> </p>';
    const INGREDIENTS_ET = '<p>(*Kontrollitud mahetoode)</p>';
    const NUTRITIONAL_VALUE = '<p>Energy value  kJ / kcal</p>
                                <p>Fat g</p>
                                <p>including saturated fatty acids g</p>
                                <p>Mono-unsaturated fat g</p>
                                <p>Polyunsaturated fat g</p>
                                <p>Carbohydrate g</p>
                                <p>including sugars g</p>
                                <p>Protein g</p>
                                <p>Salt  g</p>
                                <p>Fibers  g</p>
                                <p>Vitamin E  mg ( %*)</p>';

    const NUTRITIONAL_VALUE_ET = '<p>Energiasisaldus  kJ /  kcal</p>
                                <p>Rasvad g</p>
                                <p>s.h küllastunud rasvhappeid g</p>
                                <p>monoküllastumata rasvhapped g</p>
                                <p>polüküllastumata rasvhapped g</p>
                                <p>Süsivesikuid  g</p>
                                <p>neist suhkruid  g</p>
                                <p>Valke  g</p>
                                <p>Soola  g</p>
                                <p>Kiudaineid  g</p>
                                <p>Vitamiin E  mg ( %*)</p>';
    const RECOMMENDED_STORAGE    = ' <p>Store at room temperature and out of direct sunlight.</p><p>Store in a dry and cool place</p>';
    const RECOMMENDED_STORAGE_ET = '<p>Säilita toetemperatuuril. Pärast avamist, külmkapis.</p><p>Hoia kuivas ja jahedas.</p>';
    const BRAND = 'Producent';
    const PRODUCT = 'produkty';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import XML';

    /**
     * ImportXML constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileXml = $this->getXml(self::URL_API);
        if (!$fileXml) exit;

        $xml = simplexml_load_file($fileXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        if (!$xml) {
            foreach (libxml_get_errors() as $error) {
                Log::error('Error loading XML:' . $error->message);
                return false;
            }
        }

        foreach ($xml->{self::PRODUCT} as $productParse) {

            $product = Product::where('sku', $productParse->{self::SKU})->first();

            if (!$product) {

                if($productParse->DostepnyOd == '' && $productParse->{self::QUANTITY} == 0) continue;
                $product = new Product();

                // attributes
                $attributes              = $this->getAttributes(($productParse->{self::ATTRIBUTES})[0]);
                $product->is_gluten_free = $attributes[0][self::IS_GLUTEN_FREE] ? true : false;
                $product->is_vegan       = $attributes[0][self::IS_VEGAN] == 'WEGETARIAŃSKI' ? true : false;
                $isCertificateBio        = $attributes[0]['Certyfikat bio'] == 'jest' ? true : false;

                $product->sku            = $productParse->{self::SKU};
                $product->name           = $productParse->{self::NAME};
                $product->product_url    = $productParse->{self::PRODUCT_URL};

                $product->meta_title     = '';
                $product->meta_title_et  = '';
                $product->meta_description    = '';
                $product->meta_description_et = '';
                $product->meta_keywords       = '';
                $product->meta_keywords_et    = '';

                $product->purchase_price  = $this->getPurchasePrice($productParse->{self::PURCHASE_PRICE});
                $product->price           = $this->getPrice($product->purchase_price);

                // unit l, kg, pieces. For 'pieces' a translation of a word
                $unitTmp = str_replace('.', '', $productParse->{self::UNIT});
                $product->unit = ($unitTmp == 'szt') ? 'pieces' : $unitTmp;
                $product->weight = isset($productParse->{self::WEIGHT}) ? (float)$productParse->{self::WEIGHT} : null;

                $product->quantity = (int)$productParse->{self::QUANTITY};
                $product->short_description =
                    '<ul><li>Certified organic product</li><li>Free shipping in Estonia from 60 EUR</li>';
                $product->short_description_et =
                    '<ul><li>Kontrollitud mahetoode</li><li>Tasuta transport alates 60 EUR</li>';

                // for description
                $outStock = '';
                if ($productParse->{self::IN_STOCK} == 'True') {
                    $inStock = 'We ship within 2-5 working days';
                    $inStockEt = 'Pakiautomaati 2-5 tööpäeva jooksul';
                    $product->status = Product::PRODUCT_IN_STOCK;

                } else {
                    $date = date("d.m.Y", strtotime($productParse->DostepnyOd));
                    $inStock = 'Out of stock! Expected arrival date ' . $date;
                    $inStockEt = 'Läbi müüdud! Eeldatav saabumiskuupäev ' . $date;
                    $product->status = 0;

                    if (!empty($productParse->DostepnyOd)) {
                        $product->delivery_date = $productParse->DostepnyOd;
                    }

                    $outStock = 'out-stock';
                }
                $product->short_description .= '<li class="delivery ' . $outStock . '">' . $inStock . '</li></ul>';
                $product->short_description_et .= '<li class="delivery ' . $outStock .'">' . $inStockEt . '</li></ul>';

                $product->ingredients = self::INGREDIENTS;
                $product->ingredients_et = self::INGREDIENTS_ET;
                $product->nutritional_value = self::NUTRITIONAL_VALUE;
                $product->nutritional_value_et = self::NUTRITIONAL_VALUE_ET;
                $product->recommended_storage_et = self::RECOMMENDED_STORAGE_ET;
                $product->origin_country = $attributes[0][self::ORIGIN_COUNTRY] ? $attributes[0][self::ORIGIN_COUNTRY] : null;
                $product->origin_country_et = $attributes[0][self::ORIGIN_COUNTRY] ? $attributes[0][self::ORIGIN_COUNTRY] : null;

                $str = $productParse->{self::BRAND};
                $product->brand = substr($str, 0, strpos($str, ' ('));
                $product->brand_et = $product->brand;

                $imagesUrl = $productParse->{self::IMAGES_URL};
                if (!empty($imagesUrl[0])) {
                    $product->image = $this->getImagesPath($imagesUrl[0]);
                } else {
                    $product->image = null;
                }

                $product->save();

                $category = Category::where('slug', 'moderation')->first();
                $product->categories()
                    ->attach($category->id);

            } else {  // for existing products

                if($product->is_blacklist == 1){
                    continue;
                }


                // for inStock = 0 and deliveryData = 0
                if(($productParse->DostepnyOd == '' && $productParse->{self::QUANTITY} == 0)) {
                    $product->is_active = 0;
                    $product->status = Product::PRODUCT_NOT_IN_STOCK;
                    $product->save();
                    continue;
                }

                /* Generate exists description for product */
                if ($productParse->{self::IN_STOCK} == 'True') {
                    $product->short_description = $this->getShortDescription($product->short_description, 'We ship within 2-5 working days ');
                    $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Pakiautomaati 2-5 tööpäeva jooksul ');
                    $product->status = Product::PRODUCT_IN_STOCK;
                } else {
                    $date = date("d.m.Y", strtotime($productParse->DostepnyOd));
                    $product->delivery_date = $productParse->DostepnyOd;
                    $product->short_description = $this->getShortDescription($product->short_description, 'Out of stock! Expected arrival date ' . $date, false);
                    $product->short_description_et = $this->getShortDescription($product->short_description_et, 'Läbi müüdud! Eeldatav saabumiskuupäev  '. $date, false);
                    $product->status = Product::PRODUCT_NOT_IN_STOCK;
                }
                $product->is_active = 1;

                $oldCount = $product->quantity;
                $newCount = (int)$productParse->{self::QUANTITY};
                if ($oldCount != $newCount) {
                    $product->quantity = $newCount;
                }

                $product->save();
            }

        }
        Log::info('import xml from bioplanet.xml');
    }


    /*
        lets say price in PLN is 6,64
        Which means that purcahse price = 1,93 (6,64/4,2+0,35)
        Price = 1,93x1,35x1,2=3,13~round up 3,19
        Price without VAT will be 3,19/1,2=2,66
    */
    /**
     * @param $purchasePriceFromXML
     * @return float
     */
    private function getPurchasePrice($purchasePriceFromXML)
    {
        $purchasePriceFromXML = str_replace(',','.',$purchasePriceFromXML);
        return round((float)$purchasePriceFromXML / 4.2 + 0.35, 2);
    }

    /**
     * @param $purchasePrice
     * @return float
     */
    private function getPrice($purchasePrice)
    {
        $price = round((float)$purchasePrice * 1.35 * 1.2, 1);

        // 3,13 ~round up 3,19
        $roundUpPrice = $price + 0.09;

        return $roundUpPrice;
    }

    /**
     * get parameters of attributes
     * @param $data
     * @return mixed
     */
    private function getAttributes($data)
    {
        $attributes = $data;
        foreach ($data as $v) {
            $attributes[$v->nazwa] = $v->label;
        }
        return $attributes;
    }


    /**
     * get storage path for image of products
     * @param $url
     * @return string
     */
    private function getImagesPath(string $url)
    {
        // return products/March2017/YydiXJIsaQaWJ7XvR3mx.jpggit
        $fileName = Str::random(20) . '.jpg';

        $folderName = date('F') . date('Y');

        if (!file_exists(storage_path('app/public/products/' . $folderName))) {
            mkdir(storage_path('app/public/products/' . $folderName), 0777, true);
        }

        $storagePath = 'products/' . $folderName . '/' . $fileName;

        if ($this->loadingFile($url, storage_path('app/public/' . $storagePath))) {
            return $storagePath;
        } else {
            return false;
        }

    }

    /**
     * @return string
     */
    private function getXml($url)
    {
        $fileName = 'bioplanet_pl.xml';
        $storagePath = storage_path('import_xml/' . $fileName);

        if($this->loadingFile($url, $storagePath)) {
            return $storagePath;
        } else {
            return false;
        }
    }

    /**
     * @param $url
     * @param $storagePath
     * @return bool
     */
    private function loadingFile($url, $storagePath){
        $client = new \GuzzleHttp\Client();
        try {
            $client->request(
                'GET',
                $url,
                ['sink' => $storagePath]
            );
        } catch (Exception $e) {
            Log::error('Error download from ' . $url);
            return false;
        }

        return true;
    }

    private function getShortDescription($shortDescription, $newText, $inStock = true)
    {
        $dom = new Dom;
        $dom->load($shortDescription);
        $li = $dom->find('li.delivery')[0];

        if ($li) {

            $li->delete();

            $ul = $dom->find('ul')[0];
            $newLi = new \PHPHtmlParser\Dom();

            if (!$inStock) {
                $outStock = 'out-stock';
            } else {
                $outStock = '';
            }

            $newLi->load('<li class="delivery ' . $outStock . '" >' . $newText . '</li>');

            $ul->addChild($newLi->root);

            return $dom->outerHtml;

        } else {
            return $shortDescription;
        }

    }
}
