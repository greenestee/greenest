<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;
use Illuminate\Support\Facades\Storage as Storage;
use App\Page;

class SitemapCreate extends Command
{
    const SITEMAP_EN = 'sitemap.xml';
    const SITEMAP_ET = 'sitemap-et.xml';
    const SITEMAP_STARTER_STRING = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'."\n";
    const SITEMAP_FINISH_STRING = '</urlset>';
    const PER_PAGE = 30;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * SitemapCreate constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit','256M');
        \Log::info('Sitemap creating started');
        // get id for category "moderation"
        $categoryModeration = Category::where('slug', 'moderation')->first();

        $categories = Category::where('is_main', 1)
            ->where('id','!=', $categoryModeration->id)
            ->where('is_active', 1)
            ->get();

        $data = self::SITEMAP_STARTER_STRING
            . $this->getSitemapBody($categories, 'en')
            . self::SITEMAP_FINISH_STRING;

        Storage::disk('public_sitemap')->put(self::SITEMAP_EN, $data);

        unset($data);

        $data = self::SITEMAP_STARTER_STRING
            . $this->getSitemapBody($categories, 'et')
            . self::SITEMAP_FINISH_STRING;
        Storage::disk('public_sitemap')->put(self::SITEMAP_ET, $data);
    }

    /**
     * @param $categories
     * @param $lng
     * @return string
     */
    public function getSitemapBody($categories, $lng)
    {

        $data = '';
        //:TODO refactoring/ in config (array)
        if ($lng == 'en'){
            $url = "https://greenest.ee/en/";
            $product = "product/";
            $slug = 'slug';
            $pages_url = 'pages/';
        } else {
            $url = "https://greenest.ee/et/";
            $product = "toode/";
            $slug = 'slug_et';
            $pages_url = 'pages/';
        }

        foreach ($categories as $category) {

            // category
            $data .= $this->getXmlBlock(
                $url . urlencode($category->$slug),
                'daily',
                0.5
            );

            // child categories
            $childCategies = $category->children->where('is_active', 1);
            foreach ($childCategies as $childCategory) {

                // child category
                $data .= $this->getXmlBlock(
                    $url . "{$category->$slug}?category={$childCategory->$slug}",
                    'monthly',
                    0.2);

                // Write products by category
                $productsByCategory = $childCategory->products->where('is_active', 1);
                foreach ($productsByCategory as $productByCategory) {
                    // sitemapBlockProduct
                    $data .= $this->getXmlBlock(
                        $url . $product . urlencode($productByCategory->$slug),
                        'weekly',
                        0.5
                    );
                }
            }
        }


        // pages
        $pages = Page::where('status', 'ACTIVE')->get();
        foreach ($pages as $page) {
            // category
            $data .= $this->getXmlBlock(
                $url . $pages_url . urlencode($page->$slug),
                'daily',
                0.5
            );
        }

        return $data;
    }



    /**
     * generate xml block
     * @param $item
     * @param $changefreq
     * @param $priority
     * @return string
     */
    public function getXmlBlock($item, $changefreq, $priority)
    {
        return <<<EOT
        \n
<url>
    <loc>$item</loc>
    <changefreq>$changefreq</changefreq>
    <priority>$priority</priority>
</url>
EOT;
    }
}