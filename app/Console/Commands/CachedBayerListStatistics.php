<?php

namespace App\Console\Commands;

use App\Repositories\BuyerStatisticRepository;
use Illuminate\Console\Command;

class CachedBayerListStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cached:bayer-list-statistic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $buyerStatRep;

    public function __construct(BuyerStatisticRepository $buyerStatisticRepository)
    {
        parent::__construct();
        $this->buyerStatRep = $buyerStatisticRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->buyerStatRep->getOrderStat();
    }


}
