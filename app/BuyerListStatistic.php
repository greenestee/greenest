<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyerListStatistic extends Model
{
    public $timestamps = false;
}
