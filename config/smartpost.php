<?php

return [

    'login' => env('SMARTPOST_LOGIN', null),
    'password' => env('SMARTPOST_PASSWORD', null),
    'labelsFormat' => env('SMARTPOST_LABELS_FORMAT', 'A7-8')
];