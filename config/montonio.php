<?php

return [


    'mode' => [
        'is_test' => env('MONTONIO_MODE_IS_TEST', false)
    ],



    'api' => [
        'live' => [
            'key' => env('MONTONIO_API_KEY_LIVE'),
            'secret' => env('MONTONIO_API_SECRET_LIVE'),

            'url' => 'https://api.payments.montonio.com/pis',
        ],
        'test' => [
            'key' => env('MONTONIO_API_KEY_TEST'),
            'secret' => env('MONTONIO_API_SECRET_TEST'),

            'url' => 'https://api.sandbox-payments.montonio.com/pis',
        ],
    ],

    'redirect' => [
        'live' => [
            'url' => 'https://payments.montonio.com',
        ],
        'test' => [
            'url' => 'https://sandbox-payments.montonio.com',
        ],
    ]
];