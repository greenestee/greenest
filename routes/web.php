<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('language/{locale}', function($locale) {
    \App::setLocale($locale);
    return redirect()->back();
});*/

if (env('TECH_WORKS', false)) {
    Route::any('/{any?}', function () {
        return view('technical-works');
    });
}

Route::get('language/{locale}', 'PagesController@changeLanguage')->name('changeLang');

Route::get('/logout', function () {
    Cart::destroy();
    \Auth::logout();
    return \Redirect::back();
})->name('logout');

/* BINDING ROUTES */
Route::bind('category_slug', function ($slug) {
    $locale = App::getLocale();
    if ($locale == 'et') {
        $url = 'slug_et';
    } else {
        $url = 'slug';
    }

    $data = App\Category::where($url, $slug)->isMain()->first();
    if ($data)
    {
        return App\Category::where($url, $slug)->isMain()->firstOrFail();
    }
    else
    {
        die(redirect('/'));
       // return redirect('');
    }
});

Route::bind('page_slug', function ($slug) {
    $locale = App::getLocale();
    if ($locale == 'et') {
        $url = 'slug_et';
    } else {
        $url = 'slug';
    }
    return App\Page::where($url, $slug)->active()->firstOrFail();
});

/* ADMIN ROUTES */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::group(['middleware' => 'admin.user'], function () {
        //Uncomment to import products to partyware
        Route::get('new-products-import-to-partyware', 'ProductsController@importForPartywareView')->name('partyware.get');
        Route::post('new-products-import-from-xls-to-partyware', 'ProductsController@importXlsForPartyware')->name('partyware-xls.post');
        Route::post('new-products-import-images-from-zip-to-partyware', 'ProductsController@importImagesForPartyware')->name('partyware-images.post');
        Route::post('updatecountries', 'ProductsController@updateCountries')->name('file.country.post');
        Route::get('downloadcountries', 'ProductsController@downloadCountries')->name('file.country.get');
        Route::get('/downloadlabels', 'ProductsController@getLabels')->name('download.labels');
        Route::get('/export', 'ProductsController@exportToExcel')->name('exportToExcel');
        Route::get('categories/activate/{category}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getCategoryActivate')->name('activeCategory');
        Route::get('categories/disable/{category}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getCategoryDisable')->name('disableCategory');
        Route::get('products/activate/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getProductActivate')->name('activeProduct');
        Route::get('products/disable/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getProductDisable')->name('disableProduct');
        Route::get('products/setblacklist/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@setBlackList')->name('blackList');
        Route::post('buyer-list-statistics', '\App\Http\Controllers\Voyager\VoyagerBreadController@getTopBuyerPeriod')->name('top.buyer.year');

        Route::post('dpd-generate-labels', 'OrderController@makeDPDLabels')->name('dpd.make.labels');
        Route::post('dpd-reject-shipment', 'OrderController@rejectDPDShipment')->name('dpd.delete.shipment');
        Route::post('dpd-renew-shipment', 'OrderController@renewDPDShipment')->name('dpd.renew.shipment');
        Route::get('dpd-shipping', 'OrderController@viewListDPDShipping')->name('admin.view.dpd.sipping.status.list');

        Route::get('download/refrigerated/cards', 'OrderController@downloadRefrigeratedShippingCards')->name('download.refrigerated.package.cards');

        Route::post('change-order-item-quantity', 'OrderController@changeOrderItemQuantity');

        // view coupons
        Route::get('coupons', 'CouponController@viewList');
        // add
        Route::post('coupons/create', 'CouponController@store')->name('admin.coupon.store');
        // edit
        Route::post('coupons/update', 'CouponController@update')->name('admin.coupon.update');
        // delete
        Route::post('coupons/delete', 'CouponController@delete')->name('admin.coupon.delete');

        Route::get('/confirm-price', '\App\Http\Controllers\Voyager\VoyagerPriceController@getConfirmPrice')->name('voyager.confirmPrice.index');
        Route::post('/confirm/new/price/{productId}', '\App\Http\Controllers\Voyager\VoyagerPriceController@acceptNewPrice')->name('voyager.confirmPrice.accept');
        Route::post('/confirm/edit/price/{productId}', '\App\Http\Controllers\Voyager\VoyagerPriceController@editNewPrice')->name('voyager.confirmPrice.edit');
        Route::post('/confirm/reject/price/{productId}', '\App\Http\Controllers\Voyager\VoyagerPriceController@rejectNewPrice')->name('voyager.confirmPrice.reject');
        Route::post('/confirm/delete', '\App\Http\Controllers\Voyager\VoyagerPriceController@deleteProductTmp')->name('voyager.confirmPrice.delete');


        Route::get('/deleted-product', '\App\Http\Controllers\Voyager\VoyagerDeletedProductController@index')->name('voyager.deleted.product.index');
        Route::get('/deleted-product/{id}/edit', '\App\Http\Controllers\Voyager\VoyagerDeletedProductController@editDeletedProduct')->name('edit.deleted.product');
        Route::put('/deleted-product/update/{id}', '\App\Http\Controllers\Voyager\VoyagerDeletedProductController@update')->name('deleted.product.update');

        Route::post('/translate/word/{id}/remove',  '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@removeTranslateWord')->name('remove.word.translate');
        Route::get('/translate/word', '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@getViewTranslate')->name('voyager.get.view.translate.word');
        Route::post('/translate/word/add', '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@addTranslateWord')->name('add.word.translate');
        Route::get('/translate/{word}', '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@getViewTranslate')->name('voyager.get.translate.word');
        Route::post('/translate/word/{id}/edit', '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@editTranslateWord')->name('edit.translate.word');

        Route::post('/get/translate/text', '\App\Http\Controllers\Voyager\VoyagerTranslateWordController@getTranslateText')->name('get.translate.text');
    });

});




/* CHECKOUT ROUTES */
Route::group(['prefix' => 'checkout'], function () {
	Route::get('/', 'CartController@getCheckout')->middleware('updateUserCart');
	Route::post('/', 'CartController@postCheckout');
	Route::get('/invoice/{order}', 'CartController@getInvoice');
    Route::get('/getPayseraIp/{order}', 'CartController@getPayseraIp');
    Route::get('/getPayseraPayments/{order}', 'CartController@getPayseraPayments');
	Route::get('/success/{orderId}/{sign}', 'CartController@successSwedbankPayment');
	Route::post('/success/{orderId}/{sign}', 'CartController@successSwedbankPayment');

    Route::get('/redirect/{orderId}', 'MontonioController@redirect')->name('montonio.payment.redirect');
    Route::post('/webhook/{orderId}', 'MontonioController@webhook')->name('montonio.payment.webhook');
});

Route::group(['prefix' => 'ebanking'], function () {
	Route::get('/', 'CartController@ebanking');
	Route::get('/accept', 'CartController@ebankingAccept');
	Route::get('/cancel', 'CartController@ebankingCancel');
	Route::get('/callback', 'CartController@ebankingCallback');

});


/* CART ROUTES */
Route::group(['prefix' => 'cart', 'middleware'=>'updateUserCart'], function () {
	Route::post('/add/{product}', 'CartController@getAdd');
	Route::post('/take/{product}', 'CartController@getTake');
    Route::post('/update_quantity/{product}', 'CartController@getUpdate')->name('cartGetUpdate');

    Route::get('/remove/{product}', 'CartController@getRemove');
    Route::get('/destroy', 'CartController@getDestroy');
    //for google tag manager
    Route::get('ajax/data_layer/{step}', 'Admin\PermissionController@ajaxGetDataLayer');
    Route::get('ajax/data_layer/{step1}', 'Admin\PermissionController@ajaxGetDataLayer');
    Route::get('ajax/data_layer/{step1}', 'Admin\PermissionController@ajaxGetDataLayer');
    //Ajax routes for data layer
    Route::get('ajax/data_layer_checkout/', 'CartController@fetchDataLayerForCheckoutAjax');
});

/* CORS ROUTES*/
Route::group(['middleware' => 'cors'], function() {
    Route::post('/webhook-verification', 'CartController@webhookVerification');
});

/* SITE ROUTES */
Route::group(['middleware'=>'updateUserCart'], function () {
    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'BlogController@index')->name('articles');
        Route::get('/{category}/{slug}', 'BlogController@getArticle')->name('article');
    });
    Route::group(['prefix' => 'blogi'], function () {
        Route::get('/', 'BlogController@index')->name('articles');
        Route::get('/{category}/{slug}', 'BlogController@getArticle')->name('article_et');
    });


    Route::get('/', 'PagesController@getWelcome');
    Route::get('/status/{order}', 'CartController@getStatusPayment');
    Route::get('/refund/{order}', 'CartController@refundPayment')->name('refund');
//    Route::get('/webhook-verification', 'CartController@webhookVerification');

//    Route::get('/test', 'Payment\PaymentController@index');
//    Route::get('/listactivated', 'Payment\PaymentController@listActivatedmethods'); //:TODO remove
//    Route::get('/status/{id}', 'Payment\PaymentController@status');
    Route::get('/search', 'SearchController@getResult');
    Route::get('/{category_slug}', 'ProductsController@getList')->name('category');
    Route::get('/pages/{page_slug}', 'PagesController@getPage')->name('page');

    Route::get('/product/{slug}', 'ProductsController@getProductItem')->name('products');
    Route::get('/toode/{slug}', 'ProductsController@getProductItem')->name('products_et');
});

/* AJAX SITE ROUTES */
Route::post('/cart_add', 'Ajax\Cart@addProduct');
Route::post('/cart_add_spec', 'Ajax\Cart@displayProductsCount');

Route::post('/dynamic_search', 'DynamicSearchController@index');

Auth::routes();

Route::get('/password/success', function () {
    return view('auth.passwords.success');
});

/*Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});*/
