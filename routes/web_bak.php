<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('language/{locale}', function($locale) {
    \App::setLocale($locale);
    return redirect()->back();
});*/

Route::get('language/{locale}', 'PagesController@changeLanguage')->name('changeLang');

Route::get('/logout', function () {
    Cart::destroy();
    \Auth::logout();
    return \Redirect::back();
})->name('logout');

/* BINDING ROUTES */
Route::bind('category_slug', function ($slug) {
    $locale = App::getLocale();
    if ($locale == 'et') {
        $url = 'slug_et';
    } else {
        $url = 'slug';
    }
    return App\Category::where($url, $slug)->isMain()->firstOrFail();
});

Route::bind('page_slug', function ($slug) {
    $locale = App::getLocale();
    if ($locale == 'et') {
        $url = 'slug_et';
    } else {
        $url = 'slug';
    }
    return App\Page::where($url, $slug)->active()->firstOrFail();
});

/* ADMIN ROUTES */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('categories/activate/{category}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getCategoryActivate')->name('activeCategory');
    Route::get('categories/disable/{category}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getCategoryDisable')->name('disableCategory');
    Route::get('products/activate/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getProductActivate')->name('activeProduct');
    Route::get('products/disable/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@getProductDisable')->name('disableProduct');
    Route::get('products/setblacklist/{product}', '\App\Http\Controllers\Voyager\VoyagerBreadController@setBlackList')->name('blackList');
});




/* CHECKOUT ROUTES */
Route::group(['prefix' => 'checkout'], function () {
	Route::get('/', 'CartController@getCheckout')->middleware('updateUserCart');
	Route::post('/', 'CartController@postCheckout');
	Route::get('/invoice/{order}', 'CartController@getInvoice');
    Route::get('/getPayseraIp/{order}', 'CartController@getPayseraIp');
    Route::get('/getPayseraPayments/{order}', 'CartController@getPayseraPayments');

});

Route::group(['prefix' => 'ebanking'], function () {
	Route::get('/', 'CartController@ebanking');
	Route::get('/accept', 'CartController@ebankingAccept');
	Route::get('/cancel', 'CartController@ebankingCancel');
	Route::get('/callback', 'CartController@ebankingCallback');

});


/* CART ROUTES */
Route::group(['prefix' => 'cart', 'middleware'=>'updateUserCart'], function () {
	Route::get('/add/{product}', 'CartController@getAdd');
	Route::get('/take/{product}', 'CartController@getTake');
	Route::get('/remove/{product}', 'CartController@getRemove');
    Route::get('/destroy', 'CartController@getDestroy');
    //for google tag manager
    Route::get('ajax/data_layer/{step}', 'Admin\PermissionController@ajaxGetDataLayer');
    Route::get('ajax/data_layer/{step1}', 'Admin\PermissionController@ajaxGetDataLayer');
    Route::get('ajax/data_layer/{step1}', 'Admin\PermissionController@ajaxGetDataLayer');
    //Ajax routes for data layer
    Route::get('ajax/data_layer_checkout/', 'CartController@fetchDataLayerForCheckoutAjax');
});

/* CORS ROUTES*/
Route::group(['middleware' => 'cors'], function() {
    Route::post('/webhook-verification', 'CartController@webhookVerification');
});

/* SITE ROUTES */
Route::group(['middleware'=>'updateUserCart'], function () {
    Route::get('/', 'PagesController@getWelcome');
    Route::get('/status/{order}', 'CartController@getStatusPayment');
    Route::get('/refund/{order}', 'CartController@refundPayment')->name('refund');
//    Route::get('/webhook-verification', 'CartController@webhookVerification');

//    Route::get('/test', 'Payment\PaymentController@index');
//    Route::get('/listactivated', 'Payment\PaymentController@listActivatedmethods'); //:TODO remove
//    Route::get('/status/{id}', 'Payment\PaymentController@status');
    Route::get('/search', 'SearchController@getResult');
    Route::get('/{category_slug}', 'ProductsController@getList')->name('category');
    Route::get('/pages/{page_slug}', 'PagesController@getPage')->name('page');

    Route::get('/product/{slug}', 'ProductsController@getProductItem')->name('products');
    Route::get('/toode/{slug}', 'ProductsController@getProductItem')->name('products_et');
});

/* AJAX SITE ROUTES */
Route::post('/cart_add', 'Ajax\Cart@addProduct');
Route::post('/cart_add_spec', 'Ajax\Cart@displayProductsCount');


Auth::routes();

Route::get('/password/success', function () {
    return view('auth.passwords.success');
});

/*Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});*/
