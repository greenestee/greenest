<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/download-smartpost-package-cards', 'OrderController@createSmartpostPackageCards')->name('download.smartpost.package.cards');

Route::post('/check-printed', 'OrderController@checkPrinted')->name('check.printed.cards');

Route::post('/check-coupon', 'CouponController@checkCoupon');

Route::post('/complete-partially-order', 'OrderController@completePartiallyOrder')->name('admin.order.itemStatus');

Route::post('/change-order-item-status', 'OrderController@changeOrderItemStatus')->name('admin.order.itemStatus');
Route::post('/change-order-item-comment', 'OrderController@changeOrderItemComment')->name('admin.order.change.item.comment');
Route::post('/send-missing-order-item-notification', 'OrderController@sendMissingProductNotification')->name('admin.order.sendMissingNotification');
Route::post('/send-delay-forwarded-orders-notification', 'OrderController@sendDelayedOrdersNotification')->name('admin.order.sendDelayedNotification');

Route::get('/admin/accept-debatable-product/{id}', 'ProductsController@acceptDebatableProduct')->name('admin.acceptDebatableProduct');

Route::post('/send-answer-on-missing-product', 'OrderController@receiveClientAnswerOnMissingProduct')->name('client.sendAnswerOnMissingProduct');


