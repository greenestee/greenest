<?php

return [
    'shop-name'             => 'Nature Design OÜ',
    'shop-description'      => 'Organic & Biodegradable products',
    'payer'                 => 'Maksja',
    'mail'                  => 'E-mail',
    'reg-nr'                => 'Reg nr',
    'phone'                 => 'Telefon',
    'date'                  => 'Kuupäev',
    'invoice-number'        => 'Arve number',
    'ref-number'            => 'Viitenumber',
    'pay-date'              => 'Makse tähtpäev',
    'product-name'          => 'Toode',
    'quantity'              => 'Kogus',
    'net-price'             => 'Hind',
    'sum'                   => 'Summa',
    'vat'                   => 'Käibemaks',
    'total-sum'             => 'Summa kokku',
    'payment-individual'    => 'Maksevõimalused eraisikutele',
    'payment-businesses'    => 'Maksevõimalused ettevõtetele',
    'pay-options'           => 'Makseviisid',
    'dear'                  => 'Hea',
    'invoice'               => 'Arve',
    'client'                => 'klient',
    'thy'                   => 'Täname',
    'team'                  => 'pere',
    'status'                => 'STAATUS',
];