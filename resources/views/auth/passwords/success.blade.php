@extends('layouts.app')

@section('title', ' - Reset Password')



@section('body-class', 'class="text"')

@section('content')
    <div class="row">
        <div class="text-wrapper large-12 column">
            
            <div class="wrapper-form">
                {!! Form::open(['action' => 'Auth\ResetPasswordController@reset', 'method' => 'POST']) !!}
                    <p>Email {!! Form::email('email', null, ['style' => 'width:290px;']) !!}</p>
                    <p>New password {!! Form::password('password', ['style' => 'width:290px;']) !!}</p>
                   <p>Password confirmation {!! Form::password('password_confirmation', ['style' => 'width:290px;']) !!}</p>
                    <button type="submit" class=" place-order" style="width:120px;">Reset</button>
                {!! Form::close() !!}
             </div>
            @if (count($errors) > 0)
                <br>
                <div class="alert callout" style="width:290px;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="modalHome reveal" id="success" aria-hidden="false" data-reveal="y9oyqj-reveal" role="dialog" data-yeti-box="success" data-resize="success" tabindex="-1" style="display: none;top: 247px;padding: 0;">
        <div class="modalHome-header ">
          <a href="#close" class="btn-close" aria-hidden="true" data-close>&#215;</a> <span id="modal-title">Success</span>
        </div>
         
        <div class="modalHome-body">
            <div style="width:100%;min-height:180px;background-color:#fff;">
                <div class="text-center">You password is changed and you are logged in now. Happy shopping.</div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
  var popup = new Foundation.Reveal($('#success'));
  popup.open();
</script>
@endpush