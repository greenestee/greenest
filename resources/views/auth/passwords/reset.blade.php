@extends('layouts.app')

@section('title', ' - Reset Password')



@section('body-class', 'class="text"')

@section('content')
    <div class="row">
        <div class="text-wrapper large-12 column">
            
            <div class="wrapper-form">
                {!! Form::open(['action' => 'Auth\ResetPasswordController@reset', 'method' => 'POST']) !!}
                    <p>Email {!! Form::email('email', null, ['style' => 'width:290px;']) !!}</p>
                    <p>New password {!! Form::password('password', ['style' => 'width:290px;']) !!}</p>
                    <p>Password confirmation {!! Form::password('password_confirmation', ['style' => 'width:290px;']) !!}</p>

                    {!! Form::hidden('token', $token) !!}
                    <button type="submit" class=" place-order" style="width:120px;">Reset</button>
                {!! Form::close() !!}
             </div>
            @if (count($errors) > 0)
                <br>
                <div class="alert callout" style="width:290px;">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection