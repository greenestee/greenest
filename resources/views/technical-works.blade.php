@extends('layouts.welcome')

@section('content')

    <div class="row" style="height: 300px;">
        <div class="large-2 medium-2 small-1 columns">&nbsp;</div>
        <div class="text-center large-8 medium-8 small-10 columns" style="background-color: #170701c9;border-radius: 20px;color: #f4f4f4;">
            <br>
            <br>
            @if (App::getLocale() == 'en')
                <h1>Dear clients!</h1>
                <p>greenest.ee is moving to the new server to make it much faster. It should take a couple of hours. </p>
                <p>Please come back a little bit later. Sorry for the inconvenience.</p>
            @else
                <h1>Kallid kliendid!</h1>
                <p>greenest.ee kolib oma poe uude serverisse, et pood muutuks kiiremaks.</p>
                <p>Tulge natukese aja pärast tagasi.</p>
            @endif
            <br>
            <br>
            <br>
        </div>
        <div class="large-2 medium-2 small-1 columns">&nbsp;</div>
    </div>

@endsection