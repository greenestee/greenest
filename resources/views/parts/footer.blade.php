    <div class="vsz_cart-scroll">
      <div class="shopping-cart @if(Cart::count()) cart_active @endif">
          <span id="rtf">{{ getProductsCount() }}</span>
      </div>
    </div>
    <footer>
      <div class="bottom-menu">
        <div class="row">

<!--           <div class="large-3 columns">
          &nbsp
          </div> -->

          <!-- <div class="large-9 columns"> -->
          <div class="large-12 columns">

            <div class="bottom-nav">
              <ul>
                  @if (isset($footerNav))
                  @foreach($footerNav as $key => $item)
                    <li><a href="{{ url($key) }}">{{ $item }}</a></li>
                  @endforeach
                  @endif
              <ul>
            </div> 

          </div>
        </div>  

        <div class="green-bg">
          <div class="row">
              <div class="large-12 columns">
                <div>
                  <span>(c) {{ Carbon\Carbon::now()->year }}  {{ App::getLocale() == 'et' ? Voyager::setting('copyright_et') : Voyager::setting('copyright') }}</span>
                </div>  
              </div>
          </div>  
        </div>  
      </div>
    </footer>