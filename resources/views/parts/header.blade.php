    <header>
      <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
      <meta name="author" content="Greenest" />
      <meta name="rating" content="General"/>
<META NAME="Category" CONTENT="Food">
<meta name="copyright" content="https://greenest.ee/ - 2019"/>
<META NAME="Language" CONTENT="en">
<meta name="robots" content="index, follow"/>
<meta name="googlebot" content="index, follow"/>
<meta name="revisit-after" content="1 days"/>
<META NAME="Publisher" CONTENT="ee">
<meta name="Classification" content="Food, Shopping ">
<meta name="distribution" content="global" />
<link rel="canonical" href="https://greenest.ee/" />
<link rel="prerender" href="https://greenest.ee/">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="icon" href="favicon.png" type="image/x-icon" />
<link rel="alternate" type="text/directory" title="vCard" ref="vCard.vcf" />
<link type="text/plain" rel="author" href="humans.txt" />
      <div class="top-bg">
        <div class="row">
          <div class="large-3 medium-3 columns">
            <div class="logo">
              <a href="{{ App::getLocale() == 'et' ? url('/toit-ja-jook') : url('/food-and-drinks') }}">
                <img src="{{ App::getLocale() == 'et' ? asset('/Greenest-logo-et.png') : asset('/Greenest-logo.png') }}" class="logo-img-{{ App::getLocale() }}">
              </a>
            </div>  
          </div>
          <div class="large-6 medium-6 columns ">
            <div class="callback">
              <span>{{ App::getLocale() == 'et' ? Voyager::setting('header_first_row_et') : Voyager::setting('header_first_row') }}</span><br/>
              <span>{{ App::getLocale() == 'et' ? Voyager::setting('header_second_row_et') : Voyager::setting('header_second_row') }}</span>
              <span class="tel">{{ Voyager::setting('phone_number') }}</span>
            </div>
          </div>     
          <div class="large-3 medium-3 columns">
            <div class="shopping-cart @if(Cart::count()) cart_active @endif"> 
              <a href="{{ action('CartController@getCheckout') }}"><span><img src="{{ asset('/cart.png') }}"><span id="cartInfo">{{ getCartInfo() }}</span></span></a>
              <div class="title-bar" data-responsive-toggle="example-menu2" data-hide-for="medium" style="display: block;">
                <button class="menu-icon" type="button" data-toggle="example-menu2"></button>
                <div class="title-bar-title"></div>
              </div>
            </div>
          </div>  
        </div>
      </div>   
     
     <div class="top-menu">
       <div class="row">
          <div class="large-9 columns ">
            <nav class="menu site-menu" id="example-menu2">
              <ul>
                  @if (isset($headerNav))
                    @foreach($headerNav as $key => $item)
                      <li><a href="{{ url($key) }}" @if(Request::is($key)) class="active-item" @endif>{{ $item }}</a></li>
                    @endforeach
                  @endif
              </ul>
            </nav>  
          </div>     
          <div class="large-3 columns">
            <div class="search">
              <!-- <div class="row"> -->

                    {!! Form::open(['action' => 'SearchController@getResult', 'class' => 'form-wrapper form-wrapper-button', 'method' => 'get']) !!}
                        @if(Auth::check())
                            <div class="search-custom search-custom-out">
                                <a href="{{ route('logout') }}" id="search-button-red" class="user-auth">Log Out</a>
                            </div>
                        @else
                            <div class="search-custom">
                                  <a href="#" id="search-button"></a>
                            </div>
                        @endif
                            <div class="search-custom">
                                {{-- {!! Form::input('text', 'q', null, ['class' => 'dynamic-search-field', 'id' => 'search', 'placeholder' => request()->get('q') ? : trans('static_text.Search'), 'minlength' => 2]) !!} --}}
                                <select
                                    class="dynamic-search-field"
                                    id="search"
                                    name="q"
                                    data-placeholder="@lang('static_text.Search')"
                                    data-count-text='@lang('static_text.SearchCount')'
                                > </select>

                            </div>
                        <div class="dropdown">
                            <select class="languages-select" onchange="location=this.value">
                                @php
                                    $currentLocale = App::getLocale();
                                    $languages = config('languages.locale');
                                    foreach ($languages as $key => $name) {
                                        if ($currentLocale == $key) {
                                            echo '<option value="' . route('changeLang', $key) . '" selected="selected">'
                                             . $name . '</a></option>';
                                        } else {
                                            echo '<option value="' . route('changeLang', $key) . '" >'
                                             . $name . '</a></option>';
                                        }
                                    }
                                @endphp
                            </select>
                            {{--{!! Form::select('size', $languages, 'en') !!}--}}
                        </div>
                    {!! Form::close() !!}
               <!-- </div>    -->
            </div>  
          </div>  
        </div>

        <div class="vsz-catalogue-toggle">
          <a class="menu-icon" href="#"></a>
          <div class="clearfix"></div>
        </div> 
      
      </div>
      
    </header>
