<div class="modalHome reveal" id="sign-in" aria-hidden="false" data-reveal="y9oyqj-reveal" role="dialog" data-yeti-box="sign-in" data-resize="sign-in" tabindex="-1" style="display: none;top: 247px;padding: 0;">
    <div class="modalHome-header ">
        <a href="#close" class="btn-close" aria-hidden="true" data-close>&#215;</a> <span id="modal-title">Login</span>
    </div>

    <div class="modalHome-body">
        <div style="width:100%;min-height:180px;background-color:#fff;">
            {!! Form::open(['url' => 'login', 'method' => 'POST', 'id' => 'sign_in']) !!}
            {!! Form::email('email', null, ['placeholder' => 'Enter email']) !!}
            {!! Form::password('password', ['placeholder' => 'Enter password', 'id' => 'password']) !!}
            {!! Form::button('Login', ['type' => 'submit', 'class' => 'Login-button']) !!}
            <div style="text-align:center;"><a href="#" id="forgot-pass">Forgot password?</a></div>
            {!! Form::close() !!}
        </div>
    </div>
</div>