@php
    if (App::getLocale() == 'et') {
        $name = $product->name_et;
        $brand = $product->brand_et;
        $ingredients = $product->ingredients_et;
        $nutritional_value = $product->nutritional_value_et;
        $recommended_storage = $product->recommended_storage_et;
        $allergenic_information = $product->allergenic_information_et;
        $description = $product->description_et;
        $method_of_preparation = $product->method_of_preparation_et;
        $how_to_use = $product->how_to_use_et;
        $origin_country = $product->origin_country_et;
        $origin_country_of_ingredients = $product->origin_country_of_ingredients_et;
        $shelf_life = $product->shelf_life_et;
    } else {
        $name = $product->name;
        $brand = $product->brand;
        $ingredients = $product->ingredients;
        $nutritional_value = $product->nutritional_value;
        $recommended_storage = $product->recommended_storage;
        $allergenic_information = $product->allergenic_information;
        $description = $product->description;
        $method_of_preparation = $product->method_of_preparation;
        $how_to_use = $product->how_to_use;
        $origin_country = $product->origin_country;
        $origin_country_of_ingredients = $product->origin_country_of_ingredients;
        $shelf_life = $product->shelf_life;
    }
@endphp

<div class="reveal xlarge" id="product{{ $product->id }}" data-reveal>

    @if($product->is_gluten_free)
        <img data-src="{{ asset('/img/gluten.png') }}" alt="" src="{{ $imgplaceholder }}" class="@if($product->is_vegan) gluten @else vegan @endif">
        <noscript>
            <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
        </noscript>
    @endif

    @if($product->is_vegan)
        <img data-src="{{ asset('/img/vegan.png') }}" alt="" src="{{ $imgplaceholder }}" class="vegan">
        <noscript>
            <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
        </noscript>
    @endif



    <div class="row"><h3>{{ $name }}</h3></div>

    <div class="row">

        <div class="large-6 small-12 columns">

            <div class="vsz_img_wrapper">

                <img data-src="{{ $product->getImage() }}" src="{{ $imgplaceholder }}" alt="{{ $product->name }}" class="lazy-modal-img">
                <noscript>
                    <img src="{{ $product->getImage() }}" alt="{{ $product->name }}">
                </noscript>

            </div>

        </div>

        <div class="large-6 small-12 columns">

            @if($brand)

                <h4>{{ trans('static_text.Brand') }}</h4>

                {{ $brand }}</br>

            @endif



            @if($ingredients)

                <h4>{{ trans('static_text.Ingredients') }}</h4>

                {!! $ingredients !!}

            @endif



            @if($nutritional_value)

                <h4>{{ trans('static_text.Nutritional_value_of_100_g') }}</h4>

                {!! $nutritional_value !!}

            @endif



            @if($recommended_storage)

                <h4>{{ trans('static_text.Recommended_storage') }}</h4>

                {!! $recommended_storage !!}

            @endif



            @if($allergenic_information)

                <h4>{{ trans('static_text.Allergenic_information') }}</h4>

                {!! $allergenic_information !!}

            @endif

        </div>

        {{-- <div class="row">

            <div class="large-12 small-12 columns vsz_more_information">

                <p class="vsz_pieces">{!! $product->getAmount() !!}</p>

                <p class="price">€{{ $product->getPrice() }}</p>

                <div class="btn-wrapper">

                    <button type="button" @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @endif class="alert button buy_btn" data-product="{{ $product->id }}" data-page="single" onclick="dataLayer.push({'event':'addToCart','ecommerce':{'currencyCode':'EUR','add':{'actionField':{'list':'Product Page'},'products':[{'name':'{{ $product->name }}','sku':'{{ $product->sku }}','price':'{{ $product->getPrice() }}','category':'@if(isset($mainCategory->slug)) {{ $mainCategory->slug }} @endif ','quantity':1}]}}});">{{ trans('static_text.buy') }}</button>

                </div>

                <p class="vsz_per_price">{{ $product->getPricePerPiece() }} €</p>

            </div>

        </div> --}}



        {{-- <div class="row">

            <div class="large-12 small-12 columns">

                @if($description)

                    <div class="inline-row">

                        <h4>{{ trans('static_text.Description') }}</h4>

                        {!! $description !!}

                    </div>

                @endif

                @if($method_of_preparation)

                    <div class="inline-row">

                        <h4>{{ trans('static_text.Method_of_preparation') }}</h4>

                        {!! $method_of_preparation !!}

                    </div>

                @endif

                @if($how_to_use)

                    <div class="inline-row">

                        <h4>{{ trans('static_text.How_to_use') }}</h4>

                        {!! $how_to_use !!}

                    </div>

                @endif

                @if($origin_country)

                    <div class="country-row">

                        <h4>{{ trans('static_text.Country_of_origin') }}</h4>

                        {!! $origin_country !!}

                    </div>

                @endif

                @if($origin_country_of_ingredients)

                    <div class="country-row">

                        <h4>{{ trans('static_text.Country_of_origin_of_ingredients') }}</h4>

                        {!! $origin_country_of_ingredients !!}

                    </div>

                @endif

                @if($shelf_life)

                    <div class="inline-row">

                        <h4>{{ trans('static_text.Shelf_life') }}</h4> {!! $shelf_life !!} days

                    </div>

                @endif

            </div>

        </div> --}}



    </div>

    <button class="close-button" data-close aria-label="Close modal" type="button">

        <span aria-hidden="true">&larr;</span>

    </button>

</div>
