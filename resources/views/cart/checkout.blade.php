@extends('layouts.app')

@php
	$locale = App::getLocale();
@endphp

@section('title', ' - Checkout')

@section('gtm-dataLayer')
	<script>
		{!!  session('dataLayer') !!}
	</script>
@endsection

@section('body-class', 'class="checkout"')

@section('google-tranlsate')
	@if($locale != 'et')
		<div id="google_translate_element" style="width: 13%;">
			<a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
		</div>
		<script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
		</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	@endif
@endsection

@section('content')



	@if(!Cart::count())



		<div class="row text-center">

			<h2>@lang('checkout.Empty cart')</h2>

		</div>



	@else



		<div class="row small-collapse">

			<div class="large-12 columns order-rewiew">

				<img src="{{ asset('/order.png') }}"/><span>@lang('checkout.Order Review')</span>

			</div>

		</div>



			@include('cart.items')



	      <div class="row small-collapse order">

	          <div class="large-12 columns empty-cart">

	            <a href="{{ action('CartController@getDestroy') }}">@lang('checkout.Empty cart')</a>

	          </div>

	      </div>

          <div class="row small-collapse total-value">
            <div class="large-6 columns">&nbsp;</div>
            <div class="large-6 columns">
                <div class="text-left" style="display: inline-block;">
                    <table cellpadding="0" cellspacing="0" border="0" valign="top">
                        <tbody>
                            <tr>
                                <td><span>@lang('checkout.Subtotal'):</span><br></td>
                                <td><span id="vsz-subtotal">{{ Cart::subtotal() }}</span> EUR</td>
                            </tr>
                            <tr>
                                <td><span>@lang('checkout.Shipping and handling'):</span></td>
                                <td><span id="shipping">0,00</span> EUR<br></td>
                            </tr>
                            <tr>
                                <td><span>@lang('checkout.SUM'):</span></td>
                                <td><span id="subtotal" data-value="{{ Cart::subtotal() }}">{{ Cart::subtotal() }}</span> EUR</td>
                            </tr>
                            <tr>
                                <td><span>@lang('checkout.VAT 20%'):</span></td>
                                <td><span id="tax" data-value="{{ Cart::tax() }}">{{ Cart::tax() }}</span> EUR</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><span>@lang('checkout.TOTAL SUM'):</span></th>
                                <th><span><span id="total"  data-value="{{ Cart::total() }}">{{ Cart::total() }}</span> EUR</span></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>



          </div>



	      <div class="row  checkout-buttons">

	          <div class="large-3 column">

	               &nbsp;

	          </div>

	           <div class="large-3 column">

	            <a class=" red-button" href="{{ $back_url }}">@lang('checkout.BACK TO THE SHOPPING')</a>

	          </div>

	           <div class="large-3 column">

	            <a href="#checkout" class="red-button">

                     @lang('checkout.GO TO CHECKOUT')

	            	<img src="{{ asset('/arrow.png') }}">

	            </a>

	          </div>

	          <div class="large-3 column ">

	             &nbsp;

	          </div>

	      </div>

           <div style="width: 300px; margin: 0 auto; text-align: center;">
                <label>
                    <input type="checkbox" id="need-company-info" />
                    @lang('checkout.Need_company_info')
                </label>
           </div>


      	@include('cart.form')



      @endif



      @include('cart.modals')



@endsection



@section('css')

    <link href="{{ asset('/css/jquery.placecomplete.css') }}" rel="stylesheet" />

@endsection



@push('scripts')

<script>
$('form.form_class').submit(function(e){
  if( $(this).hasClass('form-submitted') ){
    e.preventDefault();
    return;
  }
  $(this).addClass('form-submitted');
});

</script>



	<script src="{{ asset('/assets/js/ui.js') }}"></script>

	<script src="{{ asset('/js/jquery.placecomplete.js') }}"></script>

	<script src="{{ asset('/js/vendor/foundation.min.js') }}"></script>

	<script src="{{ asset('/js/vendor/what-input.js') }}"></script>

  	<script src="{{ asset('/js/vendor/custom.js') }}"></script>
    <script src="{{ asset('/js/custom.js') }}"></script>

        <script>

            function updateQuantity(target, value) {
                if (!target) return;
                if (!value) {
                    target.value = '';
                    return;
                }
                value = value && value > 1 ? value : 1;
                target.value = value;
                var id = $(target).attr('data-id');
                var form = $(target.form);
                var tooltip = form.find('.checkout-tooltip');
                tooltip.hide();
                $('#checkout_shipping_stock_title').hide();
                $('#checkout_shipping_stock_options').hide();
                $('#checkout_shipping_options').find('input[type=radio]').prop('checked', false).prop('required', false);
                $('.last-form-block').removeClass('small-centered');

                $.ajax({
                    url: '/cart/update_quantity/' + form.attr('data-id'),
                    method: 'POST',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {
                        quantity: value
                    },
                    success: function(response) {
                        form.find('.checkout-tooltip-container').data('stock', response.in_stock || 0);
                        if (!!response.is_available_immediately) {
                            if (!!response.enough_in_stock) {
                                tooltip.text("@lang('checkout.enough_in_stock')");
                                tooltip.closest('.product-cart-item').find('.top-name-label').show();
                            } else {
                                tooltip.text((response.in_stock === 1
                                            ? "@lang('checkout.not_enough_in_stock')"
                                            : "@lang('checkout.not_enough_in_stock_multiple')")
                                                    .replace(/\${in_stock}/gi, response.in_stock || 0));
                                tooltip.closest('.product-cart-item').find('.top-name-label').hide();
                            }
                        } else {
                            if (response.cart_content === 'all_on_order') {
                                /*tooltip.text("@lang('checkout.all_on_order_cart_content')");*/
                                tooltip.text("{!! str_replace("\n", '\n', Lang::get('checkout.all_on_order_cart_content')) !!}");
                            } else {
                                tooltip.text("@lang('checkout.mixed_cart_content')");
                            }
                        }
                        tooltip.css('display', '');
                        if (response.cart_content !== 'mixed') {
                            $('#checkout_shipping_stock_title').hide();
                            $('#checkout_shipping_stock_options').hide();
                            $('#checkout_shipping_options').find('input[type=radio]').prop('checked', false).prop('required', false);
                            $('.last-form-block').removeClass('small-centered');
                        } else {
                            $('#checkout_shipping_stock_title').show();
                            $('#checkout_shipping_stock_options').show();
                            $('#checkout_shipping_options').find('input[type=radio]').prop('required', true);
                            $('.last-form-block').addClass('small-centered');
                        }

                        console.log(response.cart_weight);

                        if (response.cart_weight && response.cart_weight >= 30) {
                            $('#radio_shipping_label_parcel_terminal').hide();
                            $('#radio_shipping_text_parcel_terminal').hide();
                            $('#radio_shipping_br_parcel_terminal').hide();
                            $('#parcel_terminal_data_fields').hide();

                            $('#radio_shipping_label_courier_estonia').hide();
                            $('#radio_shipping_text_courier_estonia').hide();
                            $('#radio_shipping_br_courier_estonia').hide();

                            if ($('#radio_shipping_label_heavy_shipping').css('display') == 'none') {
                                $('#radio_shipping_label_heavy_shipping').show();
                                $('#radio_shipping_text_heavy_shipping').show();
                                $('#radio_shipping_br_heavy_shipping').show();

                                $('#radio_shipping_button_heavy_shipping').prop('checked', true);
                            }
                        }
                        // else if (response.cart_weight && response.cart_weight >= 20 && response.cart_weight < 30) {
                        //     $('#radio_shipping_label_parcel_terminal').hide();
                        //     $('#radio_shipping_text_parcel_terminal').hide();
                        //     $('#radio_shipping_br_parcel_terminal').hide();
                        //     $('#parcel_terminal_data_fields').hide();
                        //
                        //     $('#radio_shipping_label_heavy_shipping').hide();
                        //     $('#radio_shipping_text_heavy_shipping').hide();
                        //     $('#radio_shipping_br_heavy_shipping').hide();
                        //
                        //     if ($('#radio_shipping_label_courier_estonia').css('display') == 'none') {
                        //         $('#radio_shipping_label_courier_estonia').show();
                        //         $('#radio_shipping_text_courier_estonia').show();
                        //         $('#radio_shipping_br_courier_estonia').show();
                        //     }
                        //
                        //     $('#radio_shipping_button_courier_estonia').prop('checked', true);
                        // }
                        else {
                            $('#radio_shipping_label_heavy_shipping').hide();
                            $('#radio_shipping_text_heavy_shipping').hide();
                            $('#radio_shipping_br_heavy_shipping').hide();

                            if ($('#radio_shipping_label_courier_estonia').css('display') == 'none') {
                                $('#radio_shipping_label_courier_estonia').show();
                                $('#radio_shipping_text_courier_estonia').show();
                                $('#radio_shipping_br_courier_estonia').show();
                            }

                            if ($('#radio_shipping_label_parcel_terminal').css('display') == 'none') {
                                $('#parcel_terminal_data_fields').show();
                                $('#radio_shipping_label_parcel_terminal').show();
                                $('#radio_shipping_text_parcel_terminal').show();
                                $('#radio_shipping_br_parcel_terminal').show();

                                $('#radio_shipping_button_parcel_terminal').prop('checked', true);
                            }
                        }

                        dataLayer.push(response.dataLayer);
                        form.find('.item-price').text(response.item && response.item.price || '-');
                        form.find('.item-netprice').text(response.item && response.item.netPrice || '-');
                        $('#vsz-subtotal').text(response.subtotal).attr('data-value', response.subtotal);
                        $('#subtotal').text(response.subtotal).attr('data-value', response.subtotal);
                        $('#tax').text(response.tax).attr('data-value', response.tax);
                        $('#total').text(response.totalSum).attr('data-value', response.totalSum);
                        if ($('input[name=shipping_method]:checked').length) {
                            var shipping = Number($('input[name=shipping_method]:checked').attr('data-value')).toFixed(2);
                            response.subtotal = parseFloat(response.subtotal.replace(',', '.').replace(' ', ''));
                            if (response.subtotal > 50 && $('input[name=shipping_method][value=parcel_terminal]:checked').length) {
                                shipping = (0).toFixed(2);
                            }
                            var subtotal = Number(parseFloat($('#subtotal').attr('data-value').replace(',', '.').replace(' ', '')) + parseFloat(shipping)).toFixed(2);
                            var tax = Number(parseFloat($('#tax').attr('data-value').replace(',', '.').replace(' ', '')) + parseFloat(shipping * 0.2)).toFixed(2);
                            var total = Number(parseFloat(subtotal)  + parseFloat(tax)).toFixed(2);
                            // var shippingControls = $('input[name=shipping_method]');
                            var shippingParcel = $('input[name=shipping_method][value=parcel_terminal]');
                            var parcelValue = parseFloat(shippingParcel.attr('data-value') || 0);
                            var parcelTextValue = (Math.ceil(parcelValue * 100 * 1.2) / 100.0).toFixed(2).replace('.', ',');
                            var parcelTextEl = shippingParcel.closest('.radio-label').next('.radio-text');
                            if (response.subtotal > 50) {
//                                shipping = (0).toFixed(2);
                                parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
                                // shippingControls.each(function(ind, el) {
                                //     var textEl =  $(el).closest('.radio-label').next('.radio-text')
                                //     textEl.text((textEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
                                // });
                            } else {
                                parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' ' + parcelTextValue));
                                // shippingControls.each(function(ind, el) {
                                //     var shippingValue = parseFloat($(el).attr('data-value') || 0);
                                //     var textShippingValue = (Math.ceil(shippingValue * 100 * 1.2) / 100.0).toFixed(2).replace('.', ',');
                                //     var textEl =  $(el).closest('.radio-label').next('.radio-text');
                                //     textEl.text((textEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' ' + textShippingValue));
                                // });
                            }

                            $('#shipping').text(shipping.replace('.', ','));
                            $('#subtotal').text(subtotal.replace('.', ','));
                            $('#tax').text(tax.replace('.', ','));
                            $('#total').text(total.replace('.', ','));
                        }
                        var amountInCart = response.amount_in_cart || 0;
                        var cartInfo = $('#cartInfo').text(amountInCart + ' '
                            + (amountInCart == 1 ? '@lang('static_text.1_product')' : '@lang('static_text.multiple_products')')
                            + ' = ' + response.totalSum + ' €');
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            }

            function setCaretPosition(ctrl, start, end) {
                if (ctrl.setSelectionRange){
                    ctrl.focus();
                    ctrl.setSelectionRange(start, end);
                } else if (ctrl.createTextRange) {
                    var range = ctrl.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', end);
                    range.moveStart('character', start);
                    range.select();
                }
            };

            $('input[name=quantity]').on('input', function(event){
                updateQuantity(this, parseInt(this.value.replace(/[^0-9]/g, '')));
            });
            $('input[name=quantity]').on('click', function(event){
                var target = this;
                if (target && target.value) {
                    setCaretPosition(target, 0, target.value.length);
                }
            });
            $('input[name=quantity]').on('blur', function(event){
                var value = parseInt(this.value.replace(/[^0-9]/g, ''));
                if (!value) {
                    updateQuantity(this, 1);
                }
            });
            $('input[name=quantity]').on('keydown', function(event){
                if (event.key === 'Enter' || event.keyCode === 13) {
                    var nextInput = $(this).closest('.product-cart-item').next('.product-cart-item').find('input[name=quantity]');
                    nextInput.focus();
                    if (nextInput.length && nextInput[0].value) {
                        setCaretPosition(nextInput[0], 0, nextInput[0].value.length);
                    }
                }
            });
            $('a[data-action=decrease]').on('click', function(event){
                var target = $(this).closest('.count-input').find('input[name=quantity]')[0];
                target.focus();
                updateQuantity(target, (parseInt(target && target.value.replace(/[^0-9]/g, '') || '1') - 1) || 1);
                if (target && target.value) {
                    setCaretPosition(target, 0, target.value.length);
                }
                return false;
            });
            $('a[data-action=increase]').on('click', function(event){
                var target = $(this).closest('.count-input').find('input[name=quantity]')[0];
                target.focus();
                updateQuantity(target, parseInt(target && target.value.replace(/[^0-9]/g, '') || '1') + 1);
                if (target && target.value) {
                    setCaretPosition(target, 0, target.value.length);
                }
                return false;
            });

            var activeQuantityInput = $('.product-cart-item--mixed').first().find('input[name=quantity]');
            if (!activeQuantityInput.length) {
                activeQuantityInput = $('.product-cart-item').first().find('input[name=quantity]');
            }
            var touched = false;
            activeQuantityInput.click().focus();
            if (activeQuantityInput.length && activeQuantityInput[0].value) {
                setCaretPosition(activeQuantityInput[0], 0, activeQuantityInput[0].value.length);
            }

            var $el = $("#address");

            $(function() {
                // $("[name=shipping_method]:checked").empty() && $("[name=shipping_method]").first().trigger('change');

                @if((float)str_replace(' ', '', Cart::subtotal()) > (float)50)
                    if($("[name=shipping_method]").size()){
                        // var shippingControls = $('input[name=shipping_method]');
                        // shippingControls.each(function(ind, el) {
                        //     var textEl =  $(el).closest('.radio-label').next('.radio-text');
                        //     textEl.text((textEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
                        // });
                        var shippingParcel = $('input[name=shipping_method][value=parcel_terminal]');
                        var parcelValue = parseFloat(shippingParcel.attr('data-value') || 0);
                        var parcelTextValue = (Math.ceil(parcelValue * 100 * 1.2) / 100.0).toFixed(2).replace('.', ',');
                        var parcelTextEl = shippingParcel.closest('.radio-label').next('.radio-text');
                        parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
                    }
                @endif
                if($("[name=shipping_method]:checked").size()){
                    var originProductsSubtotal = $('#vsz-subtotal').text().replace(',', '.').replace(' ', '');
                    var shippingMethod = 0;
                    var shipingExtraFee = parseFloat($('input[name=shipping_stock]:checked').attr('data-value') || 0);
                    if (!(parseFloat(originProductsSubtotal) > 50 && $('input[name=shipping_method][value=parcel_terminal]:checked').length)) {
                        shippingMethod = parseFloat($('input[name=shipping_method]:checked').attr('data-value') || 0);
                    }
                    var shipping = shippingMethod + shipingExtraFee;
                    var subtotal = parseFloat($('#subtotal').attr('data-value').replace(',', '.').replace(' ', '')) + shipping;
                    var tax = parseFloat($('#tax').attr('data-value').replace(',', '.').replace(' ', '')) + (shipping * 0.2);
                    var total = subtotal + tax;

                    $('#shipping').text(shipping.toFixed(2).replace('.', ','));
                    $('#subtotal').text(subtotal.toFixed(2).replace('.', ','));
                    $('#tax').text(tax.toFixed(2).replace('.', ','));
                    $('#total').text(total.toFixed(2).replace('.', ','));
                }

                $el.placecomplete({

                    placeServiceResult:function(data,status){
                        var dict = window.sessionStorage ? JSON.parse(window.sessionStorage.getItem('checkout') || '{}') : {};

                        $('#billing_address').val($('#select2-address-container').text());
                        dict['billing_address'] = {value: $('#select2-address-container').text()};

                        data.address_components.forEach(function(component) {

                          component.types.forEach(function(type) {

                            if (type === 'administrative_area_level_1') {

                              $("input[name=billing_state]").val(component.long_name)
                              dict['billing_state'] = {value: component.long_name || ''};
                            }

                            if (type === 'locality') {

                              $("input[name=billing_city]").val(component.long_name)
                              dict['billing_city'] = {value: component.long_name || ''};
                            }

                            if (type === 'country') {

                              $("input[name=billing_country]").val(component.long_name)
                              dict['billing_country'] = {value: component.long_name || ''};
                            }

                            if (type === 'postal_code') {

                              $("input[name=billing_zip_code]").val(component.long_name)
                              dict['billing_zip_code'] = {value: component.long_name || ''};
                            }

                          })

                        });

                        window.sessionStorage && window.sessionStorage.setItem('checkout', JSON.stringify(dict));
                    }

                });

//dataLayer
                $('input[name="terms"]').change(function(){

                    if (!$('.checkboxes').hasClass('checked')) {

                        var $shippingMethod = $('input[name="shipping_method"]:checked').val();

                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $.ajax({
                                type: "GET",
                                url: '/cart/ajax/data_layer_checkout/',
                                dataType: 'json',
                                success: function (data) {
                                    // step - 1
                                    data.ecommerce.checkout.actionField.option = $shippingMethod;
                                    dataLayer.push(data);


                                    var $box_2 = $('#box_2');
                                    var $box_3 = $('#box_3');

                                    var checkoutOption = {
                                        'postCode': $box_2.find('input[name="billing_zip_code"]').val(),
                                        'province': $box_2.find('input[name="billing_state"]').val(),
                                        'city': $box_2.find('input[name="billing_city"]').val(),
                                        '$country': $box_2.find('input[name="billing_country"]').val(),
                                        'company': $box_3.find('input[name="company_name"]').val(),
                                        'firstLastName': $('#name').val(),
                                        'regNumber': $box_3.find('input[name="company_reg_number"]').val(),
                                        'email': $('#email').val(),
                                        'vatNumber': $box_3.find('input[name="vat_number"]').val(),
                                        'billingAdrCheckbox': $box_3.find('.checkbox').prop('checked')
                                    };

                                    dataLayer.push({
                                        'event': 'checkoutOption',
                                        'ecommerce': {
                                            'checkout_option': {
                                                'actionField': {'step': 2, 'option': checkoutOption}
                                            }
                                        }
                                    });


                                    // step - 3

                                    checkoutOption = {
                                        'shippingAddress': $('.shipping-address').find('input[name="shipping_address"]').val(),
                                        'shippingPostCode': $('.shipping-address').find('input[name="shipping_zip_code"]').val(),
                                        'shippingCity': $('.shipping-address').find('input[name="shipping_city"]').val(),
                                        'shippingState': $('.shipping-address').find('input[name="shipping_state"]').val(),
                                        'shippingCountry': $('.shipping-address').find('input[name="shipping_country"]').val()
                                    }

                                    dataLayer.push({
                                        'event': 'checkoutOption',
                                        'ecommerce': {
                                            'checkout_option': {
                                                'actionField': {'step': 3, 'option': checkoutOption}
                                            }
                                        }
                                    });


                                    // step - 4
                                    checkoutOption = {
                                        'paymentMethod': $('input[name="payment_method"]:checked').val(),
                                        'comment': $('textarea[name="comment"]').val(),
                                        'hearAboutUs': $('select[name="hear_about_us"]').val()
                                    }

                                    dataLayer.push({
                                        'event': 'checkoutOption',
                                        'ecommerce': {
                                            'checkout_option': {
                                                'actionField': {'step': 4, 'option': checkoutOption}
                                            }
                                        }
                                    });


                                    $('.shipping-method').addClass('checked');
                                },
                                error: function (data) {
                                }
                            });

                    }
                    $('.checkboxes').addClass('checked');
                });

            });
        </script>

        <script>
            $('.item-modal-control').click(function(){
                $('#' + $(this).data('open')).foundation('open');
                $('#' + $(this).data('open')).find('img[data-src]').each(function(index, element) {
                    element.src = $(element).data('src');
                    $(element).removeAttr('data-src');
                });
            });

            $('.reveal.xlarge').mouseleave(function() {
              $(this).foundation('close');
            });
        </script>

        <script>
        if ($('#google_translate_element').length){
            $('#close-translate').css('display', 'inline');
        }

        $('#close-translate').on('click', function(event){
            event.preventDefault();
            $('#google_translate_element').css('display', 'none');
        });
    </script>

@endpush
