@extends('layouts.welcome')

@section('ga-data-layer')
    <script>
        {!!  \Illuminate\Support\Facades\Cache::store('file')->get($dataLayerKey ?? '', '') !!}
    </script>
    <!-- to avoid sending google analytics dataLayer multiple times -->
    @php
        \Illuminate\Support\Facades\Cache::store('file')->forget($dataLayerKey ?? '');
    @endphp
@endsection

@section('content')

    <style>
        .red-button{
            background: linear-gradient(to top, rgba(240,0,15,1) 0%,rgba(195,3,56,1) 100%);
            text-align: center;
            border: 1px solid #991039;

        }
        .red-button:hover{
            background: linear-gradient(to bottom, rgba(240,0,15,1) 0%,rgba(195,3,56,1) 100%);


        }
        .red-button{
            margin: 10px;
            display: inline-block;
            width: 100%;
            padding-bottom: 10px;
            padding-top: 10px;
            font-family: Open Sans;
            font-weight: bold;
            color:#fff;
        }
    </style>
    <div class="row" style="height: 300px;">
        <div class="large-2 medium-2 small-1 columns">&nbsp;</div>
        <div class="text-center large-8 medium-8 small-10 columns" style="background-color: #170701c9;border-radius: 20px;color: #f4f4f4;">
            <br>
            <br>
            @if (App::getLocale() == 'en')
                <h1>Dear client!</h1>
                <p>We've got your payment, thank you! We'll start processing your order. </p>
                <br>
                <a href="{{ url('/en') }}" class="red-button">@lang('checkout.BACK TO THE SHOPPING')</a>
            @else
                <h1>Kallid kliend!</h1>
                <p>Täname! Oleme Sinu makse kätte saanud. Tellimus võetakse tööle esimesel võimalusel. </p>
                <br>
                <a href="{{ url('/et') }}" class="red-button">@lang('checkout.BACK TO THE SHOPPING')</a>
            @endif
            <br>
            <br>
            <br>
        </div>
        <div class="large-2 medium-2 small-1 columns">&nbsp;</div>
    </div>

@endsection