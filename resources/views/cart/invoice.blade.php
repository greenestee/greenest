<!doctype html>
<html class="no-js" lang="en">
  <head>
        <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-5P7XVX3');</script>
      <!-- End Google Tag Manager -->
      <meta charset="utf-8" />
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>@if(!empty($page_type) && in_array($page_type, ['single_product', 'category'])) @yield('title') @else {{ App::getLocale() == 'et' ? Voyager::setting('title_et') : Voyager::setting('title') }} @yield('title') @endif</title> @yield('meta')
      <link rel="stylesheet" href="{{ asset('/assets/css/app.min.css') }}">
      <meta name="_token" content="{{ csrf_token() }}"> @yield('css')
      <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
      <link rel="shortcut icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">
      <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
      <script type="text/javascript">
          window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
          heap.load("1424900628");
      </script>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title>Greenest - Invoice</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/invoice.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
      <script>
          {!!  \Illuminate\Support\Facades\Cache::store('file')->get("{$order->id}dataLayer", '') !!}
      </script>
      <!-- Facebook Pixel Code -->
      <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '2263036823807892');
          fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
                     src="https://www.facebook.com/tr?id=2263036823807892&ev=PageView&noscript=1"
          /></noscript>
      <!-- End Facebook Pixel Code -->
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P7XVX3"
                    height="0" width="0" style="display:none;visibility:hidden">
            </iframe>
        </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- to avoid sending google analytics dataLayer multiple times -->
    @php
        \Illuminate\Support\Facades\Cache::store('file')->forget("{$order->id}dataLayer");
        $langPostfix = App::getLocale() == 'et' ? '_et' : '';
    @endphp

      <script type="text/javascript" charset="utf-8">
                        	    var wtpQualitySign_projectId  = 97767;
                        	    var wtpQualitySign_language   = "en";
                            	</script><script src="https://bank.paysera.com/new/js/project/wtpQualitySigns.js" type="text/javascript" charset="utf-8"></script>
    
    <section class="header row">
        <div class="row bitmap">
            <div class="large-12 text-center headWrapper">
                <h2>@lang('invoice.shop-name')</h2>
                <p>@lang('invoice.shop-description')</p>
            </div>
        </div>
    </section>
    
    <section class="clientBio row">
        <div class="row">
            <div class="large-5 small-6 large-offset-1 column playerWrapper">
                <h5>@lang('invoice.payer'): @if($order->company_name) {{ $order->company_name }} @else {{ $order->name }} @endif</h5>
                <p>@lang('invoice.mail'): {{ $order->email }}</p>
                @if($order->shipping_method == 'parcel_terminal')
                    <p>{{ $order->parcelTerminal->provider }}, {{ $order->parcelTerminal->name }}</p>
                    <p>@lang('invoice.phone'): {{ $order->shipping_parcel_terminal_phone }}</p>
                @else
                    <p>{{ $order->billing_address }}</p>
                    <p>@if($order->company_reg_number) @lang('invoice.reg-nr'): {{ $order->company_reg_number }}; @endif @if($order->vat_number) @lang('invoice.vat'): {{ $order->vat_number }} @endif</p>
                    <p>@lang('invoice.phone'): {{ $order->phone }}</p>
                @endif
            </div>
            <div class="large-5 small-6 end column text-center dataWrapper">
                <p><span>@lang('invoice.date'):</span> {{ $order->created_at->format('d.m.Y') }}</p>
                <p><span>@lang('invoice.invoice-number'):</span> {{ $order->id }}</p>
                <p><span>@lang('invoice.ref-number'):</span> {{ \App\Http\Libraries\MeritAktiva\MeritAktiva::getRefNo($order->id) }}</p>
                <p class="redText">@lang('invoice.pay-date'):
                    @if($order->company_name)
                        {{ $order->created_at->addWeek()->format('d.m.Y') }}
                    @else
                        {{ $order->created_at->addDays(3)->format('d.m.Y') }}
                    @endif
                </p>
            </div>
        </div>
        <div class="row productInfo">
            <div class="large-offset-1 large-10">
                <table class="unstriped responsive">
                    <thead>
                    <tr>
                        <th class="tableTitle">@lang('invoice.product-name')</th>
                        <th class="text-center tableTitle" width="200">@lang('invoice.quantity')</th>
                        <th class="text-center tableTitle" width="150">@lang('invoice.net-price')</th>
    
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->items as $item)
                    <tr>
                        <td class="nest">{{ $item->name }}</td>
                        <td class="text-center">{{ $item->quantity }}</td>
                        <td class="text-center">{{ formatNumber($item->price) }}</td>
                    </tr>
                    @endforeach
                    <tr class="total">
                        <td class="nest">&nbsp;</td>
                        <td class="text-right">@lang('invoice.sum'): <br>@lang('invoice.vat') 20%<br></td>
                        <td class="text-center">{{ formatNumber($order->subtotal) }}<br>{{ formatNumber($order->tax) }}<br></td>
                    </tr>
                     <tr>
                        <td class="nest">&nbsp;</td>
                        <td class="text-right"><span>@lang('invoice.total-sum'):</span></td>
                        <td class="text-center"><span>
                                @if ($order->discount_coefficient > 0)
                                    <s style="color: darkred;">{{ formatNumber($order->sum_total / (1 - $order->discount_coefficient)) }} €</s>
                                    <br>
                                    {{ formatNumber($order->sum_total) }} €
                                @else
                                    {{ formatNumber($order->sum_total) }} €
                                @endif
                            </span></td>
                    </tr>
                    <tr class="address">
                        <td class="nest"> 
                            <span>Kurekivi tee 2, Rae vald</span><br>
                            <span>Lehmja, Harjumaa 75306</span><br>
                            <span>LHV Pank: <span>EE527700771005774026</span></span>
                        </td>
                        <td  colspan="2" >     <span>Nature Design OÜ</span><br>
                            <span>Registrikood: 12802934</span><br>
                            <span>EE101777465</span>
                        </td>
                       
                    </tr>
                    <tr class="contact">
                        <td class="nest"> 
                            <img src="{{ asset('/assets/img/shape-3.png') }}"><a href="mailto:sales@greenest.ee">sales@greenest.ee</a>
                        </td>
                        <td  colspan="2" >  <img src="{{ asset('/assets/img/layer-37.png') }}">   <span>+372 51 900 330</span>
                          
                        </td>
                       
                    </tr>
                    @if ( ! empty($order->e_banking_payments))
                        <tr class="payment">
                            <td class="nest" colspan="3">
                                <span>@lang('invoice.pay-options')</span>
                                @foreach($order->e_money_payments->getPaymentMethods() as $paysera)
                                    @if ($paysera->getKey() == 'wallet')
                                        <a href="{{url($order->locale . '/ebanking/?payment='.$paysera->getKey().'&sum='.$order->sum_total.'&order_id='.$order->id)}}">
                                            <img class="bank-image" src="{{$paysera->getLogoUrl()}}">
                                        </a>
                                    @endif
                                @endforeach
                                @foreach($order->e_banking_payments->getPaymentMethods() as $payment)
                                    <a href="{{url($order->locale . '/ebanking/?payment='.$payment->getKey().'&sum='.$order->sum_total.'&order_id='.$order->id)}}">
                                        <img class="bank-image" src="{{$payment->getLogoUrl()}}">
                                    </a>
                                @endforeach
                            </td>
                        </tr>
                    @endif

                    @if ($order->payment_method == 'bank_transfer')
                     <tr class="payment">
                        <td class="nest" colspan="3"> 
                            <span>@lang('invoice.payment-individual'):</span>
                            <a href="https://www.swedbank.ee/private" target="_blank"><img src="{{ asset('/assets/img/swedbank.png') }}"></a>
                            <a href="https://www.seb.ee/ip/ipank" target="_blank"><img src="{{ asset('/assets/img/2.png') }}"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et" target="_blank"><img src="{{ asset('/assets/img/3.png') }}"></a>
                            <a href="https://www.lhv.ee" target="_blank"><img src="{{ asset('/assets/img/4.png') }}"></a>
                        </td>
                       
                    </tr>
                    <tr class="payment">
                        <td class="nest" colspan="3"> 
                            <span>@lang('invoice.payment-businesses'):</span>
                            <a href="https://business.swedbank.ee" target="_blank"><img src="{{ asset('/assets/img/swedbank.png') }}"></a>
                            <a href="https://www.seb.ee/cgi-bin/unet3.sh/ufirma.w" target="_blank"><img src="{{ asset('/assets/img/2.png') }}"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et" target="_blank"><img src="{{ asset('/assets/img/3.png') }}"></a>
                            <a href="https://www.lhv.ee" target="_blank"><img src="{{ asset('/assets/img/4.png') }}"></a>
                        </td>
                       
                    </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    @include('cart.modals')

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="/js/vendor/foundation.min.js" type="text/javascript"></script>
    <script>
        setTimeout(function(){
            var popup = new Foundation.Reveal($('#new-order'));
            popup.open();
        }, 10000);
    </script>
  </body>
</html>