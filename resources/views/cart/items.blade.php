@php
	$locale = App::getLocale();
	if ($locale == 'et') {
		$name = 'name_et';
	} else {
		$name = 'name';
	}
	$imgplaceholder = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
@endphp
@foreach(Cart::content() as $item)
	@if (is_null($item->model))
		@continue(1)
	@endif
	@php($is_available_immediately = in_array($item->id, $availableProducts))

		<div
			class="row small-collapse order product-cart-item {{ $cart_content === 'mixed' && !$is_available_immediately ? 'product-cart-item--mixed' : '' }}"
			data-product-id={{ $item->id }}
		>

			<div class="large-6 columns order-item">

				@if($item->associatedModel)

					<div class="wrapper-img-checkout">
						<img data-src="{{ $item->model->getImage() }}" src="{{ $imgplaceholder }}" alt="">
                        <noscript>
                            <img src="{{ $item->model->getImage() }}" alt="">
                        </noscript>
					</div>

					<p class="top-name item-modal-control" data-open="product{{ $item->id }}">
						{{ $item->model->$name }} <span>{!! $item->model->getAmount() !!}</span>
						@if($item->model->isRefrigerated())
							<img data-src="{{ asset('/img/snowflake.svg') }}" width="60" alt="" src="{{ $imgplaceholder }}" class="">
							<noscript>
								<img src="{{ asset('/img/snowflake.svg') }}" width="60" alt="" class="">
							</noscript>
						@endif

						@if($item->model->isFrozen())
							<img data-src="{{ asset('/img/snowflake-18.svg') }}" width="60" alt="" src="{{ $imgplaceholder }}" class="">
							<noscript>
								<img src="{{ asset('/img/snowflake-18.svg') }}" width="60" alt="" class="">
							</noscript>
						@endif
					</p>

				@else

					<p class="top-name item-modal-control" data-open="product{{ $item->id }}">
						{{ $item->$name }}
					</p>

				@endif

				<p class="top-name-label" style="{{ !$is_available_immediately || $productsQuantity[$item->id] < $item->qty ? 'display: none' : 'display: block'  }}">
					@lang('checkout.enough_in_stock')
				</p>

			</div>



			<div class="large-6 columns">
				<form class="item-form" action="{{ route('cartGetUpdate', [$item->id]) }}" method="POST" target="_blank" data-id="{{ $item->id }}" onsubmit="return false" >
					{{ csrf_field() }}
					<div class="row">
						<div class="small-12 medium-3 large-3 columns wrapper">

							<span>@lang('checkout.price')</span><br>

							<span>€<b class="item-price">{{ formatNumber($item->total) }}</b></span>

						</div>

						<div class="small-12 medium-3 large-3 columns wrapper checkout-tooltip-container">

							<span>@lang('checkout.QTY')</span><br>

							<div class="count-input space-bottom">
								
									
									<a class="incr-btn" data-action="decrease" href="#">–</a>

									<input class="quantity" type="text" name="quantity" value="{{ $item->qty }}" />

									<a class="incr-btn" data-action="increase" href="#">&plus;</a>

							</div>

							<span class="checkout-tooltip" style="">
								@if(!$is_available_immediately)
									@if($cart_content === 'all_on_order')
										@lang('checkout.all_on_order_cart_content')
									@else
										@lang('checkout.mixed_cart_content')
										<span class="checkout-tooltip-replace">
											@lang('checkout.all_on_order_cart_content')
										</span>
									@endif
								@else
									@if($productsQuantity[$item->id] < $item->qty)
										@if($productsQuantity[$item->id] === 1)
											@lang('checkout.not_enough_in_stock')
										@else
											{{ preg_replace('/\${in_stock}/i', $productsQuantity[$item->id], Lang::get('checkout.not_enough_in_stock_multiple')) }}
										@endif
									@else
										@lang('checkout.enough_in_stock')
									@endif
								@endif
							</span>

						</div>

						<div class="small-12 medium-3 large-3 columns wrapper">

							<span>@lang('checkout.Net price')</span><br>

							<span>€<b class="item-netprice">{{ formatNumber($item->subtotal) }}</b></span>

						</div>

						<div class="small-12 medium-3 large-3 columns wrapper">

							<a class="close" href="{{ action('CartController@getRemove', $item->id) }}"><img src="{{ asset('/close.png') }}"></a>

						</div>
					</div>
				</form>

			</div>

			<!-- <div class="large-1 columns wrapper">

				

			</div>



			<div class="large-1 columns  wrapper">

				

			</div>

			

			<div class="large-1 columns  wrapper">

				

			</div>



			<div class="large-1 columns  wrapper">

				

			</div>



			<div class="large-1 columns wrapper">

				<span>price</span><br>

				<span>€{{ formatNumber($item->total) }}</span>

			</div>



			<div class="large-1 columns  wrapper">

				<span>QTY</span><br>

				<div class="count-input space-bottom">

					<a class="incr-btn" data-action="decrease" href="{{ action('CartController@getTake', $item->id) }}">–</a>

					<input class="quantity" type="text" name="quantity" value="{{ $item->qty }}"/>

					<a class="incr-btn" data-action="increase" href="{{ action('CartController@getAdd', $item->id) }}">&plus;</a>

				</div>

			</div>

			

			<div class="large-1 columns  wrapper">

				<span>net price</span><br>

				<span>€{{ formatNumber($item->subtotal) }}</span>

			</div>



			<div class="large-1 columns  wrapper">

				<a class="close" href="{{ action('CartController@getRemove', $item->id) }}"><img src="{{ asset('/close.png') }}"></a>    

			</div> -->

		</div>

	@php
		$product = $products->where('id', $item->id)->first();
	@endphp
	@include('cart.item-modal')

    @endforeach
