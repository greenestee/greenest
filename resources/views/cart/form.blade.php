<div class="row  forms" id="checkout">
    @php
        $codes = [
            ["code" => "+45", "country" => "Denmark", "min_number_length" => 8, "max_number_length" => 8],
            ["code" => "+372", "country" => "Estonia", "number_length" => 7, "max_number_length" => 10],
            ["code" => "+358", "country" => "Finland", "number_length" => 5, "max_number_length" => 12],
            ["code" => "+33", "country" => "France", "number_length" => 9, "max_number_length" => 9],
            ["code" => "+49", "country" => "Germany", "number_length" => 10, "max_number_length" => 11],
            ["code" => "+371", "country" => "Latvia", "number_length" => 8, "max_number_length" => 8],
            ["code" => "+370", "country" => "Lithuania", "number_length" => 8, "max_number_length" => 8],
            ["code" => "+352", "country" => "Luxembourg", "number_length" => 9, "max_number_length" => 9],
            ["code" => "+31", "country" => "Netherlands", "number_length" => 9, "max_number_length" => 9],
            ["code" => "+47", "country" => "Norway", "number_length" => 8, "max_number_length" => 8],
            ["code" => "+46", "country" => "Sweden", "number_length" => 7, "max_number_length" => 10],
            ["code" => "+41", "country" => "Switzerland", "number_length" => 9, "max_number_length" => 9],
            ["code" => "+44", "country" => "United Kingdom", "number_length" => 10, "max_number_length" => 10],
            ["code" => "+1", "country" => "United States", "number_length" => 10, "max_number_length" => 10]
        ];
        $codesForSelect = array();
        foreach ($codes as $code) {
            $codesForSelect[$code["code"]] = $code["code"];
        }
    @endphp
    @if(Auth::check())
        {!! Form::model(Auth::user(), ['class' => 'checkout-form form_class', 'action' => 'CartController@postCheckout', 'method' => 'POST', 'id' => 'checkout']) !!}
        @php
            $phone = Auth::user()->shipping_parcel_terminal_phone;
            foreach ($codesForSelect as $c) {
                if (substr($phone, 0, strlen($c)) === $c) {
                    $code = $c;
                    $phone = substr($phone, strlen($c));
                }
            }
        @endphp
    @else
        {!! Form::open(['class' => 'checkout-form form_class', 'action' => 'CartController@postCheckout', 'method' => 'POST', 'id' => 'checkout']) !!}
        @php
            $geoip = geoip($ip);
            foreach ($codes as $co) {
                if ($co["country"] == $geoip['country']) {
                    $code = $co["code"];
                }
            }
            if (empty($code)) {
                $code = '+372';
            }
            $phone = null;
        @endphp
    @endif
        <div class="large-4 column top-columns shipping-method" id="box_1">
            <div class="wrapper-form">
                <h5><img src="{{ asset('shipping.png') }}">@lang('checkout.Shipping and handling')</h5>
                <p class="radio-shipping">
                    @foreach($shipping_methods as $key => $method)
                        <span class="radio-field checkout-tooltip-container" id="radio_shipping_field_{{ $key }}">
                            <label id="radio_shipping_label_{{ $key }}" class="radio-label" @if (!$method['view']) style="display: none;" @endif>
                                <input data-value="{{ $method['price'] }}" name="shipping_method" type="radio"
                                       @if ($method['active']) checked="checked" @endif value="{{ $key }}"
                                       id="radio_shipping_button_{{ $key }}" required>
                            </label>
                            <span id="radio_shipping_text_{{ $key }}" class="radio-text" @if (!$method['view']) style="display: none;" @endif>{{ $method['name'] }}</span>
                            <br id="radio_shipping_br_{{ $key }}" @if (!$method['view']) style="display: none;" @endif>
                            @if (in_array($key, ['customer_pickup', 'parcel_terminal']))
                                <span class="checkout-tooltip checkout-tooltip--red" id="radio_shipping_tooltip_{{ $key }}" data-timeout="15000" style="">
                                    @lang("checkout.${key}_tooltip")
                                </span>
                            @endif
                        </span>
                    @endforeach
                    <span class="required-field"
                          id="required-parcel">*</span>
                @if($allowParcelTerminal)
                    <div id="parcel_terminal_data_fields" style="{{ !$shipping_methods['parcel_terminal']['view'] ? 'display: none;' : '' }}">
                        {!! Form::select('shipping_parcel_terminal_phone_code', $codesForSelect, $code, ["style" => "width: 19%;padding-right: 8px;", 'id' => 'tel-code']) !!}
                        {!! Form::text('shipping_parcel_terminal_phone', $phone, ['id' => 'telephone', 'placeholder' => 'Telefoni number, ilma +372 koodita!', 'maxlength' => 11, "style" => "width: 75%;", "pattern" => "^\d+$"]) !!}
                        <p>@lang('checkout.Name of the parcel terminal') <span id="parcel-terminal" class="required-field">*</span></p>
                        {!! Form::select('shipping_parcel_terminal_id', $shipping_methods['parcel_terminal']['items'], null, ['class' => 'chosen-select', 'id' => 'select-parcel']) !!}
                    </div>
                @endif
                </p>
                <h5 id="checkout_shipping_stock_title" style="display: {{$cart_content === 'mixed' ? 'block' : 'none'}};">
                    <img src="{{ asset('shipping-method.png') }}">@lang('checkout.shipping_stock_title')
                </h5>
                <p class="radio-shipping" id="checkout_shipping_stock_options" style="display: {{$cart_content === 'mixed' ? 'block' : 'none'}};">
                    <label class="radio-label">
                        {!! Form::radio('shipping_stock', 'ship_all_products', null, ['data-value' => 0]) !!}
                    </label>
                    <span> @lang('checkout.ship_all_products') </span><br>
                    <label class="radio-label">
                        {!! Form::radio('shipping_stock', 'ship_available', null, ['data-value' => 2.5]) !!}
                    </label>
                    <span> @lang('checkout.ship_available_products') </span>
                </p>
            </div>
        </div>
    <div class="large-4 column top-columns billing-address" id="box_2">
        <div class="wrapper-form">
            <h5 class="image-billing">@lang('checkout.Billing address') </h5>
            {!! Form::hidden('billing_address', null, ['id' => 'billing_address']) !!}
            <select id="address" data-placeholder="@lang('checkout.Search for address')">
                @if(old('billing_address'))
                    <option value="{{ old('billing_address') }}"> {{ old('billing_address') }}</option>
                @endif
            </select>
            <div class="row">
                <div class="large-6 column">
                    <p>@lang('checkout.Zip/Postal code') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'billing_zip_code') !!}
                    <p>@lang('checkout.State/Province') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'billing_state') !!}
                </div>
                <div class="large-6 column">
                    <p>@lang('checkout.City') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'billing_city') !!}
                    <p>@lang('checkout.Country') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'billing_country') !!}
                </div>

            </div>
        </div>
    </div>
    <div class="large-4 column top-columns billing-address" id="box_3">
        <div class="wrapper-form">

            <div class="row">
                <div class="large-6 column">
                    <p>@lang('checkout.Company')</p>
                    {!! Form::input('text', 'company_name') !!}
                    <p>@lang('checkout.First and Last Name') <span class="required-field" id="required-name">*</span></p>
                    {!! Form::input('text', 'name', null, ['id' => 'name']) !!}
                    <p>@lang('checkout.Telephone') <span class="required-field" id="required-phone">*</span></p>
                    {!! Form::input('text', 'phone', null, ['id' => 'phone']) !!}
                </div>
                <div class="large-6 column">
                    <p>@lang('checkout.Registration number')</p>
                    {!! Form::input('text', 'company_reg_number') !!}
                    <p>@lang('checkout.Email address') <span class="required-field" id="required-email">*</span></p>
                    {!! Form::input('text', 'email', null, ['id' => 'email']) !!}
                    <p>@lang('checkout.Tax/Vat Number')</p>
                    {!! Form::input('text', 'vat_number') !!}
                </div>
            </div>
            <input type="checkbox" class="checkbox" value="1">@lang('checkout.Ship to the same address')<br>
        </div>
    </div>
    <div class="large-4 column shipping-address" id="box_4">
        <div class="wrapper-form">
            <h5><img src="{{ asset('address.png') }}">@lang('checkout.Shipping address')</h5>
            <p>@lang('checkout.Address')</p>
            {!! Form::input('text', 'shipping_address') !!}

        </div>
    </div>
    <div class="large-4 column shipping-address" id="box_5">
        <div class="wrapper-form">

            <div class="row">
                <div class="large-6 column">
                    <p>@lang('checkout.Zip/Postal code') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'shipping_zip_code') !!}
                </div>
                <div class="large-6 column">
                    <p>@lang('checkout.City') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'shipping_city') !!}

                </div>
            </div>
        </div>
    </div>
    <div class="large-4 column shipping-address" id="box_6">
        <div class="wrapper-form">

            <div class="row">
                <div class="large-6 column">
                    <p>@lang('checkout.State/Province') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'shipping_state') !!}

                </div>
                <div class="large-6 column">
                    <p>@lang('checkout.Country') <span class="required-field">*</span></p>
                    {!! Form::input('text', 'shipping_country') !!}

                </div>
            </div>
        </div>
    </div>
    <div class="large-4 column" id="box_7">
        <div class="wrapper-form">
            <h5><img src="{{ asset('payment-process.png') }}">@lang('checkout.Payment Method')</h5>
            <p class="radio-payment">
                @foreach($payment_methods as $key => $method)
                    <span>
                        {!! Form::radio('payment_method', $key, $method['checked']) !!}
                        <span class="checkout-tooltip-container">
                            {{ $method['name'] }}
                            @if (in_array($key, ['hire_purchase_lhv', 'hire_purchase_seb', 'montonio_swedbank_ee',
                                    'montonio_seb_ee', 'montonio_lhv_ee', 'montonio_luminor_ee', 'montonio_coop_pank_ee',
                                    'montonio_citadele_ee', 'montonio_op_fi', 'montonio_nordea_fi']))
                                <span class="checkout-tooltip checkout-tooltip--red checkout-tooltip--payment" id="radio_payment_tooltip_{{ $key }}" data-timeout="15000" style="">
                                    @lang("checkout.${key}_tooltip")
                                </span>
                            @endif
                        </span>
                    </span>@if(!$loop->last)<br>@endif
                @endforeach
            </p>
            <input type="text" placeholder="Discount coupon" maxlength="20" name="coupon"
                {{--onchange="couponChange(this.value);"--}} id="coupon">
        </div>
    </div>
    <div class="large-4 column" >
        <div class="wrapper-form" style="display: flex;">

            <textarea placeholder="Comments" rows="5" name="comment" cols="50" style="flex:  1 1 auto;"></textarea>

        </div>
    </div>
    <div class="large-4 column last-form-block {{$cart_content === 'mixed' ? 'small-centered' : ''}}" id="box_8">
        <div class="wrapper-form">
            <h5>@lang('checkout.How did you hear about us?')</h5>
            <select size="1" name="hear_about_us">
                <option value="Unspecified">@lang('checkout.please choose')</option>
                <option value="Google">Google</option>
                <option value="Facebook">Facebook</option>
                <option value="Instagram">Instagram</option>
                <option value="Friend">@lang('checkout.Friend')</option>
                <option value="Blog">@lang('checkout.Blog')</option>
                <option value="Radio">@lang('checkout.Radio')</option>
                <option value="Magazine">@lang('checkout.Magazine')</option>
            </select>
        </div>
    </div>

    <div class="large-12 column checkboxes">
        {!! Form::checkbox('terms') !!}<span>@lang('checkout.Terms_part_1') <a
                    href="{{ action('PagesController@getPage', (\Request::is('en/*')) ? 'terms' : 'tellimistingimused') }}"
                    target="_blank">@lang('checkout.Terms_part_2')</a> @lang('checkout.Terms_part_3')</span><br>

        {!! Form::checkbox('sign_up', 1, null, ['id' => 'sign_up', 'class' => 'sign-up']) !!}<span class="sign-up">@lang('checkout.CREATE ACCOUNT')</span>

    </div>

    <div class="row">
        <div class="large-3 column small-centered">
            <button type="submit" class=" place-order">@lang('checkout.PLACE THE ORDER')</button>
        </div>
    </div>
    {!! Form::close() !!}


</div>

@if (count($errors) > 0)
    <div class="row">
        <div class="large-6 large-offset-3 columns">
            <div class="alert callout large">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li><span>{{ $error }}</span></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
