<div class="modalHome reveal" id="new-order" aria-hidden="false" data-reveal="y9oyqj-reveal" role="dialog" data-yeti-box="sign-up" data-resize="sign-up" tabindex="-1" style="display: block;top: 247px;padding: 0;">
    <div class="modalHome-header ">
      <a href="#close" class="btn-close" aria-hidden="true" data-close>&#215;</a> <span>New order</span>
    </div>
     
    <div class="modalHome-body">
      <div style="width:100%;min-height:180px;background-color:#fff;">
        <div class="text-center">{{ App::getLocale() == 'et' ? Voyager::setting('order_text_et') : Voyager::setting('order_text') }}</div>
    </div>
  </div>
</div>

<div class="modalHome reveal" id="sign-up" aria-hidden="false" data-reveal="y9oyqj-reveal" role="dialog" data-yeti-box="sign-up" data-resize="sign-up" tabindex="-1" style="display: none;top: 247px;padding: 0;">
    <div class="modalHome-header ">
      <a href="#close" class="btn-close" aria-hidden="true" data-close>&#215;</a> <span>Create account</span>
    </div>
     
    <div class="modalHome-body">
        <div style="width:100%;min-height:180px;background-color:#fff;">
          {!! Form::open(['action' => 'CartController@postCheckout', 'method' => 'POST', 'id' => 'checkout_sign_up']) !!}
            {!! Form::email('email', null, ['placeholder' => 'Enter email']) !!}
            {!! Form::password('password', ['placeholder' => 'Enter password']) !!}
            {!! Form::password('password_confirmation', ['placeholder' => 'Password confirmation']) !!}
            {!! Form::button('Send', ['type' => 'submit', 'class' => 'Login-button']) !!}
          {!! Form::close() !!}
        </div>
    </div>
</div>