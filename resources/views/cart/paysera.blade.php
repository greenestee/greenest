<!DOCTYPE html>
<html>
<head>
    <title>Paysera redirect</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2263036823807892');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2263036823807892&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
    <h3>Please wait, there is a redirect to the bank's page for payment.</h3>
    <form style="display: none;" action="{{$payUrl}}" method="post">

        <?php foreach ($requestData as $key => $val): ?>
        <input type="hidden"
               name="<?php echo $key ?>"
               value="<?php echo htmlspecialchars($val); ?>" />
        <?php endforeach; ?>

        <input id="button" type="submit" value="Pay" />
    </form>
</body>
    <script>
        $(document).ready(function() {
            $("#button").click();
        });
    </script>
    <script src="{{ asset('/js/jquery.placecomplete.js') }}"></script>
</html>