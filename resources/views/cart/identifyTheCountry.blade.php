<!DOCTYPE html>
<html>
<head>
    <title>Country Selection</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2263036823807892');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2263036823807892&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<div style="display: flex; height: 80%">
    <div style="margin: auto; font-size: 16px" class="checkout-buttons">
        <h2 style="margin: 10px; font-size: 24px; font-family: proximanovalight, sans-serif; font-weight: 700;">Choose
            the country in which You will make payment...</h2>
        <form action="{{action('CartController@getPayseraPayments', $order)}}" method="get" class="wrapper-form">

            <select name="country" style="margin: 10px; font-size: 16px">
                @foreach($paymentCountryList as $key => $country)
                    <option @if ($key == $slectCountry)  selected
                            @endif value={{$key}}>{{$country->getTitle('en')}}</option>
                @endforeach

            </select>

            <button id="button" type="submit" class="red-button" style="text-transform: uppercase;">VALI RIIK</button>
        </form>
    </div>
</div>

</body>
</html>