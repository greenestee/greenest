<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Greenest</title>
</head>
<body>

<form action="{{ $url }}" method="POST" id="lhv-form">
    {{ csrf_field() }}
    <input type="hidden" name="VK_SERVICE" value="{{ $data['VK_SERVICE'] }}">
    <input type="hidden" name="VK_VERSION" value="{{ $data['VK_VERSION'] }}">
    <input type="hidden" name="VK_SND_ID" value="{{ $data['VK_SND_ID'] }}">
    <input type="hidden" name="VK_STAMP" value="{{ $data['VK_STAMP'] }}">
    <input type="hidden" name="VK_AMOUNT" value="{{ $data['VK_AMOUNT'] }}">
    <input type="hidden" name="VK_CURR" value="{{ $data['VK_CURR'] }}">
    <input type="hidden" name="VK_REF" value="{{ $data['VK_REF'] }}">
    <input type="hidden" name="VK_MSG" value="{{ $data['VK_MSG'] }}">
    <input type="hidden" name="VK_RETURN" value="{{ $data['VK_RETURN'] }}">
    <input type="hidden" name="VK_CANCEL" value="{{ $data['VK_CANCEL'] }}">
    <input type="hidden" name="VK_DATETIME" value="{{ $data['VK_DATETIME'] }}">
    <input type="hidden" name="VK_MAC" value="{{ $data['VK_MAC'] }}">
    <noscript>
        <button type="submit">Go to LHV page</button>
    </noscript>
</form>
<script type="text/javascript">
    document.body.appendChild(document.createTextNode('Redirecting...'));
    document.getElementById('lhv-form').submit();
</script>
</body>
</html>