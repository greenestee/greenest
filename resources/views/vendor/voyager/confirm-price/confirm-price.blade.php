@extends('voyager::master')

@section('page_title','All '.$namePage)

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-wallet"></i> {{ $namePage }}.  {{ $countAllCompareProducts }} products waiting for price updates!
    </h1>
@stop
<style>
    #modal_form {
        position: fixed;
        top: 43% !important;
        left: 50%;
        margin-top: -150px;
        margin-left: -150px;
        display: none;
        opacity: 0;
        z-index: 15;
    }

    #overlay {
        z-index:10;
        position:fixed;
        background-color:#000;
        opacity:0.6;
        -moz-opacity:0.6;
        filter:alpha(opacity=60);
        width:100%;
        height:100%;
        top:0;
        left:0;
        cursor:pointer;
        display:none;
    }
</style>

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Brand</th>
                                <th class="text-center">Purchase Price Old, €</th>
                                <th class="text-center">Purchase Price New, €</th>
                                <th class="text-center">Retail Price Old, €	</th>
                                <th class="text-center">Retail Price New, €	</th>
                                <th class="text-center">Margin Old, € </th>
                                <th class="text-center">Margin New, € </th>
                                <th class="text-center">Margin With Old Retail and new Purchase, €</th>
                                <th class="text-center">Is Active</th>
                                <th class="text-center">Sku</th>

                                <th class="actions text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataTypeContent as $data)
                                <tr id="table-tr-data-{{ $data->id }}">
                                    <td class="text-center">
                                        {{ $data->id}}
                                    </td>
                                    <td class="text-center">
                                        {{ $data->name}}
                                    </td>
                                    <td class="text-center">
                                        {{ $data->brand}}
                                    </td>

                                    <td class="text-center">
                                        {{ $data->purchase_price_old }}
                                    </td>
                                    <td class="text-center">
                                        <span style="color: @if ($data->purchase_price_new <= $data->purchase_price_old)green @else red @endif">{{ $data->purchase_price_new }}<span>
                                    </td>
                                    <td class="text-center">
                                        {{ $data->price_old }}
                                    </td>
                                    <td class="text-center">
                                        <span style="color: @if ($data->price_new >= $data->price_old)green @else red @endif">{{ $data->price_new}}<span>
                                    </td>
                                    <td class="text-center">
                                        {{ (((int)(($data->price_old / 1.2 - $data->purchase_price_old)*100))/100)}}
                                    </td>
                                    <td class="text-center">
                                        {{ (((int)(($data->price_new / 1.2 - $data->purchase_price_new)*100))/100)}}
                                    </td>
                                    <td class="text-center">
                                        {{ (((int)(($data->price_old / 1.2 - $data->purchase_price_new)*100))/100)}}
                                    </td>
                                    <td class="text-center">
                                        @if ($data->is_active == 1)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    </td>
                                    <td class="text-center"> 
                                        {{ $data->sku }}
                                    </td>
                                    <td class="no-sort no-click">
                                        <button class="btn btn-sm btn-success pull-right" onclick="acceptPrice({{ $data->id }})">
                                            Accept the new price
                                        </button>

                                        <div class="go btn btn-sm btn-primary pull-right edit" data-id="{{ $data->id }}">
                                            <input type="hidden" data-id="{{ $data->id }}" value="{{ $data->price_new }}">
                                            <i class="voyager-edit"></i> Edit the price
                                        </div>

                                        <button class="btn btn-sm btn-warning pull-right" onclick="rejectPrice({{ $data->id }})">
                                            Ignore the change
                                        </button>

                                        @if($data->is_blacklist)
                                            <div class="btn btn-sm btn-blacklist pull-right active" data-id="{{ $data->old_id }}">BlackListed</div>
                                        @else
                                            <div class="btn btn-sm btn-blacklist pull-right" data-id="{{ $data->old_id }}">BlackList</div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (isset($dataType->server_side) && $dataType->server_side)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }} to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete this {{ $dataType->display_name_singular }}?
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="modal_close_top" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Edit retail price
                    </h4>
                </div>
                <div class="modal-footer">
                    Set new price
                    <input required id="price-edit-input-form" type="number" min="1" step="any" name="new_retail_price" />
                    <input type="button" class="btn" id="btn-execute-edit" onclick="editPrice(this.dataset.id, $('#price-edit-input-form').val())" data-id="_" value="Yes, confirm this price">
                    <button id="modal_close" type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="overlay"></div>
@stop

@section('javascript')
    {{-- DataTables --}}
    <script>
        $(document).ready(function() {

            $('#modal_close, #overlay').click( function() {
                    $('#modal_form').css('display', 'none');
                    $('#overlay').fadeOut(400);
            });
        });
        $('td').on('click', '.go', function(e) {
//            $('#edit_form')[0].action = $('#edit_form')[0].action.replace('__id', $(e.target).data('id'));
            var price = $(e.target).find('input').val();
            $('#price-edit-input-form').val(price);
            $('#btn-execute-edit').attr('data-id', $(e.target).find('input').data('id'));
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function(){
                    $('#modal_form')
                        .css('display', 'block')
                        .animate({opacity: 1, top: '50%'}, 200);
                });
//            $('#modal_form').modal('show');
        });


        $('.btn-blacklist').on('click', function(e) {
            $btn = $(this)
            id = $btn.data('id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: ' /admin/products/setblacklist/' + id,
                success: function (data) {
                    if (data == 'on') {
                        $btn.addClass('active');
                        $btn.html('BlackListed')
                    } else if (data == 'off') {
                        $btn.removeClass('active');
                        $btn.html('BlackList')
                    }
                },
                error: function (data) {
                }
            });
        });


        function acceptPrice(id) {
            $('#table-tr-data-' + id).css('display', 'none');
            ajaxRequest('/admin/confirm/new/price/' + id, {});
        }

        function editPrice(id, price) {
            $('#table-tr-data-' + id).css('display', 'none');
            ajaxRequest('/admin/confirm/edit/price/' + id, { new_retail_price: price });
            $('#modal_close').click();
        }

        function rejectPrice(id) {
            $('#table-tr-data-' + id).css('display', 'none');
            ajaxRequest('/admin/confirm/reject/price/' + id, {});
        }

        function ajaxRequest(uri, data) {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                dataType: "json",
                url: uri,
                data: data,
                success: function (data) {
                    if (data.status && data.status === "success") {
                        console.log(uri + '  SUCCESS');
                    } else {
                        console.log(uri + '  FAILED');
                    }
                },
                error: function (data) {
                    console.log(uri + '  FAILED with error');
                }
            });
        }

    </script>
@stop