@extends('voyager::master')

@section('page_title','All DPD Shipments')

@section('page_header')
    <h1 class="page-title">
        DPD Shipments
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <form action="{{ route('dpd.make.labels') }}" method="POST">
                            {{ csrf_field() }}
                            <nobr>
                                <button type="submit" class="btn btn-warning right">DPD Package Cards</button>
                                <label>
                                    Package numbers space separated<br>
                                    <input type="text" placeholder="Package numbers" name="package_ids">
                                </label>
                                <input type="text" placeholder="Orders ID" name="ids">
                                <label style="margin: 0 10px;">
                                    From
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortFrom" id="dpd-date-start">
                                </label>
                                <label style="margin: 0 10px;">
                                    To
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortTo"
                                           id="dpd-date-end">
                                </label>
                            </nobr>
                        </form>
                        <br>
                        <div class="btn-group" style="padding: 10px 20px;border: solid 1px;margin-bottom: 10px;">
                            <h5>View settings</h5>
                            <input type="checkbox" id="checkbox-active-order" onchange="viewChange()" checked> Active orders
                            <br>
                            <input type="checkbox" id="checkbox-shipped-order" onchange="viewChange()" checked> Shipped orders
                        </div>
                        <table id="dataTable" class="row table table-hover">
                            <thead>
                            <tr>
                                <th>Printed</th>
                                <th>Order ID</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Type</th>
                                <th>Address</th>
                                <th>Package number</th>
                                <th>Status</th>
                                <th>Shipment Created At</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shipments as $shipment)
                                <tr>
                                    <td style="display: none;">
                                        @if (in_array($shipment->status, $deliveredStatesShipment))
                                            s
                                        @else
                                            a
                                        @endif
                                    </td>
                                    <td>
                                        @if ($shipment->printed === 0) ❌ @else ✔ @endif️
                                    </td>
                                    <td>{{ $shipment->order_id }}</td>
                                    <td>{{ $shipment->name }}</td>
                                    <td>{{ $shipment->shipping_parcel_terminal_phone }}</td>
                                    <td>{{ $shipment->shipping_method }}</td>
                                    <td>
                                        @if ($shipment->shipping_method == 'parcel_terminal')
                                            {{ $shipment->parcel_country }}, {{ $shipment->parcel_city }}, {{ $shipment->parcel_address }}, {{ $shipment->parcel_postalcode }}
                                        @endif
                                        @if ($shipment->shipping_method == 'courier_estonia')
                                            {{ $shipment->shipping_country }}, {{ $shipment->shipping_city }}, {{ $shipment->shipping_address }}, {{ $shipment->shipping_zip_code }}
                                        @endif
                                    </td>
                                    <td>{{ $shipment->shipment_id }}</td>
                                    <td>{{ $shipment->status }}</td>
                                    <td>{{ $shipment->created_at }}</td>

                                    <td class="no-sort no-click" id="bread-actions">
                                        <a href="/en/admin/orders/{{ $shipment->order_id }}" class="btn btn-sm btn-info pull-right">
                                            View order
                                        </a>
                                        <br>
                                        <form action="{{ route('dpd.delete.shipment') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="shipment_id" value="{{ $shipment->shipment_id }}">
                                            <button type="submit" class="btn btn-sm btn-danger pull-right">Reject shipment</button>
                                        </form>
                                        <br>
                                        <form action="{{ route('dpd.renew.shipment') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="order_id" value="{{ $shipment->order_id }}">
                                            <button type="submit" class="btn btn-sm btn-info pull-right">Renew shipment</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ config('voyager.assets_path') }}/lib/css/responsive.dataTables.min.css">
@stop

@section('javascript')
    <!-- DataTables -->
    <script src="{{ config('voyager.assets_path') }}/lib/js/dataTables.responsive.min.js"></script>
    <script>
        function viewChange() {
            var act, sh, table, reg;
            act = document.getElementById('checkbox-active-order').checked;
            sh = document.getElementById('checkbox-shipped-order').checked;
            reg = '(';
            if (act && sh) {
                reg = reg + 'a|s';
            } else {
                reg = reg + (sh ? 's' : (act ? 'a' : ''));
            }
            reg = reg + ')';
            table = $('#dataTable').DataTable();
            table.column(0).search(reg, true, false).draw();
        }
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "order": []
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });
    </script>
@stop
