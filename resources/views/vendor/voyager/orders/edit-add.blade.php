@extends('voyager::master')

@section('page_title','Show order '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>Show order {{ $dataTypeContent->id }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form role="form" action="{{ route('voyager.orders.update', $dataTypeContent->id) }}" method="POST"
              onsubmit="event.preventDefault();alsoCheckMissed();this.submit();" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}
            <div class="row">
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="col-md-12">
                 <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Order information
                                <span class="panel-desc">The information of order</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Payment and Shipping method</h4>
                                    <i class="voyager-credit-card"></i> <b>Payment method</b> : {{ $dataTypeContent->payment_method }}<br>
                                    <i class="voyager-truck"></i> <b>Shipping method</b> : {{ $dataTypeContent->shipping_method }}<br>
                                    @if($dataTypeContent->shipping_method == 'parcel_terminal')
                                        <i class="voyager-telephone"></i> <b>Phone</b> : {{ $dataTypeContent->shipping_parcel_terminal_phone }} <br>
                                        <b>Provider</b> : {{ $dataTypeContent->parcelTerminal->provider }} <br>
                                        <b>Name</b> : {{ $dataTypeContent->parcelTerminal->name }}
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <h4>Billing address</h4>
                                    <i class="voyager-world"></i> <b>Country</b> : {{ $dataTypeContent->billing_country }} <br>
                                    <i class="voyager-trees"></i> <b>State provice</b> : {{ $dataTypeContent->billing_state }} <br>
                                    <i class="voyager-location"></i> <b>City</b> : {{ $dataTypeContent->billing_city }} <br>
                                    <i class="voyager-milestone"></i> <b>Zip/Postat code</b> :{{ $dataTypeContent->billing_zip_code }} <br>
                                    <i class="voyager-home"></i> <b>Address</b> : {{ $dataTypeContent->billing_address }} <br>
                                    <i class="voyager-person"></i> <b>First and Last Name</b> : {{ $dataTypeContent->name }} <br>
                                    <i class="voyager-mail"></i> <b>Email</b> : {{ $dataTypeContent->email }} <br>
                                    <i class="voyager-telephone"></i> <b>Phone</b> : {{ $dataTypeContent->phone }} <br>
                                    <i class="voyager-warning"></i> <b>Tax/VAT Number</b> : {{ $dataTypeContent->vat_number }} <br>
                                    <i class="voyager-company"></i> <b>Company</b> : {{ $dataTypeContent->company_name }} <br>
                                    <i class="voyager-info-circled"></i> <b>Registration Number</b> : {{ $dataTypeContent->company_reg_number }} <br>
                                </div>
                                <div class="col-md-3">
                                    <h4>Shipping address</h4>
                                    <i class="voyager-world"></i> <b>Country</b> : {{ $dataTypeContent->shipping_country }} <br>
                                    <i class="voyager-trees"></i> <b>State provice</b> : {{ $dataTypeContent->shipping_state }} <br>
                                    <i class="voyager-location"></i> <b>City</b> : {{ $dataTypeContent->shipping_city }} <br>
                                    <i class="voyager-milestone"></i> <b>Zip/Postat code</b> : {{ $dataTypeContent->shipping_zip_code }} <br>
                                    <i class="voyager-home"></i> <b>Address</b> : {{ $dataTypeContent->shipping_address }} <br>
                                </div>
                                <div class="col-md-3">
                                    <h4>Other</h4>
                                    <i class="voyager-megaphone"></i> <b>How did you hear about us?</b> : {{ $dataTypeContent->hear_about_us }} <br>
                                    <i class="voyager-bubble"></i> <b>Comments</b> : {{ $dataTypeContent->comment }} <br>
                                    @php
                                        $ship_all_together = $dataTypeContent->items->where('name', 'Ship all products together')->first();
                                        $ship_asap = $dataTypeContent->items->where('name', 'Ship available products ASAP! (Extra fee 3 EUR)')->first();
                                    @endphp
                                    @if(isset($ship_all_together) || isset($ship_asap))
                                        <i class="voyager-bubble"></i> <b>Shipping method</b> : {{ isset($ship_all_together) ? $ship_all_together->name : $ship_asap->name }} <br>
                                    @endif
                                    @if (!empty($dataTypeContent->coupon) && $dataTypeContent->discount_coefficient > 0)
                                        <br><br>
                                        <i class="voyager-code"></i> <b>Discount code</b> : {{ $dataTypeContent->coupon }} <br>
                                        <b>Discount percent</b> : {{ $dataTypeContent->discount_coefficient * 100 }} % <br>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-8">
                    <!-- ### Name ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="voyager-basket"></i> Order items
                                <span class="panel-desc">The items of order</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th style="text-align: center">Product id</th>
                                    <th style="text-align: center">Name</th>
                                    <th style="text-align: center">Q-ty</th>
                                    <th style="text-align: center">Price, €</th>
                                    <th style="text-align: center">Sum total, €</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($dataTypeContent->itemsWithMissed()->get() as $item)
                                    <tr @if($item->missing != null && $item->missing == 1) style="background-color:#ffb7a1;" @endif>
                                        <td style="text-align: center">
                                            {{ $item->product_id }}
                                            @if (isset($item->product) && $item->product->isRefrigerated())
                                                <br>
                                                <img src="/img/snowflake.svg" width="30" height="30">
                                            @endif
                                            @if (isset($item->product) && $item->product->isFrozen())
                                                <br>
                                                <img src="/img/snowflake-18.svg" width="30">
                                            @endif
                                        </td>
                                        <td style="text-align: center">
                                            @if ($dataTypeContent->id != $item->order_id)
                                                <div class="float-right" style="padding: 5px;background-color: orange;">
                                                    Product PINNED from order:
                                                    <a style="text-decoration: none;" target="_blank"
                                                       href="/en/admin/orders/{{$item->order_id}}/edit">
                                                        {{$item->order_id}}
                                                    </a>
                                                </div>
                                                <br>
                                            @endif
                                            {{ $item->name }}
                                            @if($item->product)
                                                <br>
                                                <a href="{{ $item->product->product_url }}">
                                                    {{ $item->product->product_url }}
                                                </a>
                                                <br>
                                                <div style="padding: 5px;margin: 5px;border: solid 1px;vertical-align: middle;">
                                                    <div id="commentItem{{ $item->id }}View">
                                                        <a onclick="changeView('commentItem{{ $item->id }}View', 'commentItem{{ $item->id }}Actions')"
                                                           id="commentItem{{ $item->id }}Comment"
                                                           style="text-decoration: none;padding: 3px 15px;@if (!empty($item->comment)) background-color: #8b000066;color: #8b0000; @endif">
                                                            {{ $item->comment ?: 'Click to add comment' }}
                                                        </a>
                                                        <br>
                                                        <br>
                                                        <a href="{{ $item->comment_link }}" target="_blank" id="commentItem{{ $item->id }}ViewLink"
                                                           style="padding: 3px 10px;background-color: #8b000066;color: #8b0000;@if (empty($item->comment_link)) display: none; @endif">
                                                            {{ $item->comment_link_text ?: 'LINK' }}
                                                        </a>
                                                    </div>
                                                    <div id="commentItem{{ $item->id }}Actions" style="display: none;width: 100%;flex-wrap: wrap;">
                                                        <div style="width: 80%;">
                                                            <input type="text" max="255" value="{{ $item->comment }}" id="commentItem{{ $item->id }}Text"
                                                                   style="width: 100%;" placeholder="Comment..."> <br>
                                                            <nobr>
                                                                <input type="text" max="255" value="{{ $item->comment_link_text }}" id="commentItem{{ $item->id }}LinkText"
                                                                       style="width: 59%;" placeholder="Link text...">
                                                                <input type="text" max="255" value="{{ $item->comment_link }}" id="commentItem{{ $item->id }}Link"
                                                                       style="width: 40%;" placeholder="Link...">
                                                            </nobr>
                                                            <input type="hidden" value="{{ $item->id }}" id="commentItem{{ $item->id }}ItemID">
                                                        </div>
                                                        <div style="width: 19%;">
                                                            <a class="btn btn-success btn-sm"
                                                               onclick="saveCommentItem('commentItem{{ $item->id }}')">
                                                                Save
                                                            </a>
                                                            <a class="btn btn-default btn-sm"
                                                               onclick="changeView('commentItem{{ $item->id }}Actions', 'commentItem{{ $item->id }}View')">
                                                                Close
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                        </td>
                                        <td style="text-align: center">{{ $item->quantity }}</td>
                                        <td style="text-align: center">{{ $item->price }}</td>
                                        <td style="text-align: center">
                                            {{ $item->sum_total }}
                                            @if($item->product_id != null)
                                                    <input type="hidden" name="item_id" value="{{$item->id}}">
                                                    <div style="display: flex;justify-content: space-around;">
                                                        <div style="display: flex;flex-direction: column;">
                                                            <label>M</label><input class="item_status" data-itemId="{{$item->id}}" name="missing" type="checkbox" @if($item->missing != null && $item->missing == 1) checked @endif>
                                                        </div>
                                                        <div style="display: flex;flex-direction: column;">
                                                            <label>IS</label><input class="item_status" data-itemId="{{$item->id}}" name="in_stock" type="checkbox" @if($item->in_stock != null && $item->in_stock == 1) checked @endif>
                                                        </div>
                                                    </div>
                                            @endif
                                        </td>
                                      </tr>
                                @endforeach
                                  <tr bgcolor="#f1f1f1">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Subtotal: {{ number_format($dataTypeContent->subtotal, 2) }}</strong></td>
                                    <td><strong>Tax: {{  number_format($dataTypeContent->tax, 2) }}</strong></td>
                                  </tr>
                                  <tr bgcolor="#f1f1f1">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><h5><strong>Total: {{ number_format($dataTypeContent->sum_total, 2) }}</strong></h5></td>
                                  </tr>
                                </tbody>
                              </table>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i>Edit status</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="status"><b>Status</b></label>
                                <select class="form-control" name="status" id="order-status-select"
                                    onchange="if (this.value === 'completed') {
                                        document.getElementById('checkboxes-for-pinned-order-items').style.display = '';
                                    } else {
                                        document.getElementById('checkboxes-for-pinned-order-items').style.display = 'none';
                                    }">
                                    <option value="new" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'new'){{ 'selected="selected"' }}@endif>New</option>
                                    <option value="paid" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'paid'){{ 'selected="selected"' }}@endif>Paid</option>
                                    <option value="forwarded" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'forwarded'){{ 'selected="selected"' }}@endif>The order has been forwarded</option>
                                    <option value="thursday_shipping" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'thursday_shipping'){{ 'selected="selected"' }}@endif>Thursday shipping</option>
                                    <option value="shipped" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'shipped'){{ 'selected="selected"' }}@endif>Shipped</option>
                                    <option value="completed" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'completed'){{ 'selected="selected"' }}@endif>Сompleted</option>
                                    <option value="rejected" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'rejected'){{ 'selected="selected"' }}@endif>Rejected</option>
                                    <option value="partially_completed" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'partially_completed'){{ 'selected="selected"' }}@endif>Partially completed</option>
                                    <option value="partially_completed_and_forwarded" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'partially_completed_and_forwarded'){{ 'selected="selected"' }}@endif>Partially completed and forwarded</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-md-offset-4">
                <!-- ### Send email about missing products ### -->
                <div class="panel panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i>Send email about missing products to client</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <button class="btn btn-success" id="send_email" data-orderId="{{$dataTypeContent->id}}" type="button">
                                Send email
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="action-close" value="{{ URL::previous() }}">
            <button type="submit" class="btn btn-primary pull-right">
                Update status
            </button><br><br><br>
            <div class="pull-right" id="checkboxes-for-pinned-order-items"
                 @if ($dataTypeContent->status !== 'completed') style="display: none;" @endif>
                @foreach ($dataTypeContent->itemsWithMissed()->get()->where('missing', 1)
                    ->where('order_id', '!=', $dataTypeContent->id) as $missItem)
                    <input type="checkbox" class="checkbox-pinned-order-close"
                           value="{{$missItem->order_id}}"> Also complete PINNED order {{$missItem->order_id}}
                    @if (!$loop->last)
                        <br>
                    @endif
                @endforeach
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
    <script>
        function alsoCheckMissed() {
            var status = document.getElementById('order-status-select').value;
            if (status === 'completed') {
                var checkboxes = document.getElementsByClassName('checkbox-pinned-order-close');

                if (checkboxes.length > 0) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        var box = checkboxes[i];
                        if (box.checked) {
                            var id = box.value;
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                                },
                                type: "POST",
                                dataType: "json",
                                url: '/api/complete-partially-order',
                                data: {
                                    orderId: id
                                },
                                success: function (data) {
                                    console.log(data);
                                }
                            });
                        }
                    }
                }
            }
        }
        function changeView(hideId, showId) {
            var hideElement = document.getElementById(hideId);
            var showElement = document.getElementById(showId);
            showElement.style.display = hideElement.style.display === 'flex' ? '' : 'flex';
            hideElement.style.display = 'none';
        }
        function saveCommentItem(ID) {
            var text = document.getElementById(ID + 'Text').value;
            var item = document.getElementById(ID + 'ItemID').value;
            var linkText = document.getElementById(ID + 'LinkText').value;
            var link = document.getElementById(ID + 'Link').value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: "POST",
                dataType: "json",
                url: '/api/change-order-item-comment',
                data: {
                    itemId: item,
                    comment: text,
                    link: link,
                    linkText: linkText
                },
                success: function (data) {
                    if (data.status === "success") {
                        var comment = document.getElementById(ID + 'Comment');
                        var linkEl = document.getElementById(ID + 'ViewLink');
                        comment.innerHTML = text ? text : 'Click to add comment';
                        comment.style.color = text ? '#8b0000' : '#337ab7';
                        comment.style.backgroundColor = text ? '#8b000066' : '#8b000000';
                        linkEl.href = link;
                        linkEl.innerHTML = linkText;
                        linkEl.style.display = link ? '' : 'none';
                    }
                    changeView(ID + 'Actions', ID + 'View')
                }
            });
        }
        $(document).ready(function() {

            $('.item_status').on('change', function () {

                var itemId = $(this).attr('data-itemId');
                var statusName = $(this).attr('name');
                var value = $(this).is(':checked');
                var output = $(this);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    type: "POST",
                    dataType: "json", //Expected data format from server
                    url: '/api/change-order-item-status',
                    data: {
                        itemId: itemId,
                        statusName: statusName,
                        value: value
                    },
                    success: function (data) {
                        if (statusName == 'missing') {
                            if (value == true) {
                                $(output).closest('tr').css({"background-color": "#ffb7a1"});
                            } else {
                                $(output).closest('tr').removeAttr( 'style' );
                            }
                        }
                    }
                });

            });

            $('#send_email').on('click', function () {

                var orderId = $(this).attr('data-orderId');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    type: "POST",
                    dataType: "json", //Expected data format from server
                    url: '/api/send-missing-order-item-notification',
                    data: {
                        orderId: orderId
                    },
                    success: function (data) {
console.log(data);
                    }
                });

            });

        });
    </script>
@stop