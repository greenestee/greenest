<!-- ### DETAILS ### -->
<div class="panel panel panel-bordered panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="icon wb-clipboard"></i>Edit status</h3>
        <div class="panel-actions">
            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="status"><b>Status</b></label>
            <select class="form-control" name="status" id="order-status-select{{ $dataTypeContent->id }}"
                    onchange="if (this.value === 'completed') {
                                        document.getElementById('checkboxes-for-pinned-order-items').style.display = '';
                                    } else {
                                        document.getElementById('checkboxes-for-pinned-order-items').style.display = 'none';
                                    }">
                <option value="new" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'new'){{ 'selected="selected"' }}@endif>New</option>
                <option value="paid" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'paid'){{ 'selected="selected"' }}@endif>Paid</option>
                <option value="forwarded" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'forwarded'){{ 'selected="selected"' }}@endif>The order has been forwarded</option>
                <option value="thursday_shipping" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'thursday_shipping'){{ 'selected="selected"' }}@endif>Thursday shipping</option>
                <option value="shipped" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'shipped'){{ 'selected="selected"' }}@endif>Shipped</option>
                <option value="completed" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'completed'){{ 'selected="selected"' }}@endif>Сompleted</option>
                <option value="rejected" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'rejected'){{ 'selected="selected"' }}@endif>Rejected</option>
                <option value="partially_completed" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'partially_completed'){{ 'selected="selected"' }}@endif>Partially completed</option>
                <option value="partially_completed_and_forwarded" @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'partially_completed_and_forwarded'){{ 'selected="selected"' }}@endif>Partially completed and forwarded</option>
            </select>
        </div>
    </div>
</div>