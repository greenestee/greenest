<!-- ### Name ### -->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="voyager-basket"></i> Order items
            <span class="panel-desc">The items of order</span>
        </h3>
        <div class="panel-actions">
            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center">Product id</th>
                <th style="text-align: center">Name</th>
                <th style="text-align: center">Q-ty</th>
                <th style="text-align: center">Price, €</th>
                <th style="text-align: center">Sum total, €</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dataTypeContent->itemsWithMissed()->get() as $item)
                <tr @if($item->missing != null && $item->missing == 1) style="background-color:#ffb7a1;" @endif>
                    <td style="text-align: center">
                        {{ $item->product_id }}
                        @if (isset($item->product) && $item->product->isRefrigerated())
                            <br>
                            <img src="/img/snowflake.svg" width="30" height="30">
                        @endif
                        @if (isset($item->product) && $item->product->isFrozen())
                            <br>
                            <img src="/img/snowflake-18.svg" width="30">
                        @endif
                    </td>
                    <td style="text-align: center">
                        @if ($dataTypeContent->id != $item->order_id)
                            <div class="float-right" style="padding: 5px;background-color: orange;">
                                Product PINNED from order:
                                <a style="text-decoration: none;" target="_blank"
                                   href="/en/admin/orders/{{$item->order_id}}/edit">
                                    {{$item->order_id}}
                                </a>
                            </div>
                            <br>
                        @endif
                        {{ $item->name }}
                        @if($item->product)
                            <br>
                            <a href="{{ $item->product->product_url }}">
                                {{ $item->product->product_url }}
                            </a>
                            <br>
                            <div style="padding: 5px;margin: 5px;border: solid 1px;vertical-align: middle;">
                                <div id="commentItem{{ $item->id }}View{{ $dataTypeContent->id }}">
                                    <a onclick="changeView('commentItem{{ $item->id }}View{{ $dataTypeContent->id }}', 'commentItem{{ $item->id }}Actions{{ $dataTypeContent->id }}')"
                                       id="commentItem{{ $item->id }}Comment{{ $dataTypeContent->id }}"
                                       style="text-decoration: none;padding: 3px 15px;@if (!empty($item->comment)) background-color: #8b000066;color: #8b0000; @endif">
                                        {{ $item->comment ?: 'Click to add comment' }}
                                    </a>
                                    <br>
                                    <br>
                                    <a href="{{ $item->comment_link }}" target="_blank" id="commentItem{{ $item->id }}ViewLink{{ $dataTypeContent->id }}"
                                       style="padding: 3px 10px;background-color: #8b000066;color: #8b0000;@if (empty($item->comment_link)) display: none; @endif">
                                        {{ $item->comment_link_text ?: 'LINK' }}
                                    </a>
                                </div>
                                <div id="commentItem{{ $item->id }}Actions{{ $dataTypeContent->id }}" style="display: none;width: 100%;flex-wrap: wrap;">
                                    <div style="width: 80%;">
                                        <input type="text" max="255" value="{{ $item->comment }}" id="commentItem{{ $item->id }}Text{{ $dataTypeContent->id }}"
                                               style="width: 100%;" placeholder="Comment..."> <br>
                                        <nobr>
                                            <input type="text" max="255" value="{{ $item->comment_link_text }}" id="commentItem{{ $item->id }}LinkText{{ $dataTypeContent->id }}"
                                                   style="width: 59%;" placeholder="Link text...">
                                            <input type="text" max="255" value="{{ $item->comment_link }}" id="commentItem{{ $item->id }}Link{{ $dataTypeContent->id }}"
                                                   style="width: 40%;" placeholder="Link...">
                                        </nobr>
                                        <input type="hidden" value="{{ $item->id }}" id="commentItem{{ $item->id }}ItemID{{ $dataTypeContent->id }}">
                                    </div>
                                    <div style="width: 19%;">
                                        <a class="btn btn-success btn-sm"
                                           onclick="saveCommentItem('commentItem{{ $item->id }}', '{{ $dataTypeContent->id }}')">
                                            Save
                                        </a>
                                        <a class="btn btn-default btn-sm"
                                           onclick="changeView('commentItem{{ $item->id }}Actions{{ $dataTypeContent->id }}', 'commentItem{{ $item->id }}View{{ $dataTypeContent->id }}')">
                                            Close
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </td>
                    <td style="text-align: center">
                        <div id="viewQuantitySection{{ $item->id }}">
                            <span id="itemQuantity{{ $item->id }}">{{ $item->quantity }}</span>
                            @if (!empty($item->product_id) && $item->order_id == $dataTypeContent->id)
                                <br>
                                <button class="btn btn-sm btn-info" onclick="editOrderItemCount('{{ $item->id }}', true)">
                                    &#x270E;
                                </button>
                            @endif
                        </div>
                        <div id="editQuantitySection{{ $item->id }}" style="display: none;">
                            <input type="number" id="newQuantityItem{{ $item->id }}" min="1" step="1" value="{{ $item->quantity }}">
                            <button class="btn btn-sm btn-danger" onclick="editOrderItemCount('{{ $item->id }}', false)">
                                Cancel
                            </button>
                            <button class="btn btn-sm btn-success" onclick="updateOrderItemCount('{{ $item->id }}', '{{ $dataTypeContent->id }}')">
                                Save
                            </button>
                        </div>
                    </td>
                    <td style="text-align: center">
                        <span id="itemPriceSubtotal{{ $item->id }}">
                            {{ $item->price }}
                        </span>
                    </td>
                    <td style="text-align: center">
                        <span id="itemPriceTotal{{ $item->id }}">
                            {{ $item->sum_total }}
                        </span>
                        @if($item->product_id != null)
                            <input type="hidden" name="item_id" value="{{$item->id}}">
                            <div style="display: flex;justify-content: space-around;">
                                <div style="display: flex;flex-direction: column;">
                                    <label>M</label><input class="item_status" data-itemId="{{$item->id}}" name="missing" type="checkbox" @if($item->missing != null && $item->missing == 1) checked @endif>
                                </div>
                                <div style="display: flex;flex-direction: column;">
                                    <label>IS</label><input class="item_status" data-itemId="{{$item->id}}" name="in_stock" type="checkbox" @if($item->in_stock != null && $item->in_stock == 1) checked @endif>
                                </div>
                            </div>
                        @endif
                    </td>
                </tr>
            @endforeach
            <tr bgcolor="#f1f1f1">
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <strong>
                        Subtotal:
                        <span id="orderSubtotal{{ $dataTypeContent->id }}">
                            {{ number_format($dataTypeContent->subtotal, 2) }}
                        </span>
                    </strong>
                </td>
                <td>
                    <strong>
                        Tax:
                        <span id="orderTax{{ $dataTypeContent->id }}">
                            {{  number_format($dataTypeContent->tax, 2) }}
                        </span>
                    </strong>
                </td>
            </tr>
            <tr bgcolor="#f1f1f1">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <h5>
                        <strong>
                            Total:
                            <span id="orderTotal{{ $dataTypeContent->id }}">
                                {{ number_format($dataTypeContent->sum_total, 2) }}
                            </span>
                        </strong>
                    </h5>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>