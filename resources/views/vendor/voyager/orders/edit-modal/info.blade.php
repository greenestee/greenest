<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">
            Order information
            <span class="panel-desc">The information of order</span>
        </h3>
        <div class="panel-actions">
            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <h4>Payment and Shipping method</h4>
                <i class="voyager-credit-card"></i> <b>Payment method</b> : {{ $dataTypeContent->payment_method }}<br>
                <i class="voyager-truck"></i> <b>Shipping method</b> : {{ $dataTypeContent->shipping_method }}<br>
                @if($dataTypeContent->shipping_method == 'parcel_terminal')
                    <i class="voyager-telephone"></i> <b>Phone</b> : {{ $dataTypeContent->shipping_parcel_terminal_phone }} <br>
                    <b>Provider</b> : {{ $dataTypeContent->parcelTerminal->provider }} <br>
                    <b>Name</b> : {{ $dataTypeContent->parcelTerminal->name }}
                @endif
            </div>
            <div class="col-md-3">
                <h4>Billing address</h4>
                <i class="voyager-world"></i> <b>Country</b> : {{ $dataTypeContent->billing_country }} <br>
                <i class="voyager-trees"></i> <b>State provice</b> : {{ $dataTypeContent->billing_state }} <br>
                <i class="voyager-location"></i> <b>City</b> : {{ $dataTypeContent->billing_city }} <br>
                <i class="voyager-milestone"></i> <b>Zip/Postat code</b> :{{ $dataTypeContent->billing_zip_code }} <br>
                <i class="voyager-home"></i> <b>Address</b> : {{ $dataTypeContent->billing_address }} <br>
                <i class="voyager-person"></i> <b>First and Last Name</b> : {{ $dataTypeContent->name }} <br>
                <i class="voyager-mail"></i> <b>Email</b> : {{ $dataTypeContent->email }} <br>
                <i class="voyager-telephone"></i> <b>Phone</b> : {{ $dataTypeContent->phone }} <br>
                <i class="voyager-warning"></i> <b>Tax/VAT Number</b> : {{ $dataTypeContent->vat_number }} <br>
                <i class="voyager-company"></i> <b>Company</b> : {{ $dataTypeContent->company_name }} <br>
                <i class="voyager-info-circled"></i> <b>Registration Number</b> : {{ $dataTypeContent->company_reg_number }} <br>
            </div>
            <div class="col-md-3">
                <h4>Shipping address</h4>
                <i class="voyager-world"></i> <b>Country</b> : {{ $dataTypeContent->shipping_country }} <br>
                <i class="voyager-trees"></i> <b>State provice</b> : {{ $dataTypeContent->shipping_state }} <br>
                <i class="voyager-location"></i> <b>City</b> : {{ $dataTypeContent->shipping_city }} <br>
                <i class="voyager-milestone"></i> <b>Zip/Postat code</b> : {{ $dataTypeContent->shipping_zip_code }} <br>
                <i class="voyager-home"></i> <b>Address</b> : {{ $dataTypeContent->shipping_address }} <br>
            </div>
            <div class="col-md-3">
                <h4>Other</h4>
                <i class="voyager-megaphone"></i> <b>How did you hear about us?</b> : {{ $dataTypeContent->hear_about_us }} <br>
                <i class="voyager-bubble"></i> <b>Comments</b> : {{ $dataTypeContent->comment }} <br>
                @php
                    $ship_all_together = $dataTypeContent->items->where('name', 'Ship all products together')->first();
                    $ship_asap = $dataTypeContent->items->where('name', 'Ship available products ASAP! (Extra fee 3 EUR)')->first();
                @endphp
                @if(isset($ship_all_together) || isset($ship_asap))
                    <i class="voyager-bubble"></i> <b>Shipping method</b> : {{ isset($ship_all_together) ? $ship_all_together->name : $ship_asap->name }} <br>
                @endif
                @if (!empty($dataTypeContent->coupon) && $dataTypeContent->discount_coefficient > 0)
                    <br><br>
                    <i class="voyager-code"></i> <b>Discount code</b> : {{ $dataTypeContent->coupon }} <br>
                    <b>Discount percent</b> : {{ $dataTypeContent->discount_coefficient * 100 }} % <br>
                @endif
            </div>
        </div>

    </div>
</div>