<!-- Modal -->
<div id="order-edit{{ $dataTypeContent->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:90%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit order {{ $dataTypeContent->id }}</h4>
            </div>
            <div class="modal-body">
                @include('vendor.voyager.orders.edit-modal.order-edit-index-modal', ['dataTypeContent' => $dataTypeContent])
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn-close-modal{{ $dataTypeContent->id }}" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>