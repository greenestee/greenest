<script>
    function updateStatus(orderId) {
        var status = document.getElementById('order-status-select' + orderId).value;
        alsoCheckMissed(orderId);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: "POST",
            dataType: "json",
            url: '/admin/orders/' + orderId,
            data: {
                _method: 'PUT',
                status: status,
                is_ajax: true
            },
            success: function (data) {
                console.log(data);
                var newclass = '';
                switch (status) {
                    case 'completed':
                        newclass = 'default';
                        break;
                    case 'paid':
                        newclass = 'warning';
                        break;
                    case 'shipped':
                        newclass = 'info';
                        break;
                    case 'rejected':
                        newclass = 'danger';
                        break;
                    case 'forwarded':
                        newclass = 'forwarded-order';
                        break;
                    case 'partially_completed':
                        newclass = 'partially_completed-order';
                        break;
                    case 'partially_completed_and_forwarded':
                        newclass = 'partially_completed_and_forwarded-order';
                        break;
                    default:
                        newclass = 'success';
                }
                var row = $('#row-order-' + orderId);
                row.removeClass();
                row.addClass(newclass);
                $('#btn-close-modal' + orderId).click();
            }
        });
    }
    function alsoCheckMissed(orderId) {
        var status = document.getElementById('order-status-select' + orderId).value;
        if (status === 'completed') {
            var checkboxes = document.getElementsByClassName('checkbox-pinned-order-close');

            if (checkboxes.length > 0) {
                for (var i = 0; i < checkboxes.length; i++) {
                    var box = checkboxes[i];
                    if (box.checked) {
                        var id = box.value;
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{csrf_token()}}'
                            },
                            type: "POST",
                            dataType: "json",
                            url: '/api/complete-partially-order',
                            data: {
                                orderId: id
                            },
                            success: function (data) {
                                console.log(data);
                            }
                        });
                    }
                }
            }
        }
    }
    function changeView(hideId, showId) {
        var hideElement = document.getElementById(hideId);
        var showElement = document.getElementById(showId);
        showElement.style.display = hideElement.style.display === 'flex' ? '' : 'flex';
        hideElement.style.display = 'none';
    }
    function saveCommentItem(ID, orderId) {
        var text = document.getElementById(ID + 'Text' + orderId).value;
        var item = document.getElementById(ID + 'ItemID' + orderId).value;
        var linkText = document.getElementById(ID + 'LinkText' + orderId).value;
        var link = document.getElementById(ID + 'Link' + orderId).value;

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: "POST",
            dataType: "json",
            url: '/api/change-order-item-comment',
            data: {
                itemId: item,
                comment: text,
                link: link,
                linkText: linkText
            },
            success: function (data) {
                if (data.status === "success") {
                    var comment = document.getElementById(ID + 'Comment' + orderId);
                    var linkEl = document.getElementById(ID + 'ViewLink' + orderId);
                    comment.innerHTML = text ? text : 'Click to add comment';
                    comment.style.color = text ? '#8b0000' : '#337ab7';
                    comment.style.backgroundColor = text ? '#8b000066' : '#8b000000';
                    linkEl.href = link;
                    linkEl.innerHTML = linkText;
                    linkEl.style.display = link ? '' : 'none';
                }
                changeView(ID + 'Actions' + orderId, ID + 'View' + orderId)
            }
        });
    }

    function editOrderItemCount(itemId, open) {
        var viewQuantity = $('#viewQuantitySection' + itemId);
        var editQuantity = $('#editQuantitySection' + itemId);
        var itemQuantity = $('#itemQuantity' + itemId);

        if (open) {
            viewQuantity.css('display', 'none');
            editQuantity.css('display', 'block');
            itemQuantity.focus();
        } else {
            viewQuantity.css('display', 'block');
            editQuantity.css('display', 'none');
        }
    }

    function updateOrderItemCount(itemId, orderId) {
        var newQuantity = $('#newQuantityItem' + itemId);

        var itemQuantity = $('#itemQuantity' + itemId);

        if (newQuantity.val() === itemQuantity.val()) {
            editOrderItemCount(itemId, false);
            return;
        }

        var itemSubtotal = $('#itemPriceSubtotal' + itemId);
        var itemTotal = $('#itemPriceTotal' + itemId);

        var orderSubtotal = $('#orderSubtotal' + orderId);
        var orderTax = $('#orderTax' + orderId);
        var orderTotal = $('#orderTotal' + orderId);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: "POST",
            dataType: "json",
            url: '/admin/change-order-item-quantity',
            data: {
                quantity: newQuantity.val(),
                itemId: itemId
            },
            success: function (data) {
                if (data.status === "success") {
                    if (data.item) {
                        if (data.item.quantity) {
                            itemQuantity.html(data.item.quantity);
                        }
                        if (data.item.subtotal) {
                            itemSubtotal.html(parseFloat(data.item.subtotal).toFixed(2));
                        }
                        if (data.item.total) {
                            itemTotal.html(parseFloat(data.item.total).toFixed(2));
                        }
                    }
                    if (data.order) {
                        if (data.order.subtotal) {
                            orderSubtotal.html(parseFloat(data.order.subtotal).toFixed(2));
                        }
                        if (data.order.tax) {
                            orderTax.html(parseFloat(data.order.tax).toFixed(2));
                        }
                        if (data.order.total) {
                            orderTotal.html(parseFloat(data.order.total).toFixed(2));
                        }
                    }
                } else {
                    console.log(data);
                }
                editOrderItemCount(itemId, false);
            },
            error: function (data) {
                console.log(data);
                editOrderItemCount(itemId, false);
            }
        });
    }

    $(document).ready(function() {

        $('.item_status').on('change', function () {

            var itemId = $(this).attr('data-itemId');
            var statusName = $(this).attr('name');
            var value = $(this).is(':checked');
            var output = $(this);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: "POST",
                dataType: "json", //Expected data format from server
                url: '/api/change-order-item-status',
                data: {
                    itemId: itemId,
                    statusName: statusName,
                    value: value
                },
                success: function (data) {
                    if (statusName == 'missing') {
                        if (value == true) {
                            $(output).closest('tr').css({"background-color": "#ffb7a1"});
                        } else {
                            $(output).closest('tr').removeAttr( 'style' );
                        }
                    }
                }
            });

        });

        $('.send_email').on('click', function () {

            var orderId = $(this).attr('data-orderId');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                type: "POST",
                dataType: "json", //Expected data format from server
                url: '/api/send-missing-order-item-notification',
                data: {
                    orderId: orderId
                },
                success: function (data) {
                    console.log(data);
                }
            });

        });

    });
</script>