<div class="col-md-12">
    @include('vendor.voyager.orders.edit-modal.info', ['dataTypeContent' => $dataTypeContent])
</div>

<div class="row">

    <div class="col-md-8">
        @include('vendor.voyager.orders.edit-modal.items', ['dataTypeContent' => $dataTypeContent])
    </div>

    <div class="col-md-4">
        @include('vendor.voyager.orders.edit-modal.edit-status', ['dataTypeContent' => $dataTypeContent])
    </div>

</div>


<div class="col-md-4 col-md-offset-4">
    <!-- ### Send email about missing products ### -->
    <div class="panel panel panel-bordered panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-clipboard"></i>Send email about missing products to client</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <button class="btn btn-success send_email" data-orderId="{{$dataTypeContent->id}}" type="button">
                    Send email
                </button>
            </div>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary pull-right" onclick="updateStatus('{{$dataTypeContent->id}}')">
    Update status
</button><br><br><br>
<div class="pull-right" id="checkboxes-for-pinned-order-items"
     @if ($dataTypeContent->status !== 'completed') style="display: none;" @endif>
    @foreach ($dataTypeContent->itemsWithMissed()->get()->where('missing', 1)
        ->where('order_id', '!=', $dataTypeContent->id) as $missItem)
        <input type="checkbox" class="checkbox-pinned-order-close"
               value="{{$missItem->order_id}}"> Also complete PINNED order {{$missItem->order_id}}
        @if (!$loop->last)
            <br>
        @endif
    @endforeach
</div>