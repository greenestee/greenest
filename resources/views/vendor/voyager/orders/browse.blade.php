@extends('voyager::master')

@section('page_title','All '.$dataType->display_name_plural)

@section('css')
    <style>
        /* Center the loader */
        #loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }
            to {
                bottom: 0px;
                opacity: 1
            }
        }

        @keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }
            to {
                bottom: 0;
                opacity: 1
            }
        }

        #myDiv {
            display: none;
            text-align: center;
        }

        /*forwarded order*/
        .forwarded-order {
            background-color: #98fb98;
        }
        .forwarded-order:hover {
            background-color: #d5ffd5 !important;
        }

        /*forwarded order*/
        .thursday-shipping-order {
            background-color: #247efb;
            color: #fff;
        }
        .thursday-shipping-order:hover {
            background-color: #8cbcfb !important;
            color: #fff;
        }

        /*partially completed order*/
        .partially_completed-order {
            background-color: #e5c8f2;
        }
        .partially_completed-order:hover {
            background-color: #f5eafa !important;
        }

        /*partially completed and forwarded order*/
        .partially_completed_and_forwarded-order {
            background-color: #ffff80;
        }
        .partially_completed_and_forwarded-order:hover {
            background-color: #ffffcc !important;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-wallet"></i> {{ $dataType->display_name_plural }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form action="{{ route('download.labels') }}" method="GET">
                            <nobr>
                                <button type="submit" class="btn btn-info right" id="printLabel">Download labels</button>
                                <input type="text" placeholder="Orders ID" name="ids">
                                <label style="margin: 0 10px;">
                                    From
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortFrom" id="date-start">
                                </label>
                                <label style="margin: 0 10px;">
                                    To
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortTo"
                                           id="date-end">
                                </label>
                            </nobr>
                        </form>
                        <form action="{{ route('download.refrigerated.package.cards') }}" method="GET">
                            <nobr>
                                <button type="submit" class="btn btn-primary right">Download refrigerated cards</button>
                                <input type="text" placeholder="Orders ID" name="ids">
                                <label style="margin: 0 10px;">
                                    From
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortFrom"
                                           id="refrigerated_package_cards_sortFrom">
                                </label>
                                <label style="margin: 0 10px;">
                                    To
                                    <input type="text" value="" class="form-control input-sm w-400" name="sortTo"
                                           id="refrigerated_package_cards_sortTo">
                                </label>
                            </nobr>
                        </form>
                        {{--<form action="{{ route('dpd.make.labels') }}" method="POST">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<nobr>--}}
                                {{--<button type="submit" class="btn btn-warning right">DPD Package Cards</button>--}}
                                {{--<input type="text" placeholder="Orders ID" name="ids">--}}
                                {{--<label style="margin: 0 10px;">--}}
                                    {{--From--}}
                                    {{--<input type="text" value="" class="form-control input-sm w-400" name="sortFrom" id="dpd-date-start">--}}
                                {{--</label>--}}
                                {{--<label style="margin: 0 10px;">--}}
                                    {{--To--}}
                                    {{--<input type="text" value="" class="form-control input-sm w-400" name="sortTo"--}}
                                           {{--id="dpd-date-end">--}}
                                {{--</label>--}}
                            {{--</nobr>--}}
                        {{--</form>--}}
                        <form id="getPackageCards" method="GET">
                            <nobr>
                                <button type="submit" class="btn btn-danger right">Smartpost Package Cards</button>
                                <input id="package_cards_ids" type="text" placeholder="Orders ID" name="ids">
                                <label style="margin: 0 10px;">
                                    From
                                    <input id="package_cards_sortFrom" type="text" value=""
                                           class="form-control input-sm w-400" name="sortFrom">
                                </label>
                                <label style="margin: 0 10px;">
                                    To
                                    <input id="package_cards_sortTo" type="text" value=""
                                           class="form-control input-sm w-400" name="sortTo">
                                </label>
                            </nobr>
                        </form>
                        <br>
                        <div>
                            <nobr>
                                <button type="button" class="btn btn-success right" id="send_delay_forwarded_orders">Send notifications for checked forwarded orders</button>
                            </nobr>
                        </div>
                        <div class="pull-right dataTables_filter">
                            {!! Form::open(['method' => 'GET']) !!}
                                <input name="q" minlength="2" class="form-control input-sm" value="{{ Request::get('q') }}" placeholder="name or sku or product...">
                                <button type="submit" class="btn btn-info">Search</button>
                            {!! Form::close() !!}
                        </div>
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    @php
                                        $appends = array();
                                        $params = '';
                                        if (isset($q)) {
                                            $appends['q'] = $q;
                                            $params = http_build_query($appends) . '&';
                                        }
                                    @endphp
                                    {{--<th> <input type="checkbox" checked disabled> </th>--}}
                                    @foreach($dataType->browseRows as $row)
                                    <th>
                                        @if($orderBy == $row->field)
                                            <b>{{ $row->display_name }}</b>
                                        @else
                                            {{ $row->display_name }}
                                        @endif
                                        <a href="{{ url()->current() }}?{{ $params }}sort={{ $row->field }}.asc">⬇</a>
                                        <a href="{{ url()->current() }}?{{ $params }}sort={{ $row->field }}.desc">⬆</a>
                                    </th>
                                    @endforeach
                                    <th class="actions">Actions</th>
                                    <th>Package card status</th>
                                    <th style="display: flex;flex-direction: column;"><label for="check_all_forwarded"><b>SD</b></label><input type="checkbox" id="check_all_forwarded"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTypeContent as $data)
                                <tr class="{{ $data->getStatusClass() }}" id="row-order-{{ $data->id }}">
                                    {{--<td class="labels"> <input type="checkbox" value="{{ $data->id }}" > </td>--}}
                                    @foreach($dataType->browseRows as $row)
                                    <td>
                                        @if($row->type == 'image')
                                            <img src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                        @elseif ($row->field == 'id')
                                            {{ $data->{$row->field} }}
                                            @if ($data->isHaveRefrigerated())
                                                <br>
                                                <img src="/img/snowflake.svg" width="40" height="40">
                                            @endif
                                        @else
                                            {{ $data->{$row->field} }}
                                        @endif
                                    </td>
                                    @endforeach
                                    <td class="no-sort no-click">
                                        @if($data->mollie_payment_status == 'Paid')
                                            <a href="{{ route('refund', $data->id) }}" class="btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> Refund
                                            </a>
                                        @endif
                                        <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}">
                                            <i class="voyager-trash"></i> Delete
                                        </div>
                                        {{--<a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}" class="btn-sm btn-primary pull-right edit">--}}
                                            {{--<i class="voyager-edit"></i> Edit--}}
                                        {{--</a>--}}
                                        @include('vendor.voyager.orders.edit-modal.edit-modal-btn', ['dataTypeContent' => $data])
                                        <a href="{{ route('voyager.'.$dataType->slug.'.show', $data->id) }}" class="btn-sm btn-warning pull-right">
                                            <i class="voyager-eye"></i> View
                                        </a>
                                    </td>
                                    <td>
                                        @if(isset($data->shipment))
                                            ✓
                                        @endif
                                    </td>
                                    <td>
                                        @if($data->status == 'forwarded')
                                            <input type="checkbox" name="delay_forwarded_order" data-orderId="{{$data->id}}" class="delay-forwarded-order_checkbox">
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if (isset($dataType->server_side) && $dataType->server_side)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }} to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries</div>
                            </div>
                            <div class="pull-right">
                                @php
                                    if (isset($order)) {
                                        $appends['sort'] = "{$orderBy}.{$order}";
                                    }
                                @endphp
                                {{ $dataTypeContent->appends($appends)->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($dataTypeContent as $data)
        @include('vendor.voyager.orders.edit-modal.modal-window', ['dataTypeContent' => $data])
    @endforeach

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete this {{ $dataType->display_name_singular }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This {{ $dataType->display_name_singular }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Package cards</h4>
                </div>
                <div class="modal-body">
                    <div id="loader"></div>
                    <div style="display:none;" id="myDiv" class="animate-bottom">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default spin" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="checkShipments" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Some of your cards have already been printed</h4>
                </div>
                <div class="modal-body" id="modal-body-choice">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="modal-confirm">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop

@section('javascript')
    {{-- DataTables --}}

    @include('vendor.voyager.orders.edit-modal.scripts')

    <script>
        @if (!$dataType->server_side)
            $(document).ready(function () {
                $('#dataTable').DataTable({ "order": [] });
            });
        @endif

        $('td').on('click', '.delete', function(e) {
            $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(e.target).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $("#date-start").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#date-end").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#dpd-date-start").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#dpd-date-end").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#package_cards_sortFrom").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#package_cards_sortTo").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#refrigerated_package_cards_sortFrom").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
        $("#refrigerated_package_cards_sortTo").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            orientation: 'bottom'
        });
    </script>
    <script>
        var answer;

        $('#getPackageCards').submit(function ($event) {
            $event.preventDefault();
            var ids = $('#package_cards_ids').val() && $('#package_cards_ids').val();
            var fortFrom = $('#package_cards_sortFrom').val() && $('#package_cards_sortFrom').val();
            var fortTo = $('#package_cards_sortTo').val() && $('#package_cards_sortTo').val();
            if (!(ids || (fortFrom && fortTo))) {
                alert('Need list of ids or date from and date to.');
                return;
            }

            $.ajax({
                url: "{{ route('check.printed.cards') }}",
                method: "POST",

                data: {
                    item_id: ids,
                    sortFrom: fortFrom,
                    sortTo: fortTo,// Second add quotes on the value.
                },
                success: function (response) {
                    $("#modal-body-choice").append("<p>Which one to print again:</p>");
                    answer = response;
                    var array = response.shippedOrders;
                    if (array.length > 0) {
                        array.forEach(function(entry) {
                            $("#modal-body-choice").append("<div class='checkbox'><label><input type='checkbox' name='confirmCards' value='"+entry+"'>Would you like to print "+entry+" again?</label> </div>");
                        });
                        $("#checkShipments").modal('show');
                    } else {
                        var chosen = response.notShippedOrders;

                        chosen = chosen.join(' ');

                        $('#myModal').modal('show');
                        $.get(
                            "{{ route('download.smartpost.package.cards') }}",
                            {
                                ids: chosen,
                                sortFrom: fortFrom,
                                sortTo: fortTo,
                                print_previously_printed: true
                            }
                        )
                            .success(function (data) {
                                data.errors.forEach(function (element) {
                                    $("#myDiv").append("<div class=\"alert alert-danger\" role=\"alert\">" + element + "</div>");
                                });
                                if (data.pdf) {
                                    $("#myDiv").append("<a type=\"button\" class=\"btn btn-success\" download href='" + data.pdf + "'>Download package cards</a>");
                                }
                            })
                            .error(function () {
                                $("#myDiv").append("<div class=\"alert alert-danger\" role=\"alert\">An error occurred</div>");
                            })
                            .complete(function () {
                                document.getElementById("loader").style.display = "none";
                                document.getElementById("myDiv").style.display = "block";
                            });
                    }
                }
            });
        });
        $('#modal-confirm').on('click', function() {
            var fortFrom = $('#package_cards_sortFrom').val() && $('#package_cards_sortFrom').val();
            var fortTo = $('#package_cards_sortTo').val() && $('#package_cards_sortTo').val();
            var chosen = [];
            $.each($("input[name='confirmCards']"), function() {
                if ($(this).prop("checked") == true) {
                    chosen.push($(this).val());
                }
            });

            if(Array.isArray(answer.notShippedOrders)) {
                answer.notShippedOrders.forEach(function(entry) {
                    chosen.push(entry);
                });
            } else {
                chosen.push(answer.notShippedOrders);
            }

            chosen = chosen.join(' ');

            $('#myModal').modal('show');
            $.get(
                "{{ route('download.smartpost.package.cards') }}",
                {
                    ids: chosen,
                    sortFrom: fortFrom,
                    sortTo: fortTo,
                    print_previously_printed: true
                }
            )
                .success(function (data) {
                    data.errors.forEach(function (element) {
                        $("#myDiv").append("<div class=\"alert alert-danger\" role=\"alert\">" + element + "</div>");
                    });
                    if (data.pdf) {
                        $("#myDiv").append("<a type=\"button\" class=\"btn btn-success\" download href='" + data.pdf + "'>Download package cards</a>");
                    }
                })
                .error(function () {
                    $("#myDiv").append("<div class=\"alert alert-danger\" role=\"alert\">An error occurred</div>");
                })
                .complete(function () {
                    document.getElementById("loader").style.display = "none";
                    document.getElementById("myDiv").style.display = "block";
                });
        });
        $('#checkShipments').on('hidden.bs.modal', function (e) {
            $("#modal-body-choice").empty();
        });
        $('#myModal').on('hidden.bs.modal', function (e) {
            document.getElementById("loader").style.display = "block";
            document.getElementById("myDiv").style.display = "none";
            document.getElementById("myDiv").innerHTML = "";
        })
    </script>
    <script>
        $( document ).ready(function() {
            $("#check_all_forwarded").on('change',function(){
                $(".delay-forwarded-order_checkbox").prop('checked',$(this).is(":checked"));
            });

            $("#send_delay_forwarded_orders").on('click',function(){
                var checkedVals = $('.delay-forwarded-order_checkbox:checked').map(function() {
                    return $(this).attr('data-orderId');
                }).get();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    type: "POST",
                    dataType: "json", //Expected data format from server
                    url: '/api/send-delay-forwarded-orders-notification',
                    data: {
                        orderIds: checkedVals
                    },
                    success: function (data) {
                        alert('Notifications will be sent!');
                    }
                });
            });

        });
    </script>
@stop
