@extends('voyager::master')

@section('page_title','All '.$dataType->display_name_plural)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body table-responsive">
                    <div class="row">
                        <h5>Choose period</h5>
                        <form action="{{ route('top.buyer.year') }}" method="post">
                            {{ csrf_field() }}
                            <select name="year" id="">
                                <option value="all_time">All time</option>
                                @foreach ($years as $key => $year)
                                    <option value="{{ $key }}">{{ $year }}</option>
                                @endforeach
                            </select>
                            <button id="button" type="submit" class="red-button" style="text-transform: uppercase; margin-left: 15px;background-color: aquamarine">Get top buyer</button>
                        </form>
                        <table id="dataTable" class="row table table-hover">
                            <thead>
                            <tr>
                                <p><span style="color: darkblue">Percentage of buyers who bought more than once</span><strong> {!! $percent !!} %</strong></p>
                            </tr>
                            <tr>
                                @foreach($dataType->browseRows as $rows)
                                    <th>{{ $rows->display_name }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataTypeContent as $data)
                                <tr>
                                    @foreach($dataType->browseRows as $row)
                                        <td>
                                            {{ $data->{$row->field} }}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (isset($dataType->server_side) && $dataType->server_side)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }} to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <link rel="stylesheet" href="{{ config('voyager.assets_path') }}/lib/css/responsive.dataTables.min.css">
    @endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ config('voyager.assets_path') }}/lib/js/dataTables.responsive.min.js"></script>
    @endif
    <script>
        @if (!$dataType->server_side)
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "order": []
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });
                @endif

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });
    </script>
@stop
