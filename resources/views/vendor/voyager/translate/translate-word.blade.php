@extends('voyager::master')

@section('page_title', 'Add word translate')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-cup"></i> {{ 'Translate Word' }}
    </h1>
@stop

@section('content')
    <form action="{{ route('add.word.translate') }}" method="POST" style="margin-left: 20px; margin-top: 20px;">
        {{ csrf_field() }}
        <input type="text" name="word_en" required>
        <input type="text" name="word_et" required>
        <button type="submit">Add translate</button>
    </form>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Word in En</th>
                                    <th class="text-center">Word in Et</th>
                                    <th class="actions text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($words as $word)
                                <tr>
                                    <form action="{{ route('edit.translate.word', $word->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <td class="text-center">
                                            <input name="word_en" value="{{ $word->word_en }}">
                                        </td>
                                        <td class="text-center">
                                            <input name="word_et" value="{{ $word->word_et }}">
                                        </td>
                                        <td class="no-sort no-click">
                                            <button class="btn-sm pull-right" type="submit">Update translate</button>
                                            <div class="btn-sm btn-danger pull-right delete" data-id="{{ $word->id }}">
                                                <i class="voyager-trash"></i> Delete
                                            </div>
                                        </td>
                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (isset($dataType->server_side) && $dataType->server_side)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }} to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends(Request::except('page'))->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <i class="voyager-trash"></i> Are you sure you want to delete this Translate?
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('remove.word.translate', ['id' => '__id']) }}" id="delete_form" method="POST">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This Translate Word">
                        </form>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    {{-- DataTables --}}
    <script>
        $('td').on('click', '.delete', function(e) {
            $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(e.target).data('id'));
            $('#delete_modal').modal('show');
        });
    </script>
@stop