@extends('voyager::master')

@if(isset($dataTypeContent->id))
    @section('page_title','Edit '.$dataType->display_name_singular)
@else
    @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    @php
        $isImmediatelyAvailable = (strpos($dataTypeContent->slug, 'available-immediately') !== false);
    @endphp
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)) Update Category @else Add New Category @endif</h3>
                    </div>

                        <form role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.categories.update', $dataTypeContent->id) }}@else{{ route('voyager.categories.store') }}@endif" method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->
                            @if(isset($dataTypeContent->id))
                                {{ method_field("PUT") }}
                            @endif
                            {{ csrf_field() }}

                            <div class="panel-body">

                                <div class="form-group">
                                    <label for="parent_id">Parent Category</label>
                                    
                                    <select @if($isImmediatelyAvailable) disabled @endif class="form-control select2" name="parent_id">
                                        <option value="">No parent</option>
                                        @foreach(App\Category::isMain()->get() as $category)
                                            <optgroup label="{{ $category->name }}">
                                                
                                                <option value="{{ $category->id }}" 
                                                    @if(isset($dataTypeContent->parent_id) && $dataTypeContent->parent_id == $category->id)
                                                    {{ 'selected="selected"' }}@endif
                                                    > 
                                                    {{ $category->name }} 
                                                </option>

                                                @foreach($category->children as $child)
                                                    <option value="{{ $child->id }}"
                                                        @if(isset($dataTypeContent->parent_id) && $dataTypeContent->parent_id == $child->id)
                                                        {{ 'selected="selected"' }}@endif
                                                        >
                                                        &nbsp;&nbsp;&nbsp;&nbsp;{{ $child->name }} 
                                                    </option>
                                                @endforeach

                                            </optgroup>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="is_main">Is Main</label>
                                    <br>
                                    <input @if($isImmediatelyAvailable) disabled @endif type="checkbox" name="is_main" class="toggleswitch" @if(isset($dataTypeContent->is_main) && $dataTypeContent->is_main){{ 'checked="checked"' }}@endif>
                                </div>

                                <div class="form-group">
                                    <label for="name">Name [EN]</label>
                                    <input type="text" class="form-control" name="name" placeholder="Name" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">Name [ET]</label>
                                    <input type="text" class="form-control" name="name_et" placeholder="Name" value="@if(isset($dataTypeContent->name_et)){{ $dataTypeContent->name_et }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">URL slug [EN]</label>
                                    <input @if($isImmediatelyAvailable) disabled @endif type="text" class="form-control" @if(!$isImmediatelyAvailable) id="slug" @endif name="slug"
                                        placeholder="slug" data-slug-origin="name"
                                        @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "slug") !!}@endif
                                        value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">URL slug [ET]</label>
                                    <input @if($isImmediatelyAvailable) disabled @endif type="text" class="form-control" @if(!$isImmediatelyAvailable) id="slug_et" @endif name="slug_et"
                                           placeholder="slug" data-slug-origin="name"
                                           @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "slug_et") !!}@endif
                                           value="@if(isset($dataTypeContent->slug_et)){{ $dataTypeContent->slug_et }}@endif">
                                </div>
                                
                                <div class="form-group">
                                    <label for="name">Meta Title [EN]</label>
                                    <input type="text" class="form-control" name="meta_title" placeholder="Meta Title" value="@if(isset($dataTypeContent->meta_title)){{ $dataTypeContent->meta_title }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">Meta Title [ET]</label>
                                    <input type="text" class="form-control" name="meta_title_et" placeholder="Meta Title" value="@if(isset($dataTypeContent->meta_title_et)){{ $dataTypeContent->meta_title_et }}@endif">
                                </div>
                                
                                <div class="form-group">
                                    <label for="name">Meta Description [EN]</label>
                                    <input type="text" class="form-control" name="meta_description" placeholder="Meta Description" value="@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">Meta Description [ET]</label>
                                    <input type="text" class="form-control" name="meta_description_et" placeholder="Meta Description" value="@if(isset($dataTypeContent->meta_description_et)){{ $dataTypeContent->meta_description_et }}@endif">
                                </div>
                                
                                <div class="form-group">
                                    <label for="name">Meta Keywords [EN]</label>
                                    <input type="text" class="form-control" name="meta_keywords" placeholder="Meta Keywords" value="@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif">
                                </div>

                                <div class="form-group">
                                    <label for="name">Meta Keywords [ET]</label>
                                    <input type="text" class="form-control" name="meta_keywords_et" placeholder="Meta Keywords" value="@if(isset($dataTypeContent->meta_keywords_et)){{ $dataTypeContent->meta_keywords_et }}@endif">
                                </div>
                                
                                <div class="form-group">
                                    <label for="is_active">Is Active</label>
                                    <br>
                                    <input type="checkbox" name="is_active" class="toggleswitch" @if(isset($dataTypeContent->is_active) && $dataTypeContent->is_active){{ 'checked="checked"' }}@endif>
                                </div>

                            </div>

                            <div class="panel-footer">
                                @if(isset($dataTypeContent->id))
                                    <button type="submit" class="btn btn-primary" name="action-close" value="{{ URL::previous() }}">
                                        Update & Close
                                    </button>
                                @else
                                    <button type="submit" class="btn btn-primary">
                                        <i class="icon wb-plus-circle"></i> Create
                                    </button>
                                @endif
                            </div>
                        </form>
                </div>           
            </div>
        </div>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('.toggleswitch').bootstrapToggle();

        $('document').ready(function () {
            $('#slug').slugify();
        });
    </script>
    <script src="{{ config('voyager.assets_path') }}/js/slugify.js"></script>
@stop