<!-- Modal fullscreen -->
<div class="modal modal-fullscreen fade" id="categoriesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Categories</h4>
      </div>
      <div class="modal-body">
          @php
              $related_categories = $dataTypeContent->categories()->get()->pluck('id');
              $categories = App\Category::isMain()->with('children.children')->orderBy('name')->get();
          @endphp

          <div class="row">
            @foreach($categories as $main)
              <div class="col-md-2">
                <label><h4><input type="checkbox" value="{{ $main->id }}" id="main{{$main->id}}" class="main-categories"> {{ $main->name }}</h4></label>
              </div>
            @endforeach
          </div>

            @foreach($categories as $main)
              <div class="category-row hide" data-category-id="{{ $main->id }}">

                <h4>{{ $main->name }}</h4>

                <div class="row">
                  @foreach($main->children->sortBy('name') as $category)

                    @if($loop->first)
                      <div class="col-md-2">
                    @elseif($loop->index % ceil($main->children->count() / 5) == 0)
                      </div> <div class="col-md-2">
                    @endif

                      <div class="checkbox">
                        <label><input type="checkbox" name="categories[]" value="{{ $category->id }}" @if($related_categories->contains($category->id)) checked @endif>{{ $category->name }}</label>
                      </div>

                      @foreach($category->children->sortBy('name') as $child)
                        <div class="checkbox" style="padding-left:17px;">
                          <label><input type="checkbox" name="categories[]" value="{{ $child->id }}" @if($related_categories->contains($child->id)) checked @endif ><i>{{ $child->name }}</i></label>
                        </div>
                      @endforeach

                      @if($loop->last)
                        </div>
                      @endif

                  @endforeach
                </div>

              </div>
            @endforeach

      </div>
      <div class="modal-footer">
        @if(isset($dataTypeContent->id))
          <button type="submit" name="action-close" value="{{ URL::previous() }}" class="btn btn-success">
              Update & Close
          </button>
        @endif
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

                  <!--  -->

<!--   <div class="modal fade" id="categoriesModals" role="dialog">
    <div class="modal-dialog modal-lg">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Categories</h4>
        </div>
        <div class="modal-body">
          <p>






                                    @php
                                        $related_categories = $dataTypeContent->categories()->get()->pluck('id');
                                    @endphp

                                    @foreach(App\Category::isMain()->with('children.children')->orderBy('name')->get() as $main)

                                        <h4>{{ $main->name }}</h4>

                                            @foreach($main->children->sortBy('name') as $category)

                                                <option value="{{ $category->id }}" @if($related_categories->contains($category->id)){{ 'selected="selected"' }}@endif>{{ $category->name }}</option>

                                                @foreach($category->children->sortBy('name') as $child)

                                                    <option value="{{ $child->id }}" @if($related_categories->contains($child->id)){{ 'selected="selected"' }}@endif>&nbsp;&nbsp;&nbsp;&nbsp;{{ $child->name }}</option>

                                                @endforeach

                                            @endforeach



                                    @endforeach


          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div> -->