@extends('voyager::master')

@if(isset($dataTypeContent->id))
    @section('page_title','Edit '.$dataType->display_name_singular)
@else
    @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
    </h1>
    <link rel="stylesheet" type="text/css" href="/css/datepicker/bootstrap-datepicker.css">
@stop

@section('content')
    <div class="page-content container-fluid">
        <form role="form" action="@if(isset($dataTypeContent->id)){{ route('voyager.products.update', $dataTypeContent->id) }}@else{{ route('voyager.products.store') }}@endif" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">

                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="col-md-8">
                    <!-- ### Name ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Product name [EN]
                                <span class="panel-desc">The name of your product</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="@if(old('name')) {{ trim(old('name')) }} @elseif(isset($dataTypeContent->name)){{ trim($dataTypeContent->name) }}@endif">
                            <input type="text" id="hidden_name" name="hidden_name" hidden>
                        </div>

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Product name [ET]
                                <span class="panel-desc">The name of your product</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" id="name_et" name="name_et" placeholder="Name" value="@if(old('name_et')) {{ trim(old('name_et')) }} @elseif(isset($dataTypeContent->name_et)){{ trim($dataTypeContent->name_et) }}@elseif(old('name'))MAHE {{ trim(old('name')) }} @elseif(isset($dataTypeContent->name))MAHE {{ $dataTypeContent->name }}@else{{"MAHE"}} @endif">
                            <input type="text" id="hidden_name_et" name="hidden_name_et" hidden>
                        </div>
                    </div>

                    <!-- ### Promoted ### -->
                    @if(\App\Product::getPromotedCount() < 13)
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Promoted product
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($dataTypeContent->is_promoted)
                                    <label><input type="checkbox" value="1" name="is_promoted" checked>Is promoted</label>
                                @else
                                    <label><input type="checkbox" value="1" name="is_promoted">Promote the product</label>
                                @endif
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="form-control" id="from" name="promoted_from" value="@if(old('promoted_from')) {{ old('promoted_from') }} @elseif(isset($dataTypeContent->promoted_from)){{ $dataTypeContent->promoted_from }}@endif">
                                    <div class="input-group-addon">to</div>
                                    <input type="text" class="form-control" id="to" name="promoted_to" value="@if(old('promoted_to')) {{ old('promoted_to') }} @elseif(isset($dataTypeContent->promoted_to)){{ $dataTypeContent->promoted_to }}@endif">
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Promoted product
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <p>12 products are already chosen for promotion!</p>
                            </div>
                        </div>
                @endif
                <!-- ### Short description ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Short description [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="short_description" style="border:0px;">@if(old('short_description')) {{ old('short_description') }} @elseif(isset($dataTypeContent->short_description)){{ $dataTypeContent->short_description }}@else <ul><li>Certified organic product</li><li>Free shipping in Estonia from 60 EUR</li><li>We ship within 2-7 working days</li></ul> @endif</textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Short description [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="short_description_et" style="border:0px;">@if(old('short_description_et')) {{ old('short_description_et') }} @elseif(isset($dataTypeContent->short_description_et)){{ $dataTypeContent->short_description_et }}@else <ul><li>Kontrollitud mahetoode</li><li>Tasuta transport alates 60 EUR</li><li>Pakiautomaati 2-7 tööpäeva jooksul</li></ul> @endif</textarea>
                    </div><!-- .panel -->

                    <!-- ### Ingredients ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Ingredients [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        {{--<textarea class="richTextBox" name="ingredients" style="border:0px;">@if(old('ingredients')) {{ old('ingredients') }} @elseif(isset($dataTypeContent->ingredients)){{ $dataTypeContent->ingredients }}@else <p>(*Certified organic ingredient)</p>@endif</textarea>--}}
                        <textarea class="richTextBox" name="ingredients" style="border:0px;"><p>(*Certified organic ingredient)</p></textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Ingredients [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="ingredients_et" style="border:0px;">@if(old('ingredients_et')) {{ old('ingredients_et') }} @elseif(isset($dataTypeContent->ingredients_et)){{ $dataTypeContent->ingredients_et }}@else <p>(*Kontrollitud mahetoode)</p>@endif</textarea>
                    </div><!-- .panel -->

                    <!-- ### Nutritional value ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Nutritional value [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="nutritional_value" id="nutritionalValue" style="border:0px;">
                            @if(old('nutritional_value')) {{ old('nutritional_value') }} @elseif(isset($dataTypeContent->nutritional_value)){{ $dataTypeContent->nutritional_value }}@else
                                <p>Energy value &nbspkJ /  kcal</p>
                                <p>Fat &nbspg</p>
                                <p>including saturated fatty acids g</p>
                                <p>Carbohydrates &nbspg</p>
                                <p>including sugars &nbspg</p>
                                <p>Protein &nbspg</p>
                                <p>Salt &nbspg</p>
                            @endif
                        </textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Nutritional value [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <script>
                            function translateNut() {
                                var nutVal = tinymce.get("nutritionalValue").getContent();
                                var nutValArr = nutVal.replace(/<\/p>/gi, '').split('<p>');
                                var jqxhr = $.ajax({
                                    url: '/admin/get/translate/text',
                                    method: 'POST',
                                    data: {text: JSON.stringify(nutValArr)}
                                })
                                    .done(function(response) {
                                        var nutValEt = $('#nutritionalValueEt').val(response.text);
                                        tinymce.get("nutritionalValueEt").setContent(response.text);
                                    })
                                    .fail(function() {
                                        console.log('error');
                                    })
                            }
                        </script>
                        <button type="button" onclick="translateNut()">Translate</button>

                        <textarea class="richTextBox" name="nutritional_value_et" id="nutritionalValueEt" style="border:0px;">
                            @if(old('nutritional_value_et')) {{ old('nutritional_value_et') }} @elseif(isset($dataTypeContent->nutritional_value_et)){{ $dataTypeContent->nutritional_value_et }}@else

                            @endif
                        </textarea>
                    </div><!-- .panel -->

                    <!-- ### Recommended storage ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Recommended storage [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="recommended_storage" style="border:0px;">
                            @if(old('recommended_storage')) {{ old('recommended_storage') }} @elseif(isset($dataTypeContent->recommended_storage)){{ $dataTypeContent->recommended_storage }}@else
                                <p>Store in a dry and cool place.</p>
                                <p>Store in a dry and cool place, after opening in the refrigerator.</p>
                                <p>Store in a dry, cool and dark place.</p>
                                <p>Store at room temperature.</p>
                            @endif
                        </textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Recommended storage [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="recommended_storage_et" style="border:0px;">
                            @if(old('recommended_storage_et')) {{ old('recommended_storage_et') }} @elseif(isset($dataTypeContent->recommended_storage_et)){{ $dataTypeContent->recommended_storage_et }}@else
                                <p>Hoida kuivas ja jahedas.</p>
                                <p>Hoida kuivas ja jahedas, pärast avamist külmikus.</p>
                                <p>Hoida kuivas, jahedas ja pimedas.</p>
                                <p>Hoida toatemperatuuril.</p>
                            @endif
                        </textarea>
                    </div><!-- .panel -->

                    <!-- ### Allergenic information ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Allergenic information [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="allergenic_information" style="border:0px;">@if(old('allergenic_information')) {{ old('allergenic_information') }} @elseif(isset($dataTypeContent->allergenic_information)){{ $dataTypeContent->allergenic_information}}@endif</textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Allergenic information [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="allergenic_information_et" style="border:0px;">@if(old('allergenic_information_et')) {{ old('allergenic_information_et') }} @elseif(isset($dataTypeContent->allergenic_information_et)){{ $dataTypeContent->allergenic_information_et}}@endif</textarea>
                    </div><!-- .panel -->

                    <!-- ### Description ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Product description [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="description" style="border:0px;">@if(old('description')) {{ old('description') }} @elseif(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Product description [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="description_et" style="border:0px;">@if(old('description_et')) {{ old('description_et') }} @elseif(isset($dataTypeContent->description_et)){{ $dataTypeContent->description_et }}@endif</textarea>
                    </div><!-- .panel -->


                    <!-- ### Method of preparation ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Method of preparation [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="method_of_preparation" style="border:0px;">@if(old('method_of_preparation')) {{ old('method_of_preparation') }} @elseif(isset($dataTypeContent->method_of_preparation)){{ $dataTypeContent->method_of_preparation}}@endif</textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>Method of preparation [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="method_of_preparation_et" style="border:0px;">@if(old('method_of_preparation_et')) {{ old('method_of_preparation_et') }} @elseif(isset($dataTypeContent->method_of_preparation_et)){{ $dataTypeContent->method_of_preparation_et}}@endif</textarea>
                    </div><!-- .panel -->



                    <!-- ### How to use ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>How to use [EN]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="how_to_use" style="border:0px;">@if(old('how_to_use')) {{ old('how_to_use') }} @elseif(isset($dataTypeContent->how_to_use)){{ $dataTypeContent->how_to_use}}@endif</textarea>

                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-book"></i>How to use [ET]</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                            </div>
                        </div>
                        <textarea class="richTextBox" name="how_to_use_et" style="border:0px;">@if(old('how_to_use_et')) {{ old('how_to_use_et') }} @elseif(isset($dataTypeContent->how_to_use_et)){{ $dataTypeContent->how_to_use_et}}@endif</textarea>
                    </div><!-- .panel -->
                </div>
                <div class="col-md-4">

                    <!-- ### CATEGORIES ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> Categories</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                @foreach($dataTypeContent->categories()->get() as $category)
                                    <b>@if($category->parent_id) {{ $category->parent()->first()->name }} -> @endif {{ $category->name }}</b></br>
                                @endforeach
                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#categoriesModal">Show all categories</button>
                                @include('vendor.voyager.products.modal')
                            </div>
                        </div>
                    </div>

                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i>Product Details</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">URL slug [EN]</label>
                                <input type="text" class="form-control" id="slug" name="slug" data-slug-origin="hidden_name"
                                       placeholder="slug"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "slug") !!}@endif
                                       value="">

                                <label for="name">URL slug [ET]</label>
                                <input type="text" class="form-control" id="slug_et" name="slug_et" data-slug-origin="hidden_name_et"
                                       placeholder="slug"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "slug_et") !!}@endif
                                       value="">
                            </div>
                            <div class="form-group">
                                <label for="name">Meta title [EN]</label>
                                <input type="text" class="form-control" id="meta_title" name="meta_title"
                                       placeholder="Meta title"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_title") !!}@endif
                                       value="@if(old('meta_title')) {{ old('meta_title') }} @elseif(isset($dataTypeContent->meta_title)){{ $dataTypeContent->meta_title }}@endif">

                                <label for="name">Meta title [ET]</label>
                                <input type="text" class="form-control" id="meta_title_et" name="meta_title_et"
                                       placeholder="Meta title"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_title_et") !!}@endif
                                       value="@if(old('meta_title_et')) {{ old('meta_title_et') }} @elseif(isset($dataTypeContent->meta_title_et)){{ $dataTypeContent->meta_title_et }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Meta description [EN]</label>
                                <input type="text" class="form-control" id="meta_description" name="meta_description"
                                       placeholder="Meta description"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_description") !!}@endif
                                       value="@if(old('meta_description')) {{ old('meta_description') }} @elseif(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif">

                                <label for="name">Meta description [ET]</label>
                                <input type="text" class="form-control" id="meta_description_et" name="meta_description_et"
                                       placeholder="Meta description"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_description_et") !!}@endif
                                       value="@if(old('meta_description_et')) {{ old('meta_description_et') }} @elseif(isset($dataTypeContent->meta_description_et)){{ $dataTypeContent->meta_description_et }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Meta keywords [EN]</label>
                                <input type="text" class="form-control" id="meta_keywords" name="meta_keywords"
                                       placeholder="Meta keywords"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_keywords") !!}@endif
                                       value="@if(old('meta_keywords')) {{ old('meta_keywords') }} @elseif(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif">

                                <label for="name">Meta keywords [ET]</label>
                                <input type="text" class="form-control" id="meta_keywords_et" name="meta_keywords_et"
                                       placeholder="Meta keywords"
                                       @if(isset($dataTypeContent)){!! isFieldSlugAutoGenerator($dataTypeContent, "meta_keywords_et") !!}@endif
                                       value="@if(old('meta_keywords_et')) {{ old('meta_keywords_et') }} @elseif(isset($dataTypeContent->meta_keywords_et)){{ $dataTypeContent->meta_keywords_et }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Brand [EN]</label>
                                <input type="text" class="form-control" name="brand" placeholder="Brand" value="@if(old('brand')) {{ old('brand') }} @elseif(isset($dataTypeContent->brand)){{ $dataTypeContent->brand }}@endif">

                                <label for="name">Brand [ET]</label>
                                <input type="text" class="form-control" name="brand_et" placeholder="Brand" value="@if(old('brand_et')) {{ old('brand_et') }} @elseif(isset($dataTypeContent->brand_et)){{ $dataTypeContent->brand_et }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Amount in box</label>
                                <input type="number" class="form-control" name="amount" placeholder="amount" "@if(old('amount')) value="{{ old('amount') }}" @elseif(isset($dataTypeContent->amount)) value="{{ $dataTypeContent->amount }}" @else value="1" @endif">
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-8">
                                    <label for="name">Weight</label>
                                    <input type="number" step="0.001" class="form-control" name="weight" placeholder="weight" @if(old('weight')) value="{{ old('weight') }}" @elseif(isset($dataTypeContent->weight)) value="{{ $dataTypeContent->weight }}" @else value="0" @endif">
                                </div>
                                <div class="col-xs-4">
                                    <label for="unit">Unit</label>
                                    <select class="form-control" name="unit">
                                        <option value="kg" @if(old('weight') && old('weight') == 'kg') selected @elseif(isset($dataTypeContent->unit) && $dataTypeContent->unit == 'kg'){{ 'selected="selected"' }}@endif>Kg</option>
                                        <option value="pieces" @if(old('weight') && old('weight') == 'pieces') selected @elseif(isset($dataTypeContent->unit) && $dataTypeContent->unit == 'pieces'){{ 'selected="selected"' }}@endif>Pieces</option>
                                        <option value="l" @if(old('weight') && old('weight') == 'l') selected @elseif(isset($dataTypeContent->unit) && $dataTypeContent->unit == 'l'){{ 'selected="selected"' }}@endif>Liter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">SKU</label>
                                <input type="text" class="form-control" name="sku" placeholder="SKU" value="@if(old('sku')) {{ old('sku') }} @elseif(isset($dataTypeContent->sku)){{ $dataTypeContent->sku }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Source url</label>
                                <input type="text" class="form-control" name="product_url" placeholder="Source url" value="@if(old('product_url')) {{ old('product_url') }} @elseif(isset($dataTypeContent->product_url)){{ $dataTypeContent->product_url }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Country of origin [EN]</label>
                                <input type="text" class="form-control" name="origin_country" placeholder="Country of origin" value="@if(old('origin_country')) {{ old('origin_country') }} @elseif(isset($dataTypeContent->origin_country)){{ $dataTypeContent->origin_country}}@endif">

                                <label for="name">Country of origin [ET]</label>
                                <input type="text" class="form-control" name="origin_country_et" placeholder="Country of origin" value="@if(old('origin_country_et')) {{ old('origin_country_et') }} @elseif(isset($dataTypeContent->origin_country_et)){{ $dataTypeContent->origin_country_et}}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Country of origin of ingredients [EN]</label>
                                <input type="text" class="form-control" name="origin_country_of_ingredients" placeholder="Country of origin of ingredients" value="@if(old('origin_country_of_ingredients')) {{ old('origin_country_of_ingredients') }} @elseif(isset($dataTypeContent->origin_country_of_ingredients)){{ $dataTypeContent->origin_country_of_ingredients}}@endif">

                                <label for="name">Country of origin of ingredients [ET]</label>
                                <input type="text" class="form-control" name="origin_country_of_ingredients_et" placeholder="Country of origin of ingredients" value="@if(old('origin_country_of_ingredients_et')) {{ old('origin_country_of_ingredients_et') }} @elseif(isset($dataTypeContent->origin_country_of_ingredients_et)){{ $dataTypeContent->origin_country_of_ingredients_et}}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Shelf life [EN]</label>
                                <input type="text" class="form-control" name="shelf_life" placeholder="Shelf life" value="@if(old('shelf_life')) {{ old('shelf_life') }} @elseif(isset($dataTypeContent->shelf_life)){{ $dataTypeContent->shelf_life}}@endif">

                                <label for="name">Shelf life [ET]</label>
                                <input type="text" class="form-control" name="shelf_life_et" placeholder="Shelf life" value="@if(old('shelf_life_et')) {{ old('shelf_life_et') }} @elseif(isset($dataTypeContent->shelf_life_et)){{ $dataTypeContent->shelf_life_et}}@endif">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="is_lightweight" @if(old('is_lightweight')) checked @elseif(isset($dataTypeContent->is_lightweight) && $dataTypeContent->is_lightweight){{ 'checked="checked"' }}@endif>
                                <label for="is_lightweight">Is lightweight</label>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="is_vegan" @if(old('is_vegan')) checked @elseif(isset($dataTypeContent->is_vegan) && $dataTypeContent->is_vegan){{ 'checked="checked"' }}@endif>
                                <label for="is_vegan">Is vegan</label>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="is_gluten_free" @if(old('is_gluten_free')) checked @elseif(isset($dataTypeContent->is_gluten_free) && $dataTypeContent->is_gluten_free){{ 'checked="checked"' }}@endif>
                                <label for="is_gluten_free">Gluten free</label>
                            </div>
                            <div class="form-group">
                                <label for="name">Purchase Price, €</label>
                                <input type="number" step="0.01" class="form-control" name="purchase_price" placeholder="Purchase price" @if(old('purchase_price')) value="{{ old('purchase_price') }}" @elseif(isset($dataTypeContent->purchase_price)) value="{{ $dataTypeContent->purchase_price}}" @endif">
                            </div>
                            <div class="form-group">
                                <label for="name"><b>Retail Price, €</b></label>
                                <input type="number" step="0.01" class="form-control" name="price" placeholder="Price" @if(old('price')) value="{{ old('price') }}" @elseif(isset($dataTypeContent->price)) value="{{ $dataTypeContent->price }}" @endif">
                            </div>

                            <div class="form-group">
                                <label for="name"><b>In stock</b></label>
                                <input type="number" class="form-control" name="quantity" placeholder="Quantity" @if(old('quantity')) value="{{ old('quantity') }}" @elseif(isset($dataTypeContent->quantity)) value="{{ $dataTypeContent->quantity }}" @endif">
                            </div>


                            <div class="form-group">
                                <label for="is_active"><b>Status</b></label>
                                <select class="form-control" name="is_active">
                                    <option value="1" @if(isset($dataTypeContent->is_active) && $dataTypeContent->is_active == '1'){{ 'selected="selected"' }}@endif>Active</option>
                                    <option value="0" @if(old('is_active') && old('is_active') == '0') selected @elseif(isset($dataTypeContent->is_active) && $dataTypeContent->is_active == '0'){{ 'selected="selected"' }}@endif>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> Image</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(isset($dataTypeContent->image))
                                <img src="{{ Voyager::image( $dataTypeContent->image, '/storage/' . $dataTypeContent->image) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>

                    <!-- ### SEO CONTENT ### -->
                    {{--<div class="panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> SEO Content</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">Meta Description</label>
                                <textarea class="form-control" name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Meta Keywords</label>
                                <textarea class="form-control" name="meta_keywords">@if(isset($dataTypeContent->meta_keywords)){{ $dataTypeContent->meta_keywords }}@endif</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">SEO Title</label>
                                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title" value="@if(isset($dataTypeContent->seo_title)){{ $dataTypeContent->seo_title }}@endif">
                            </div>
                        </div>
                    </div>--}}

                    <div class="row pull-right">
                        @if(!isset($dataTypeContent->id))
                            <button type="submit" name="action" value="save-with-copy" class="btn btn-success">
                                Create & Make a copy
                            </button>
                        @else
                            <button type="submit" name="action-close" value="{{ URL::previous() }}" class="btn btn-success">
                                Update & Close
                            </button>
                        @endif
                        <button type="submit" class="btn btn-primary">
                            @if(isset($dataTypeContent->id)){{ 'Update Product' }}@else<?= '<i class="icon wb-plus-circle"></i> Create New Product'; ?>@endif
                        </button>
                    </div>

                </div>
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>

@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('#slug').slugify();
            $('#slug_et').slugify();
            $(".input-daterange").datepicker({
                format: 'yyyy-mm-dd',
                startDate: '0',
                autoclose: true
            });

            // for eng
            var name = $("input[name='name']").val().trim();
            var brand = $("input[name='brand']").val().trim();

            //for et
            var brand_et = $("input[name='brand_et']").val().trim();

            // For slug en language
            $("input[name='slug']").val(getSlug(brand,name));
            $("input#hidden_name").val(getSlug(brand, name));
            $("input#hidden_name").trigger("change");

            // For slug et language
//            $("input#name_et").val("MAHE " + name);
//            $("input[name='slug_et']").val($("input#name_et").val());

            $("input#hidden_name_et").val(getSlug(brand_et, $("input#name_et").val()));
            $("input#hidden_name_et").trigger("change");
        });
    </script>
    <script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
    <script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
    <script src="{{ config('voyager.assets_path') }}/js/slugify.js"></script>
    <script>
        $(".modal-fullscreen").on('show.bs.modal', function () {
            setTimeout( function() {
                $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
            }, 0);
        });

        $(".modal-fullscreen").on('hidden.bs.modal', function () {
            $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        });

        $('.main-categories').click(function() {
            if ($(this).is(':checked')) {
                $("[data-category-id='" + $(this).val() + "']").removeClass('hide');
            } else {
                $("[data-category-id='" + $(this).val() + "']").addClass('hide');
            }
        });

        $('.category-row input:checked').each(function(){
            $(this).parents('.hide').removeClass('hide');
            $('#main' + $(this).parents('[data-category-id]').data('category-id')).prop('checked', true);
        });

        $("input[name='name']").keyup(function() {
            var name = $(this).val();
            var brand = $("input[name='brand']").val();

            // $("input#name_et").val("MAHE " + name);
            // $("input[name='slug']").val(getSlug(brand,name));
            // $("input[name='slug_et']").val(getSlug("MAHE " + brand,name));

            $("input#hidden_name").val(getSlug(brand, name));
            $("input#hidden_name").trigger("change");

            // $("input#hidden_name_et").val(getSlug("MAHE " + brand,name));
            // $("input#hidden_name_et").trigger("change");
        });

        $("input[name='name_et']").keyup(function() {
            var name_et = $(this).val();
            var brand_et = $("input[name='brand_et']").val();

            $("input[name='slug_et']").val(getSlug(brand_et,name_et));

            $("input#hidden_name_et").val(getSlug(brand_et,name_et));
            $("input#hidden_name_et").trigger("change");
        });

        $("input[name='brand']").keyup(function() {
            var name = $("input[name='name']").val();
            var brand = $(this).val();

            $("input[name='slug']").val(getSlug(brand,name));
        });

        $("input[name='brand_et']").keyup(function() {
            var name_et = $("input[name='name_et']").val();
            var brand_et = $(this).val();

            $("input[name='slug_et']").val(getSlug(brand_et,name_et));
        });

        function getSlug(brand, name) {
            var slug = brand ? brand + '-' + name : name;
            return slug.replace(/\s/g, '-').toLowerCase();
        }

    </script>

    <script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
@stop

@section('css')
    <style>
        body {
            background: #72cffa;
        }
        /* .modal-fullscreen */

        .modal-fullscreen {
            background: transparent;
        }
        .modal-fullscreen .modal-content {
            background: transparent;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .modal-backdrop.modal-backdrop-fullscreen {
            background: #ffffff;
        }
        .modal-backdrop.modal-backdrop-fullscreen.in {
            opacity: .97;
            filter: alpha(opacity=97);
        }

        /* .modal-fullscreen size: we use Bootstrap media query breakpoints */

        .modal-fullscreen .modal-dialog {
            margin: 0;
            margin-right: auto;
            margin-left: auto;
            width: 100%;
        }
        @media (min-width: 768px) {
            .modal-fullscreen .modal-dialog {
                width: 750px;
            }
        }
        @media (min-width: 992px) {
            .modal-fullscreen .modal-dialog {
                width: 970px;
            }
        }
        @media (min-width: 1200px) {
            .modal-fullscreen .modal-dialog {
                width: 1240px;
            }
        }

        /* centering styles for jsbin */
        html,
        body {
            width:100%;
            height:100%;
        }
        html {
            display:table;
        }
        body {
            display:table-cell;
            vertical-align:middle;
        }
        body > .btn {
            display: block;
            margin: 0 auto;
        }
        input[type=number]::-webkit-inner-spin-button,
        input[typeg=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: textfield;
            appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance:textfield;
        }
    </style>
@stop