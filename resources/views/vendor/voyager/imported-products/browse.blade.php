@extends('voyager::master')

@section('page_title','All '.$dataType->display_name_plural)

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-cup"></i> {{ $dataType->display_name_plural }}
        @if (Voyager::can('add_'.$dataType->name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success">
                <i class="voyager-plus"></i> Add New
            </a>
        @endif
        <div class="pull-right dataTables_filter">
            {!! Form::open(['method' => 'GET']) !!}
                <input name="q" minlength="2" class="form-control input-sm" value="{{ Request::get('q') }}" placeholder="name or sku or brand...">
                <button type="submit" class="btn btn-info">Search</button>
            {!! Form::close() !!}
        </div>
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    @foreach($dataType->browseRows as $row)
                                        <th class="text-center">{{ $row->display_name }}</th>
                                    @endforeach
                                    <th class="actions text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataTypeContent as $data)
                                <tr >
                                    @foreach($dataType->browseRows as $row)
                                    <td class="text-center">
                                        @if($row->type == 'image')
                                            <img src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="max-width:100px;max-height:100px">
                                        @elseif($row->field == 'is_active')
                                            @if($data->{$row->field} == 1)
                                                Yes
                                            @else
                                               <span class="text-danger"> No </span>
                                            @endif
                                        @else
                                            {{ $data->{$row->field} }}

                                            @if($row->display_name == "Name")
                                                @if($data->status == 1 || $data->quantity != 0 )

                                                    <br>
                                                    <span style="color: green">In stock: {{ $data->quantity }}<span>
                                                @else
                                                                <br>
                                                                <span style="color: red">In stock: {{ $data->quantity }}</span> <br>
                                                                <span style="color: red">@if(!empty($data->delivery_date)) Expected arrival date {{date("d.m.Y", strtotime($data->delivery_date)) }} @endif<span>
                                                @endif
                                            @endif
                                        @endif
                                    </td>
                                    @endforeach
                                    <td class="no-sort no-click">
                                    <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}">
                                        <i class="voyager-trash"></i> Delete
                                    </div>
                                    <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}" class="btn-sm btn-primary pull-right edit">
                                        <i class="voyager-edit"></i> Edit
                                    </a>
                                    @if($data->is_active)
                                    <a href="{{ route('disableProduct', $data->id) }}" class="btn-sm btn-warning pull-right">
                                        <i class="voyager-eye"></i> Disable
                                    </a>
                                    @else
                                    <a href="{{ route('activeProduct', $data->id) }}" class="btn-sm btn-success pull-right">
                                        <i class="voyager-eye"></i> Activate
                                    </a>
                                    @endif
                                    @if($data->is_blacklist)
                                        <div class="btn-sm btn-blacklist pull-right active" data-id="{{ $data->id }}">BlackListed</div>
                                    @else
                                        <div class="btn-sm btn-blacklist pull-right" data-id="{{ $data->id }}">BlackList</div>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if (isset($dataType->server_side) && $dataType->server_side)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }} to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends(Request::except('page'))->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> Are you sure you want to delete this {{ $dataType->display_name_singular }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Yes, Delete This {{ $dataType->display_name_singular }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    {{-- DataTables --}}
    <script>
        $('td').on('click', '.delete', function(e) {
            $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(e.target).data('id'));
            $('#delete_modal').modal('show');
        });


        $('.btn-blacklist').on('click', function(e) {
            $btn = $(this)
            id = $btn.data('id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: ' /admin/products/setblacklist/' + id,
                success: function (data) {
                    if (data == 'on') {
                        $btn.addClass('active');
                        $btn.html('BlackListed')
                    } else if (data == 'off') {
                        $btn.removeClass('active');
                        $btn.html('BlackList')
                    }
                },
                error: function (data) {
                }
            });
        });



    </script>
@stop
