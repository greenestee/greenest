<!doctype html>
<html class="no-js" lang="en">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5P7XVX3');</script>
        <!-- End Google Tag Manager -->
        <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ App::getLocale() == 'et' ? Voyager::setting('title_et') : Voyager::setting('title') }}</title>
        <meta name="title" content="{{ App::getLocale() == 'et' ? Voyager::setting('meta_title_et') : Voyager::setting('meta_title') }}">
        <meta name="description" content="{{ App::getLocale() == 'et' ? Voyager::setting('meta_description_et') : Voyager::setting('meta_description') }}">
        <meta name="keywords" content="{{ App::getLocale() == 'et' ? Voyager::setting('meta_keywords_et') : Voyager::setting('meta_keywords') }}">
        <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
        <meta name="author" content="Greenest" />
        <meta name="rating" content="General"/>
        <META NAME="Category" CONTENT="Food">
        <meta name="copyright" content="https://greenest.ee/ - 2017"/>
        <META NAME="Language" CONTENT="en">
        <meta name="robots" content="index, follow"/>
        <meta name="googlebot" content="index, follow"/>
        <meta name="revisit-after" content="1 days"/>
        <META NAME="Publisher" CONTENT="ee">
        <meta name="Classification" content="Food, Shopping ">
        <meta name="distribution" content="global" />
        <link rel="canonical" href="https://greenest.ee/" />
        <link rel="prerender" href="https://greenest.ee/">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="qr-code" href="greenest.ee.png">
        <link rel="icon" href="favicon.png" type="image/x-icon" />
        <link rel="alternate" type="text/directory" title="vCard" ref="vCard.vcf" />
        <link type="text/plain" rel="author" href="humans.txt" />

        <link rel="stylesheet" href="{{ asset('/assets/css/app.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">
        <link rel="stylesheet" href="./js/slick/slick.css">
        <link rel="stylesheet" href="./js/slick/slick-theme.css">
		<script type="text/javascript">
            window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
            heap.load("1424900628");
        </script>
        @yield('ga-data-layer')
        <script  src="./js/jquery-2.2.4.min.js"></script>
        <script  src="./js/slick/slick.min.js"></script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2263036823807892');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=2263036823807892&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->
    </head>

    <body class="welcome-page">
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-98071091-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "url": "https://greenest.ee/en/",
          "logo": "https://greenest.ee/Greenest-logo.png",
          "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+372 51 900 330",
            "contactType": "customer service"
          }]
        }
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org/",
          "@type": "Website",
          "name": "Greenest",
          "description": "Ultra interesting. Super impressive.",
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "5",
            "bestRating": "5",
            "ratingCount": "5"
          }
        }
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Person",
          "name": "Greenest",
          "url": "https://greenest.ee/",
          "sameAs": [
            "https://www.facebook.com/greenest.ee/",
            "https://www.youtube.com/channel/UCcBEcbrVrGJbOAHoHTiqiVQ",
            "https://twitter.com/greenest_ee",
            "https://www.pinterest.com/greenestee/"
          ]
        }
        </script>

        <div class="main-wrapper">
            <div class="content"> 
                <header>
                    @if (isset($headerNav))
                    <a href="{{ url($headerNav->keys()->first()) }}"><img src="{{ asset('/Greenest-logo2.png') }}"></a>
                    @endif
                </header>

                @yield('content')

            </div>
        </div>
    </body>
</html>