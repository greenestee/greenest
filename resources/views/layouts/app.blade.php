<!doctype html>
<html class="no-js" lang="{{ App::getLocale() }}">

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5P7XVX3');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <title>@if(!empty($page_type) && in_array($page_type, ['single_product', 'category'])) @yield('title') @else {{ App::getLocale() == 'et' ? Voyager::setting('title_et') : Voyager::setting('title') }} @yield('title') @endif</title>
    @yield('meta')
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="preconnect" href="//fonts.gstatic.com" crossorigin>
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com/">
    <link rel="stylesheet" href="{{ asset('/assets/css/app.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}">
    @yield('css')
    <link rel="shortcut icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">
    <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
	<script type="text/javascript">
window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
heap.load("1424900628");
</script>
    @yield('gtm-dataLayer')
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2263036823807892');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2263036823807892&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
	</head>
    <style>
        li.out-stock {
            list-style-image: url(../red.png) !important;
            color: red !important;
        }
    </style>
<body @yield( 'body-class')>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5P7XVX3"
            height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="main-wrapper">
        <div class="content"> @yield('google-tranlsate') @include('parts.header') @yield('content') @include('parts.footer') </div> @include('parts.modal-login')
        <script src="{{ asset('/js/jquery-2.2.4.min.js') }}"></script>
        <script src="{{ asset('/assets/js/app.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script src="{{ asset('/assets/js/style.js') }}"></script>
        <script src="{{ asset('js/languages.js') }}"></script>
        <script src="{{ asset('/assets/js/login.js') }}"></script> 
        @stack('scripts')
        <script type="text/javascript">
            (function(d, s, id) {
                var z = d.createElement(s);
                z.type = "text/javascript";
                z.id = id;
                z.async = true;
                z.src = "//static.zotabox.com/1/3/137d744d28bd3e006d4b74d98b90acc8/widgets.js";
                var sz = d.getElementsByTagName(s)[0];
                sz.parentNode.insertBefore(z, sz)
            }(document, "script", "zb-embed-code"));
        </script>
    </div>
</body>

</html>
