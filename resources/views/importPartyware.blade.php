<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/assets/css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">
    <meta name="verify-paysera" content="1a4bdae52b9862e738ddd47b92d5474e">
</head>
<body>
    @if(session()->has('answer_xls'))
        <p style="color: darkgreen;">{{ session()->get('answer_xls') }}</p>
        @php session()->forget('answer_xls') @endphp
    @endif
    <p>Choose xlsx file to import products:</p>
    <form action="{{ route('partyware-xls.post') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="uploadFiles"><br>
        <button>Import xlsx</button>
    </form>
    <br>
    <br>
    @if(session()->has('answer_img'))
        <p style="color: darkgreen;">{{ session()->get('answer_img') }}</p>
        @php session()->forget('answer_img') @endphp
    @endif
    <p>Choose zip file to import images for products:</p>
    <form action="{{ route('partyware-images.post') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="uploadZipFile"><br>
        <button>Import zip</button>
    </form>
</body>
</html>
