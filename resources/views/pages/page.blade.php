@extends('layouts.app')

@php
$locale = App::getLocale();
if ($locale == 'et') {
    $title = $page->title_et;
    $meta_description = $page->meta_description_et;
    $meta_keywords = $page->meta_keywords_et;
    $body = $page->body_et;
} else {
    $title = $page->title;
    $meta_description = $page->meta_description;
    $meta_keywords = $page->meta_keywords;
    $body = $page->body;
}

@endphp

@section('title', ' - ' . $title )


@section('meta')

	<meta name="description" content="{{ $meta_description }}">

	<meta name="keywords" content="{{ $meta_keywords }}">
	

	

@endsection



@section('body-class', 'class="text"')



@section('content')

    <div class="row">

        <div class="text-wrapper large-12 column">

            <p>{!! $body !!}</p>

        </div>

    </div>

@endsection