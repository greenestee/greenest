@extends('layouts.welcome')





@section('content')



    <div class="row">


        <h1>{{ App::getLocale() == 'et' ? Voyager::setting('description_et') : Voyager::setting('description') }}</h1>
        {{--<h2 style="color: white;">{{ trans('test.test') }}</h2>--}}


        <div class="large-12 column">
            <!--page content start-->
            @if(\App\Product::getPromotedCount()>0)
                <div class="main-box">
            @endif

                    <div class="slider__wrapper">
                        @foreach($products as $key => $product)
                            @if(($loop->iteration % 2 == 1))
                                <div class="slider__item">
                                    <section class="products list-item">
                            @endif
                                <div class="row product-item">
                                    <div class="product-icons @if(!$product->is_gluten_free || !$product->is_vegan) one-items @endif ">
                                        @if($product->is_gluten_free)
                                            <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
                                        @endif

                                        @if($product->is_vegan)
                                            <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
                                        @endif
                                    </div>
                                    <p class="top-name">@if(App::getLocale() == 'en') {{ $product->name }}@else {{ $product->name_et }} @endif</p>
                                    <div class="clearfix"></div>

                                    <div class="vsz_img_wrapper_list">
                                        <a href="{{ App::getLocale() }}/product/{{ $product->slug }}" data-open="product{{ $product->id }}" class="item-modal"><img id="product-img-{{ $product->id }}" src="{{ $product->getImage() }}" alt="@if(App::getLocale() == 'en') {{ $product->name }}@else {{ $product->name_et }} @endif"></a>
                                    </div>

                                    <p class="bottom-name">@if(App::getLocale() == 'en') {{ $product->name }}@else {{ $product->name_et }} @endif</p>

                                    <div class="vsz_description_list">
                                        <a href="{{ App::getLocale() }}/product/{{ $product->slug }}">

                                           @if(App::getLocale() == 'en') {!! $product->short_description !!} @else {!! $product->short_description_et !!} @endif
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="vsz_more_inforamtion_list">
                                        <div class="pieces-wrapper">
                                            <p>{!! $product->getAmount() !!}</p>
                                        </div>
                                        <p class="price">€{{ $product->getPrice() }}</p>
                                        <div class="list_pieces-wrapper">
                                            <p>{!! $product->getAmount() !!}</span> </p>
                                            <p class="price">€{{ $product->getPrice() }}</p>
                                        </div>
                                        <div class="btn-wrapper">
                                            <span>{{ $product->getPricePerPiece() }} €</span>  <a href="{{ App::getLocale() }}/product/{{ $product->slug }}" class="alert button buy_btn" data-product="{{ $product->id }}" data-page="list">{{ trans('static_text.buy') }}</a>
                                        </div>
                                    </div>
                                </div>
                            @include('products.item-modal')
                            @if(($loop->iteration % 2 == 0))
                                    </section>
                                </div>
                            @endif
                        @endforeach
                            @if((count($products) % 2 != 0))
                                    </section>
                                </div>
                            @endif
                    </div>


                @if(\App\Product::getPromotedCount()>0)
                    </div>
                @endif
        <!--page content end-->

        </div>
    </div>

    @php
        $currentLocale = App::getLocale();
    @endphp

    <a href="{{ url($headerNav->keys()->first()) }}" id="buy-now">
        {{ $currentLocale == 'en' ? 'Shop now' : 'Lähen ostma' }}
    </a>

    <script>
        $('.slider__wrapper').slick({
            dots:     	 	  false,
            autoplaySpeed: 	3000,
            autoplay: 		  true,
            arrows:   		  false,
            infinite: 		  true,
            pauseOnFocus:   true,
            pauseOnHover:   true
        });

    </script>



@endsection






