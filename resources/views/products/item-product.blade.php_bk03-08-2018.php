@extends('layouts.app', ['page_type' => $page_type])

@php
    $locale = App::getLocale();
@endphp
@section('gtm-dataLayer')
    <script>
        {!! $dataLayer !!}
    </script>
@endsection
@if(!empty($product))
    @if(!empty($meta_title))
        @section('title'){{ $meta_title }}@endsection
    @endif

    @if(!empty($meta_description) || !empty($meta_keywords))
        @section('meta')
            @if(!empty($meta_description))
                <meta name="description" content="{{ $meta_description }}">
            @endif

            @if(!empty($meta_keywords))
                <meta name="keywords" content="{{ $meta_keywords }}">
            @endif
        @endsection
    @endif
@endif

@section('google-tranlsate')
    @if($locale != 'et')
        <div id="google_translate_element" style="width: 13%;">
            <a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
        </div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    @endif
@endsection

@section('content')
@php
    if (App::getLocale() == 'et') {
        $name = $product->name_et;
        $brand = $product->brand_et;
        $ingredients = $product->ingredients_et;
        $nutritional_value = $product->nutritional_value_et;
        $recommended_storage = $product->recommended_storage_et;
        $allergenic_information = $product->allergenic_information_et;
        $description = $product->description_et;
        $method_of_preparation = $product->method_of_preparation_et;
        $how_to_use = $product->how_to_use_et;
        $origin_country = $product->origin_country_et;
        $origin_country_of_ingredients = $product->origin_country_of_ingredients_et;
        $shelf_life = $product->shelf_life_et;
    } else {
        $name = $product->name;
        $brand = $product->brand;
        $ingredients = $product->ingredients;
        $nutritional_value = $product->nutritional_value;
        $recommended_storage = $product->recommended_storage;
        $allergenic_information = $product->allergenic_information;
        $description = $product->description;
        $method_of_preparation = $product->method_of_preparation;
        $how_to_use = $product->how_to_use;
        $origin_country = $product->origin_country;
        $origin_country_of_ingredients = $product->origin_country_of_ingredients;
        $shelf_life = $product->shelf_life;
    }
@endphp
<div class="row">
<div class="pp1 large-12 column">
    <div class="product-icons">
        @if($product->is_gluten_free)
            <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
        @endif

        @if($product->is_vegan)
            <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
        @endif
    </div>

    <div class="row"><h3>{{ $name }}</h3></div>
    <div class="row">
        <div class="large-6 small-12 columns">
            <div class="vsz_img_wrapper">
                <a href="#" data-open="product{{ $product->id }}" class="item-modal">
                    <img id="product-img-{{ $product->id }}" src="{{ $product->getImage() }}" alt="{{ $name }}">
                </a>
            </div>
        </div>
        <div class="large-6 small-12 columns">
            @if($brand)
                <h4>{{ trans('static_text.Brand') }}</h4>
                {{ $brand }}</br>
            @endif

            @if($ingredients)
                <h4>{{ trans('static_text.Ingredients') }}</h4>
                {!! $ingredients !!}
            @endif

            @if($nutritional_value)
                <h4>{{ trans('static_text.Nutritional_value_of_100_g') }}</h4>
                {!! $nutritional_value !!}
            @endif

            @if($recommended_storage)
                <h4>{{ trans('static_text.Recommended_storage') }}</h4>
                {!! $recommended_storage !!}
            @endif

            @if($allergenic_information)
                <h4>{{ trans('static_text.Allergenic_information') }}</h4>
                {!! $allergenic_information !!}
            @endif
        </div>
        <div class="row">
            <div class="large-12 small-12 columns vsz_more_information">
                <p class="vsz_pieces">{!! $product->getAmount() !!}</p>
                <p class="price">€{{ $product->getPrice() }}</p>
                <div class="btn-wrapper">
                    <button @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @endif  type="button" class="alert button buy_btn" data-product="{{ $product->id }}" data-page="single" onclick="dataLayer.push({'event':'addToCart','ecommerce':{'currencyCode':'EUR','add':{'actionField':{'list':'Product Page'},'products':[{'name':'{{ $product->name }}','sku':'{{ $product->sku }}','price':'{{ $product->getPrice() }}','category':'{{ session('category') }}','quantity':1}]}}});">{{ trans('static_text.buy') }}</button>
                </div>
                <p class="vsz_per_price">{{ $product->getPricePerPiece() }} €</p>
            </div>
        </div>

        <div class="row">
            <div class="large-12 small-12 columns">
                @if($description)
                    <div class="inline-row">
                        <h4>{{ trans('static_text.Description') }}</h4>
                        {!! $description !!}
                    </div>
                @endif
                @if($method_of_preparation)
                    <div class="inline-row">
                        <h4>{{ trans('static_text.Method_of_preparation') }}</h4>
                        {!! $method_of_preparation !!}
                    </div>
                @endif
                @if($how_to_use)
                    <div class="inline-row">
                        <h4>{{ trans('static_text.How_to_use') }}</h4>
                        {!! $how_to_use !!}
                    </div>
                @endif
                @if($origin_country)
                    <div class="country-row">
                        <h4>{{ trans('static_text.Country_of_origin') }}</h4>
                        {!! $origin_country !!}
                    </div>
                @endif
                @if($origin_country_of_ingredients)
                    <div class="country-row">
                        <h4>{{ trans('static_text.Country_of_origin_of_ingredients') }}</h4>
                        {!! $origin_country_of_ingredients !!}
                    </div>
                @endif
                @if($shelf_life)
                    <div class="inline-row">
                        <h4>{{ trans('static_text.Shelf_life') }}</h4> {!! $shelf_life !!} days
                    </div>
                @endif
            </div>
        </div>

    </div>

</div>
</div>
@endsection
@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script>
    (function(){
        $('.hollow.button.float-right.list').on('click',function(e) {
            $('.products ').addClass('tile-list');
            $('.products ').removeClass('list-item');
        });

        $('.hollow.button.float-right.tile').on('click',function(e) {
            $('.products ').removeClass('tile-list');
            $('.products ').addClass('list-item');
        });
    })();
</script>

<script>
    $('a.item-modal').click(function(){
        $('#' + $(this).data('open')).foundation('open');
    });

    $('.reveal.xlarge').mouseleave(function() {
        $(this).foundation('close');
    });
</script>
<script>
        if ($('#google_translate_element').length){
            $('#close-translate').css('display', 'inline');
        };

        $('#close-translate').on('click', function(event){
            event.preventDefault();
            $('#google_translate_element').css('display', 'none');
        });
    </script>
@endpush
