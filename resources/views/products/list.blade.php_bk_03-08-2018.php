    @extends('layouts.app', ['page_type' => $page_type])

    @php
        $locale = App::getLocale();
    @endphp

    @if(!empty($meta_title))
        @section('title')
            {{ $meta_title }}
        @endsection
    @endif

    @section('meta')
        <meta name="description" content="{{ $meta_description }}">
        <meta name="keywords" content="{{ $meta_keywords }}">
    @endsection

    @section('gtm-dataLayer')
        <script>
            {!! $dataLayer !!}
        </script>
    @endsection
    @section('google-tranlsate')
        @if($locale != 'et')
            <div id="google_translate_element" style="width: 13%;">
                <a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
            </div>
            <script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        @endif
    @endsection

    @section('content')

        <div class="row small-collapse ">
            <!-- <div class="large-3 columns">
              &nbsp
              </div> -->
            <div class="large-push-3 large-8 column">
                <div class="counter-wrapper">
                    <p class="text-center count-products">{{ $products->total() }} products</p>

                    <div id="vsz_display_items">
                        <ul>
                            @php $paginateItems = collect([30, 60, 120, 240]) @endphp

                            @foreach($paginateItems as $item)
                                <li @if(!$paginateItems->contains(request()->get('items')) && $item == 30 || request()->get('items') == $item) class="active" @endif >
                                    <a href="{{ count(Request::except(['page', 'items'])) ?
                                                url()->current() . '?' . http_build_query(Request::except(['page', 'items'])) . '&items=' . $item :
                                                url()->current() . '?items=' . $item }}">
                                        {{ $item }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="large-1 columns">

            </div>

        </div>
        <div class="row small-collapse large-collapse site-wrapper">
            <aside class="small-6 large-3 column left-bar">
                <ul class="vertical menu" data-accordion-menu  id="example-menu">
                    @foreach($mainCategory->children->where('is_active', 1)->sortBy('name') as $category)
                        @if($category->getCountProducts())
                            <li @if($checked->contains($category->id) || $checked->intersect($category->children->pluck('id'))->count()) class="checked" @endif>
                                {!! Form::checkbox('categories[]', 1, $checked->contains($category->id), ['id' => $category->id, 'onclick' => 'window.location=' . "'" . urlByCategory($locale == 'et' ? $category->slug_et : $category->slug) . "'"]) !!}
                                <label for="{{ $category->id }}">
                                    <a class="text-sidebar" href="{{ urlByCategory($locale == 'et' ? $category->slug_et : $category->slug) }}">
                                        {{ $locale == 'et' ? $category->name_et : $category->name }}
                                    </a>
                                    <span>({{ $category->getCountProducts() }})</span>
                                </label>
                            </li>
                            @if(!$category->children->isEmpty())
                                <ul>
                                    @foreach($category->children->where('is_active', 1)->sortBy('name') as $child)
                                        @if($child->products->where('image', '!=', null)->where('is_active', 1)->count())
                                            <li>
                                                {!! Form::checkbox('categories[]', 1, $checked->contains($child->id), ['id' => $child->id, 'onclick' => 'window.location=' . "'" . urlByCategory($child->slug) . "'"]) !!}
                                                <label for="{{ $child->id }}">
                                                    <a class="text-sidebar" href="{{ urlByCategory($locale == 'et' ? $child->slug_et : $child->slug) }}">
                                                        {{ $locale == 'et' ? $child->name_et : $child->name }}
                                                    </a>
                                                    <span>({{ $child->products->where('image', '!=', null)->where('is_active', 1)->count() }})</span>
                                                </label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        @endif
                    @endforeach

                </ul>
            </aside>
            <div class="large-9 column">
                <!--page content start-->

                <section class="products list-item">

                    @foreach($products as $key => $product)
                        @php
                            $short_desc = $locale == 'et' ? $product->short_description_et : $product->short_description;
                            $slug = $locale == 'et' ? $product->slug_et : $product->slug
                        @endphp
                        <div class="row product-item">
                            <div class="product-icons @if(!$product->is_gluten_free || !$product->is_vegan) one-items @endif ">
                                @if($product->is_gluten_free)
                                    <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
                                @endif

                                @if($product->is_vegan)
                                    <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
                                @endif
                            </div>
                            <a class="top-name" href="{{ url($locale.'/'.($locale == 'et' ? 'toode' : 'product').'/'. $slug ) }}"><p class="top-name">{{ $locale == 'et' ? $product->name_et : $product->name }}</p></a>
                            <div class="clearfix"></div>

                            <div class="vsz_img_wrapper_list">
                                <a href="#" data-open="product{{ $product->id }}" class="item-modal"><img id="product-img-{{ $product->id }}" src="{{ $product->getImage() }}" alt="{{ $locale == 'et' ? $product->name_et : $product->name }}"></a>
                            </div>

                            <p class="bottom-name">{{ $locale == 'et' ? $product->name_et : $product->name }}</p>

                            <div class="vsz_description_list">

                                <a href="{{ url($locale.'/'.($locale == 'et' ? 'toode' : 'product').'/'. $slug ) }}">
                                    {!! $short_desc !!}
                                </a>
                            </div>
                            <div class="clearfix"></div>

                            <div class="vsz_more_inforamtion_list">
                                <div class="pieces-wrapper">
                                    <p>{!! $product->getAmount() !!}</p>
                                </div>
                                <p class="price">€{{ $product->getPrice() }}</p>
                                <div class="list_pieces-wrapper">
                                  <p>{!! $product->getAmount() !!}</span> </p>
                                  <p class="price">€{{ $product->getPrice() }}</p>
                                </div>
                                <div class="btn-wrapper">
                                  <span>{{ $product->getPricePerPiece() }} €</span>  <button @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @endif type="button" class="alert button buy_btn" data-product="{{ $product->id }}" data-page="list"   onclick="dataLayer.push({'event':'addToCart','ecommerce':{'currencyCode':'EUR','add':{'actionField':{'list':'Product Page'},'products':[{'name':'{{ $product->name }}','sku':'{{ $product->sku }}','price':'{{ $product->getPrice() }}','category':'{{  $mainCategory->slug }}','quantity':1}]}}});">{{ trans('static_text.buy') }}</button>
                                </div>
                            </div>
                        </div>

                        @include('products.item-modal')

                        @if($loop->iteration % 3 == 0)
                            <div class="line"></div>
                        @endif

                    @endforeach

                </section>

                {{ $products->appends(request()->except('page'))->links() }}

                <!--page content end-->

            </div>
        </div>

    @endsection

    @push('scripts')
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script>
          (function(){
           $('.hollow.button.float-right.list').on('click',function(e) {
              $('.products ').addClass('tile-list');
              $('.products ').removeClass('list-item');
          });

            $('.hollow.button.float-right.tile').on('click',function(e) {
              $('.products ').removeClass('tile-list');
              $('.products ').addClass('list-item');
          });
          })();
        </script>

        <script>
            $('a.item-modal').click(function(){
                $('#' + $(this).data('open')).foundation('open');
            });

            $('.reveal.xlarge').mouseleave(function() {
              $(this).foundation('close');
            });
        </script>
        <script>
            if ($('#google_translate_element').length){
                $('#close-translate').css('display', 'inline');
            };

            $('#close-translate').on('click', function(event){
                event.preventDefault();
                $('#google_translate_element').css('display', 'none');
            });
        </script>
    @endpush
