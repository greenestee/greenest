    @extends('layouts.app', ['page_type' => $page_type])

    @php
        $locale = App::getLocale();
        $imgplaceholder = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    @endphp

    @if(!empty($meta_title))
        @section('title')
            {{ $meta_title }}
        @endsection
    @endif

    @section('meta')
        <meta name="description" content="{{ $meta_description }}">
        <meta name="keywords" content="{{ $meta_keywords }}">
    @endsection

    @section('gtm-dataLayer')
        <script>
            {!! $dataLayer !!}
        </script>
    @endsection
    @section('google-tranlsate')
        @if($locale != 'et')
            <div id="google_translate_element" style="width: 13%;">
                <a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
            </div>
            <script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

            <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "item": {
                  "@id": "<?php echo url()->current() ?>",
                  "name": "category"
                }
              }]
            }
            </script>
        @endif
    @endsection


    @section('content')

        <?php
            $langPostfix = $locale == 'en' ? '' : '_et';
            $productInLang = $locale == 'et' ? 'toode' : 'product';

            $arr = array();
            $i = 0;
            foreach($products as $key => $product) {
                if($i < 3) {
                    $arr[$i]["@type"] = "ListItem";
                    $arr[$i]["position"] = $i+1;
                    $arr[$i]["url"] = url($locale . '/' . $productInLang . '/' . $product->{"slug{$langPostfix}"});
                    $i++;
                } else {
                    break;
                }
            }

            // echo json_encode($arr); die;
            $arr1["@context"] = 'http://schema.org';
            $arr1["@type"] = 'ListItem';
            $arr1["itemListElement"] = $arr;
            echo '<script type="application/ld+json">';

            echo str_replace('\/', '/', json_encode($arr1));
            
            echo "</script>";
        ?>
        
        {{--@if( $mainCategory->slug === 'food-and-drinks' )--}}
            @php
                $today = date("Y-m-d H:i:s");
                $close_date = "2021-05-31 06:00:00";
            @endphp
            @if($close_date > $today)
                <div class="announcement" id="announcement" style="display: none; animation-delay: 30s;">
                    <button type="button" class="announcement__close" onclick="document.getElementById('announcement').style.display = 'none'">Close</button>
                    {{-- <p class="announcement__text">@lang('static_text.announcement_close')</p> --}}
{{--                    <p class="announcement__text"><b>@lang('static_text.announcement_nb_prev')</b></p>--}}
{{--                    <p class="announcement__text">@lang('static_text.holiday_season_deliver')</p>--}}
                    <p class="announcement__title">@lang('static_text.announcement_title')</p>
                    <p class="announcement__text">@lang('static_text.announcement_first')</p>
                    <p class="announcement__text">@lang('static_text.announcement_second')</p>
                    <p class="announcement__text">@lang('static_text.announcement_third')</p>
                    <p class="announcement__text">@lang('static_text.announcement_close')</p>
                </div>
            @endif
            <script type="text/javascript">
                if (window && window.localStorage) {
                    var announcementLastDate = window.localStorage.getItem('announcementLastDate');
                    var todayDate = '{{ date("Y-m-d") }}';
                    if (!announcementLastDate || announcementLastDate !== todayDate) {
                        var announcement = document.getElementById('announcement');
                        if (announcement) {
                            announcement.style.display = null;
                        }
                        window.localStorage.setItem('announcementLastDate', todayDate);
                    }
                }
            </script>
        {{--@endif--}}
        <div class="row small-collapse ">
            <!-- <div class="large-3 columns">
              &nbsp
              </div> -->
            <div class="large-push-3 large-8 column">
                <div class="counter-wrapper">
                    <p class="text-center count-products">
                        @if($products->total() == 1)
                            {{ $products->total() }} {{ trans('static_text.1_product') }}
                        @else
                            {{ $products->total() }} {{ trans('static_text.multiple_products') }}
                        @endif
                    </p>

                    <div id="vsz_display_items">
                        <ul>
                            {{ trans('static_text.show') }}
                            @php $paginateItems = collect([30, 60, 120, 240]) @endphp

                            @foreach($paginateItems as $item)
                                <li @if((!$paginateItems->contains(request()->get('items')) && $item == 30) || request()->get('items') == $item) class="active" @endif >
                                    <a href="{{ count(Request::except(['page', 'items'])) ?
                                                url()->current() . '?' . http_build_query(Request::except(['page', 'items'])) . '&items=' . $item :
                                                url()->current() . '?items=' . $item }}">
                                        {{ $item }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="large-1 columns">

            </div>

        </div>
        <div class="row small-collapse large-collapse site-wrapper">
            <aside class="small-6 large-3 column left-bar">
                <ul class="vertical menu" data-accordion-menu  id="example-menu">
                    @foreach($mainCategory->children->where('is_active', 1)->sortBy("name" . $langPostfix) as $category)
                        @if($category->getCountProducts())
                            <li @if($checked->contains($category->id) || $checked->intersect($category->children->pluck('id'))->count()) class="checked" @endif>
                                {!! Form::checkbox('categories[]', 1, $checked->contains($category->id), ['id' => $category->id, 'onclick' => 'window.location=' . "'" . urlByCategory($category->{"slug{$langPostfix}"}) . "'"]) !!}
                                <label for="{{ $category->id }}" style="font-size: 0;">
                                    <a class="text-sidebar" href="{{ urlByCategory($category->{"slug{$langPostfix}"}) }}">
                                        {{ $category->{"name{$langPostfix}"} }}
                                    </a>
                                    <span>({{ $category->getCountProducts() }})</span>
                                </label>
                            </li>
                            @if(!$category->children->isEmpty())
                                <ul>
                                    @foreach($category->children->where('is_active', 1)->sortBy('name') as $child)
                                        @if($child->products->where('image', '!=', null)->where('is_active', 1)->count())
                                            <li>
                                                {!! Form::checkbox('categories[]', 1, $checked->contains($child->id), ['id' => $child->id, 'onclick' => 'window.location=' . "'" . urlByCategory($child->slug) . "'"]) !!}
                                                <label for="{{ $child->id }}" style="font-size: 0;">
                                                    <a class="text-sidebar" href="{{ urlByCategory($child->{"slug{$langPostfix}"}) }}">
                                                        {{ $child->{"name{$langPostfix}"} }}
                                                    </a>
                                                    <span>({{ $child->products->where('image', '!=', null)->where('is_active', 1)->count() }})</span>
                                                </label>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        @endif
                    @endforeach

                </ul>
            </aside>
            <div class="large-9 column">
                <!--page content start-->

                <section class="products list-item">

                    @foreach($products as $key => $product)
                        <div class="row product-item">

                            <div class="vsz_more_inforamtion_list">
                                <div class="pieces-wrapper">
                                    <p>{!! $product->getAmount() !!}</p>
                                </div>
                                <p class="price">€{{ $product->getPrice() }}</p>
                                <div class="list_pieces-wrapper">
                                  <p>{!! $product->getAmount() !!}</span> </p>
                                  <p class="price">€{{ $product->getPrice() }}</p>
                                </div>
                                <div class="btn-wrapper">
                                  <span>{{ $product->getPricePerPiece() }} €</span>  <button @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @endif type="button" class="alert button buy_btn" data-product="{{ $product->id }}" data-page="list"   onclick="dataLayer.push({'event':'addToCart','ecommerce':{'currencyCode':'EUR','add':{'actionField':{'list':'Product Page'},'products':[{'name':'{{ $product->name }}','sku':'{{ $product->sku }}','price':'{{ $product->getPrice() }}','category':'{{  $mainCategory->slug }}','quantity':1}]}}});">{{ trans('static_text.buy') }}</button>
                                </div>
                            </div>

                            <div class="product-icons @if(!$product->is_gluten_free || !$product->is_vegan) one-items @endif ">
                                @if($product->isRefrigerated())
                                    <img data-src="{{ asset('/img/snowflake.svg') }}" width="60" alt="" src="{{ $imgplaceholder }}" class="">
                                    <noscript>
                                        <img src="{{ asset('/img/snowflake.svg') }}" width="60" alt="" class="">
                                    </noscript>
                                @endif

                                @if($product->isFrozen())
                                    <img data-src="{{ asset('/img/snowflake-18.svg') }}" width="60" alt="" src="{{ $imgplaceholder }}" class="">
                                    <noscript>
                                        <img src="{{ asset('/img/snowflake-18.svg') }}" width="60" alt="" class="">
                                    </noscript>
                                @endif

                                @if($product->is_gluten_free)
                                    <img data-src="{{ asset('/img/gluten.png') }}" alt="" src="{{ $imgplaceholder }}" class="@if($product->is_vegan) gluten @else vegan @endif">
                                    <noscript>
                                        <img src="{{ asset('/img/gluten.png') }}" alt="" class="@if($product->is_vegan) gluten @else vegan @endif">
                                    </noscript>
                                @endif

                                @if($product->is_vegan)
                                    <img data-src="{{ asset('/img/vegan.png') }}" alt="" src="{{ $imgplaceholder }}" class="vegan">
                                    <noscript>
                                        <img src="{{ asset('/img/vegan.png') }}" alt="" class="vegan">
                                    </noscript>
                                @endif
                            </div>

                            <a class="top-name" href="{{ url($locale . '/' . $productInLang . '/' . $product->{"slug{$langPostfix}"}) }}"><p class="top-name product-title">{{ $product->{"name{$langPostfix}"} }}</p></a>

                            <div class="vsz_img_wrapper_list">
                                <a href="#" data-open="product" class="item-modal" data-info="{{ json_encode([
                                    'name' => $product->{"name{$langPostfix}"} ?: '','image' => $product->getImage(),
                                    'brand' => $product->{"brand{$langPostfix}"} ?: '',
                                    'is_gluten_free' => $product->is_gluten_free,
                                    'is_vegan' => $product->is_vegan,
                                    'status' => $product->status,
                                    'ingredients' => $product->{"ingredients{$langPostfix}"} ?: '',
                                    'nutritional_value' => $product->{"nutritional_value{$langPostfix}"} ?: '',
                                    'recommended_storage' => $product->{"recommended_storage{$langPostfix}"} ?: '',
                                    'allergenic_information' => $product->{"allergenic_information{$langPostfix}"} ?: '',
                                    'amount' => $product->getAmount(),
                                    'price' => $product->getPrice(),
                                    'buy_button' => "dataLayer.push({'event':'addToCart','ecommerce':{'currencyCode':'EUR','add':{'actionField':{'list':'Product Page'},'products':[{'name':'{$product->name}','sku':'{$product->sku}','price':'{$product->getPrice()}','category':'" . (isset($mainCategory->slug) ? $mainCategory->slug : "") . "','quantity':1}]} } });",
                                    'price_per_piece' => $product->getPricePerPiece(),
                                    'description' => $product->{"description{$langPostfix}"} ?: '',
                                    'method_of_preparation' => $product->{"method_of_preparation{$langPostfix}"} ?: '',
                                    'how_to_use' => $product->{"how_to_use{$langPostfix}"} ?: '',
                                    'origin_country_of_ingredients' => $product->{"origin_country_of_ingredients{$langPostfix}"} ?: '',
                                    'origin_country' => $product->{"origin_country{$langPostfix}"} ?: '',
                                    'shelf_life' => $product->{"shelf_life{$langPostfix}"} ?: '']) }}">

                                    <img id="product-img-{{ $product->id }}" data-src="{{ $product->getImage() }}" src="{{ $imgplaceholder }}" alt="{{ $product->{"name{$langPostfix}"} }}">
                                    <noscript>
                                        <img src="{{ $product->getImage() }}" alt="{{ $product->{"name{$langPostfix}"} }}">
                                    </noscript>
                                </a>
                            </div>

                            <p class="bottom-name">{{ $product->{"name{$langPostfix}"} }}</p>

                            <div class="vsz_description_list">

                                <a href="{{ url($locale . '/' . $productInLang . '/' . $product->{"slug{$langPostfix}"}) }}">
                                    {!! $product->getShortDescription() !!}
                                </a>
                            </div>

                        </div>

                        {{--@include('products.item-modal')--}}

                        @if($loop->iteration % 3 == 0)
                            <div class="line"></div>
                        @endif

                    @endforeach

                    <div class="reveal xlarge" id="product" data-reveal>
                        <img src="{{ asset('/img/gluten.png') }}" id="modal-is_gluten_free-view" alt="" class="">
                        <img src="{{ asset('/img/vegan.png') }}" id="modal-is_vegan-view" alt="" class="vegan">

                        <div class="row"><h3><span id="modal-name-value"></span></h3></div>
                        <div class="row">
                            <div class="large-6 small-12 columns" id="modal-image-view">
                                <div class="vsz_img_wrapper">
                                    <img src="" alt="" id="modal-image-value">
                                </div>
                            </div>
                            <div class="large-6 small-12 columns">
                                <div id="modal-brand-view">
                                    <h4>{{ trans('static_text.Brand') }}</h4>
                                    <span id="modal-brand-value"></span></br>
                                </div>
                                <div id="modal-ingredients-view">
                                    <h4>{{ trans('static_text.Ingredients') }}</h4>
                                    <span id="modal-ingredients-value"></span>
                                </div>
                                <div id="modal-nutritional_value-view">
                                    <h4>{{ trans('static_text.Nutritional_value_of_100_g') }}</h4>
                                    <span id="modal-nutritional_value-value"></span>
                                </div>
                                <div id="modal-recommended_storage-view">
                                    <h4>{{ trans('static_text.Recommended_storage') }}</h4>
                                    <span id="modal-recommended_storage-value"></span>
                                </div>
                                <div id="modal-allergenic_information-view">
                                    <h4>{{ trans('static_text.Allergenic_information') }}</h4>
                                    <span id="modal-allergenic_information-value"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 small-12 columns vsz_more_information">
                                    <p class="vsz_pieces"><span id="modal-amount-value"></span></p>
                                    <p class="price">€<span id="modal-price-value"></span></p>
                                    <div class="btn-wrapper">
                                        <button type="button" id="modal-buy_button-value" class="alert button buy_btn" data-product="" data-page="single" onclick="">{{ trans('static_text.buy') }}</button>
                                    </div>
                                    <p class="vsz_per_price"><span id="modal-price_per_piece-value"></span> €</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 small-12 columns">
                                    <div id="modal-description-view">
                                        <div class="inline-row">
                                            <h4>{{ trans('static_text.Description') }}</h4>
                                            <span id="modal-description-value"></span>
                                        </div>
                                    </div>
                                    <div id="modal-method_of_preparation-view">
                                        <div class="inline-row">
                                            <h4>{{ trans('static_text.Method_of_preparation') }}</h4>
                                            <span id="modal-method_of_preparation-value"></span>
                                        </div>
                                    </div>
                                    <div id="modal-how_to_use-view">
                                        <div class="inline-row">
                                            <h4>{{ trans('static_text.How_to_use') }}</h4>
                                            <span id="modal-how_to_use-value"></span>
                                        </div>
                                    </div>
                                    <div id="modal-origin_country-view">
                                        <div class="country-row">
                                            <h4>{{ trans('static_text.Country_of_origin') }}</h4>
                                            <span id="modal-origin_country-value"></span>
                                        </div>
                                    </div>
                                    <div id="modal-origin_country_of_ingredients-view">
                                        <div class="country-row">
                                            <h4>{{ trans('static_text.Country_of_origin_of_ingredients') }}</h4>
                                            <span id="modal-origin_country_of_ingredients-value"></span>
                                        </div>
                                    </div>
                                    <div id="modal-shelf_life-view">
                                        <div class="inline-row">
                                            <h4>{{ trans('static_text.Shelf_life') }}</h4>
                                            <span id="modal-shelf_life-value"></span> days
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&larr;</span>
                        </button>
                    </div>

                </section>

                {{ $products->appends(request()->except('page'))->links() }}

                <!--page content end-->

            </div>
        </div>

    @endsection

    @push('scripts')
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script>
          (function(){
           $('.hollow.button.float-right.list').on('click',function(e) {
              $('.products ').addClass('tile-list');
              $('.products ').removeClass('list-item');
          });

            $('.hollow.button.float-right.tile').on('click',function(e) {
              $('.products ').removeClass('tile-list');
              $('.products ').addClass('list-item');
          });
          })();
        </script>

        <script>
            $('a.item-modal').click(function(){
                hideModalElements();
                showElementsModal($(this).data('info'));
                console.log($(this).data('info'));
                $('#' + $(this).data('open')).foundation('open');
                $('#' + $(this).data('open')).find('img[data-src]').each(function(index, element) {
                    element.src = $(element).data('src');
                    $(element).removeAttr('data-src');
                });
            });

            $('.reveal.xlarge').mouseleave(function() {
              $(this).foundation('close');
            });

            function showElementsModal(data) {
                var viewElements = ['brand', 'ingredients', 'nutritional_value', 'recommended_storage',
                    'allergenic_information', 'description', 'method_of_preparation', 'how_to_use',
                    'origin_country_of_ingredients', 'shelf_life', 'origin_country', 'name', 'price', 'price_per_piece',
                    'amount'
                ];
                var elementValue, elementView;
                for (var i = 0; i < viewElements.length; i++) {
                    if (data[viewElements[i]].length) {
                        elementValue = document.getElementById('modal-' + viewElements[i] + '-value');
                        elementView = document.getElementById('modal-' + viewElements[i] + '-view');
                        elementValue.innerHTML = data[viewElements[i]];
                        if (elementView) {
                            elementView.style.display = '';
                        }
                    }
                }
                if (data['is_gluten_free']) {
                        elementView = document.getElementById('modal-is_gluten_free-view');
                        elementView.classList.remove(['vegan','gluten']);
                        elementView.classList.add([(data['is_vegan'] ? 'gluten' : 'vegan')]);
                        elementView.style.display = '';
                }
                if (data['is_vegan']) {
                    elementView = document.getElementById('modal-is_vegan-view');
                    elementView.style.display = '';
                }
                elementValue = document.getElementById('modal-buy_button-value');
                elementValue.disabled = data['status'] === 0;
                elementValue.onclick = data['buy_button'];
                if (data['image'].length) {
                    elementValue = document.getElementById('modal-image-value');
                    elementValue.src = data['image'];
                    elementValue.alt = data['name'];
                    elementView = document.getElementById('modal-image-view');
                    elementView.style.display = '';
                }
            }

            function hideModalElements() {
                var modalElements = [
                    'modal-is_gluten_free-view', 'modal-is_vegan-view', 'modal-image-view', 'modal-brand-view',
                    'modal-ingredients-view', 'modal-nutritional_value-view', 'modal-recommended_storage-view',
                    'modal-allergenic_information-view', 'modal-description-view', 'modal-method_of_preparation-view',
                    'modal-how_to_use-view', 'modal-origin_country_of_ingredients-view', 'modal-shelf_life-view',
                    'modal-origin_country-view'
                ];
                var element;
                for (var i = 0; i < modalElements.length; i++) {
                    element = document.getElementById(modalElements[i]);
                    element.style.display = 'none';
                }
            }
        </script>
        <script>
            if ($('#google_translate_element').length){
                $('#close-translate').css('display', 'inline');
            }

            $('#close-translate').on('click', function(event){
                event.preventDefault();
                $('#google_translate_element').css('display', 'none');
            });
        </script>
    @endpush
