@extends('voyager::master')

@section('page_title','All DPD Shipments')

@section('page_header')
    <h1 class="page-title">
        Coupons manage
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body table-responsive">
                        <form action="{{ route('admin.coupon.store') }}" method="post">
                            {{ csrf_field() }}
                            <h4>Add coupons</h4>
                            <label style="width: 10%; margin: 0 5px;">
                                Count coupons <br>
                                <input type="number" min="1" value="1" name="count" style="width: 60%;">
                            </label>
                            <label style="width: 15%; margin: 0 5px;">
                                Available count usages of coupons <br>
                                <input type="number" name="remaining_number_of_uses" style="width: 40%;" id="rnou_new" min="0" value="1">
                            </label>
                            <label style="width: 10%; margin: 0 5px;">
                                Unlimited count usages of coupons <br>
                                <input type="checkbox" name="unlimited"
                                       onchange="document.getElementById('rnou_new').disabled = this.checked;">
                            </label>
                            <label style="width: 8%; margin: 0 0 0 5px;">
                                Discount % <br>
                                <input type="number" min="1" max="100" style="width: 80%;" value="10"
                                       onchange="document.getElementById('sc_new').value = this.value / 100;">
                                <input type="number" min="0.01" max="1" step="0.01" name="sale_coefficient" id="sc_new" value="0.1"
                                       style="display: none;">
                            </label>

                            <button type="submit" class="btn btn-success">Create</button>
                        </form>
                        <br>
                        <hr>
                        <table id="dataTable" class="row table table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;">Coupon</th>
                                <th>Count uses</th>
                                <th>Sale %</th>
                                <th>Remain number of uses</th>
                                <th class="actions"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr>
                                    <td style="text-align: center;">
                                        {{ $coupon->code }}
                                    </td>
                                    <td>
                                        {{ $coupon->count_uses }}
                                    </td>
                                    <td>
                                        <input type="number"
                                               value="{{ $coupon->sale_coefficient * 100 }}"
                                               onchange="document.getElementById('sc{{ $coupon->id }}value').value = this.value / 100;"
                                               min="1" max="100"
                                        >
                                    </td>
                                    <td>
                                        <input type="number"
                                               value="{{ $coupon->remaining_number_of_uses }}"
                                               {{ !$coupon->unlimited ? '' : 'disabled' }}
                                               id="rnou{{ $coupon->id }}input"
                                               onchange="document.getElementById('rnou{{ $coupon->id }}value').value = this.value;"
                                               min="0"
                                            >
                                        <br>
                                        Unlimited <input type="checkbox"
                                                {{ !$coupon->unlimited ? '' : 'checked' }}
                                                onchange="
                                                        document.getElementById('rnou{{ $coupon->id }}input').disabled = this.checked;
                                                        document.getElementById('unlimited{{ $coupon->id }}value').checked = this.checked;
                                                    "
                                            >
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.coupon.update') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="number" name="remaining_number_of_uses"
                                                   value="{{ $coupon->remaining_number_of_uses }}"
                                                   id="rnou{{ $coupon->id }}value"
                                                   style="display: none;"
                                                >
                                            <input type="number" name="sale_coefficient"
                                                   value="{{ $coupon->sale_coefficient}}"
                                                   id="sc{{ $coupon->id }}value"
                                                   step="0.01" min="0.01" max="1"
                                                   style="display: none;"
                                                >
                                            <input type="checkbox" name="unlimited"
                                                    {{ !$coupon->unlimited ? '' : 'checked' }}
                                                    style="display: none;"
                                                   id="unlimited{{ $coupon->id }}value"
                                                >
                                            <input type="hidden" name="id" value="{{ $coupon->id }}">
                                            <button type="submit" class="btn btn-sm btn-warning">Update</button>
                                        </form>
                                        <form action="{{ route('admin.coupon.delete') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $coupon->id }}">
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ config('voyager.assets_path') }}/lib/css/responsive.dataTables.min.css">
@stop

@section('javascript')
    <!-- DataTables -->
    <script src="{{ config('voyager.assets_path') }}/lib/js/dataTables.responsive.min.js"></script>
    <script>
        function viewChange() {
            var act, sh, table, reg;
            act = document.getElementById('checkbox-active-order').checked;
            sh = document.getElementById('checkbox-shipped-order').checked;
            reg = '(';
            if (act && sh) {
                reg = reg + 'a|s';
            } else {
                reg = reg + (sh ? 's' : (act ? 'a' : ''));
            }
            reg = reg + ')';
            table = $('#dataTable').DataTable();
            table.column(0).search(reg, true, false).draw();
        }
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                "order": []
                @if(config('dashboard.data_tables.responsive')), responsive: true @endif
            });
        });

        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });
    </script>
@stop
