@php
    if (App::getLocale() == 'et') {
        $title = $article->title_et;
        $text = $article->text_et;
    } else {
        $title = $article->title;
        $text = $article->text;
    }
@endphp

<div class="reveal xlarge" id="product{{ $article->id }}" data-reveal>


    <div class="row">
        <h3>{{ $title }}</h3>
    </div>

    <div class="row">

    </div>

</div>