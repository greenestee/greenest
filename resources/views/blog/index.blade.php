@extends('layouts.app')

@php
    $locale = App::getLocale();
@endphp

@section('title')
     - Blog
@endsection

@section('meta')
    <meta name="description" content="blog">
    <meta name="keywords" content="greenest, blog">
@endsection

@section('google-tranlsate')
    @if($locale != 'et')
        <div id="google_translate_element" style="width: 13%;">
            <a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
        </div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "item": {
                  "@id": <?php echo url()->current() ?>,
                  "name": "category"
                }
              }]
            }
            </script>
    @endif
@endsection


@section('content')

    <?php

    $arr = array();

    $i = 0;
    foreach($articles as $key => $article)
    {
        if($i < 3)
        {
            $slug = $locale == 'et' ? $article->slug_et : $article->slug;

            $arr[$i]["@type"] = "ListItem";
            $arr[$i]["position"] = $i+1;
            $arr[$i]["url"] = url($locale.'/'.($locale == 'et' ? 'artikkle' : 'article').'/'. $slug );
            $i++;
        }
        else
        {
            break;
        }
    }

    // echo json_encode($arr); die;
    $arr1["@context"] = 'http://schema.org';
    $arr1["@type"] = 'ListItem';
    $arr1["itemListElement"] = $arr;
    echo '<script type="application/ld+json">';

    echo str_replace('\/', '/', json_encode($arr1));

    echo "</script>";
    ?>
    <div class="row small-collapse ">
        <!-- <div class="large-3 columns">
          &nbsp
          </div> -->
        <div class="large-push-3 large-8 column">
            <div class="counter-wrapper">
                <p class="text-center count-products">@if($articles->total() == 1) {{ $articles->total() }} {{ trans('static_text.1_article') }} @else {{ $articles->total() }} {{ trans('static_text.multiple_articles') }} @endif</p>

                <div id="vsz_display_items">
                    <ul>
                        {{ trans('static_text.show') }}
                        @php $paginateItems = collect([30, 60, 120, 240]) @endphp

                        @foreach($paginateItems as $item)
                            <li @if(!$paginateItems->contains(request()->get('items')) && $item == 30 || request()->get('items') == $item) class="active" @endif >
                                <a href="{{ count(Request::except(['page', 'items'])) ?
                                                url()->current() . '?' . http_build_query(Request::except(['page', 'items'])) . '&items=' . $item :
                                                url()->current() . '?items=' . $item }}">
                                    {{ $item }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="large-1 columns">

        </div>

    </div>
    <div class="row small-collapse large-collapse site-wrapper">
        <aside class="small-6 large-3 column left-bar">
            <ul class="vertical menu" data-accordion-menu  id="example-menu">
                @foreach($categories as $category)
                    @if($category->getCountArticles())
                        <li>
                            @if(isset($checked))
                                {!! Form::checkbox('categories[]', 1, $checked->contains($category->id), ['id' => $category->id, 'onclick' => 'window.location=' . "'" . urlByCategory($locale == 'et' ? $category->slug_et : $category->slug) . "'"]) !!}
                            @endif
                            <label for="{{ $category->id }}" style="font-size: 0;">
                                <a class="text-sidebar" href="{{ urlByCategory($locale == 'et' ? $category->slug_et : $category->slug) }}">
                                    {{ $locale == 'et' ? $category->name_et : $category->name }}
                                </a>
                                <span>({{ $category->getCountArticles() }})</span>
                            </label>
                        </li>
                    @endif
                @endforeach

            </ul>
        </aside>
        <div class="large-9 column">
            <!--page content start-->

            <section class="products list-item">

                @foreach($articles as $key => $article)
                    @php
                    $slug = $locale == 'et' ? $article->slug_et : $article->slug;
                    $link = url($locale.'/'.($locale == 'et' ? "blogi/{$article->blogCategory->slug_et}" : "blog/{$article->blogCategory->slug}") .'/'. $slug );
                    @endphp
                    <div class="row product-item">
                        <a class="top-name" href="{{ $link }}"><p class="top-name">{{ $locale == 'et' ? $article->title_et : $article->title }}</p></a>
                        <div class="clearfix"></div>

                        <div style="display: flex; flex-direction: row;">

                            <div class="vsz_img_wrapper_list">
                                <img id="product-img-{{ $article->id }}" src="{{ $article->getImage() }}" alt="{{ $locale == 'et' ? $article->title_et : $article->title }}">
                            </div>

                            <div class="vsz_description_list" style="margin-right: 30px; font-size: 0.9em; align-items: stretch;">
                                <a href="{{ $link }}" style=" color: black;">
                                    <p>{!! $locale == 'et' ? $article->text_et : $article->text !!}</p>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>

{{--                    @include('blog.item-modal')--}}

                    @if($loop->iteration % 3 == 0)
                        <div class="line"></div>
                    @endif

                @endforeach

            </section>

        {{ $articles->appends(request()->except('page'))->links() }}

        <!--page content end-->

        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        (function(){
            $('.hollow.button.float-right.list').on('click',function(e) {
                $('.products ').addClass('tile-list');
                $('.products ').removeClass('list-item');
            });

            $('.hollow.button.float-right.tile').on('click',function(e) {
                $('.products ').removeClass('tile-list');
                $('.products ').addClass('list-item');
            });
        })();
    </script>

    <script>
        $('a.item-modal').click(function(){
            $('#' + $(this).data('open')).foundation('open');
        });

        $('.reveal.xlarge').mouseleave(function() {
            $(this).foundation('close');
        });
    </script>
    <script>
        if ($('#google_translate_element').length){
            $('#close-translate').css('display', 'inline');
        };

        $('#close-translate').on('click', function(event){
            event.preventDefault();
            $('#google_translate_element').css('display', 'none');
        });
    </script>
@endpush
