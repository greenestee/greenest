@extends('layouts.app')

@php
    $locale = App::getLocale();
@endphp
@if(!empty($article))
    @if(!empty($meta_title))
@section('title') - {{ $meta_title }}@endsection
@endif

@if(!empty($ameta_description) || !empty($meta_keywords))
@section('meta')
    @if(!empty($meta_description))
        <meta name="description" content="{{ $meta_description }}">
        <meta property="og:description" content="{{ $meta_description }}">
    @endif

    @if(!empty($meta_keywords))
        <meta name="keywords" content="{{ $meta_keywords }}">
    @endif
    <meta property="og:title" content="{{ $meta_title }}">
    <meta property="og:image" content="{{ $article->getImage() }}">
    <meta property="og:image:url" content="{{ $article->getImage() }}">
    <meta property="og:image:secure_url" content="{{ $article->getImage() }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:site_name" content="Greenest">
    <meta property="og:locale" content="et_EE">
@endsection
@endif
@endif

@section('google-tranlsate')
    @if($locale != 'et')
        <div id="google_translate_element" style="width: 13%;">
            <a id="close-translate" href="" title="" style="position: absolute; right: 0; display: none; top: 10px; color: #F40C1A;">X</a>
        </div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'da,de,el,es,et,fi,fr,lt,lv,no,pl,ru,sv', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
            }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "item": {
              "@id": <?php echo url()->current() ?>,
              "name": "category"
            }
          }]
        }
        </script>
    @endif
@endsection

@section('content')
    @php
        if (App::getLocale() == 'et') {
            $title = $article->title_et;
            $text = $article->text_et;
            $image = $article->image;
            $created_at = $article->created_at;
            $video_link = $article->video_link;
        } else {
            $title = $article->title;
            $text = $article->text;
            $image = $article->image;
            $created_at = $article->created_at;
            $video_link = $article->video_link;
        }
    @endphp
    <div class="row">
        <div class="pp1 large-12 column" style="width: 100%;">
            <div class="row"><h3>{{ $title }}</h3></div>
            <div class="row">
                <div class="large-6 small-12 columns">
                    @if($article->getImage() != '')
                    <div class="vsz_img_wrapper">
                        <img id="article-img-{{ $article->id }}" src="{{ $article->getImage() }}" alt="{{ $title }}" style="height: auto;">
                    </div>
                    @endif
                    @if($article->video_link)
                        <div class="vsz_img_wrapper" style="border: none;">
                            <iframe width="420" height="315" src="{{$article->video_link}}" frameborder="0" allowfullscreen></iframe>
                        </div>
                    @endif
                    @if(isset($products))
                        @foreach($products as $product)
                            @if($product->is_active == 1)
                                <div class="vsz_img_wrapper" style="display: flex; flex-direction: row; align-items: center; justify-content: space-between;">
                                    <img id="product-img-{{$product->id}}" src="{{ $product->getImage() }}" @if(isset($product->name)) alt="{{ $product->name }}" @endif style="height: auto; max-height: 100px;" >
                                    <div onclick="window.open('{{ url($locale.'/'.($locale == 'et' ? "toode/{$product->slug}" : "product/{$product->slug}")) }}', '_blank')" style="cursor: pointer;">{{ $product->name }}</div>
                                    <div>
                                    <p class="price">€{{ $product->getPrice() }}</p>
                                    <div class="btn-wrapper">
                                        <button type="button" @if($product->status == \App\Product::PRODUCT_NOT_IN_STOCK) disabled @else data-product="{{$product->id}}" @endif class="alert button buy_btn">{{ trans('static_text.buy') }}</button>
                                    </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        @if($products->count() > 1)
                            <div class="btn-wrapper" style="text-align: center; margin-top: 10px;">
                                <button type="button" class="alert button buy_all_btn">{{ trans('static_text.buy_all_these_products') }}</button>
                            </div>
                        @endif
                    @endif
                </div>
                @if(isset($text))
                    <div class="inline-row">
                        <p>{!! $text !!}</p>
                    </div>
                @endif
                <br>
                <div class="row">
                    <div class="large-12 small-12 columns">
                        @if(isset($created_at))
                            <div class="inline-row" style="text-align: right">
                                {!! $created_at !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        (function(){
            $('.hollow.button.float-right.list').on('click',function(e) {
                $('.products ').addClass('tile-list');
                $('.products ').removeClass('list-item');
            });

            $('.hollow.button.float-right.tile').on('click',function(e) {
                $('.products ').removeClass('tile-list');
                $('.products ').addClass('list-item');
            });
        })();
    </script>

    <script>
        $('a.item-modal').click(function(){
            $('#' + $(this).data('open')).foundation('open');
        });

        $('.reveal.xlarge').mouseleave(function() {
            $(this).foundation('close');
        });
    </script>
    <script>
        if ($('#google_translate_element').length){
            $('#close-translate').css('display', 'inline');
        };

        $('#close-translate').on('click', function(event){
            event.preventDefault();
            $('#google_translate_element').css('display', 'none');
        });
    </script>
@endpush
