<h4>New order</h4>

<h5>Billing address</h5>
<ul>
    <li> <b>Country</b> : {{ $order['billing_country'] }} </li>
    <li> <b>State provice</b> : {{ $order['billing_state'] }} </li>
    <li> <b>City</b> : {{ $order['billing_city'] }} </li>
    <li> <b>Zip/Postat code</b> :{{ $order['billing_zip_code'] }} </li>
    <li> <b>Address</b> : {{ $order['billing_address'] }} </li>
    <li> <b>First and Last Name</b> : {{ $order['name'] }} </li>
    <li> <b>Email</b> : {{ $order['email'] }} </li>
    <li> <b>Phone</b> : {{ $order['phone'] }} </li>
    <li> <b>Tax/VAT Number</b> : {{ $order['vat_number'] }} </li>
    <li> <b>Company</b> : {{ $order['company_name'] }} </li>
    <li> <b>Registration Number</b> : {{ $order['company_reg_number'] }} </li>
</ul>

<h5>Shipping address</h5>
<ul>
    <li> <b>Country</b> : {{ $order['shipping_country'] }} </li>
    <li> <b>State provice</b> : {{ $order['shipping_state'] }} </li>
    <li> <b>City</b> : {{ $order['shipping_city'] }} </li>
    <li> <b>Zip/Postat code</b> : {{ $order['shipping_zip_code'] }} </li>
    <li> <b>Address</b> : {{ $order['shipping_address'] }} </li>
</ul>

<h5>Other</h5>
<ul>
    <li> <b>How did you hear about us?</b> : {{ $order['hear_about_us'] }} </li>
    <li> <b>Comments</b> : {{ $order['comment'] }} </li>
</ul>