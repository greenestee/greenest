<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greenest - Invoice</title>
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
</head>

<body style="box-sizing: inherit;margin: 0;padding: 0;background: #fefefe;font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-weight: normal;line-height: 1.5;color: #0a0a0a;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;">

    <section class="header row" style="box-sizing: inherit;display: block;margin-right: auto;margin-left: auto;box-shadow: 0 0 10px rgba(0,0,0,0.5);">
        <div class="row bitmap" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: 0;margin-left: 0;background-image: url({{ asset('/assets/img/bitmap.jpg') }});background-repeat: no-repeat;background-size: cover;box-shadow: 0 0 10px rgba(0, 0, 0, 0.35);border-radius: 0.3125rem 0.3125rem 0 0;">
            <div class="large-12 text-center headWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 100%;text-align: center;">
                <h2 style="box-sizing: inherit;margin: 0;padding: 0;font-family: androgyne;font-style: normal;font-weight: normal;color: #fff;text-rendering: optimizeLegibility;font-size: 2.5rem;line-height: 1.4;margin-top: 10px;margin-bottom: 0;orphans: 3;widows: 3;page-break-after: avoid;">Nature Design OÜ</h2>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 1rem;font-size: inherit;line-height: 1.6;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-family: 'Droid Sans', sans-serif;color: #fff;">Organic & Biodegradable products</p>
            </div>
        </div>
    </section>

    <h3>Dear @if(isset($order->name)) {{ $order->name }}! @else client! @endif</h3>

    <p>We are very sorry to inform you that product
        @foreach($orderItems as $orderItem)
            {{$orderItem->name}} {{$orderItem->price}} €,
        @endforeach
        is missing from your order. We would like to know what you want us to do about it. Do you wish:</p>
    <form action="{{route('client.sendAnswerOnMissingProduct')}}" method="post">
        {{csrf_token()}}
        <input type="hidden" name="orderId" value="{{$order->id}}">
        <input type="hidden" name="productIds" value="{{$productsIds}}">
        <input type="radio" name="choice" id="choice1" value="refund">
        <label for="choice1">Full refund for the missing product</label>
        <br>
        <input type="radio" name="choice" id="choice2" value="includeInNextOrder">
        <label for="choice2">I’ll include it to my next order with an extra gift</label>
        <br>
        <button type="submit">Send Answer</button>
    </form>

@if(!isset($for_admin))
    <p>With kind regards,</p>
    <p>greenest.ee family</p>
    <p>+372 51 900 330</p>
@endif

</body>
</html>