<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greenest - @lang('invoice.invoice')</title>
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
  </head>

    <body style="box-sizing: inherit;margin: 0;padding: 0;background: #fefefe;font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;font-weight: normal;line-height: 1.5;color: #0a0a0a;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;">

    @if(isset($for_admin))
        @include('emails.admin-info')
    @else
        <h3>@lang('invoice.dear') @if($order['name']) {{ $order['name'] }}! @else @lang('invoice.client')! @endif</h3>
        <h3>{{ $text }}</h3>
    @endif

    <section class="header row" style="box-sizing: inherit;display: block;margin-right: auto;margin-left: auto;box-shadow: 0 0 10px rgba(0,0,0,0.5);">
        <div class="row bitmap" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: 0;margin-left: 0;background-image: url({{ asset('/assets/img/bitmap.jpg') }});background-repeat: no-repeat;background-size: cover;box-shadow: 0 0 10px rgba(0, 0, 0, 0.35);border-radius: 0.3125rem 0.3125rem 0 0;">
            <div class="large-12 text-center headWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 100%;text-align: center;">
                <h2 style="background: center -3px / contain no-repeat url('{{ App::getLocale() == 'et' ? asset('/Greenest-logo-et.png') : asset('/Greenest-logo.png') }}'); height: 85px;box-sizing: inherit;margin: 0;padding: 0;margin-top: 10px;margin-bottom: 0;orphans: 3;widows: 3;page-break-after: avoid;"></h2>
            </div>
        </div>
    </section>
    
    <section class="clientBio row" style="box-sizing: inherit;display: block;margin-right: auto;margin-left: auto;margin-top: 0;padding-top: 3.4375rem;box-shadow: 0 0 10px rgba(0,0,0,0.5);margin-bottom: 150px;">
        <div class="row" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: -0.625rem;margin-left: -0.625rem;">
            <div class="large-5 large-offset-1 column playerWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 41.66667%;float: left;padding-right: 0.625rem;padding-left: 0.625rem;margin-left: 8.33333%;padding-top: 1.5rem;">
                <h5 style="box-sizing: inherit;margin: 0;padding: 0;font-family: proximanovabold;font-style: normal;font-weight: 700;color: inherit;text-rendering: optimizeLegibility;font-size: 1.5rem;line-height: 1.5rem;margin-top: 0;margin-bottom: 0.5rem;">@lang('invoice.payer'): @if($order['company_name']) {{ $order['company_name'] }} @else {{ $order['name'] }} @endif</h5>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">@lang('invoice.mail'): {{ $order['email'] }}</p>
                @if($order['shipping_method'] == 'parcel_terminal')
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">{{ $order['parcel_terminal']['provider'] }}, {{ $order['parcel_terminal']['name'] }}</p>
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">@lang('invoice.phone'): {{ $order['shipping_parcel_terminal_phone'] }}</p>
                @else
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">{{ $order['billing_address'] }}</p>
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">@if($order['company_reg_number']) @lang('invoice.reg-nr'): {{ $order['company_reg_number'] }}; @endif @if($order['vat_number']) @lang('invoice.vat'): {{ $order['vat_number'] }} @endif</p>
                    <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovalight;">@lang('invoice.phone'): {{ $order['phone'] }}</p>
                @endif
            </div>
            <div class="large-5 end column text-center dataWrapper" style="box-sizing: inherit;margin: 0;padding: 0;width: 41.66667%;float: left;padding-right: 0.625rem;padding-left: 0.625rem;text-align: center;background-color: rgba(243, 243, 243, 0.78);border-radius: 0.3125rem 0.25rem 0.25rem 0.3125rem;padding-top: 1.875rem;padding-bottom: 1.375rem;">
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovabold;"><span style="box-sizing: inherit;font-weight: 700;font-family: proximanovalight;">@lang('invoice.date'):</span> {{ date('d.m.Y', strtotime($order['created_at'])) }}</p>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovabold;"><span style="box-sizing: inherit;font-weight: 700;font-family: proximanovalight;">@lang('invoice.invoice-number'):</span> {{ $order['id'] }}</p>
                <p style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;font-weight: 300;font-family: proximanovabold;"><span style="box-sizing: inherit;font-weight: 700;font-family: proximanovalight;">@lang('invoice.ref-number'):</span> {{ \App\Http\Libraries\MeritAktiva\MeritAktiva::getRefNo($order['id']) }} </p>
                @if(isset($status))
                    <p class="redText">@lang('invoice.status'):
                        {{ $status }}
                    </p>
                @elseif( ! isset($status) || $status != 'Paid')
                    <p class="redText" style="box-sizing: inherit;margin: 0;padding: 0;margin-bottom: 0;font-size: 1.125rem;line-height: 1.5rem;text-rendering: optimizeLegibility;orphans: 3;widows: 3;color: #c20a0a;font-weight: 300;font-family: proximanovabold;">@lang('invoice.pay-date'):
                        @if($order['company_name'])
                            {{ date('d.m.Y', strtotime(date('d.m.Y', strtotime($order['created_at'])) . '+7 days')) }}
                        @else
                            {{ date('d.m.Y', strtotime(date('d.m.Y', strtotime($order['created_at'])) . '+3 days')) }}
                        @endif
                    </p>
                @endif

            </div>
        </div>
        <div class="row productInfo" style="box-sizing: inherit;margin: 0;padding: 0;max-width: none;margin-right: -0.625rem;margin-left: -0.625rem;margin-top: 3.625rem;">
            <div class="large-offset-1 large-10" style="box-sizing: inherit;margin: 0;padding: 0;">
                <table class="unstriped" style="box-sizing: inherit;width: 100%;margin-bottom: 150px;border-radius: 0;border-top: 0.4375rem solid #0a0a0a;border-spacing: 0;font-size: 1.125rem;">
                    <thead style="box-sizing: inherit;display: table-header-group;border: 0.125rem solid #f1f1f1;background-color: #fefefe;background: #f8f8f8;color: #0a0a0a;">
                    <tr style="box-sizing: inherit;page-break-inside: avoid;background: transparent;">
                        <th class="tableTitle" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: left;background-color: #ebebeb;">@lang('invoice.product-name')</th>
                        <th class="text-center tableTitle" width="200" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: center;background-color: #ebebeb;">@lang('invoice.quantity')</th>
                        <th class="text-center tableTitle" width="150" style="box-sizing: inherit;margin: 0;padding: 1.25rem;font-family: proximanovabold;font-weight: bold;text-align: center;background-color: #ebebeb;">@lang('invoice.net-price')</th>
    
                    </tr>
                    </thead>
                    <tbody style="box-sizing: inherit;border: 1px solid #f1f1f1;background-color: #fefefe;">
                    @foreach($order['items'] as $item)
                    <tr style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;">{{ $item['name'] }} @if(isset($for_admin) && $item['product']) <br> <a href="{{ $item['product']['product_url'] }}">{{ $item['product']['product_url'] }}</a>  @endif</td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">{{ $item['quantity'] }}</td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;">{{ str_replace('.', ',', $item['price']) }}</td>
                    </tr>
                    @endforeach
                     <tr class="total" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;">&nbsp;</td>
                        <td class="text-right" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;text-align: right;">@lang('invoice.sum'): <br style="box-sizing: inherit;">@lang('invoice.vat') 20%<br style="box-sizing: inherit;"></td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;text-align: center;">{{ str_replace('.', ',', $order['subtotal']) }}<br style="box-sizing: inherit;">{{ str_replace('.', ',', $order['tax']) }}<br style="box-sizing: inherit;"></td>
                    </tr>
                     <tr style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;margin-bottom: 15px;color: #c51919;">&nbsp;</td>
                        <td class="text-right" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: right;border: none;margin-bottom: 15px;color: #c51919;background: #ebebeb;"><span style="box-sizing: inherit;">@lang('invoice.total-sum'):</span></td>
                        <td class="text-center" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovabold;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;text-align: center;border: none;margin-bottom: 15px;color: #c51919;background: #ebebeb;"><span style="box-sizing: inherit;">{{ str_replace('.', ',', $order['sum_total']) }} €</span></td>
                    </tr>
                    <tr class="address" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;"> 
                            <span style="box-sizing: inherit;">Kurekivi tee 2, Rae vald</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">Lehmja, Harjumaa 75306</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">LHV Pank: <span style="box-sizing: inherit;font-family: proximanovabold;">EE527700771005774026</span></span>
                        </td>
                        <td colspan="2" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;border: none;">     <span style="box-sizing: inherit;">@lang('invoice.shop-name')</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">Registrikood: 12802934</span><br style="box-sizing: inherit;">
                            <span style="box-sizing: inherit;">EE101777465</span>
                        </td>
                       
                    </tr>
                    <tr class="contact" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;padding-left: 1.875rem;border: none;"> 
                            <img src="{{ asset('/assets/img/shape-3.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin-right: 10px;margin-top: -5px;max-width: 100% !important;"><a href="mailto:sales@greenest.ee" style="box-sizing: inherit;background-color: transparent;-webkit-text-decoration-skip: objects;line-height: inherit;color: #1779ba;text-decoration: underline;cursor: pointer;">sales@greenest.ee</a>
                        </td>
                        <td colspan="2" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: 0.0625rem solid #c4c4c4;border: none;">  <img src="{{ asset('/assets/img/layer-37.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin-right: 10px;margin-top: -5px;max-width: 100% !important;">   <span style="box-sizing: inherit;">+372 51 900 330</span>
                          
                        </td>
                       
                    </tr>
                     <tr class="payment" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" colspan="3" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;"> 
                            <span style="box-sizing: inherit;">@lang('invoice.payment-individual'):</span>
                            <a href="https://www.swedbank.ee/private"><img src="{{ asset('/assets/img/swedbank.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.seb.ee/ip/ipank"><img src="{{ asset('/assets/img/2.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et"><img src="{{ asset('/assets/img/3.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.lhv.ee"><img src="{{ asset('/assets/img/4.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                        </td>
                       
                    </tr>
                    <tr class="payment" style="box-sizing: inherit;page-break-inside: avoid;border-bottom: 1px solid #f1f1f1;background-color: #fefefe;">
                        <td class="nest" colspan="3" style="box-sizing: inherit;margin: 0;padding: 0.5rem 0.625rem 0.625rem;font-family: proximanovaregular;padding-top: 1rem;padding-bottom: 1rem;border-bottom: none;padding-left: 1.875rem;"> 
                            <span style="box-sizing: inherit;">@lang('invoice.payment-businesses'):</span>
                            <a href="https://business.swedbank.ee"><img src="{{ asset('/assets/img/swedbank.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.seb.ee/cgi-bin/unet3.sh/ufirma.w"><img src="{{ asset('/assets/img/2.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://netbank.nordea.com/pnb/login.do?ts=EE&language=et"><img src="{{ asset('/assets/img/3.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                            <a href="https://www.lhv.ee"><img src="{{ asset('/assets/img/4.png') }}" style="box-sizing: inherit;border-style: none;display: inline-block;vertical-align: middle;height: auto;-ms-interpolation-mode: bicubic;page-break-inside: avoid;margin: 15px;min-width: 110px;max-width: 100% !important;"></a>
                        </td>
                       
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </section>

    @if(!isset($for_admin))
        <h3>@lang('invoice.thy')!</h3>
        <h3>greenest.ee @lang('invoice.team')</h3>
    @endif

  </body>
</html>