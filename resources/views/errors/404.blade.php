@extends('layouts.app')

@section('title', ' - Page not found')

@section('content')

	<div class="row text-center">
		<h1>404 - Page not found</h1>
	</div>

@endsection
