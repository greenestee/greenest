FROM php:7.2-fpm

RUN set -ex; \
    \
    pecl install xdebug-2.6.0 ; \
    docker-php-ext-enable xdebug ; \
    \
    apt-get update; \
    apt update; \
    apt install -y git; \
    apt-get install -y --no-install-recommends \
        libbz2-dev \
        libfreetype6-dev \
        curl \
        libxml++2.6-dev \
        libjpeg-dev \
        libpng-dev \
        libwebp-dev \
        libxpm-dev \
        libzip-dev \
    ; \
    \
#    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-xpm; \
    docker-php-ext-install -j "$(nproc)" \
#        bz2 \
        pdo_mysql \
#        bcmath \
#        gd \
#        soap \
#        sockets \
        mysqli \
        opcache \
        zip

# set recommended PHP.ini settings
RUN set -ex; \
    \
    { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
    } > $PHP_INI_DIR/conf.d/opcache-recommended.ini; \
    \
    { \
        echo 'session.cookie_httponly = 1'; \
        echo 'session.use_strict_mode = 1'; \
    } > $PHP_INI_DIR/conf.d/session-strict.ini; \
    \
    { \
        echo 'allow_url_fopen = On'; \
        echo 'max_execution_time = 600'; \
        echo 'memory_limit = 512M'; \
    } > $PHP_INI_DIR/conf.d/php-fpm-misc.ini
