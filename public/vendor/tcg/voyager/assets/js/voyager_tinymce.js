function setImageValue(url){
  $('.mce-btn.mce-open').parent().find('.mce-textbox').val(url);
}

$(document).ready(function(){

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  tinymce.init({
    menubar: false,
    height : "300",
    selector:'textarea.richTextBox',
    skin: 'voyager',
    plugins: 'link, lists, image, code, youtube, giphy, table, textcolor',
    extended_valid_elements : 'input[onclick|value|style|type]',
    file_browser_callback: function(field_name, url, type, win) {
            if(type =='image'){
              $('#upload_file').trigger('click');
            }
        },
    toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code greengb graybg',
    convert_urls: false,
    image_caption: true,
    image_title: true,
    setup: function (editor) {
            editor.addButton('greengb', {
                text: 'Green bgColor',
                onclick: function () {
                    editor.execCommand('mceInsertContent', false, '<div class="green-bg text-wrapper"><p>' + editor.selection.getContent() + '</p></div>');
                }
            });
            editor.addButton('graybg', {
                text: 'Gray bgColor',
                onclick: function () {
                    editor.execCommand('mceInsertContent', false, '<div class="gray-bg text-wrapper"><p>' + editor.selection.getContent() + '</p></div>');
                }
            });
        }
  });

});
