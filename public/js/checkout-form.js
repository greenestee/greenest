$(function(){

//Shipping Method
var $shippingMethod = $('input[name="shipping_method"]:checked').val();

var $telCode = $('#tel-code').val();
var $telephone = $('#telephone').val();
var $parcelTeminal = $('#select-parcel').val();

//Billing adress
//first container
var $billingAdress = $('#select2-address-container').text();

var $postCode = $('.billing-address').find('input[name="billing_zip_code"]').val();
var $province = $('.billing-address').find('input[name="billing_state"]').val();
var $city = $('.billing-address').find('input[name="billing_city"]').val();    
var $country = $('.billing-address').find('input[name="billing_country"]').val();    

//second container
var $company = $('.billing-address').find('input[name="company_name"]').val();   
var $firstLastName = $('#name').val(); 
var $phone = $('#phone').val();
var $regNumber = $('.billing-address').find('input[name="company_reg_number"]').val(); 
var $email = $('#email').val();
var $vatNumber = $('.billing-address').find('input[name="vat_number"]').val();
var $billingAdrCheckbox = $('.billing-address').find('.checkbox').prop('checked');


//shipping address
var $shippingAddress = $('.shipping-address').find('input[name="shipping_address"]').val();   
var $shippingPostCode = $('.shipping-address').find('input[name="shipping_zip_code"]').val();
var $shippingCity = $('.shipping-address').find('input[name="shipping_city"]').val();
var $shippingState = $('.shipping-address').find('input[name="shipping_state"]').val();
var $shippingCountry = $('.shipping-address').find('input[name="shipping_country"]').val();

//Payment method
var $paymentMethod = $('input[name="payment_method"]:checked').val();


//comments
 var comment = $('textarea[name="comment"]').val();

//hear About Us
 var hearAboutUs = $('select[name="hear_about_us"]').val();

 //footer checkboxes
 var $term = $('input[name="terms"]').prop('checked');
 var $singIn = $('#sign_up').prop('checked');
});