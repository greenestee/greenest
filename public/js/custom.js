$(document).ready(function () {

    $.ajaxSetup({

        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}

    });

    $('.vsz-catalogue-toggle a').click(function () {
        var catalogueMenu = $('ul#example-menu');
        catalogueMenu.toggle();
        if (catalogueMenu.css('display') === 'none') {
            catalogueMenu.css('display', '');
        }
    });


    if (($("#vsz_display_items").length > 0)) {

        $('.vsz-catalogue-toggle').css('visibility', 'inherit');

    }


    $('.button.alert').click(function () {

        $('.vsz_cart-scroll').css('display', 'block');

    });


    if (($(".cart_active").length > 0)) {

        $('.vsz_cart-scroll').css('display', 'block');

    }


    $(".vsz_cart-scroll").click(function () {

        $("html, body").animate({scrollTop: 0}, "slow");

        return false;

    });

    // $('.top-bg .columns:last-child').addClass('top-menu-scroll');
    // $('.top-menu').addClass('top-menu-scroll');

    // Optimalisation: Store the references outside the event handler:
    // var $window = $(window);
    // var $pane = $('#pane1');

    // function checkWidth() {
    //     var windowsize = $window.width();
    //     if (windowsize > 768) {
    //         $('.top-bg .columns:last-child').addClass('top-menu-scroll');
    //         $('.top-menu').addClass('top-menu-scroll');
    //     } else {
    //         $('.top-bg .columns:last-child').removeClass('top-menu-scroll');
    //         $('.top-menu').removeClass('top-menu-scroll');
    //     }
    // }
    // checkWidth();
    // $(window).resize(checkWidth);

    function debounce(fn, ms) {
        var timeoutID;
        return function() {
            var context = this;
            var args = arguments;
            clearTimeout(timeoutID);
            timeoutID = setTimeout(function() {fn.apply(context, args)}, ms);
        };
    }

    function throttle(fn, ms) {
        var inThrottle;
        return function() {
            var args = arguments;
            var context = this;
            if (!inThrottle) {
                fn.apply(context, args);
                inThrottle = true;
                setTimeout(function() {inThrottle = false}, ms);
            }
        };
    }

    function elementInViewport(el) {
        var rect = null;
        try {
            rect = el.getBoundingClientRect();
        } catch (err) {}
        rect = rect || {top: 0, bottom: 0, left: 0, right: 0, width: 0, height: 0};
        return rect.top <= (window.innerHeight || document.documentElement.clientHeight) + 300;
    }

    function updateFixedAndLazyElements() {
        if ($(window).scrollTop() > 100) {
            var windowsize = $(window).width();
            $(".vsz_cart-scroll").css('display', 'block');
            if (windowsize < 426) {
                $('.top-bg').addClass('top-bg-indent');
                $('.top-bg .columns:last-child').addClass('top-scroll-menu');
                $('.top-menu').addClass('top-menu-scroll');
                $('.callback').addClass('callback-fixed');
            } else {
                $('.top-bg').removeClass('top-bg-indent');
                $('.top-bg .columns:last-child').removeClass('top-scroll-menu');
                $('.top-menu').removeClass('top-menu-scroll');
                $('.callback').removeClass('callback-fixed');
            }
        } else {
            $(".vsz_cart-scroll").css('display', 'none');
            $('.top-bg').removeClass('top-bg-indent');
            $('.top-bg .columns:last-child').removeClass('top-scroll-menu');
            $('.top-menu').removeClass('top-menu-scroll');
            $('.callback').removeClass('callback-fixed');
        }

        $('img[data-src]:not(.lazy-modal-img)').each(function(index, element) {
            if (elementInViewport(element)) {
                element.src = $(element).data('src');
                $(element).removeAttr('data-src');
            }
        });
    }

    var onScroll = throttle(debounce(updateFixedAndLazyElements, 4), 100);
    var onResize = throttle(debounce(updateFixedAndLazyElements, 4), 100);
    $(window).bind('scroll', onScroll);
    $(window).resize(onResize);
    updateFixedAndLazyElements();


    $('body').on('click', '.buy_btn', function () {
        var product_id = $(this).data('product');

        var page = $(this).data('page');

        var single_block = $(this).parents('.reveal.xlarge');

        var uri = window.location.pathname;
        var lang = uri.split('/')[1];

        $.ajax({

            method: 'POST',

            url: '/cart_add',

            data: {
                'product_id': product_id,
                'lang': lang
            },

            success: function (data) {

                if (data) {

                    animateBuying(product_id);

                    setTimeout(function () {

                        if (!$('.shopping-cart').hasClass('cart_active')) $('.shopping-cart').addClass('cart_active')

                        $('#cartInfo').text(data);

                        // displayProductsCount

                        $.ajax({

                            method: 'POST',

                            url: '/cart_add_spec',

                            success: function (data) {

                                if (!$('.shopping-cart').hasClass('cart_active')) $('.shopping-cart').addClass('cart_active')

                                $('#rtf').text(data);

                            }

                        });

                    }, 2300);


                    //if(page == 'single') single_block.foundation('close');

                }

            }

        });


    });


    function animateBuying(product_id) {


        var cart = $('.shopping-cart');

        var imgtodrag = $('#product-img-' + product_id);


        if (imgtodrag) {

            var imgclone = imgtodrag.clone()

                .offset({

                    top: imgtodrag.offset().top,

                    left: imgtodrag.offset().left

                })

                .css({

                    'opacity': '0.5',

                    'position': 'absolute',

                    'height': '150px',

                    'width': '150px',

                    'z-index': '100'

                })

                .appendTo($('body'))

                .animate({left: cart.offset().left + 10}, 500)

                .animate({top: cart.offset().top + 10}, 500);


            setTimeout(function () {

                cart.effect("shake", {

                    times: 2

                }, 200);

            }, 1200);


            imgclone.animate({

                'width': 0,

                'height': 0

            }, function () {

                $(this).detach()

            });

        }

    }

    $('body').on('click', '.buy_all_btn', function () {

        var all_product_ids = $('[data-product]').map(function() {
            return $(this).attr("data-product");
        }).get();

        var page = $(this).data('page');

        var single_block = $(this).parents('.reveal.xlarge');

        var uri = window.location.pathname;
        var lang = uri.split('/')[1];

        var i = 0;

        all_product_ids.forEach(function(product_id){

            setTimeout(function () {

                $.ajax({

                    method: 'POST',

                    url: '/cart_add',

                    data: {
                        'product_id': product_id,
                        'lang': lang
                    },

                    success: function (data) {

                        if (data) {

                            animateBuying(product_id);

                            setTimeout(function () {

                                if (!$('.shopping-cart').hasClass('cart_active')) $('.shopping-cart').addClass('cart_active')

                                $('#cartInfo').text(data);

                                // displayProductsCount

                                $.ajax({

                                    method: 'POST',

                                    url: '/cart_add_spec',

                                    success: function (data) {

                                        if (!$('.shopping-cart').hasClass('cart_active')) $('.shopping-cart').addClass('cart_active')

                                        $('#rtf').text(data);

                                    }

                                });

                            }, 1300);

                        }

                    }

                });
            }, i * 500);

            i++;
        });

    });

    var select2LangEt = { 
        inputTooLong: function(e) { 
            var t = e.input.length - e.maximum, n = "Sisesta " + t + " täht";
            return t != 1 && (n += "e"), n += " vähem", n
        }, 
        inputTooShort: function(e) {
            var t = e.minimum - e.input.length, n = "Sisesta vähemalt " + t + " täht";
            return t != 1 && (n += "e"), n += "", n
        }, 
        loadingMore: function() { return "Laen tulemusi…" }, 
        maximumSelected: function(e) {
            var t = "Saad vaid " + e.maximum + " tulemus";
            return e.maximum == 1 ? t += "e" : t += "t", t += " valida", t
        }, 
        noResults: function() { return "Tulemused puuduvad" }, 
        searching: function() { return "Otsin…" }
    }

    $('.dynamic-search-field').select2({
        closeOnSelect: true,
        debug: true,
        dropdownAutoWidth: false,
        dropdownParent: $('.dynamic-search-field').parent(),
        language: $('html').attr('lang') === 'et' ? select2LangEt : 'en',
        minimumInputLength: 2,
        maximumInputLength: 0,
        maximumSelectionLength: 0,
        minimumResultsForSearch: 0,
        multiple: true,
        selectOnClose: false,
        tags: true,
        theme: 'default',
        width: 'element',
        placeholder: {
            id: '-1', // the value of the option
            text: $('.dynamic-search-field').data('placeholder')
        },
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '') return null;
            return {
                text: term + '',
                id: term + '',
                newTag: true
            };
        },
        ajax: {
            url: '/dynamic_search',
            dataType: 'json',
            type: 'POST',
            method: 'POST',
            delay: 500,
            cache: true,
            data: function (params) {
                return {
                    searchText: params.term,
                    lang: $('html').attr('lang')
                };
            },
            processResults: function (data, status) {
                console.log(status)
                var results = (data && data.results) || [];
                var resultsQuantity = data && data.resultsQuantity;
                var resultsHelp = $('.dynamic-search-field').data('count-text')
                                    .replace(/\$\{count\}/gi, resultsQuantity)
                                    .replace(/\$\{word\}/gi, status && status.term);
                return {
                    results: [{
                        text: resultsHelp,
                        id: '',
                        disabled: true
                    }].concat($.map(results, function (item) {
                        return {
                            text: item.name + ' ' + item.price.toFixed(2).replace('.',',') + '\u00a0EUR',
                            id: item.name
                        }
                    }))
                };
            }
        }
    }).on('select2:select', function (e) {
        var data = e.params && e.params.data;
        $(this).parent().find('.select2-search__field').val(data && data.id);
        $(this.form).submit();
    }).parent().on('keydown', '.select2-search__field', function(e) {
        if (e.keyCode == 13 && this.value && this.value.length > 1) { // enter key
            var option = new Option(this.value, this.value, true, true);
            $(this.form).find('.dynamic-search-field').append(option).trigger('change');
            $(this.form).find('.dynamic-search-field').trigger({
                type: 'select2:select',
                params: {
                    data: this.value
                }
            });
        }
    });

});
