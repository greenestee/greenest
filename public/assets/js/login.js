$('#search-button').on('click', function (){
  var popup = new Foundation.Reveal($('#sign-in'));
  popup.open();
});

$('#sign_in').on('submit', function(e) {
    e.preventDefault();
    var $form = $(this);
    console.log('login 1');

    $.post($form.attr('action'), $form.serialize())
        .done(function (data) {
            window.location.reload();
        })
    .fail(function(data, textStatus, xhr) {
        // window.location.reload();
        var msg = (data.responseJSON && data.responseJSON.message) ? data.responseJSON.message : 'Error';
        $('.error-msg').remove();
        $form.find('[name="password"]').after('<div class="error-msg text-center">'+ msg + '</div>');
    });

    return false;
})

$('#forgot-pass').on('click', function(){
    
    $('#sign_in').attr('id','reset-pass');
    $('#reset-pass').attr('action', '/password/email');
    $('#modal-title').text('Reset password');
    $('#password').remove();
    $('#forgot-pass').remove();
    $('.Login-button').text('Reset');
    $('#reset-pass').find('.text-center').hide();
    
    $('#reset-pass').on('submit', function() {
    
        var $form = $(this);
        $form.find('[name="email"]').after('<div class="text-center">We have e-mailed your password reset link!</div>');
        $form.find('[name="email"]').hide();
        $('.Login-button').hide();

        // $.post($form.attr('action'), $form.serialize(), function(data) {
        //
        // }, 'json')
        // .fail(function(data, textStatus, xhr) {
        //
        //         $form.find('[name="email"]').after('<div class="text-center">We have e-mailed your password reset link!</div>');
        //         $form.find('[name="email"]').hide();
        //         $('.Login-button').hide();
        //
        //     //$form.find('[name="email"]').after('<div class="text-center">Invalid email</div>');
        //
        // });
    
        return false;
    })
    
});