jQuery(function($){

  // SHIPPING ADDRESS
  $('.checkbox').click(function() {
    var dict = window.sessionStorage ? JSON.parse(window.sessionStorage.getItem('checkout') || '{}') : {};
    if ($(this).is(':checked')) {

      $('input[name="shipping_address"]').val($('input[name="billing_address"]').val());
      $('input[name="shipping_zip_code"]').val($('input[name="billing_zip_code"]').val());
      $('input[name="shipping_city"]').val($('input[name="billing_city"]').val());
      $('input[name="shipping_state"]').val($('input[name="billing_state"]').val());
      $('input[name="shipping_country"]').val($('input[name="billing_country"]').val());
        dict['shipping_address'] = {value: $('input[name="billing_address"]').val()};
        dict['shipping_zip_code'] = {value: $('input[name="billing_zip_code"]').val()};
        dict['shipping_city'] = {value: $('input[name="billing_city"]').val()};
        dict['shipping_state'] = {value: $('input[name="billing_state"]').val()};
        dict['shipping_country'] = {value: $('input[name="billing_country"]').val()};
    } else {

      $('input[name="shipping_address"]').val('');
      $('input[name="shipping_zip_code"]').val('');
      $('input[name="shipping_city"]').val('');
      $('input[name="shipping_state"]').val('');
      $('input[name="shipping_country"]').val('');
        dict['shipping_address'] = {value: ''};
        dict['shipping_zip_code'] = {value: ''};
        dict['shipping_city'] = {value: ''};
        dict['shipping_state'] = {value: ''};
        dict['shipping_country'] = {value: ''};
    }
    window.sessionStorage && window.sessionStorage.setItem('checkout', JSON.stringify(dict));
  });

  var parcelTerminalTimer;
  var customerPickupTimer;
  var paymentMethodTimer;

  $('.radio-shipping input').on('change', function() {
    var originProductsSubtotal = $('#vsz-subtotal').text().replace(',', '.').replace(' ', '');
    var shippingMethod = 0;
    var shipingExtraFee = parseFloat($('input[name=shipping_stock]:checked').attr('data-value') || 0);
    if (!(parseFloat(originProductsSubtotal) > 50 && $('input[name=shipping_method][value=parcel_terminal]:checked').length)) {
        shippingMethod = parseFloat($('input[name=shipping_method]:checked').attr('data-value') || 0);
    }
    var shipping = shippingMethod + shipingExtraFee;
    var subtotal = parseFloat($('#subtotal').attr('data-value').replace(',', '.').replace(' ', '')) + shipping;
    var tax = parseFloat($('#tax').attr('data-value').replace(',', '.').replace(' ', '')) + (shipping * 0.2);
    var total = subtotal + tax;
    // var shippingControls = $('input[name=shipping_method]');
    checkParcel();

    $('#shipping').text(shipping.toFixed(2).replace('.', ','));
    $('#subtotal').text(subtotal.toFixed(2).replace('.', ','));
    $('#tax').text(tax.toFixed(2).replace('.', ','));
    $('#total').text(total.toFixed(2).replace('.', ','));

    check_shipping_method();

  });

  $('.radio-payment input').on('change', togglePaymentTooltip);

  $('#need-company-info').on('change', check_shipping_method);

  function check_shipping_method(){
    var parcelTerminalField = $('#radio_shipping_field_parcel_terminal');
    var parcelTerminalTooltip = $('#radio_shipping_tooltip_parcel_terminal');
    var customerPickupField = $('#radio_shipping_field_customer_pickup');
    var customerPickupTooltip = $('#radio_shipping_tooltip_customer_pickup');

    var checkedShippingMethod = $("input[type=radio][name=shipping_method]:checked").val();

    if(checkedShippingMethod === 'parcel_terminal'){
        $('#tel-code, #telephone').prop('disabled', false);
        $('select[name="shipping_parcel_terminal_id"]').prop('disabled', false);

        $('.billing-address, .shipping-address').find('.required-field').hide();
        $('#required-name, #required-email').show();

        if (!$('#need-company-info').prop('checked')) {
            $('.billing-address input, .shipping-address input').prop('disabled', true);
        } else {
            $('.billing-address input[type=text]').prop('disabled', false);
            $('.billing-address input[type=checkbox], .shipping-address input').prop('disabled', true);
        }
        $('#name, #email').prop('disabled', false);


        if($('#telephone').val().length == 0){
            $('#telephone').addClass('blink_input');
        }

        $('#telephone').keyup(function(){
          $(this).removeClass('blink_input');
          if($('#select-parcel').val().length == 0){
              $('#select-parcel').parent().find('.select2-selection').addClass('blink_input');
          }
        });

        $('#select-parcel').on('change', function(){
            $('.select2-selection').removeClass('blink_input');
            if($('#name').val().length == 0){
              $('#name').addClass('blink_input');
          }

        });

        $('#name').keyup(function(){
          $(this).removeClass('blink_input');

          if($('#email').val().length == 0){
              $('#email').addClass('blink_input');
          }
        });

        $('#email').keyup(function(){
          $(this).removeClass('blink_input');
        });

        if (parcelTerminalField.length && parcelTerminalTooltip.length && parcelTerminalTooltip[0].style.display === 'none') {
            if (elementInViewport(parcelTerminalField[0])) {
                parcelTerminalTooltip.show();
                parcelTerminalTimer = setTimeout(function() {
                    parcelTerminalTooltip.hide();
                }, parcelTerminalTooltip.data('timeout') || 3000);
            }
        }

        $('#parcel-terminal, #required-parcel, #select2-select-parcel-container').show();

    } else if (checkedShippingMethod === 'customer_pickup') {
        $('#tel-code, #telephone').prop('disabled', true);
        $('select[name="shipping_parcel_terminal_id"]').prop('disabled', true);

        $('#checkout').find('.required-field').hide();
        $('#name, #email, #phone').prop('disabled', false);
        $('#required-name, #required-email, #required-phone').show();
        $('#telephone, #select-parcel').removeClass('blink_input');

        var name = $('#name');
        var email = $('#email');
        var phone= $('#phone');

        if ($('#need-company-info').prop('checked')) {
            $('.billing-address input, .shipping-address input').prop('disabled', false);
        } else {
            $('.billing-address input, .shipping-address input').prop('disabled', true);
            name.prop('disabled', false);
            email.prop('disabled', false);
            phone.prop('disabled', false);
        }

        if (name.val().length === 0) {
            name.addClass('blink_input')
        } else if (email.val().length === 0) {
            email.addClass('blink_input')
        } else if (phone.val().length === 0) {
            phone.addClass('blink_input')
        }

        name.keyup(function(){
            if ($(this).val().length > 0) {
                $(this).removeClass('blink_input');
                if ($('#email').val().length === 0){
                    $('#email').addClass('blink_input');
                } else if ($('#phone').val().length === 0){
                    $('#phone').addClass('blink_input');
                }
            } else {
                $(this).addClass('blink_input');
                $('#email').removeClass('blink_input');
                $('#phone').removeClass('blink_input');
            }
        });
        email.keyup(function(){
            if ($(this).val().length > 0) {
                $(this).removeClass('blink_input');
                if ($('#phone').val().length === 0){
                    $('#phone').addClass('blink_input');
                }
            } else {
                $(this).addClass('blink_input');
                $('#phone').removeClass('blink_input');
            }
        });
        phone.keyup(function(){
            if ($(this).val().length > 0) {
                $(this).removeClass('blink_input');
            } else {
                $(this).addClass('blink_input');
            }
        });


        $('#parcel-terminal, #required-parcel, #select2-select-parcel-container').hide();

        if (parcelTerminalField.length && parcelTerminalTooltip.length) {
            parcelTerminalTooltip.hide();
            clearTimeout(parcelTerminalTimer);
        }
    } else {
        $('#tel-code, #telephone').prop('disabled', true);
        $('select[name="shipping_parcel_terminal_id"]').prop('disabled', true);

        $('#checkout').find('.required-field').show();
        $('.billing-address input, .shipping-address input').prop('disabled', false);
        $('#name, #email, #phone, #telephone, #select-parcel').removeClass('blink_input');


        $('#name').off('keyup');

        $('#parcel-terminal, #required-parcel, #select2-select-parcel-container').hide();

        if($('#telephone').length && $('#telephone').val().length > 0 && $('#phone').val().length == 0){
            $('#phone').val($('#tel-code').val() + $('#telephone').val());
        }

        if (parcelTerminalField.length && parcelTerminalTooltip.length) {
            parcelTerminalTooltip.hide();
            clearTimeout(parcelTerminalTimer);
        }
    }

    if(checkedShippingMethod === 'customer_pickup'){
        if (customerPickupField.length && customerPickupTooltip.length && customerPickupTooltip[0].style.display === 'none') {
            if (elementInViewport(customerPickupField[0])) {
                customerPickupTooltip.show();
                customerPickupTimer = setTimeout(function() {
                    customerPickupTooltip.hide();
                }, customerPickupTooltip.data('timeout') || 3000);
            }
        }
    } else {
        if (customerPickupField.length && customerPickupTooltip.length) {
            customerPickupTooltip.hide();
            clearTimeout(customerPickupTimer);
        }
    }
  }


  function togglePaymentTooltip() {
      var paymentRadio = $("input[type=radio][name=payment_method]");
      if (paymentRadio && paymentRadio.length) {
          clearTimeout(paymentMethodTimer);
          var paymentInput, paymentTooltip;
          for (var paymentIndex = 0; paymentIndex < paymentRadio.length; paymentIndex++) {
              if (paymentRadio[paymentIndex]) {
                  paymentInput = $(paymentRadio[paymentIndex]);
                  paymentTooltip = paymentInput.parent().find('.checkout-tooltip');
                  if (paymentTooltip && paymentTooltip.length) {
                      if (paymentInput.prop('checked')) {
                          if (paymentTooltip[0].style.display === 'none' && elementInViewport(paymentInput[0])) {
                              paymentTooltip.show();
                              paymentMethodTimer = setTimeout(function() {
                                  paymentTooltip.hide();
                                  paymentMethodTimer = null;
                              }, paymentTooltip.data('timeout') || 3000);
                          }
                      } else {
                          if (paymentTooltip[0].style.display !== 'none') {
                              paymentTooltip.hide();
                          }
                      }
                  }
              }
          }
      }
  }

  // save checkout draft
   $('#checkout').on('change', function(event) {
    var target = event && event.target;
    var value = target && target.value;
    var name = target && target.name;
    var dataValue = $(target).data('value');
    if (window.sessionStorage && name) {
        var dict = JSON.parse(window.sessionStorage.getItem('checkout') || '{}');
        dict[name] = {};
        dict[name].value = target.type === 'checkbox' ? target.checked : value;
        if (dataValue || dataValue === 0) {
          dict[name].data = dataValue;
        }
        window.sessionStorage.setItem('checkout', JSON.stringify(dict));
    }

    // console.log('change', {target}, name, value);
   })

  // SIGN UP AFTER SUBMIT
  $('#checkout').on('submit', function() {
    window.sessionStorage && window.sessionStorage.clear();

    var $form = $(this).children();

    if ($('#sign_up').is(':checked')) {
      $('#checkout_sign_up input[name="email"]').val($form.find('input[name="email"]').val());

      $form.serializeArray().forEach(function(item){
        $('#checkout_sign_up').prepend('<input type="hidden" name="'+ item.name + '" value="' + item.value +'"/>');
      });

      $('#sign-up').foundation('open');
    } else {

      return true;
    }

    return false;
  });

  $('#tel-code').on('change', function() {
      checkParcel();
  });

  function checkParcel() {
      var originProductsSubtotal = $('#vsz-subtotal').text().replace(',', '.').replace(' ', '');
      var shippingParcel = $('input[name=shipping_method][value=parcel_terminal]');
      var value = $('#tel-code option:selected').text();
      if (value === '+358') {
          shippingParcel.attr('data-value', 10);
      } else {
          shippingParcel.attr('data-value', 3.33);
      }
      var parcelValue = parseFloat(shippingParcel.attr('data-value') || 0);
      var parcelTextValue = (Math.ceil(parcelValue * 100 * 1.2) / 100.0).toFixed(2).replace('.', ',');
      var parcelTextEl = shippingParcel.closest('.radio-label').next('.radio-text');

      if (parseFloat(originProductsSubtotal) > 50 && value !== '+358') {
          parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
      } else if (parseFloat(originProductsSubtotal) > 125 && value === '+358') {
          parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' 0'));
      } else {
          parcelTextEl.text((parcelTextEl.text() + '').replace(/\s\d+([.,]\d+)?(?=\s+(EUR|€))/i, ' ' + parcelTextValue));
      }
  }

function fillCheckout () {
    var checkoutform = $('#checkout');
    if (window.sessionStorage && checkoutform.length) {
        var dict = JSON.parse(window.sessionStorage.getItem('checkout') || '{}');
        var element = null;
        for (let key in dict) {
            if (dict.hasOwnProperty(key)) {
                element = checkoutform.find('[name="' + key + '"]');
                if (element.length === 1) {
                    if (element[0].type === 'checkbox' && typeof dict[key].value === 'boolean') {
                        element.prop('checked', dict[key].value);
                    } else {
                        element.val(dict[key].value);
                        if (key === 'shipping_parcel_terminal_id') {
                            element.trigger('change');
                        }
                    }
                } else if (element.length && element[0].type === 'radio') {
                    var chosen = element.filter('[value="' + dict[key].value + '"]');
                    if (chosen.length === 1 && chosen.attr('name') !== 'shipping_stock') {
                        chosen.prop('checked', true);
                        element.trigger('change');
                    }
                }
                if (key === 'billing_address' && $('#select2-address-container').length) {
                    $('#select2-address-container').text(dict[key].value);
                }
            }
        }
    }
}

  // $(window).bind("load", function() {

  $(document).ready(function() {

    fillCheckout();

    checkParcel();

    check_shipping_method();

  });

  function debounce(fn, ms) {
    var timeoutID;
    return function() {
      var context = this;
      var args = arguments;
      clearTimeout(timeoutID);
      timeoutID = setTimeout(function() {fn.apply(context, args)}, ms);
    };
  }

  function throttle(fn, ms) {
    var inThrottle;
    return function() {
      var args = arguments;
      var context = this;
      if (!inThrottle) {
        fn.apply(context, args);
        inThrottle = true;
        setTimeout(function() {inThrottle = false}, ms);
      }
    };
  }

  function elementInViewport(el) {
    var rect = null;
    try {
      rect = el.getBoundingClientRect();
    } catch (err) {}
    rect = rect || {top: 0, bottom: 0, left: 0, right: 0, width: 0, height: 0};
    return rect.top <= (window.innerHeight || document.documentElement.clientHeight);
  }

  var onScroll = throttle(debounce(updateViewportElements, 4), 100);
  var onResize = throttle(debounce(updateViewportElements, 4), 100);
  $(window).bind('scroll', onScroll);
  $(window).resize(onResize);

    function updateViewportElements(event) {
      var windowsize = $(window).width();

      if (windowsize < 426) {
        vsz_block_height();
      } else {
        $('.row.small-collapse.order').removeClass('vsz-two-line-block');
        $('.row.small-collapse.order').children('.large-5.columns').removeClass('vsz-two-line-text');
        $('.row.small-collapse.order').children('.large-7.columns.order-item').removeClass('vsz-two-line-image');
        $('.row.small-collapse.total-bold').removeClass('vsz-total-relative');
        $('.row.checkout-buttons').removeClass('vsz-checkout-button-relative');
        $('div#checkout').removeClass('vsz-checkout-form-relative');
      }

      var parcelTerminalField = $('#radio_shipping_field_parcel_terminal');
      var parcelTerminalTooltip = $('#radio_shipping_tooltip_parcel_terminal');
      var parcelTerminalInput = $('#radio_shipping_button_parcel_terminal');
      if (parcelTerminalField.length && parcelTerminalTooltip.length && parcelTerminalInput.prop('checked') && !parcelTerminalTooltip[0].style.display) {
        if (elementInViewport(parcelTerminalField[0])) {
            parcelTerminalTooltip.show();
            parcelTerminalTimer = setTimeout(function() {
                parcelTerminalTooltip.hide();
            }, parcelTerminalTooltip.data('timeout') || 3000);
        }
      }

      var customerPickupField = $('#radio_shipping_field_customer_pickup');
      var customerPickupTooltip = $('#radio_shipping_tooltip_customer_pickup');
      var customerPickupInput = $('#radio_shipping_button_customer_pickup');
      if (customerPickupField.length && customerPickupTooltip.length && customerPickupInput.prop('checked') && !customerPickupTooltip[0].style.display) {
        if (elementInViewport(customerPickupField[0])) {
            customerPickupTooltip.show();
            customerPickupTimer = setTimeout(function() {
                customerPickupTooltip.hide();
            }, customerPickupTooltip.data('timeout') || 3000);
        }
      }

      var paymentRadio = $("input[type=radio][name=payment_method]");
      if (paymentRadio && paymentRadio.length) {
          clearTimeout(paymentMethodTimer);
          var paymentInput, paymentTooltip;
          for (var paymentIndex = 0; paymentIndex < paymentRadio.length; paymentIndex++) {
              if (paymentRadio[paymentIndex] && (paymentRadio[paymentIndex].value === 'hire_purchase_seb' || paymentRadio[paymentIndex].value === 'hire_purchase_lhv')) {
                  paymentInput = $(paymentRadio[paymentIndex]);
                  paymentTooltip = paymentInput.parent().find('.checkout-tooltip');
                  if (paymentTooltip && paymentTooltip.length && paymentInput.prop('checked') && !paymentTooltip[0].style.display) {
                      if (elementInViewport(paymentInput[0])) {
                          paymentTooltip.show();
                          paymentMethodTimer = setTimeout(function() {
                              paymentTooltip.hide();
                              paymentMethodTimer = null;
                          }, paymentTooltip.data('timeout') || 3000);
                      }
                  }
              }
          }
      }
    }

vsz_block_height();

function vsz_block_height() {
  var i = 0;
  $(".top-name").each(function(index) {
      i++;
      if($(this).height() == 50) {
        // console.log($(this).find('.row.small-collapse.order'));
        var vsz_parent = $(this).find('.row.small-collapse.order');
        $(this).parents('.row.small-collapse.order').addClass('vsz-two-line-block');
        $(this).parents('.row.small-collapse.order').children('.large-5.columns').addClass('vsz-two-line-text');
        $(this).parents('.row.small-collapse.order').children('.large-7.columns.order-item').addClass('vsz-two-line-image');
      }
      if(i == 1) {
        $('.row.small-collapse.total-bold').removeClass('vsz-total-relative');
        $('.row.checkout-buttons').removeClass('vsz-checkout-button-relative');
        $('div#checkout').removeClass('vsz-checkout-form-relative');
      } else {
        $('.row.small-collapse.total-bold').addClass('vsz-total-relative');
        $('.row.checkout-buttons').addClass('vsz-checkout-button-relative');
        $('div#checkout').addClass('vsz-checkout-form-relative');
      }
    // console.log(i);
  });
}

  // });

});
