<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{	
	/**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {	
		Product::create([	
			'name' => 'Wooden Fork',
			'sku' => 'BPK1702',
			'product_url' => 'http://www.biopac.co.uk/products/cutlery-accessories/wooden-cutlery/wooden-forks.html',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Wooden Knife',
			'sku' => 'BPK1701',
			'product_url' => 'http://www.biopac.co.uk/products/cutlery-accessories/wooden-cutlery/wooden-knives.html',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Wooden Spoon',
			'sku' => 'BPK1703',
			'product_url' => 'http://www.biopac.co.uk/products/cutlery-accessories/wooden-cutlery/wooden-spoons.html',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Teaspoon',
			'sku' => 'BPK1736',
			'product_url' => 'http://www.biopac.co.uk/products/cutlery-accessories/wooden-cutlery/wooden-teaspoons.html',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Double Wall 400 ml Coffee Cup',
			'sku' => 'GBX04660',
			'product_url' => 'https://www.biologischverpacken.de/en/2301-white-double-wall-paper-cups-400ml/16oz?c=6324',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Sugarcane Lid for 400 ml Coffee Cup 90mm',
			'sku' => 'GBX04591',
			'product_url' => 'https://www.biologischverpacken.de/en/2894-bagasse-coffee-cup-lids-oe90mm',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'CPLA Lid for 400 ml Coffee Cup 90mm',
			'sku' => 'GBX04491',
			'product_url' => 'https://www.biologischverpacken.de/en/2849-bio-coffee-cup-lids-cpla-oe90mm',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => '360 ml "I\'m a Green Coffee Cup"',
			'sku' => 'BPK1164',
			'product_url' => 'http://www.biopac.co.uk/products/hot-drink-cups/compostable-hot-cups/printed-double-wall-compostable-hot-cups/12oz-double-wall-im-a-green-cup.html',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Lid for 360 ml Coffee Cup',
			'sku' => 'BPK1127',
			'product_url' => 'http://www.biopac.co.uk/white-compostable-lids-10oz-12oz-16oz-compostable-cups.html',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => '2 Compatments Sugarcane Clamshell 650 ml',
			'sku' => 'GBX1345',
			'product_url' => 'https://www.biologischverpacken.de/en/1994-bagasse-hinged-lid-containers-with-2-compt.-650ml',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => '3 Compartments Sugarcane Clamshell 800 ml',
			'sku' => 'GBX01335',
			'product_url' => 'https://www.biologischverpacken.de/en/1993-bagasse-hinged-lid-containers-with-3-compt.-800ml?c=6386',
			'pieces' => 200,
			'price' => 0.00
		]);

		Product::create([	
			'name' => '600 ml Hinged-Lid Sugarcane Container',
			'sku' => 'GBX01305',
			'product_url' => 'https://www.biologischverpacken.de/en/1990-bagasse-hinged-lid-containers-600ml-rectangular',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Elegant Sugarcane Soup Plate 230x38mm',
			'sku' => 'BWG14171',
			'product_url' => 'http://www.bioeinweggeschirr.de/Zuckerrohr-Teller-Elegance-rund-tief-R-23-cm-38-cm-hoch',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Sugarcane Plate 240mm',
			'sku' => 'GBX01425',
			'product_url' => 'https://www.biologischverpacken.de/en/1550-round-sugarcane-plates-oe-24-cm?c=6362',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Retangular Sucarcane Plate 260x130mm',
			'sku' => 'GBX08731',
			'product_url' => 'https://www.biologischverpacken.de/en/3352-rectangular-sugarcane-plates-26-x-13-cm?c=6362',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Square sugarcane plate 160x160mm',
			'sku' => 'GBX08701',
			'product_url' => 'https://www.biologischverpacken.de/en/3410-square-sugarcane-plates-16-x-16-cm',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Clear PLA Flat Bag 220x100mm',
			'sku' => 'GBX02100',
			'product_url' => 'https://www.biologischverpacken.de/en/1472-clear-pla-flat-bags-22-x-10-cm?c=6435',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Straw 203x6mm',
			'sku' => 'GBX00161',
			'product_url' => 'https://www.biologischverpacken.de/en/2707-biodegradable-straws-pla-203mm-oe6mm',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Biodegradable Jumbo Straw 255x7mm',
			'sku' => 'GBX00172',
			'product_url' => 'https://www.biologischverpacken.de/en/2708-biodegradable-jumbo-straws-pla-255mm-oe7mm',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Clear Flexible Straw 210x6mm',
			'sku' => 'BPK113101',
			'product_url' => 'http://www.biopac.co.uk/clear-flexible-straws-494.html',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Clear Smoothie Straw 200x9mm',
			'sku' => 'BPK112401',
			'product_url' => 'http://www.biopac.co.uk/clear-jumbo-smoothie-straws.html',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Wrapped Straw ',
			'sku' => 'BPK1106',
			'product_url' => 'http://www.biopac.co.uk/white-wrapped-compostable-straws.html',
			'pieces' => 1500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'The Greenest Straw Ever - 230mm Cleaned Rye',
			'sku' => 'BWGPS23',
			'product_url' => 'http://www.bioeinweggeschirr.de/bio-strohhalme-23-350',
			'pieces' => 350,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Sugarcane 2 Compartments Hot Food Box with Lid 1000 ml',
			'sku' => 'GBX001295',
			'product_url' => 'https://www.biologischverpacken.de/en/1989-bagasse-containers-2-compt.-1000ml-deep-w/lid',
			'pieces' => 400,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Triangle Palm Leaf Cake Plate 240x150x18mm',
			'sku' => 'LCH00001',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Palm-Leaf-Plates-Cases-Of-100/Palm-Leaf-10-Triangle-Slice-Plate---Case-Of-100.Html',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon RED',
			'sku' => 'LCH00002',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Red.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon PINK',
			'sku' => 'LCH00003',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Balloons--Ribbon/Compostable-Ribbon-Hot-Pink.html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon GOLD',
			'sku' => 'LCH00004',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Antique-Gold.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon SILVER',
			'sku' => 'LCH00005',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Silver.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon SMARAGD',
			'sku' => 'LCH00006',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Bottle-Green.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon NATURAL',
			'sku' => 'LCH00007',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Natural.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon WHITE',
			'sku' => 'LCH00008',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Pure-White.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon TURQUOISE',
			'sku' => 'LCH00010',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Turquoise.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon CORAL',
			'sku' => 'LCH00011',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Compostable-Ribbon-Coral-Pink.Html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Ribbon OCEAN BLUE',
			'sku' => 'LCH00012',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Balloons--Ribbon/Compostable-Ribbon-Pale-Blue.html?page=2',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Red Heart Shape Balloon',
			'sku' => 'LCH00013',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Biodegradable-Balloons-Ribbon/Biodegradable-Balloons-Red-Heart-100-Pack.Html',
			'pieces' => 10,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Compostable Mixed Color Balloons',
			'sku' => 'LCH00014',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Balloons--Ribbon/Biodegradable-Balloons-Mixed-Colours-100-Pack.html',
			'pieces' => 10,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Natural Coconut Bowl ~125x76x51mm',
			'sku' => 'LCH00015',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Palm-Leaf-Plates-Cases-Of-100/Natural-Coconut-Shell-Bowl-Case-Of-100.Html',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Designed Large Sugarcane Plate 251x190x20mm',
			'sku' => 'LCH00016',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Designer-Sugarcane-Bagasse-Plates-And-Bowls/Sugarcane-Plates-By-Sucadrop-Large-Pack-Of-50/Sugarcane-Plates-By-Sucadrop-Large---Pack-Of-50.Html',
			'pieces' => 50,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Designed Medium Sugarcane Plate 201x150x41mm',
			'sku' => 'LCH00017',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Designer-Sugarcane-Bagasse-Plates-And-Bowls/Sugarcane-Plates-By-Sucadrop-Large-Pack-Of-50/Sugarcane-Plates-By-Sucadrop-Medium---Pack-Of-50.Html',
			'pieces' => 50,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Designed Sugarcane Bowl 149x118x63mm',
			'sku' => 'LCH00018',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Designer-Sugarcane-Bagasse-Plates-And-Bowls/Sugarcane-Plates-By-Sucadrop-Large-Pack-Of-50/Sugarcane-Bowls-By-Sucadrop---Pack-Of-50.Html',
			'pieces' => 50,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Wheat Fibre 3 Corpatments Plate 260mm',
			'sku' => 'LCH00019',
			'product_url' => 'http://www.littlecherrywholesale.co.uk/Natural-Wheat-Fibre-Plates-And-Bowls/Wheat-Fibre-Plates-10-3-Compartment-Case-Of-500.html',
			'pieces' => 500,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Palm Leaf Tray 325x270x2mm',
			'sku' => 'BWG139T',
			'product_url' => 'http://www.bioeinweggeschirr.de/Palmblatt-Teller-Tablett-325-x-27-x-2-cm',
			'pieces' => 150,
			'price' => 0.00
		]);

		Product::create([	
			'name' => ' Oval Palm Leaf Tray  410x280x25mm',
			'sku' => 'BWGIPP43',
			'product_url' => 'http://www.bioeinweggeschirr.de/palmblatt-tray-43x29cm_4',
			'pieces' =>  100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Big Oval Palm Leaf Tray 480x240x50mm',
			'sku' => 'BWGIPO48',
			'product_url' => 'http://www.bioeinweggeschirr.de/palmblatttrays-48x24',
			'pieces' => 50,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Retangular Palm Leaf Tray 325x270x20',
			'sku' => 'GBX05450',
			'product_url' => 'https://www.biologischverpacken.de/en/2937-palm-leaf-trays-gastronorm-32-5x27cm',
			'pieces' => 100,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Clear PLA Cup 250 ml',
			'sku' => 'GBX00040',
			'product_url' => 'https://www.biologischverpacken.de/en/2139-pla-cold-cups-250ml/9oz',
			'pieces' => 2000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Clear PLA Cup 500 ml',
			'sku' => 'GBX00120',
			'product_url' => 'https://www.biologischverpacken.de/en/2149-pla-cold-cups-500ml/20oz',
			'pieces' => 1000,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'PLA Lid with Straw Hole for 500 ml Cup 97mm',
			'sku' => 'GBX00051',
			'product_url' => 'https://www.biologischverpacken.de/en/2157-flat-lids-pla-with-straw-hole-oe97mm',
			'pieces' => 1000,
			'price' => 0.00
		]);


		Product::create([	
			'name' => 'Anti Itch Mosquitos Roll-On 10 ml',
			'sku' => 'ARS1510',
			'product_url' => '',
			'pieces' => 12,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Mosquitos Spray 100ml',
			'sku' => 'ARS1046',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Tick Spray 100ml',
			'sku' => 'ARS1066',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Mosquitos Textilespray 100ml',
			'sku' => 'ARS1049',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Mosquitos Hautlotion 30ml',
			'sku' => 'ARS1059',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Mosquitos Universal Oil 10 ml',
			'sku' => 'ARS1042',
			'product_url' => '',
			'pieces' => 15,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Wasp Surfaces & Textile Spray 50 ml',
			'sku' => 'ARS1072',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Lavender Oil 50 ml',
			'sku' => 'ARS1100',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Citronella Oil 50 ml',
			'sku' => 'ARS1106',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Arvenöl 50 ml Braunglasflasche',
			'sku' => 'ARS1102',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Orange Oil Bio 50 ml Citrus Sinensis',
			'sku' => 'ARS1118',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Eucalyptus Oil Bio 50 ml Eucalyptus Globulus',
			'sku' => 'ARS1119',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Teatree Oil Bio, 50 ml',
			'sku' => 'ARS8080',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Kräuterdünger 250 ml',
			'sku' => 'ARS5520',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Tomatendünger 250 ml',
			'sku' => 'ARS5530',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Blumendünger 250 ml',
			'sku' => 'ARS5510',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'ARIES Orchideen Dünger 250ml',
			'sku' => 'ARS5592',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Kokoblock, 750g',
			'sku' => 'ARS5150',
			'product_url' => '',
			'pieces' => 12,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Schneckengranulat 250 g (Blechdose)',
			'sku' => 'ARS1203',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Mole-Stop 200g',
			'sku' => 'ARS1216',
			'product_url' => '',
			'pieces' => 10,
			'price' => 0.00
		]);

		Product::create([	
			'name' => 'Anti Ants Oil 250 ml',
			'sku' => 'ARS1115',
			'product_url' => '',
			'pieces' => 6,
			'price' => 0.00
		]);
	}
}