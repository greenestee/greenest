<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('title');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Site Title',
                'value'        => 'Greenest',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
            ])->save();
        }

        $setting = $this->findSetting('description');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Site Description',
                'value'        => 'The home of eco-friendly products',
                'details'      => '',
                'type'         => 'text',
                'order'        => 2,
            ])->save();
        }

        $setting = $this->findSetting('logo');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Site Logo',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 3,
            ])->save();
        }

        $setting = $this->findSetting('phone_number');
        if (!$setting->exists) {
            $setting->fill([
              'display_name' => 'Phone number',
              'value'        => '+37251900330',
              'details'      => '',
              'type'         => 'text',
              'order'        => 4,
            ])->save();
        }

        $setting = $this->findSetting('copyright');
        if (!$setting->exists) {
            $setting->fill([
              'display_name' => 'Copyright',
              'value'        => 'Nature Design OÜ Reg no. 12300034 VAT: EE1017774645 Swedbank account EE234000234545666. All Rights Reserved',
              'details'      => '',
              'type'         => 'text',
              'order'        => 5,
            ])->save();
        }

        $setting = $this->findSetting('admin_bg_image');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Admin Background Image',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 6,
            ])->save();
        }

        $setting = $this->findSetting('admin_title');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Admin Title',
                'value'        => 'Greenest',
                'details'      => '',
                'type'         => 'text',
                'order'        => 7,
            ])->save();
        }

        $setting = $this->findSetting('admin_description');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Admin Description',
                'value'        => 'makes the world a better place',
                'details'      => '',
                'type'         => 'text',
                'order'        => 8,
            ])->save();
        }

        $setting = $this->findSetting('admin_loader');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Admin Loader',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 9,
            ])->save();
        }

        $setting = $this->findSetting('admin_icon_image');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Admin Icon Image',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 10,
            ])->save();
        }

    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
