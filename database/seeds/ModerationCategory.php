<?php

use Illuminate\Database\Seeder;
use App\Category;

class ModerationCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::firstOrCreate([
            'name' => 'Service',
            'name_et' => 'Service',
            'slug' => 'service',
            'slug_et' => 'service',

            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'name' => 'Moderation',
            'name_et' => 'Moderation',
            'slug' => 'moderation',
            'slug_et' => 'moderation',
            'parent_id' => $category->id,
            'is_main' => 0,
        ]);
    }


}
