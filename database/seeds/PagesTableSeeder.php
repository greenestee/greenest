<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::firstOrNew([
            'slug' => 'contact',
        ]);
        if (!$page->exists) {
            $page->fill([
                'author_id' => 0,
                'title'     => 'Contacts',
                'excerpt'   => '',
                'body'      => '',
                'meta_description' => 'Yar Meta Description',
                'meta_keywords'    => 'Keyword1, Keyword2',
                'status'           => 'ACTIVE'
            ])->save();
        }

        $page = Page::firstOrNew([
          'slug' => 'delivery',
        ]);
        if (!$page->exists) {
            $page->fill([
              'author_id' => 0,
              'title'     => 'Delivery',
              'excerpt'   => '',
              'body'      => '',
              'meta_description' => 'Other Meta Description',
              'meta_keywords'    => 'Keyword1, Keyword2',
              'status'           => 'ACTIVE'
            ])->save();
        }

        $page = Page::firstOrNew([
          'slug' => 'about',
        ]);
        if (!$page->exists) {
            $page->fill([
              'author_id' => 0,
              'title'     => 'About Us',
              'excerpt'   => '',
              'body'      => '',
              'meta_description' => 'Other Meta Description',
              'meta_keywords'    => 'Keyword1, Keyword2',
              'status'           => 'ACTIVE'
            ])->save();
        }

        $page = Page::firstOrNew([
          'slug' => 'terms',
        ]);
        if (!$page->exists) {
            $page->fill([
              'author_id' => 0,
              'title'     => 'Terms',
              'excerpt'   => '',
              'body'      => '',
              'meta_description' => 'Other Meta Description',
              'meta_keywords'    => 'Keyword1, Keyword2',
              'status'           => 'ACTIVE'
            ])->save();
        }
    }
}
