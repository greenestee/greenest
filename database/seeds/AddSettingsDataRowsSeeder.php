<?php

use Illuminate\Database\Seeder;

class AddSettingsDataRowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::update('UPDATE settings 
            SET `order` = CASE
            WHEN id = 1 THEN 1
            WHEN id = 2 THEN 3
            WHEN id = 3 THEN 5
            WHEN id = 12 THEN 6
            WHEN id = 13 THEN 8
            WHEN id = 4 THEN 10
            WHEN id = 5 THEN 11
            WHEN id = 6 THEN 13
            WHEN id = 7 THEN 14
            WHEN id = 8 THEN 16
            WHEN id = 9 THEN 18
            WHEN id = 10 THEN 19
            WHEN id = 15 THEN 20
            WHEN id = 17 THEN 22
            WHEN id = 20 THEN 24
            WHEN id = 19 THEN 26
            WHEN id = 21 THEN 28 END
            WHERE id IN (1,2,3,12,13,4,5,6,7,8,9,10,15,17,20,19,21)
        ');

        \TCG\Voyager\Models\Setting::where('type', 'text')->update(['display_name' => DB::raw("CONCAT(display_name, ' [EN]')")]);

        \TCG\Voyager\Models\Setting::insert([
            ['key' => 'title_et', 'display_name' => 'Site Title [ET]', 'type' => 'text', 'order' => 2, 'value' => ''],
            ['key' => 'description_et', 'display_name' => 'Site Description [ET]', 'type' => 'text', 'order' => 4, 'value' => ''],
            ['key' => 'header_first_row_et', 'display_name' => 'Header first row [ET]', 'type' => 'text', 'order' => 7, 'value' => 'Soovid osta hulgi?'],
            ['key' => 'header_second_row_et', 'display_name' => 'Header second row [ET]', 'type' => 'text', 'order' => 9, 'value' => 'Helista meile:'],
            ['key' => 'copyright_et', 'display_name' => 'Copyright [ET]', 'type' => 'text', 'order' => 12, 'value' => ''],
            ['key' => 'admin_title_et', 'display_name' => 'Admin Title [ET]', 'type' => 'text', 'order' => 15, 'value' => ''],
            ['key' => 'admin_description_et', 'display_name' => 'Admin Description [ET]', 'type' => 'text', 'order' => 17, 'value' => ''],
            ['key' => 'email_text_et', 'display_name' => 'Email text [ET]', 'type' => 'text', 'order' => 21, 'value' => ''],
            ['key' => 'meta_title_et', 'display_name' => 'Meta Title [ET]', 'type' => 'text', 'order' => 23, 'value' => ''],
            ['key' => 'meta_keywords_et', 'display_name' => 'Meta Keywords [ET]', 'type' => 'text', 'order' => 25, 'value' => ''],
            ['key' => 'meta_description_et', 'display_name' => 'Meta Description [ET]', 'type' => 'text', 'order' => 27, 'value' => ''],
            ['key' => 'order_text_et', 'display_name' => 'Order Text [ET]', 'type' => 'text', 'order' => 29, 'value' => ''],
        ]);

        \Illuminate\Support\Facades\DB::update("UPDATE settings 
            SET  `value` = CASE
            WHEN `key` = 'title_et' THEN 'Ökopood | Mahepood | Vegan | Organic shop | Organic food'
            WHEN `key` = 'description_et' THEN 'The home of eco-friendly products'
            WHEN `key` = 'copyright_et' THEN 'Nature Design OÜ | Reg no. 12802934 | VAT: EE101777465 | Swedbank account EE234000234545666 | All Rights Reserved.'
            WHEN `key` = 'admin_title_et' THEN 'Greenest'
            WHEN `key` = 'admin_description_et' THEN 'makes the world a better place'
            WHEN `key` = 'email_text_et' THEN 'Thank you for you purchase. We hope youll enjoy our products. Here is your invoice.'
            WHEN `key` = 'meta_title_et' THEN 'greenest.ee – Parimate hindadega ökopood Eestis'
            WHEN `key` = 'meta_keywords_et' THEN 'ökopood, mahetoit, ökokaubad, kontrollitud mahetoode, ökokosmeetika, biolagunevad nõud, biolagunevad pakendid, vegan, gluteenivaba, organic shop'
            WHEN `key` = 'meta_description_et' THEN 'Suurima valiku ja parimate hindadega ökopood. Greenest.ee mahepood pakub nii vegan tooteid kui ka palju tooteid mis on gluteenivabad. Ökokosmeetika. Biolagunevad nõud ja pakendid ning hooajakaubad.'
            WHEN `key` = 'order_text_et' THEN 'Thank you for your purchase. The order will be processed after we have received the payment!' END
            WHERE `key` IN (
              'title_et',
              'description_et',
              'copyright_et',
              'admin_title_et',
              'admin_description_et',
              'email_text_et',
              'meta_title_et',
              'meta_keywords_et',
              'meta_description_et',
              'order_text_et')
        ");
    }
}
