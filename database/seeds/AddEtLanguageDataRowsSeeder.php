<?php

use Illuminate\Database\Seeder;

class AddEtLanguageDataRowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddCategoriesDataRowsSeeder::class);
        $this->call(AddPageDataRowsSeeder::class);
        $this->call(AddProductsDataRowsSeeder::class);
        $this->call(AddSettingsDataRowsSeeder::class);
    }
}
