<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        Category::firstOrCreate([
            'id' => 1,
            'name' => 'Food & Drinks',
            'slug' => 'food-and-drinks',
            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'id' => 2,
            'name' => 'Partyware',
            'slug' => 'partyware',
            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'id' => 3,
            'name' => 'Packing',
            'slug' => 'packing',
            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'id' => 4,
            'name' => 'Outdoor',
            'slug' => 'outdoor',
            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'id' => 5,
            'name' => 'Relaxation & Hygiene',
            'slug' => 'relaxation-and-hygiene',
            'parent_id' => NULL,
            'is_main' => 1,
        ]);

        Category::firstOrCreate([
            'id' => 6,
            'name' => 'Tableware',
            'slug' => 'tableware',
            'parent_id' => 2,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 7,
            'name' => 'Straws',
            'slug' => 'straws',
            'parent_id' => 2,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 8,
            'name' => 'Food Containers',
            'slug' => 'food-containers',
            'parent_id' => 2,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 9,
            'name' => 'Ribbon',
            'slug' => 'ribbon',
            'parent_id' => 2,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 10,
            'name' => 'Balloons',
            'slug' => 'balloons',
            'parent_id' => 2,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 11,
            'name' => 'Cutlery',
            'slug' => 'cutlery',
            'parent_id' => 6,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 12,
            'name' => 'Plates',
            'slug' => 'plates',
            'parent_id' => 6,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 13,
            'name' => 'Bowls',
            'slug' => 'bowls',
            'parent_id' => 6,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 14,
            'name' => 'Trays',
            'slug' => 'trays',
            'parent_id' => 6,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 15,
            'name' => 'Cups & Lids',
            'slug' => 'cups-and-lids',
            'parent_id' => 6,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 16,
            'name' => 'Hot Food',
            'slug' => 'hot-food',
            'parent_id' => 8,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 17,
            'name' => 'Tableware',
            'slug' => 'tableware',
            'parent_id' => 1,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 18,
            'name' => 'Food Containers',
            'slug' => 'food-containers',
            'parent_id' => 1,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 19,
            'name' => 'Cups & Lids',
            'slug' => 'cups-and-lids',
            'parent_id' => 17,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 20,
            'name' => 'Hot Food',
            'slug' => 'hot-food',
            'parent_id' => 18,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 21,
            'name' => 'Bags',
            'slug' => 'bags',
            'parent_id' => 3,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 22,
            'name' => 'Protection',
            'slug' => 'protection',
            'parent_id' => 4,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 23,
            'name' => 'Seasonal',
            'slug' => 'seasonal',
            'parent_id' => 4,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 24,
            'name' => 'Mosquitos',
            'slug' => 'mosquitos',
            'parent_id' => 22,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 25,
            'name' => 'Ticks',
            'slug' => 'ticks',
            'parent_id' => 22,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 26,
            'name' => 'Wasps',
            'slug' => 'wasps',
            'parent_id' => 22,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 27,
            'name' => 'Garden',
            'slug' => 'garden',
            'parent_id' => 23,
            'is_main' => 0,
        ]);

        Category::firstOrCreate([
            'id' => 28,
            'name' => 'Natural Oils',
            'slug' => 'natural-oils',
            'parent_id' => 5,
            'is_main' => 0,
        ]);

    }
}
