<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Product::find(1)->categories()->attach(11);

		Product::find(2)->categories()->attach(11);

		Product::find(3)->categories()->attach(11);

		Product::find(4)->categories()->attach(11);

		Product::find(5)->categories()->attach(19);

		Product::find(6)->categories()->attach(19);

		Product::find(7)->categories()->attach(19);

		Product::find(8)->categories()->attach(19);

		Product::find(9)->categories()->attach(19);

		Product::find(10)->categories()->attach(20);

		Product::find(11)->categories()->attach(20);

		Product::find(12)->categories()->attach(20);

		Product::find(13)->categories()->attach(12);

		Product::find(14)->categories()->attach(12);

		Product::find(15)->categories()->attach(12);

		Product::find(16)->categories()->attach(12);

		Product::find(17)->categories()->attach(21);

		Product::find(18)->categories()->attach(7);

		Product::find(19)->categories()->attach(7);

		Product::find(20)->categories()->attach(7);

		Product::find(21)->categories()->attach(7);

		Product::find(22)->categories()->attach(7);

		Product::find(23)->categories()->attach(7);

		Product::find(24)->categories()->attach(16);

		Product::find(25)->categories()->attach(12);

		Product::find(26)->categories()->attach(9);

		Product::find(27)->categories()->attach(9);

		Product::find(28)->categories()->attach(9);

		Product::find(29)->categories()->attach(9);

		Product::find(30)->categories()->attach(9);

		Product::find(31)->categories()->attach(9);

		Product::find(32)->categories()->attach(9);

		Product::find(33)->categories()->attach(9);

		Product::find(34)->categories()->attach(9);

		Product::find(35)->categories()->attach(9);

		Product::find(36)->categories()->attach(10);

		Product::find(37)->categories()->attach(10);

		Product::find(38)->categories()->attach(13);

		Product::find(39)->categories()->attach(12);

		Product::find(40)->categories()->attach(12);

		Product::find(41)->categories()->attach(13);

		Product::find(42)->categories()->attach(12);

		Product::find(43)->categories()->attach(14);

		Product::find(44)->categories()->attach(14);

		Product::find(45)->categories()->attach(14);

		Product::find(46)->categories()->attach(14);

		Product::find(47)->categories()->attach(15);

		Product::find(48)->categories()->attach(15);

		Product::find(49)->categories()->attach(15);

		Product::find(50)->categories()->attach(24);

		Product::find(51)->categories()->attach(24);

		Product::find(52)->categories()->attach(25);

		Product::find(53)->categories()->attach(24);

		Product::find(54)->categories()->attach(24);

		Product::find(55)->categories()->attach(24);

		Product::find(56)->categories()->attach(26);

		Product::find(57)->categories()->attach(28);

		Product::find(58)->categories()->attach(28);

		Product::find(59)->categories()->attach(28);

		Product::find(60)->categories()->attach(28);

		Product::find(61)->categories()->attach(28);

		Product::find(62)->categories()->attach(28);

		Product::find(63)->categories()->attach(27);

		Product::find(64)->categories()->attach(27);

		Product::find(65)->categories()->attach(27);

		Product::find(66)->categories()->attach(27);

		Product::find(67)->categories()->attach(27);

		Product::find(68)->categories()->attach(22);

		Product::find(69)->categories()->attach(22);

		Product::find(70)->categories()->attach(22);
    }
}
