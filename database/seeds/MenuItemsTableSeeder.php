<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (file_exists(base_path('routes/web.php'))) {
            require base_path('routes/web.php');

            $menu = Menu::where('name', 'admin')->firstOrFail();
            $footer_menu = Menu::where('name', 'footer')->firstOrFail();
            $header_menu = Menu::where('name', 'header')->firstOrFail();

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Dashboard',
                'url'        => route('voyager.dashboard', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-boat',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 1,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Orders',
                'url'        => 'admin/orders',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-wallet',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 2,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Products',
                'url'        => 'admin/products',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-cup',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 3,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Categories',
                'url'        => route('voyager.categories.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-categories',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 4,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Pages',
                'url'        => route('voyager.pages.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-file-text',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 5,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Settings',
                'url'        => route('voyager.settings.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-settings',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 6,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Media',
                'url'        => route('voyager.media.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-images',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 7,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Menu Builder',
                'url'        => route('voyager.menus.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-list',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 8,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Database Tools',
                'url'        => route('voyager.database.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-data',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 9,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Users',
                'url'        => route('voyager.users.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-person',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 10,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
                'menu_id'    => $menu->id,
                'title'      => 'Roles',
                'url'        => route('voyager.roles.index', [], false),
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-lock',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 11,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $footer_menu->id,
              'title'      => 'Delivery',
              'url'        => 'delivery',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 13,
                ])->save();
            }
            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $footer_menu->id,
              'title'      => 'Contacts',
              'url'        => 'contacts',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 14,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $footer_menu->id,
              'title'      => 'About Us',
              'url'        => 'about-us',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 15,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $footer_menu->id,
              'title'      => 'Terms',
              'url'        => 'terms',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 16,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $header_menu->id,
              'title'      => 'Food & Drinks',
              'url'        => 'food-and-drinks',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 1,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $header_menu->id,
              'title'      => 'Partyware',
              'url'        => 'partyware',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 2,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $header_menu->id,
              'title'      => 'Packing',
              'url'        => 'packing',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 3,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $header_menu->id,
              'title'      => 'Outdoor',
              'url'        => 'outdoor',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 4,
                ])->save();
            }

            $menuItem = MenuItem::firstOrNew([
              'menu_id'    => $header_menu->id,
              'title'      => 'Relaxation & Hygiene',
              'url'        => 'relaxation-and-hygiene',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                  'target'     => '_self',
                  'icon_class' => null,
                  'color'      => null,
                  'parent_id'  => null,
                  'order'      => 5,
                ])->save();
            }
        }
    }
}
