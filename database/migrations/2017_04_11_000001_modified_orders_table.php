<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifiedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('billing_country')->nullable()->change();
            $table->string('billing_state')->nullable()->change();
            $table->string('billing_city')->nullable()->change();
            $table->integer('billing_zip_code')->nullable()->change();
            $table->string('shipping_country')->nullable()->change();
            $table->string('shipping_state')->nullable()->change();
            $table->string('shipping_city')->nullable()->change();
            $table->integer('shipping_zip_code')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->string('phone')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
            $table->string('billing_country')->nullable(false)->change();
            $table->string('billing_state')->nullable(false)->change();
            $table->string('billing_city')->nullable(false)->change();
            $table->integer('billing_zip_code')->nullable(false)->change();
            $table->string('shipping_country')->nullable(false)->change();
            $table->string('shipping_state')->nullable(false)->change();
            $table->string('shipping_city')->nullable(false)->change();
            $table->integer('shipping_zip_code')->nullable(false)->change();
        });
    }
}
