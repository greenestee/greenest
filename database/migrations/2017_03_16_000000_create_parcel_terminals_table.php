<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelTerminalsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('parcel_terminals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider');
            $table->string('name');
            $table->string('address');
            $table->string('open');
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('parcel_terminals');
    }
}
