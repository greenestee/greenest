<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->float('price');
            $table->unsignedInteger('pieces');
            $table->tinyInteger('is_lightweight')->default(0);
            $table->string('image')->nullable();
            $table->string('sku')->unique();
            $table->string('product_url');
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->text('ingredients')->nullable();
            $table->text('nutritional_value')->nullable();
            $table->text('recommended_storage')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->tinyInteger('is_active')->index()->default(1);
            $table->string('categories')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
