<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEtColumnsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('name_et')->nullable();
            $table->string('slug_et')->nullable();
            $table->string('meta_title_et')->default('');
            $table->text('meta_keywords_et')->nullable();
            $table->string('meta_description_et')->default('');
            $table->text('description_et')->nullable();
            $table->text('short_description_et')->nullable();
            $table->text('nutritional_value_et')->nullable();
            $table->text('ingredients_et')->nullable();
            $table->text('recommended_storage_et')->nullable();
            $table->text('allergenic_information_et')->nullable();
            $table->text('method_of_preparation_et')->nullable();
            $table->text('how_to_use_et')->nullable();
            $table->string('brand_et')->nullable();
            $table->string('origin_country_et')->nullable();
            $table->string('origin_country_of_ingredients_et')->nullable();
            $table->string('shelf_life_et')->nullable();
        });
        DB::table('products')->where('id', '>', 0)
            ->update([
                'name_et' => DB::raw("CONCAT('MAHE ', name)"),
                'brand_et' => DB::raw('brand'),
                'slug_et' => DB::raw("CONCAT_WS('-', 'mahe', REPLACE(LOWER(brand), ' ', '-'), slug)"),
                'nutritional_value_et' => DB::raw("
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    nutritional_value,
                    'Energy value', 'Energiasisaldus'),
                    'Fat', 'Rasvad'),
                    'including saturated fatty acids', 's.h küllastunud rasvhappeid'),
                    'Mono-unsaturated fat', 'monoküllastumata rasvhapped'),
                    'Polyunsaturated fat', 'polüküllastumata rasvhappe'),
                    'Carbohydrates', 'Süsivesikuid'),
                    'including sugars', 'neist suhkruid'),
                    'Protein', 'Valke'),
                    'Salt', 'Soola'),
                    'Fibers', 'Kiudaineid'),
                    'Vitamin E', 'Vitamiin E')
                "),
                'slug' => DB::raw("CONCAT_WS('-', REPLACE(LOWER(brand), ' ', '-'), slug)"),
                'meta_title_et' => DB::raw("meta_title"),
                'meta_keywords_et' => DB::raw("meta_keywords"),
                'meta_description_et' => DB::raw("meta_description"),
                'description_et' => DB::raw("description"),
                'short_description_et' => DB::raw("
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    short_description,
                    'Certified organic product', 'Kontrollitud mahetoode'),
                    'Free shipping from', 'Tasuta transport alates'),
                    'We ship within', 'Komplekteerimine'),
                    'working days', 'tööpäeva jooksul'
                )"),
                'ingredients_et' => DB::raw("REPLACE(ingredients, '*Certified organic ingredient', '*Kontrollitud mahetoode')"),
                'recommended_storage_et' => DB::raw("
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    REPLACE(
                    recommended_storage,
                    'Store at room temperature', 'Säilita toetemperatuuril'),
                    'After opening keep in the refrigerator', 'Pärast avamist, külmkapis'),
                    'Store at room temperature and out of direct sunlight', 'Hoida toatemperatuuril. Vältida otsest päikesevalgust'),
                    'Store in a dry and cool place', 'Hoia kuivas ja jahedas'),
                    'Store in a cool and dry place', 'Hoia kuivas ja jahedas'
                    
                )"),
                'allergenic_information_et' => DB::raw("allergenic_information"),
                'method_of_preparation_et' => DB::raw("method_of_preparation"),
                'how_to_use_et' => DB::raw("how_to_use"),
                'origin_country_et' => DB::raw("origin_country"),
                'origin_country_of_ingredients_et' => DB::raw("origin_country_of_ingredients"),
                'shelf_life_et' => DB::raw("shelf_life"),
            ]);

        DB::statement("ALTER TABLE categories MODIFY slug_et VARCHAR (255) NOT NULL");
        DB::statement("ALTER TABLE categories MODIFY name_et VARCHAR (255) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('name_et');
            $table->dropColumn('slug_et');
            $table->dropColumn('meta_title_et');
            $table->dropColumn('meta_keywords_et');
            $table->dropColumn('meta_description_et');
            $table->dropColumn('description_et');
            $table->dropColumn('short_description_et');
            $table->dropColumn('ingredients_et');
            $table->dropColumn('recommended_storage_et');
            $table->dropColumn('allergenic_information_et');
            $table->dropColumn('method_of_preparation_et');
            $table->dropColumn('how_to_use_et');
            $table->dropColumn('brand_et');
            $table->dropColumn('origin_country_et');
            $table->dropColumn('origin_country_of_ingredients_et');
            $table->dropColumn('shelf_life_et');
        });
    }
}
