<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEtColumnsCategoriesTable extends Migration
{
    public $categories = [
        'Food & Drinks' => 'Toit ja Jook',
        'Relaxation & Hygiene' => 'Puhtus & Kehahooldus',
        'Outdoor' => 'Õue ja aeda',
        'Partyware' => 'Peotarvikud',
        'Packages' => 'Pakendid',
        'Baby food' => 'Beebitoit',
        'Baking and cooking' => 'Küpsetamine',
        'Canned food' => 'Purgitoit',
        'Cereal products' => 'Teraviljatooted',
        'Chocolates' => 'Šokolaadid',
        'Cocoa' => 'Kakao',
        'Coconut milk and cream' => 'Kookostooted',
        'Coffee' => 'Kohv',
        'Crunchy food' => 'Krõbinad',
        'Dairy products' => 'Piimatooted',
        'Desserts' => 'Magustoidud',
        'Drinks from nature' => 'Joogid loodusest',
        'Eggs' => 'Munad',
        'Fish products' => 'Kalatooted',
        'Flours' => 'Jahud',
        'Frozen food' => 'Külmutatud toit',
        'Fruits and berries' => 'Puuviljad ja marjad',
        'Gourmet' => 'Gurmee',
        'Honey' => 'Mesi',
        'Jams' => 'Moosid',
        'Juices' => 'Mahlad',
        'Make your life sweet' => 'Maguasined',
        'Meat products' => 'Lihatooted',
        'Nut and chocolate creams' => 'Šokolaadikreemid',
        'Nut creams' => 'Pähklikreemid',
        'Nuts' => 'Pähklid',
        'Oils' => 'Õlid',
        'Pasta' => 'Pasta',
        'Pet food' => 'Lemmikloomatoit',
        'Pizza' => 'Pizza',
        'Raw food' => 'Toortoit',
        'Sauces' => 'Kastmed',
        'Seeds and grains' => 'Seemned',
        'Soft drinks' => 'Karastusjoogid',
        'Soups and broths' => 'Supid ja puljongid',
        'Soy and tofu products' => 'Soja ja tofutooted',
        'Spice up your life' => 'Maitseained',
        'Spread the bread' => 'Määrded',
        'Superfood' => 'Väetoit',
        'Supplements' => 'Toidulisandid',
        'Sweets and snacks' => 'Maiustused ja snäkid',
        'Syrups' => 'Siirupid',
        'Tea' => 'Tee',
        'Various' => 'Mitteliigitamata tooted',
        'Vegetable juices' => 'Aedvilja mahlad',
        'Vegetable products' => 'Aedviljad',
        'Vinegars' => 'Äädikas',
        'Hot Food' => 'Kuum toit',
        'Protection' => 'Kaitse ennast',
        'Seasonal' => 'Hooajalised tooted',
        'Bags' => 'Kotid',
        'Balloons' => 'Õhupallid',
        'Food Containers' => 'Toidupakendid',
        'Ribbon' => 'Pael',
        'Straws' => 'Kõrred',
        'Tableware' => 'Lauanõud',
        'Mosquitos' => 'Sääsed',
        'Ticks' => 'Puugid',
        'Wasps' => 'Herilased',
        'Baby hygiene' => 'Lastele',
        'Beauty or the beast' => 'Pesemisvahendid',
        'Body care' => 'Kehahooldus',
        'Cosmetics' => 'Kosmeetika',
        'Creams' => 'Kreemid',
        'Hair care' => 'Juustehooldus',
        'Laundry and dishes' => 'Pesu ja nõude pesemiseks',
        'Natural Oils' => 'Naturaalsed õlid',
        'Surfaces' => 'Puhastusvahendid',
        'Teeth care' => 'Suuhooldus',
        'Garden' => 'Aeda',
        'Bowls' => 'Kausid',
        'Cups & Lids' => 'Topsid ja kaaned',
        'Cutlery' => 'Lauanõud',
        'Plates' => 'Taldrikud',
        'Trays' => 'Kandikud',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name_et')->nullable();
            $table->string('slug_et')->nullable();
            $table->string('meta_title_et')->nullable();
            $table->string('meta_keywords_et')->nullable();
            $table->string('meta_description_et')->nullable();
        });
        DB::table('categories')->where('id', '>', 0)
            ->update([
                'name_et' => DB::raw('name'),
                'slug_et' => DB::raw('slug'),
                'meta_title_et' => DB::raw('meta_title'),
                'meta_keywords_et' => DB::raw('meta_keywords'),
                'meta_description_et' => DB::raw('meta_description')
            ]);
        Schema::table('categories', function (Blueprint $table) {
            $table->unique(['slug_et', 'parent_id']);
        });

        $updateQuery = 'update categories SET name_et = CASE';
        foreach ($this->categories as $en => $et) {
            $updateQuery .= ' WHEN name = \'' . $en . '\' THEN \'' . $et . '\'';
        }

        $updateQuery .= ' END WHERE name IN (\'' . implode('\',\'', array_keys($this->categories)) . '\')';

        DB::update($updateQuery);

        DB::statement("ALTER TABLE categories MODIFY slug_et VARCHAR (255) NOT NULL");
        DB::statement("ALTER TABLE categories MODIFY name_et VARCHAR (255) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropUnique('categories_slug_et_parent_id_unique');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('name_et');
            $table->dropColumn('slug_et');
            $table->dropColumn('meta_title_et');
            $table->dropColumn('meta_keywords_et');
            $table->dropColumn('meta_description_et');
        });
    }
}
