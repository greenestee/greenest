<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('vat_number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_reg_number')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_country');
            $table->string('billing_state');
            $table->string('billing_city');
            $table->integer('billing_zip_code');
            $table->string('shipping_address')->nullable();
            $table->string('shipping_country');
            $table->string('shipping_state');
            $table->string('shipping_city');
            $table->integer('shipping_zip_code');
            $table->string('shipping_method');
            $table->string('shipping_parcel_terminal_phone')->nullable();
            $table->integer('shipping_parcel_terminal_id')->nullable();
            $table->string('payment_method');
            $table->text('comment')->nullable();
            $table->integer('hear_about_us')->nullable();
            $table->float('subtotal');
            $table->float('tax');
            $table->float('sum_total');
            $table->string('status');
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
