<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsDebatableImportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_debatable_import', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('old_sku');
            $table->string('old_url');
            $table->string('new_sku');
            $table->string('new_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_debatable_import');

    }
}
