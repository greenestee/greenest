<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_tmp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->float('price', 8, 2);
            $table->float('purchase_price', 8,2);
            $table->integer('amount')->default(1);
            $table->float('weight', 8,3)->nullable();
            $table->string('unit', 255)->nullable();
            $table->string('brand', 255)->nullable();

            $table->tinyInteger('is_lightweight')->default(0)->nullable();
            $table->tinyInteger('is_vegan')->default(0)->nullable();
            $table->tinyInteger('is_gluten_free')->default(0)->nullable();

            $table->string('image')->nullable();
            $table->string('sku')->unique();
            $table->string('product_url');

            $table->string('origin_country', 255)->nullable();
            $table->string('origin_country_of_ingredients', 255)->nullable();
            $table->string('shelf_life', 255)->nullable();

            $table->text('nutritional_value')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->text('ingredients')->nullable();
            $table->text('recommended_storage')->nullable();
            $table->text('allergenic_information')->nullable();
            $table->text('method_of_preparation')->nullable();
            $table->text('how_to_use')->nullable();
            $table->tinyInteger('is_active')->index()->default(1);
            $table->integer('categories')->nullable();
            $table->timestamps();

            $table->timestamp('promoted_from')->nullable();
            $table->timestamp('promoted_to')->nullable();
            $table->boolean('is_promoted')->nullable();

            $table->string('name_et')->nullable();
            $table->string('slug_et')->nullable();
            $table->string('meta_title_et')->default('');
            $table->text('meta_keywords_et')->nullable();
            $table->string('meta_description_et')->default('');
            $table->text('description_et')->nullable();
            $table->text('short_description_et')->nullable();
            $table->text('nutritional_value_et')->nullable();
            $table->text('ingredients_et')->nullable();
            $table->text('recommended_storage_et')->nullable();
            $table->text('allergenic_information_et')->nullable();
            $table->text('method_of_preparation_et')->nullable();
            $table->text('how_to_use_et')->nullable();
            $table->string('brand_et')->nullable();
            $table->string('origin_country_et')->nullable();
            $table->string('origin_country_of_ingredients_et')->nullable();
            $table->string('shelf_life_et')->nullable();

            $table->tinyInteger('status')->default(1);
            $table->integer('quantity')->default(1);
            $table->date('delivery_date')->nullable();
            $table->tinyInteger('is_blacklist')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_tmp');
    }
}
