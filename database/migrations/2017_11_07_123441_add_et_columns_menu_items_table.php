<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEtColumnsMenuItemsTable extends Migration
{
    private $menus = [
        'Food & Drinks' => 'Toit ja Jook',
        'Relaxation & Hygiene' => 'Puhtus & Kehahooldus',
        'Outdoor' => 'Õue ja aeda',
        'Partyware' => 'Peotarvikud',
        '	Packages' => 'Pakendid',
        'About Us' => 'Meist',
        'Contacts' => 'Kontaktid',
        'Delivery' => 'Kauba kättetoimetamine',
        'Right to return' => 'Tagastusõigus',
        'Terms and conditions' => 'Tellimistingimused',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->string('title_et')->nullable();
            $table->string('url_et')->nullable();
        });
        DB::table('menu_items')->where('id', '>', 0)
            ->update([
                'url_et' => DB::raw('url')
            ]);

        DB::table('menu_items')->where('menu_id', '=', 1)
            ->update([
                'title_et' => DB::raw('title')
            ]);

        $updateQuery = 'update menu_items SET title_et = CASE';
        foreach ($this->menus as $en => $et) {
            $updateQuery .= ' WHEN title = \'' . $en . '\' THEN \'' . $et . '\'';
        }

        $updateQuery .= ' END WHERE title IN (\'' . implode('\',\'', array_keys($this->menus)) . '\')';

        DB::update($updateQuery);

        DB::statement("ALTER TABLE menu_items MODIFY url_et VARCHAR (255) NOT NULL");
        DB::statement("ALTER TABLE menu_items MODIFY title_et VARCHAR (255) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_items', function (Blueprint $table) {
            $table->dropColumn([
                'title_et',
                'url_et'
            ]);
        });
    }
}
