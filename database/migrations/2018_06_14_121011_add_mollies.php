<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddMollies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()  //:TODO CHECK
    {
        DB::select('ALTER TABLE `orders` ADD `mollie_payment_id` VARCHAR(50) NULL AFTER `payment_method`');
        DB::select('ALTER TABLE `orders` ADD `mollie_payment_status` VARCHAR(50) NULL AFTER `mollie_payment_id`');
        DB::select('INSERT INTO `data_rows` (`data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`) VALUES (9, \'mollie_payment_status\', \'text\', \'Payment ( Mollie )\', 0, 1, 1, 0, 0, 0, NULL)');
        DB::select('ALTER TABLE `orders` ADD `mollie_was_showed` TINYINT NOT NULL DEFAULT \'1\' AFTER `mollie_payment_status`;');
        DB::select('UPDATE `orders` SET `mollie_was_showed`= 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
