<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingParcelPhoneToBuyerListStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buyer_list_statistics', function (Blueprint $table) {
            $table->string('shipping_parcel_terminal_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buyer_list_statistics', function (Blueprint $table) {
            $table->dropColumn('shipping_parcel_terminal_phone');
        });
    }
}
