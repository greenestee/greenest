    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Page;

class AddEtColumnToPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string('title_et')->nullable();
            $table->text('excerpt_et')->nullable();
            $table->text('body_et')->nullable();
            $table->string('slug_et')->nullable()->unique();
            $table->text('meta_description_et')->nullable();
            $table->text('meta_keywords_et')->nullable();
        });
        Page::where('id', '>', 0)->update([
            'title_et' => DB::raw('title'),
            'excerpt_et' => DB::raw('excerpt'),
            'body_et' => DB::raw('body'),
            'slug_et' => DB::raw('slug'),
            'meta_description_et' => DB::raw('meta_description'),
            'meta_keywords' => DB::raw('meta_keywords'),
        ]);
        DB::statement("ALTER TABLE pages MODIFY title_et VARCHAR (255) NOT NULL");
        DB::statement("ALTER TABLE pages MODIFY slug_et VARCHAR (255) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn([
                'title_et',
                'excerpt_et',
                'body_et',
                'slug_et',
                'meta_description_et',
                'meta_keywords_et'
            ]);
        });
    }
}
